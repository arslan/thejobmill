

CREATE TABLE  `transactions_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `metadata` text,
  `voucher_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;



CREATE TABLE `subscription` (
  `sub_id` int(255) NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(255) NOT NULL,
  `sub_desc` varchar(500) NOT NULL,
  `sub_status` tinyint(1) NOT NULL,
  `sub_price` decimal(10,2) NOT NULL,
  `sub_added_date` datetime NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;\


CREATE TABLE `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `expire_date` date NOT NULL,
  `value` double NOT NULL,
  `discount_type` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `job_subscriptions`(     `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,     `job_id` INT(11) NOT NULL ,     `subscription_id` INT(11) NOT NULL ,     `status` TINYINT DEFAULT '1' ,     `created_at` DATETIME ,     PRIMARY KEY (`id`)  )  ;