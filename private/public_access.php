<?php 
global $_allowed_resources ;
$_allowed_resources = array(
	'admin'=>array(
		'index'=>array(
			"login",
			"logout"
		),
	),
	
	'default'=>array(
		"error",
		"social",
		"index",
		"static",
		"user",
		"payment",
		"forum",
		'blog'=>array(
			"categories",
			"listing",
			"blogdetails",
		),
	)
);