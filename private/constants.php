<?php
	defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . ''));
	
 	define("SITE_NAME", "The Job Mill");
 	define("NAME_OF_SITE", "The Job Mill");
	
	define("ADMIN_AUTH_NAMESPACE", "ADMIN_AUTH");
	define("DEFAULT_AUTH_NAMESPACE", "DEFAULT_AUTH");
	
 	define("SITE_BASE_URL", dirname($_SERVER['PHP_SELF']));
	/*define("SITE_HOST_URL", "http://" . $_SERVER['HTTP_HOST']);
	define("SITE_HTTP_URL", "http://" . $_SERVER['HTTP_HOST'] . SITE_BASE_URL);*/
	define("SITE_HOST_URL", "http://" . $_SERVER['HTTP_HOST']);
	define("SITE_HTTP_URL", "http://" . $_SERVER['HTTP_HOST'] . SITE_BASE_URL);

	define("APPLICATION_URL", "http://" . $_SERVER['HTTP_HOST'] . SITE_BASE_URL);
	//define("APPLICATION_URL", "http://" . $_SERVER['HTTP_HOST']);
	define("ADMIN_APPLICATION_URL", SITE_HTTP_URL . "/admin");
	
	define('PRICE_SYMBOL','$');
	define('ALLOW_CACHE','FALSE');
	define("FRONT_CSS_PATH",SITE_HTTP_URL.'/public/front_css');
	define("FRONT_JS_PATH",SITE_HTTP_URL.'/public/front_js');
	define("FRONT_IMAGES_PATH",SITE_HTTP_URL.'/public/img');

	define('ADMIN_CSS_PATH', SITE_HTTP_URL.'/public/admin_css');
	define('ADMIN_JS_PATH', SITE_HTTP_URL.'/public/admin_js');

	define('ADMIN_IMAGES_PATH', SITE_HTTP_URL.'/assets/admin/img');
 	define('ADMIN_ASSETS_PATH', SITE_HTTP_URL.'/assets/admin');

	define('ADMIN_PROFILE', '/resources/admin profile images');
	define('PROPERTY_IMAGES', '/resources/property images');
	define('GALLERY_IMAGES', '/resources/gallery images');
	define('TEAM_IMAGES', '/resources/team members');
	define("IMAGE_VALID_EXTENTIONS","jpg,JPG,png,PNG,jpeg,JPEG");
	define("IMAGE_VALID_SIZE","5MB");
	define("MESSAGE_VALID_SIZE","20MB");

	define("IMG_URL",ROOT_PATH."/assets/img/");
	define("HTTP_IMG_URL",APPLICATION_URL."/assets/img/");

	/* New Theme Constatns */
	define('HTTP_IMG_PATH', SITE_HTTP_URL.'/public/img');
 	define('HTTP_PROFILE_IMAGES_PATH', SITE_HTTP_URL.'/public/resources/profile_images');
 	define('PROFILE_IMAGES_PATH', ROOT_PATH.'/public/resources/profile_images');
	define('HTTP_MEDIA_IMAGES_PATH', SITE_HTTP_URL.'/public/resources/media_images');
	define('MEDIA_IMAGES_PATH', ROOT_PATH.'/public/resources/media_images');
	define('HTTP_SLIDER_IMAGES_PATH', SITE_HTTP_URL.'/public/resources/slider_images');
	define('SLIDER_IMAGES_PATH', ROOT_PATH.'/public/resources/slider_images');
	define('HTTP_MESSAGES_PATH', SITE_HTTP_URL.'/public/resources/message_files');
	define('MESSAGES_PATH', ROOT_PATH.'/public/resources/message_files');

	define('HTTP_BLOG_IMAGES_PATH', SITE_HTTP_URL.'/public/resources/blog_images');
	define('BLOG_IMAGES_PATH', ROOT_PATH.'/public/resources/blog_images');

	define('HTTP_JOB_ATTACHMENTS', SITE_HTTP_URL.'/public/resources/job');
	define('JOB_ATTACHMENTS', ROOT_PATH.'/public/resources/job');

	define('HTTP_BG_IMAGES_PATH', SITE_HTTP_URL.'/public/resources/bg_images');
	define('BG_IMAGES_PATH', ROOT_PATH.'/public/resources/bg_images');
	
	global $roleArr,$adminallowedArr;
	
	$roleArr=array('1'=>'Full Admin','2'=>'Forum Admin');
	
	$adminallowedArr=array(
		'blog'=>array(
			'0'=>'categories',
			'1'=>'addcategory',
			'2'=>'removecategory',
			'3'=>'index',
			'4'=>'blogdetails',
			'5'=>'addblog',
			'6'=>'removeblog',
		)	
	);
		