// JavaScript Document
// override jquery validate plugin defaults
$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
	
	/*submitHandler: function(form) {
     	 //$.blockUI();
		 form.submit();
    },*/
	submitHandler: function(form) {
     	 //$.blockUI();
		 if(form.id=='job_search_form'){
		 		$.blockUI({ css: { 
				border: 'none', 
				padding: '15px', 
				//backgroundColor: '#4378BC', 
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .9, 
				color: '#4C85CE' ,
				fontSize:'30px',
				},
				/*message: 'PROCESSING....<br/><img src="'+SITE_HTTP_URL+'/public/resources/bg_images/'+processingIcon+'" />',  */
				message: '<img src="'+SITE_HTTP_URL+'/public/resources/bg_images/'+processingIcon+'" />',  
				});
				setTimeout( form.submit(), 10000);
				// form.submit();
		 }else{
			 	$.blockUI({ css: { 
				border: 'none', 
				padding: '15px', 
				//backgroundColor: '#4378BC', 
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .9, 
				color: '#4C85CE' ,
				fontSize:'30px',
				},
				/*message: 'PROCESSING....<br/><img src="'+SITE_HTTP_URL+'/public/resources/bg_images/'+processingIcon+'" />',*/
				message: '<img src="'+SITE_HTTP_URL+'/public/resources/bg_images/'+processingIcon+'" />',
				});
				setTimeout( form.submit(), 10000);
				// form.submit();
		 }
	},
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
			 error.insertAfter(element);
        } else {
            error.insertAfter(element);
        }
    }
});
if(controllerName=='job' && actionName=='searchjobs'){
 var v = $('.job_search_form').validate({});
 var b = $('#apply_form').validate({rules:{ApplyMessage:{required: true}}});
}
function getWordCount(wordString) {
  var words = wordString.split(" ");
  words = words.filter(function(words) { 
    return words.length > 0
  }).length;
  return words;
}

jQuery.validator.addMethod("wordCount",
   function(value, element, params) {
      var count = getWordCount(value);
      if(count <= params[0]) {
         return true;
      }
   },
   "Not more than {0} words allowed here"
);

$.validator.setDefaults({ ignore: ":hidden:not(.chosen-select,#job_skills,input[name=review_star])"});
$('.profile_form').validate({
	rules:{
		user_old_password:{minlength:6,maxlength:16,remote: baseUrl+"/user/checkpassword"},
 		user_password:{minlength:6 , maxlength:16},
		user_rpassword:{equalTo:'#user_password' , minlength:6, maxlength:16},
 		user_email:{required: true,email: true,/*remote: baseUrl+"/user/checkemail"*/},
		e_pitch:{wordCount: ['50']},
		sp_pitch:{wordCount: ['50']},
		msg_file:{extension:"txt|xlsx|csv|doc|docx|pdf|TXT|XLSX|CSV|DOC|DOCX|PDF"},
 	},
	messages:{
		user_rpassword:{equalTo:"Password Mismatch ,please enter correct password"},
		msg_file:{extension:"Please upload valid file (Allowed extensions txt,xlsx,csv,doc,docx,pdf)"},
	}
});
$('.delete_account_form').validate({
	rules:{
		reason:{required: true},

	}
});

$('.testi_form').validate({
	rules:{
		testimonial:{required: true},
 	},
});

jQuery.validator.addMethod("notEqual", function(value, element, param) {
  return this.optional(element) || value != param;
}, "Please specify a different (non-default) value");

$.validator.setDefaults({ ignore: ":hidden:not(#job_desc,#job_skills,#job_category,#skills)" });
if(controllerName=='job' && actionName=='index'){
	var v = $('.job_form').validate({
		rules:{
			job_desc: 
			{
				required: function() 
				{
					CKEDITOR.instances.job_desc.updateElement();
				}
			},
			job_budget:{number:true},
			/*job_min_price:{lessThan:"#job_max_price",digits:true},
  			job_max_price:{greaterThan:"#job_min_price",digits:true},*/
		},
		messages:{
			job_max_price:{greaterThan:"Value must be greater than min price"},
			job_min_price:{lessThan:"Value must be less than max price"}
		}
	});
}

jQuery.validator.addClassRules("checkemail",{remote: baseUrl+"/user/checkemail"});
jQuery.validator.addClassRules("emailexists",{remote: baseUrl+"/user/checkemail/rev/1"});
jQuery.validator.addClassRules("checkemail_exclude",{remote: baseUrl+"/user/checkemail?exclude=1"});
jQuery.validator.addClassRules("string_length_about",{maxlength:160});
jQuery.validator.addClassRules("string_length_care",{maxlength:400});
$('.mail_verification_pulsate').pulsate({color: "#fcb322"});

$(function() {
     $("img.lazy").lazyload({
         effect : "fadeIn"
     });
});
function re_init(){
	$('.checkboxes').uniform();
	$('.group-checkable').removeAttr('checked');
	$('.group-checkable').parent().removeClass('checked');
}
function checkSelects(){
 	var checkedRecords=false;	
	$(".elem_ids").each(function(index, element) {
        if(this.checked==true){
			checkedRecords=true;
		}
    });
 	
	if(!checkedRecords){
		showFlashMessage("No Records Selected for Delete , Please Select Records to delete ",1);
		return false;	
	}else{
		if(!confirm("Are you sure you want to delete")){
			return false;
		}
		$.blockUI();
	}
}
$('#deletebcchk').click(function(e) {
	var current_checked_status = $.trim($('.group-checkable').is(':checked'));
	if(current_checked_status=='false'){
		 $('.checkboxes').removeAttr('checked');
		 $('.checkboxes').parent().removeClass('checked');
	}
	else{
		$('.checkboxes').attr('checked','checked');
		$('.checkboxes').parent().addClass('checked');;
	}
});


function viewMoreDetails()
{
	 /*$.ajax({
        url: baseUrl+"/profile/socialdetails",
        type: 'POST',
        async: false,
        success: function (data) {
          	alert(data);
			$('#userdetailsModal').modal('show');
			$('#userdetailsModal .modal-body').html(data);
        },
    });*/
}

$('.carousel[data-type="multi"] .item').each(function(){
	var next = $(this).next();
	if (!next.length) {
		next = $(this).siblings(':first');
	}
	next.children(':first-child').clone().appendTo($(this));

	for (var i=0;i<1;i++) {
		next=next.next();
		if (!next.length) {
			next = $(this).siblings(':first');
		}

		next.children(':first-child').clone().appendTo($(this));
	}
});



