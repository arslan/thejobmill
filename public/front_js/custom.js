/*** CUSTOM JS ***/
$(document).ready(function(e)
{
	var query = window.location.search.substring(1);
	//if(  ($('#job_location').val()!='' || $('#job_country').val()!=''))
	 if($('#MapCanvas').length==1 && $("#job_country").val() != '' && query == '' && userId != '')
	{
		var country = $("#job_country").val();
		var value = $("#job_country").val();
		$.ajax({
			url: baseUrl+"/job/getlatlong/country/"+country,
			success: function (data)
			{
				data=JSON.parse(data);
				//console.log(data);
				if(data){
					var dataArray = data.split(",");
					var latitude = dataArray[0];
					var longitude = dataArray[1];
					var mapOptions = {
						zoom:3,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: new google.maps.LatLng(latitude,longitude)
					};
					if(controllerName=='job' && actionName=='index')
					{
						var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
						var marker = new google.maps.Marker({
							position: siteLatLng,
							map: map,
							icon: baseUrl + '/public/resources/bg_images/'+mapIcon
						});
						marker.setPosition(mapOptions.center);
						marker.setVisible(true);
					}else {
						var map = new google.maps.Map(document.getElementById("MapCanvas"),mapOptions);
					}

					var siteLatLng = new google.maps.LatLng(latitude,longitude);


					map.setCenter(mapOptions.center);

					var geocoder = new google.maps.Geocoder();

					$("#job_latitude").val(latitude);
					$("#job_longitude").val(longitude);
				}
			}
		});
	}
	$('.date-picker').datepicker({format: 'MM d, yyyy',startDate:new Date()});
    $('.date-picker').on('changeDate', function(ev){
    	$(this).datepicker('hide');
	});
	$('.date-picker').on('click', function(ev){
		$(this).datepicker('show');
	});
	$(function(){
 		$('[data-toggle="tooltip"]').tooltip()
	})
	$(function () {
 		$('[data-toggle="popover"]').popover({html: true})
	})
});

if(controllerName=='static' && actionName=='contact')
{
	$('#DropMessage').click(function(e) {
        $('html, body').animate({
			scrollTop: $(".forgot-bg").offset().top
		}, 1000);
    });
}

if(controllerName=='user' && actionName=='register')
{
	$('#fbSignIn').click(function(e) {
		//var user_type = $("input[type='radio'][name='user_type']:checked").val();
		// window.location=baseUrl+'/social/fblogin/user_type/'+user_type;

		if($("#user_terms_agreement").is(':checked')){
			window.location=baseUrl+'/social/fblogin';
		}else{
			alert("You need to agree to the Terms & Conditions and Privacy Policy in order to sign up.");
		}

    });
}

function DeleteAccount()
{
	var confirmation = confirm("Are you sure you want to delete your account? This is permanent!");
	if(confirmation)
	{
		$('#reasonModal').modal('show');
		return true;
	}else{
		return false;
	}
}

if(controllerName=='user' && actionName=='login')
{
	$('#fbSignIn').click(function(e) {
		//var user_type = $("input[type='radio'][name='user_type']:checked").val();
		// window.location=baseUrl+'/social/fblogin/user_type/'+user_type;
		window.location=baseUrl+'/social/fblogin';
	});
}

if(controllerName=='profile' && actionName=='editprofile')
{
	$(document).ready(function(e) {
		var config = {
		'.chosen-select'           : {placeholder_text_multiple:'Choose Skills',placeholder_text_single:'Choose Expertise'},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
		'.chosen-select-no-single' : {disable_search_threshold:10},
		'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		'.chosen-select-width'     : {width:"95%"}
		}

		for (var selector in config) {
			$(selector).chosen(config[selector]);
		}
    });

	/* GOOGLE AUTOCOMPLETE CODE-Starts */
	var placeSearch, autocomplete,autocomplete1;
	function initialize() {
		var id= event.target.id;
		autocomplete = new google.maps.places.Autocomplete(
			(document.getElementById(event.target.id)),
			{ types: ['geocode'] });

			google.maps.event.addListener(autocomplete, 'place_changed', function() {
			fillInAddress(id);
		});
	}

	function fillInAddress(id) {
		var place = autocomplete.getPlace();
		GetLatLong(id);
	}

	function GetLatLong(id)
	{
		var address=document.getElementById(id).value;
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({'address': address}, function(results,status)
		{
			if(status == google.maps.GeocoderStatus.OK)
			{
				var searchLoc = results[0].geometry.location;
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				/*if(id=='e_location'){
					document.getElementById('e_latitude').value=latitude;
					document.getElementById('e_longitude').value=longitude;
				}else if(id=='sp_location'){
					document.getElementById('sp_latitude').value=latitude;
					document.getElementById('sp_longitude').value=longitude;
				}*/

				document.getElementById('ud_latitude').value=latitude;
				document.getElementById('ud_longitude').value=longitude;
			}
		});
	}

	function geolocate() {
	  if (navigator.geolocation)
	  {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = new google.maps.LatLng(
			position.coords.latitude, position.coords.longitude);
			autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
			geolocation));
		});
	  }
	}
	/* GOOGLE AUTOCOMPLETE CODE-Ends */

	$('#WorkerCollapse').click(function(e) {
		var ToggleState = $("#WorkerProfile").css('display');
        $('#WorkerProfile').toggle('slow');
		$('#WorkerCollapse').toggleClass('fa-minus','fa-plus');
    });

	$('#HirerCollapse').click(function(e) {
		var ToggleState = $("#HirerProfile").css('display');
        $('#HirerProfile').toggle('slow');
		$('#HirerCollapse').toggleClass('fa-minus','fa-plus');
    });
}
function OpenSkillModal()
{
	 $('#skillModal').modal('show');
}
if(controllerName=='job' && actionName=='index')
{
	$(document).ready(function(e) {
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			// Do something!
			// Redirect to Android-site?
		}else{
			CKEDITOR.replace('job_description',
			{
				toolbar :
				[
				['Source'],
				['Image'],['Bold','Italic','Underline'],['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],['TextColor'],['Format'],['FontSize'],['Font']
				],
				height: 150,
			});
		}

		$('#job_closing_date.date-picker').datepicker('update', new Date());
		var config = {
		'.chosen-select'           : {placeholder_text_multiple:'Choose Skills',placeholder_text_single:'Choose Category'},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
		'.chosen-select-no-single' : {disable_search_threshold:10},
		'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		'.chosen-select-width'     : {width:"95%"}
		}

		for (var selector in config) {
			$(selector).chosen(config[selector]);
		}

		$("#job_skills_chosen").find('input').on('keyup', function(e)
		{
			if ($('ul.chosen-results').find('li.no-results').length > 0)
			{
				//console.log('no results');
				var search_str=$("#job_skills_chosen input").val();
				$('.no-results').html('No results match "'+search_str+'"<span class="addSkillBtn pull-right" onclick="OpenSkillModal();" style="cursor:pointer;padding-top:0px !important;"><i class="fa fa-plus"></i>&nbsp;Click here to suggest skill</span>');
		  }
		});


		$('#job_budget').removeAttr('readonly');
		$('#job_budget').rules( "add", {
		  required: true,
		});

		$('#job_fix_type').removeAttr('readonly');
		$('#job_fix_type').rules( "add", {
		  required: true,
		});

		if($('#job_min_price').val()=='' || ($('#job_min_price').val()=='0' && $('#job_max_price').val()=='0')){
			$('#job_min_price').attr('readonly','readonly');
			$("#job_min_price").rules( "remove" );
			$("#job_min_price").removeClass('required');

			$('#job_max_price').attr('readonly','readonly');
			$("#job_max_price").rules( "remove" );
			$("#job_max_price").removeClass('required');
		}else{
			$('#job_budget').attr('readonly','readonly');
			$("#job_budget").rules("remove");
			$("#job_budget").removeClass('required');

			$('#job_fix_type').attr('readonly','readonly');
			$("#job_fix_type").rules("remove");
			$("#job_fix_type").removeClass('required');

			$('#job_min_price').removeAttr('readonly');
			$('#job_min_price').rules( "add", {
				required: true,
			});

			$('#job_max_price').removeAttr('readonly');
			$('#job_max_price').rules( "add", {
				required: true,
			});
		}

		if(job_post_code=='')
		{
			//$('#job_location').attr('readonly','readonly');
		}else{
			$('#job_location').removeAttr('readonly');
		}
	});

	$('.stepwizard-step').click(function(e) {
		if($("#job_latitude").val()!=''){
			var latitude = $("#job_latitude").val();
			var longitude = $("#job_longitude").val();
		}else{
			/*var latitude = -35.3080;
			var longitude = 149.1245;*/
			var latitude = ''
			var longitude ='';
		}
        initializeMap(latitude,longitude);
    });

	$('.nextBtn').click(function(e) {
		if($("#job_latitude").val()!=''){
			var latitude = $("#job_latitude").val();
			var longitude = $("#job_longitude").val();
		}else{
			/*var latitude = -35.3080;
			var longitude = 149.1245;*/
			var latitude = ''
			var longitude ='';
		}
        initializeMap(latitude,longitude);
    });

	function enablezipcode(val)
	{
		if(val!=''){
			$('#job_location').removeAttr('readonly');
			getlatlog($('#job_location').val());
		}else{
			$('#job_location').attr('readonly','readonly');
		}
	}

	function getlatlog(value)
	{
		var country = $('#job_country').val();
		if(value.length>=4){ /* Update map when postal code is entered upto 4 digits or more - To prevent flickering of map when entering initial digits */
			$.ajax({
				url: baseUrl+"/job/getlatlong/zipcode/"+value+"/country/"+country,
				success: function (data)
				{
					data=JSON.parse(data);
					//console.log(data);
					if(data!=null){
						$('.ErrorMsg').css('display','none');
						var dataArray = data.split(",");
						var latitude = dataArray[0];
						var longitude = dataArray[1];
						$("#job_latitude").val(latitude);
						$("#job_longitude").val(longitude);
						initializeMap(latitude,longitude);
					}else{
						$('.ErrorMsg').css('display','block');
					}
				}
			});
		}else if(value=='' && country!=''){
			$.ajax({
				url: baseUrl+"/job/getlatlong/zipcode/"+value+"/country/"+country,
				success: function (data)
				{
					data=JSON.parse(data);
					//console.log(data);
					if(data!=null){
						$('.ErrorMsg').css('display','none');
						var dataArray = data.split(",");
						var latitude = dataArray[0];
						var longitude = dataArray[1];
						$("#job_latitude").val(latitude);
						$("#job_longitude").val(longitude);
						initializeMap(latitude,longitude);
					}else{
						$('.ErrorMsg').css('display','block');
					}
				}
			});
		}
	}

	$('#CurrentLocation').click(function(e) {
        $.ajax({
			url: baseUrl+"/job/getcurrentlocation",
			success: function (data)
			{
				var newData = JSON.parse(data);
				//console.log(newData.geolocation_data.latitude);

				$("#job_country").rules( "remove" );
				$("#job_country").removeClass('required');

				$("#job_location").rules( "remove" );
				$("#job_location").removeClass('required');

				if(typeof newData.geolocation_data.latitude!='undefined'){
					//var dataArray =newData.loc.split(",");
					//console.log(dataArray);
					var latitude = newData.geolocation_data.latitude;
					var longitude =newData.geolocation_data.longitude;
					$("#job_latitude").val(latitude);
					$("#job_longitude").val(longitude);
					initializeMap(latitude,longitude);
				}else{
					initializeMap(-35.3080,149.1245);
				}
			}
		});
    });

	function initializeMap(latitude,longitude)
	{

		/*var mapOptions = {
			zoom:6,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(latitude,longitude)
		};
		var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);*/
		if(latitude!=''){
			window.map = new google.maps.Map(document.getElementById('map-canvas'), {
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					//position: siteLatLng,
					//map: map,
					center: new google.maps.LatLng(latitude,longitude)
				});

			var bounds = new google.maps.LatLngBounds();
			var siteLatLng = new google.maps.LatLng(latitude,longitude);
			var marker = new google.maps.Marker({
					position: siteLatLng,
					map: map,
					//title: userlocation,
					icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			});

			//marker.set("draggable", "false");
			//map.setCenter(map.center);
			marker.setPosition(map.center);
			marker.setVisible(true);

			bounds.extend(marker.position);
			map.setCenter(bounds.getCenter());
			//map.fitBounds(bounds);
			if($('#job_location').val()!=''){
				map.fitBounds(bounds);
				map.setZoom(15);
			}else{
				map.setZoom(4);
			}

			var geocoder = new google.maps.Geocoder();
		}else{
			var mapOptions = {
			zoom:2,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(-35.3080,149.1245)
			};
			var mapnew = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
		}
	}

	$('.JobFixedAmount').click(function(e) {
		$('#job_budget').removeAttr('readonly');
		$('#job_budget').rules("add",{
			required: true,
			number: true,
		});

		$('#job_fix_type').removeAttr('readonly');
		$('#job_fix_type').rules("add",{
			required: true,
		});

		$('#job_min_price').attr('readonly','readonly');
		$("#job_min_price").rules("remove");
		$("#job_min_price").removeClass('required');

		$('#job_max_price').attr('readonly','readonly');
		$("#job_max_price").rules("remove");
		$("#job_max_price").removeClass('required');

		$("#posting-range-price select").attr('readonly','readonly');
    });

	$('#job_budget').click(function(e) {
		$('#job_budget').removeAttr('readonly');
		$('#job_budget').rules("add",{
			required: true,
			number: true,
		});

		$('#job_fix_type').removeAttr('readonly');
		$('#job_fix_type').rules("add",{
			required: true,
		});

		$('#job_min_price').attr('readonly','readonly');
		$("#job_min_price").rules("remove");
		$("#job_min_price").removeClass('required');

		$('#job_max_price').attr('readonly','readonly');
		$("#job_max_price").rules("remove");
		$("#job_max_price").removeClass('required');
		$("#posting-range-price select").attr('readonly','readonly');
    });

	$('.JobRangeAmount').click(function(e) {
		$("#posting-range-price select").removeAttr('readonly');
        $('#job_budget').attr('readonly','readonly');
		$("#job_budget").rules("remove");
		$("#job_budget").removeClass('required');

		$('#job_fix_type').attr('readonly','readonly');
		$("#job_fix_type").rules("remove");
		$("#job_fix_type").removeClass('required');

		$('#job_min_price').removeAttr('readonly');

		$('#job_max_price').removeAttr('readonly');

		$('#job_min_price').rules( "add", {
			required: true,
			digits: true,
			notEqual:"0"
		});

		$('#job_max_price').rules( "add", {
			required: true,
			digits: true,
			greaterThan: '#job_min_price',
			notEqual:"0"
		});

		/*$('.job_form').validate({messages:{
			job_min_price:{lessThan:"Password Mismatch ,please enter correct password"}
		}});*/
    });

	$('#job_min_price').click(function(e) {
		$("#posting-range-price select").removeAttr('readonly');
        $('#job_budget').attr('readonly','readonly');
		$("#job_budget").rules("remove");
		$("#job_budget").removeClass('required');

		$('#job_fix_type').attr('readonly','readonly');
		$("#job_fix_type").rules("remove");
		$("#job_fix_type").removeClass('required');

		$('#job_min_price').removeAttr('readonly');

		$('#job_max_price').removeAttr('readonly');
		$('#job_min_price').rules( "add", {
			required: true,
			digits: true,
			notEqual:"0"
		});

		$('#job_max_price').rules( "add", {
			required: true,
			digits: true,
			greaterThan: '#job_min_price',
			notEqual:"0"
		});

		/*$('.job_form').validate({messages:{
			job_min_price:{lessThan:"Password Mismatch ,please enter correct password"}
		}});*/
    });

	$('#job_max_price').click(function(e) {
		$("#posting-range-price select").removeAttr('readonly');
        $('#job_budget').attr('readonly','readonly');
		$("#job_budget").rules("remove");
		$("#job_budget").removeClass('required');

		$('#job_fix_type').attr('readonly','readonly');
		$("#job_fix_type").rules("remove");
		$("#job_fix_type").removeClass('required');

		$('#job_min_price').removeAttr('readonly');
		$('#job_max_price').removeAttr('readonly');

		$('#job_min_price').rules( "add", {
			required: true,
			digits: true,
			notEqual:"0"
		});

		$('#job_max_price').rules( "add", {
			required: true,
			digits: true,
			greaterThan: '#job_min_price',
			notEqual:"0"
		});

		/*$('.job_form').validate({messages:{
			job_min_price:{lessThan:"Password Mismatch ,please enter correct password"}
		}});*/
    });
}

if(controllerName=='job' && actionName=='jobdetails')
{

	var thisHash = window.location.hash;
	if(thisHash)
	{
		$('#ApplyForm').css('display','block');
		var divName="#ApplyForm";
		  $('html, body').animate({
		   scrollTop: $(divName).offset().top
		  }, 'slow');
	}


	$(document).ready(function(e) {
		$('form.profile_form div:nth-child(2)').after($('#rating_div'));
		if(vote!=0)
		{
			if(vote==1)
			{
				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-o-up').addClass('fa-thumbs-up');
				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-up').removeClass('fa-thumbs-o-up');

				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-o-down').css('display','none');
				/*$('.JobDetailDiv .JobRatingDiv .fa-thumbs-down').addClass('fa-thumbs-o-down');
				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-o-down').removeClass('fa-thumbs-down');*/
			}else{
				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-o-down').addClass('fa-thumbs-down');
				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-down').removeClass('fa-thumbs-o-down');

				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-o-up').hide();
				/*$('.JobDetailDiv .JobRatingDiv .fa-thumbs-up').addClass('fa-thumbs-o-up');
				$('.JobDetailDiv .JobRatingDiv .fa-thumbs-o-up').removeClass('fa-thumbs-up');*/
			}
		}
        initializeMap1(joblatitude,joblongitude);
    });

	function initializeMap1(latitude,longitude)
	{
		/*var mapOptions = {
			//zoom:11,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(latitude,longitude),

		};*/
		/*var map = new google.maps.Map(document.getElementById("mapCanvas"),mapOptions);
		var siteLatLng = new google.maps.LatLng(latitude,longitude);
		var marker = new google.maps.Marker({
				position: siteLatLng,
				map: map,
				icon: baseUrl + '/public/resources/bg_images/'+mapIcon,
		});*/

		//map = new google.maps.Map(document.getElementById('mapCanvas'),mapOptions);
		//marker = new google.maps.Marker({map: map,icon: baseUrl + '/public/img/map-logo.png'});
		//marker.set("draggable", "false");
		/*map.setCenter(mapOptions.center);
		marker.setPosition(mapOptions.center);
		marker.setVisible(true);
		var geocoder = new google.maps.Geocoder(); */


		var bounds = new google.maps.LatLngBounds();
		var siteLatLng = new google.maps.LatLng(latitude,longitude);

		window.map = new google.maps.Map(document.getElementById('mapCanvas'), {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			//position: siteLatLng,
			//map: map,
			center: new google.maps.LatLng(latitude,longitude)
		});

		var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			//title: userlocation,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
		});

		//marker.set("draggable", "false");
		//map.setCenter(map.center);
		marker.setPosition(map.center);
		marker.setVisible(true);

		bounds.extend(marker.position);
		//map.setCenter(bounds.getCenter());
		//map.fitBounds(bounds);
		map.setZoom(15);
		var geocoder = new google.maps.Geocoder();
	}

	$('#ApplyJob').click(function(e) {
        $('#ApplyForm').toggle('slow');
		return false;
    });

	$('.JobDetailDiv .JobRatingDiv .VoteTU').click(function(e) {
		if(edit!='1'){
			$('.JobDetailDiv .JobRatingDiv .VoteTU.fa-thumbs-o-up').addClass('fa-thumbs-up');
			$('.JobDetailDiv .JobRatingDiv .VoteTU.fa-thumbs-up').removeClass('fa-thumbs-o-up');

			$('.JobDetailDiv .JobRatingDiv .VoteTD.fa-thumbs-down').addClass('fa-thumbs-o-down');
			$('.JobDetailDiv .JobRatingDiv .VoteTD.fa-thumbs-o-down').removeClass('fa-thumbs-down');

			$('#job_vote').val('1');
		}
    });

	$('.JobDetailDiv .JobRatingDiv .VoteTD').click(function(e) {
        if(edit!='1'){
			$('.JobDetailDiv .JobRatingDiv .VoteTD.fa-thumbs-o-down').addClass('fa-thumbs-down');
			$('.JobDetailDiv .JobRatingDiv .VoteTD.fa-thumbs-down').removeClass('fa-thumbs-o-down');

			$('.JobDetailDiv .JobRatingDiv .VoteTU.fa-thumbs-up').addClass('fa-thumbs-o-up');
			$('.JobDetailDiv .JobRatingDiv .VoteTU.fa-thumbs-o-up').removeClass('fa-thumbs-up');

			$('#job_vote').val('-1');
		}
    });
}

function ratingForm()
{
	var value1=$('input:radio[name="review_star"]:checked').val();
	if(typeof value1 == 'undefined'){
		$('#error_msg').html('Select Rating...');
	}
	else
		return true;
	return false;
}

if(controllerName=='job' && actionName=='searchjobs')
{

	var circle;
	$(document).ready(function(e) {
		var config = {
			'.chosen-select'           : {placeholder_text_multiple:'Choose Skills',placeholder_text_single:'Choose Expertise'},
			'.chosen-select-deselect'  : {allow_single_deselect:true},
			'.chosen-select-no-single' : {disable_search_threshold:10},
			'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			'.chosen-select-width'     : {width:"95%"}
		}
		for(var selector in config) {
			$(selector).chosen(config[selector]);
		}

		$("#job_skills_chosen").find('input').on('keyup', function(e)
		{
			if ($('ul.chosen-results').find('li.no-results').length > 0)
			{
				//console.log('no results');
				var search_str=$("#job_skills_chosen input").val();
				$('.no-results').html('No results match "'+search_str+'"<hr style="margin:5px 0px; border-color:#ccc;" /><div class="addSkillBtn pull-right" onclick="OpenSkillModal();" style="cursor:pointer;padding-left:0px !important;"><i class="fa fa-plus"></i>&nbsp;Click here to suggest skill</div>');
				$('.chosen-container .chosen-results li.no-results').css('overflow','hidden');
		  }
		});

		//$('.slider').slider();
		if(type!='list'){ //alert('asf');
			/*$('.slider').slider().on('slideStop', function(ev){
				updateRadius(this.value);
			});*/

		}


		if($('#job_country').val()=='')
		{
			//@fix remove readonly if no location is selected in profile
			//$('#job_location').attr('readonly','readonly');
		}else{
			$('#job_location').removeAttr('readonly');
		}
		$('#job_closing_date.date-picker').datepicker('update', new Date());

		$('#job_search_form input, select').bind('keypress', function(e) { // Submit form on 'enter' keypress and 'Go' keypress in android
			var code = e.keyCode || e.which;
			 if(code == 13) { //Enter keycode
			   $('#job_search_form #btnsubmit').trigger('click');
			 }
		});
    });

	if(type!='list'){
		if(InfoWindow!='')
		{
			var sites = InfoWindow;
			var infowindow = null;
			var styles = [
			{
				"featureType": "landscape",
				"stylers": [
					{
						"hue": "#FFBB00"
					},
					{
						"saturation": 43.400000000000006
					},
					{
						"lightness": 37.599999999999994
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "road.highway",
				"stylers": [
					{
						"hue": "#FFC200"
					},
					{
						"saturation": -61.8
					},
					{
						"lightness": 45.599999999999994
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "road.arterial",
				"stylers": [
					{
						"hue": "#FF0300"
					},
					{
						"saturation": -100
					},
					{
						"lightness": 51.19999999999999
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "road.local",
				"stylers": [
					{
						"hue": "#FF0300"
					},
					{
						"saturation": -100
					},
					{
						"lightness": 52
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "water",
				"stylers": [
					{
						"hue": "#0078FF"
					},
					{
						"saturation": -13.200000000000003
					},
					{
						"lightness": 2.4000000000000057
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "poi",
				"stylers": [
					{
						"hue": "#00FF6A"
					},
					{
						"saturation": -1.0989010989011234
					},
					{
						"lightness": 11.200000000000017
					},
					{
						"gamma": 1
					}
				]
			}
		];

			window.map = new google.maps.Map(document.getElementById('MapCanvas'), {
       			mapTypeId: google.maps.MapTypeId.ROADMAP,
    		});
			map.setOptions({styles: styles});
			/* Setting up radius around first marker-Starts */
			var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
			/*var marker = new google.maps.Marker({
					position: siteLatLng,
					map: map,
					//icon: baseUrl + '/public/resources/bg_images/'+mapIcon
					//title: userlocation,
			});
			marker = new google.maps.Marker({map: map});*/
			setMarkers(map, sites);
			infowindow = new google.maps.InfoWindow({
							content: "loading...",
						});
			var contentNo;
		}else{

			var styles = [
			{
				"featureType": "landscape",
				"stylers": [
					{
						"hue": "#FFBB00"
					},
					{
						"saturation": 43.400000000000006
					},
					{
						"lightness": 37.599999999999994
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "road.highway",
				"stylers": [
					{
						"hue": "#FFC200"
					},
					{
						"saturation": -61.8
					},
					{
						"lightness": 45.599999999999994
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "road.arterial",
				"stylers": [
					{
						"hue": "#FF0300"
					},
					{
						"saturation": -100
					},
					{
						"lightness": 51.19999999999999
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "road.local",
				"stylers": [
					{
						"hue": "#FF0300"
					},
					{
						"saturation": -100
					},
					{
						"lightness": 52
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "water",
				"stylers": [
					{
						"hue": "#0078FF"
					},
					{
						"saturation": -13.200000000000003
					},
					{
						"lightness": 2.4000000000000057
					},
					{
						"gamma": 1
					}
				]
			},
			{
				"featureType": "poi",
				"stylers": [
					{
						"hue": "#00FF6A"
					},
					{
						"saturation": -1.0989010989011234
					},
					{
						"lightness": 11.200000000000017
					},
					{
						"gamma": 1
					}
				]
			}
		];

			window.map = new google.maps.Map(document.getElementById('MapCanvas'), {
       			//mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: new google.maps.LatLng(25.7617654,73.2293542),
				zoom:2,
    		});
			map.setOptions({styles: styles});
		}
	}

	$('.ToggleView button.hire').click(function(e) {
		$('#job_search_form').attr('action',baseUrl+'/search-jobs/map');
        $('#job_search_form').submit();
    });

	$('.ToggleView button.work').click(function(e) {
		$('#job_search_form').attr('action',baseUrl+'/search-jobs/list');
        $('#job_search_form').submit();
    });
}

if(controllerName=='profile' && actionName=='userprofile')
{
	$(".UserProfileSkills").niceScroll({
	   cursorborder: "2px solid #008ab3",
	   cursorcolor: "#008ab3",
	   cursorwidth: 2,
	   autohidemode: 'true',
	});
}

if(controllerName=='job' && actionName=='serviceproviders')
{
	$(document).ready(function(e) {
		var config = {
			'.chosen-select'           : {placeholder_text_multiple:'Choose Skills',placeholder_text_single:'Choose Expertise'},
			'.chosen-select-deselect'  : {allow_single_deselect:true},
			'.chosen-select-no-single' : {disable_search_threshold:10},
			'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			'.chosen-select-width'     : {width:"95%"}
		}
		for(var selector in config) {
			$(selector).chosen(config[selector]);
		}
    });
	input_element = document.getElementById('location');
    options = {};
    var autocomplete = new google.maps.places.Autocomplete(input_element, options );

	if(type=='map'){
		if(InfoWindow!='')
		{
			var sites = InfoWindow;
			var infowindow = null;
			 window.map = new google.maps.Map(document.getElementById('MapCanvas'), {
       			mapTypeId: google.maps.MapTypeId.ROADMAP,
    		});
			/* Setting up radius around first marker-Starts */
			var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
			/*var marker = new google.maps.Marker({
					position: siteLatLng,
					map: map,
					//icon: baseUrl + '/public/resources/bg_images/'+mapIcon
					//title: userlocation,
			});
			marker = new google.maps.Marker({map: map});*/
			setUserMarkers(map, sites);
			infowindow = new google.maps.InfoWindow({
							content: "loading...",
						});
			var contentNo;
		}else{
			 window.map = new google.maps.Map(document.getElementById('MapCanvas'), {
       			mapTypeId: google.maps.MapTypeId.ROADMAP,
				center: new google.maps.LatLng(25.7617654,73.2293542),
				zoom:2,
    		});
		}
	}


	$('.ServiceProvidersWrapper .ToggleView button.hire').click(function(e) {
		//$('form').attr('action',baseUrl+'/service-providers/map');
		$('form').attr('action',baseUrl+'/workers/map');
        $('form').submit();
    });

	$('.ServiceProvidersWrapper .ToggleView button.work').click(function(e) {
		//$('form').attr('action',baseUrl+'/service-providers/list');
		$('form').attr('action',baseUrl+'/workers/list');
        $('form').submit();
    });

}

if(controllerName=='job' && actionName=='jobhire')
{
	if(InfoWindow!='' && type=='map')
	{
		var infowindow = null;
		var sites = InfoWindow;
		var mapOptions = {
			zoom:5,
			minZoom:1,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(sites[0][2],sites[0][3])
		};

		var map = new google.maps.Map(document.getElementById("MapCanvas"),mapOptions);

		/* Setting up radius around first marker-Starts */
		var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
		var marker = new google.maps.Marker({
				position: siteLatLng,
				map: map,
				icon: baseUrl + '/public/resources/bg_images/'+mapIcon
				//title: userlocation,
		});
		//marker = new google.maps.Marker({map: map});
		//marker.set("draggable", "false");
		map.setCenter(mapOptions.center);
		marker.setPosition(mapOptions.center);
		marker.setVisible(false);

		setMarkers1(map, sites);
		infowindow = new google.maps.InfoWindow({
						content: "loading...",
					});
		var contentNo;
	}

	$('#HireMapView').click(function(e){
        window.location=baseUrl+'/hire/'+userId+'/map'
    });

	$('#HireListView').click(function(e){
        window.location=baseUrl+'/hire/'+userId+'/list'
    });
}

if(controllerName=='job' && actionName=='joblist')
{
	var thisHash = window.location.hash;
	if(thisHash)
	{
		$(function(){
		$('#Jobtabs a[href="'+thisHash+'"]').tab('show')
		});
	}

	$('#Jobtabs a').on('click', function(e){
		var el = document.getElementById(e.target.hash.split('#')[1]);
		var id = el.id;
		el.removeAttribute('id');
		window.location.hash = e.target.hash;
		el.setAttribute('id',id);
		//window.location.hash = e.target.hash;
	});

	$(document).ready(function(e) {

        if(type!='list'){
			/*if(InfoWindow1!='')
			{
				joblistMapTab1();
			}*/

			if(InfoWindow2!='')
			{
				joblistMapTab2();
			}

			if(InfoWindow3!='')
			{
				joblistMapTab3();
			}

			if(InfoWindow4!='')
			{
				joblistMapTab4();
			}
		}
    });
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		if(type!='list'){
			if(e.target.hash.split('#')[1]=='browse')
			{
				joblistMapTab1();
			}
			else if(e.target.hash.split('#')[1]=='applied')
			{
				joblistMapTab2();
			}
			else if(e.target.hash.split('#')[1]=='myjobs')
			{
				joblistMapTab3();
			}
			else if(e.target.hash.split('#')[1]=='mysavedjobs')
			{
				joblistMapTab4();
			}
		}
	});

	$('#JobListMap').click(function(e) {
        window.location = baseUrl+'/job-list/map'+thisHash;
    });

	$('#JobListList').click(function(e) {
        window.location = baseUrl+'/job-list/list'+thisHash;
    });
}

function joblistMapTab1()
{
	var sites = InfoWindow1;
	var infowindow = null;
	 window.map = new google.maps.Map(document.getElementById('MapCanvas'), {
		mapTypeId: google.maps.MapTypeId.ROADMAP,
	});
	/* Setting up radius around first marker-Starts */
	var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
	/*var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			//icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			//title: userlocation,
	});
	marker = new google.maps.Marker({map: map});*/
	setListMarkers(map, sites,'tab1');
	infowindow = new google.maps.InfoWindow({
					content: "loading...",
				});
	var contentNo;



	/*var infowindow = null;
	var sites = InfoWindow1;
	var mapOptions = {
		zoom:5,
		minZoom:1,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(sites[0][2],sites[0][3])
	};

	var map = new google.maps.Map(document.getElementById("MapCanvas"),mapOptions);

	var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
	var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			//title: userlocation,
	});
	marker = new google.maps.Marker({map: map});
	//marker.set("draggable", "false");
	map.setCenter(mapOptions.center);
	marker.setPosition(mapOptions.center);
	marker.setVisible(false);

	setListMarkers(map, sites,'tab1');
	infowindow = new google.maps.InfoWindow({
					content: "loading...",
				});
	var contentNo;*/
}

function joblistMapTab2()
{
	var infowindow = null;
	var sites = InfoWindow2;

	var mapOptions = {
		zoom:5,
		minZoom:1,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(sites[0][2],sites[0][3])
	};

	var map = new google.maps.Map(document.getElementById("MapCanvas1"),mapOptions);

	/* Setting up radius around first marker-Starts */
	var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
	var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			//title: userlocation,
	});
	marker = new google.maps.Marker({map: map});
	//marker.set("draggable", "false");
	map.setCenter(mapOptions.center);
	marker.setPosition(mapOptions.center);
	marker.setVisible(false);

	setListMarkers(map, sites,'tab2');
	infowindow = new google.maps.InfoWindow({
					content: "loading...",
				});
	var contentNo;
}

function joblistMapTab3()
{
	var infowindow = null;
	var sites = InfoWindow3;

	var mapOptions = {
		zoom:5,
		minZoom:1,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(sites[0][2],sites[0][3])
	};

	var map = new google.maps.Map(document.getElementById("MapCanvas2"),mapOptions);

	/* Setting up radius around first marker-Starts */
	var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
	var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			//title: userlocation,
	});
	marker = new google.maps.Marker({map: map});
	//marker.set("draggable", "false");
	map.setCenter(mapOptions.center);
	marker.setPosition(mapOptions.center);
	marker.setVisible(false);

	setListMarkers(map, sites,'tab3');
	infowindow = new google.maps.InfoWindow({
					content: "loading...",
				});
	var contentNo;
}

function joblistMapTab4()
{
	var infowindow = null;
	var sites = InfoWindow4;

	var mapOptions = {
		zoom:5,
		minZoom:1,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(sites[0][2],sites[0][3])
	};

	var map = new google.maps.Map(document.getElementById("MapCanvas3"),mapOptions);

	/* Setting up radius around first marker-Starts */
	var siteLatLng = new google.maps.LatLng(sites[0][2],sites[0][3]);
	var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			//title: userlocation,
	});
	marker = new google.maps.Marker({map: map});
	//marker.set("draggable", "false");
	map.setCenter(mapOptions.center);
	marker.setPosition(mapOptions.center);
	marker.setVisible(false);

	setListMarkers(map, sites,'tab4');
	infowindow = new google.maps.InfoWindow({
					content: "loading...",
				});
	var contentNo;
}

if(controllerName=='message' && actionName=='compose')
{
	$(document).ready(function(e) {
		var config = {
			'.chosen-select'           : {placeholder_text_multiple:'Choose Skills',placeholder_text_single:'Choose Expertise'},
			'.chosen-select-deselect'  : {allow_single_deselect:true},
			'.chosen-select-no-single' : {disable_search_threshold:10},
			'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			'.chosen-select-width'     : {width:"95%"}
		}
		for(var selector in config) {
			$(selector).chosen(config[selector]);
		}
		window.setInterval(function(){
		  sendDraft(DraftId); // call your function here
		}, 60000);
    });
}

function sendDraft(DraftId)
{
	//console.log($("#msg_file")[0].files);
	 var formData = new FormData($('.ComposeDiv form')[0]);
	// console.log(formData);
	 $.ajax({
        url: baseUrl+"/message/savedraft/DraftId/"+DraftId,
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
           //console.log(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });
	/*$.ajax({
		url: baseUrl+"/message/savedraft/DraftId/"+DraftId,
		data: $('.ComposeDiv form').serialize(),
		success: function (data)
		{
			//console.log(data);
		}
	});*/
}

if(controllerName=='blog' && actionName=='blogdetails')
{
	$(document).ready(function(e) {
		$(".BlogAllComments .CommentDiv").niceScroll({
		   cursorborder: "2px solid #07AE4D",
		   cursorcolor: "#07AE4D",
		   cursorwidth: 2,
		   autohidemode: 'true',
		});
	});

	$('.BlogComments h3 .AddComment').click(function(e) {
        $('html, body').animate({
			scrollTop: $(".BlogAddComment").offset().top
		}, 2000);
    });
}

if(controllerName=='forum' && actionName=='index'){
	$('#forum_topic').closest('div').addClass("col-sm-10 paddingLeft-0");
	$('#forum_topic').attr();
	$('#btnsubmit').addClass("col-sm-2 pull-right");

}

function setUserMarkers(map, markers)
{
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();

	for (var i = 0; i < markers.length; i++)
	{
	//console.log(markers[i]);
		var sites = markers[i];
		var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);

		var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
		});
		//Placing text in place of marker icon
		/*var marker = new RichMarker({
          position: siteLatLng,
          map: map,
		  flat: true,
          content: '<div style="background-color:#1281AE;color:#ffffff; border-radius:50%; width:35px; height:35px;font-weight: 400;font-size: 15px;padding-top: 8px;text-align: center;cursor:pointer">'+Content.length+'</div>',
          });*/

		gmarkers.push(marker);

		var InfoContent="<div style='margin-right:10px;'><div class='col-sm-4'><img src='"+sites[5]+"' class='img-responsive bw' /></div><div class='col-sm-8' style='font-weight:600; font-size:17px;'>"+sites[0]+"<p style='font-weight:400; font-size:12px; font-style:italic;'>"+sites[4]+"</p></div><div class='col-sm-12 padding-0 clear'><p class='clear'>&nbsp;</p><hr style='margin:10px 0px;' /><button type='button' style='padding: 5px 10px;font-size: 14px;font-weight:400;  margin:10px 0px;border-radius:3px !important;width:100%;' class='hire btn' onclick='showProfile("+sites[3]+");'>See Profile</button></div>";
		InfoContent="<div id='infobox'><div style='width:180px;position:relative;margin-bottom:5px;max-height:300px;overflow-y:auto;'>"+InfoContent+"</div></div>";
		marker.contentString = InfoContent;
		infobox = new InfoBox({
			//scontent: InfoContent,
			disableAutoPan: false,
			pixelOffset: new google.maps.Size(-140, 0),
			zIndex: null,
			boxStyle: {
						background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
						opacity: 1,
						//width: "360px"
						width: "200px"
				},
			closeBoxMargin: "12px 4px 2px 2px",
			closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
			infoBoxClearance: new google.maps.Size(1, 1)
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infobox.setContent(this.contentString);
				infobox.open(map, marker);
			}
		})(marker, i));

		bounds.extend(marker.position);
	}
	//map.fitBounds(bounds);
	var sites = InfoWindow;
	//map.setCenter(sites[0][2],sites[0][3]);
	//center the map to the geometric center of all markers
	map.setCenter(bounds.getCenter());
	map.fitBounds(bounds);
	//remove one zoom level to ensure no marker is on the edge.
	//map.setZoom(map.getZoom()-1);
	// set a minimum zoom
	// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
	/*if(map.getZoom()> 15){
	  map.setZoom(15);
	}*/
	//map.fitBounds(circle.getBounds());
	var listener = google.maps.event.addListener(map, "idle", function () {
		//map.setZoom(3);
		google.maps.event.removeListener(listener);
	});
	//var markerCluster = new MarkerClusterer(map, gmarkers);
}

function setListMarkers(map, markers,tab)
{
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();
	for (var i = 0; i < markers.length; i++)
	{
		var sites = markers[i];
		var siteLatLng = new google.maps.LatLng(sites[2], sites[3]);
		var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			title: markers[i][0],
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
		});
		gmarkers.push(marker);
		var desc=markers[i][4].substr(0,200);
		var job_title=markers[i][0];
		var closingDate = getDateDifference(new Date(),markers[i][6]);

		/* Ajax for getting similar jobs based on postal code and to show listing of all jobs of that postal code in INFOWINDOW - Starts */
		var Content='';
		$.ajax({
			url: baseUrl+"/job/getsimilarjobs/jobliststatus/1/ids/"+markers[i][7]+"/location/"+markers[i][8],
			async:false,
			json:true,
			success: function (data)
			{
				Content=JSON.parse(data);
			}
		});

		/* Ajax for getting similar jobs based on postal code and to show listing of all jobs of that postal code in INFOWINDOW - Ends */

		if(Content.length>0){

			var InfoContent="<div style='font-weight:600; font-size:17px;'>"+Content[0].job_location+"</div><hr style='margin:10px 0px;' />";
			for(var k=0;k<Content.length;k++)
			{

				var closingDate = getDateDifference(new Date(),Content[k].job_closing_date);
				InfoContent=InfoContent+"<div class='col-sm-12 padding-0'><span></span><span onclick='jobDetailsPage("+Content[k].job_id+")' style='font-weight:500; font-size:16px; cursor:pointer;'>"+Content[k].job_title+"</span>&nbsp;&nbsp;&nbsp;&nbsp;<br/><span>Completed before: "+closingDate+"</span>";

				if(tab=='tab1') //For Browse Jobs Tab
				{
					if(Content[k].job_app_status==1){
						var JobStatus = "<i class='fa fa-check'></i> Job Assigned";
					}else{
						var JobStatus = "Apply Now";
					}
					InfoContent=InfoContent+"<div style='color:#E15A6B;font-weight:600;text-align:right;cursor:pointer;' onclick='jobDetailsPage("+Content[k].job_id+")'>"+JobStatus+"</div><hr style='margin:10px 0px;' />";
				}

				if(tab=='tab2') //For Applied Jobs Tab
				{
					if(userId==Content[k].job_app_user_id){
					InfoContent=InfoContent+"<hr style='margin:10px 0px;' /><label class='label bg-grey-gallery'><i class='fa fa-info-circle' style='color:#ffffff;'></i>&nbsp;Job assigned to you</label><hr style='margin:10px 0px;' /><div class='ApplyMessage'><label><i class='fa fa-envelope-o'></i>&nbsp;Message for hirer</label><br /><span>"+Content[k].job_app_message+"</span></div>";
					}
				}

				if(tab=='tab3') //For My Jobs Tab
				{
					if(Content[k].job_app_status==1){
						var JobStatus = "<i class='fa fa-check'></i> Job Assigned";
					}else{
						var JobStatus = "";
					}
					InfoContent=InfoContent+"<div style='color:#E15A6B;font-weight:600;text-align:right;cursor:pointer;' onclick='jobDetailsPage("+Content[k].job_id+")'>"+JobStatus+"</div><label class='label-warning Applicants' onclick='ViewApplicants("+Content[k].job_id+");' style='padding: 3px 8px;border-radius: 3px;color: #ffffff;font-size: 11px;cursor:pointer;'>View Applicants&nbsp;&nbsp;&nbsp;<span class='badge badge-success'>"+Content[k].Applicants+"</span></label><hr style='margin:10px 0px;' />";
				}

				if(tab=='tab4') //For Saved Jobs Tab
				{
					if(Content[k].job_app_status==1){
						var JobStatus = "<i class='fa fa-check'></i> Job Assigned";
					}else{
						var JobStatus = "Apply Now";
					}
					InfoContent=InfoContent+"<div style='color:#E15A6B;font-weight:600;text-align:right;cursor:pointer;' onclick='jobDetailsPage("+Content[k].job_id+")'>"+JobStatus+"</div><hr style='margin:10px 0px;' />";
				}
			}

			InfoContent="<div id='infobox'><div style='min-width:300px;position:relative;margin-bottom:20px;max-height:300px;overflow-y:auto;text-align:left;'>"+InfoContent+"</div></div>";
		marker.contentString = InfoContent;

		infobox = new InfoBox({
			//scontent: InfoContent,
			disableAutoPan: false,
			pixelOffset: new google.maps.Size(-140, 0),
			zIndex: null,
			boxStyle: {
						background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
						opacity: 1,
						//width: "360px"
						width: "220px"
				},
			closeBoxMargin: "12px 4px 2px 2px",
			closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
			infoBoxClearance: new google.maps.Size(1, 1)
		});
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infobox.setContent(this.contentString);
				infobox.open(map, marker);
			}
		})(marker, i));

		bounds.extend(marker.position);
		}
		//map.fitBounds(bounds);
		var sites = markers;
		//map.setCenter(sites[0][2],sites[0][3]);
		//center the map to the geometric center of all markers
		map.setCenter(bounds.getCenter());
		map.fitBounds(bounds);
		//remove one zoom level to ensure no marker is on the edge.
		//map.setZoom(map.getZoom()-1);
		// set a minimum zoom
		// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
		/*if(map.getZoom()> 15){
		  map.setZoom(15);
		}*/
		//map.fitBounds(circle.getBounds());
		var listener = google.maps.event.addListener(map, "idle", function () {
			//map.setZoom(3);
			google.maps.event.removeListener(listener);
		});
		//var markerCluster = new MarkerClusterer(map, gmarkers);
	}
}

function jobDetailsPage(jobId)
{
	window.location=baseUrl+'/job-details/'+jobId;
}

function showProfile(userid)
{
	window.location=baseUrl+'/user-profile/'+userid;
}

function setMarkers1(map, markers)
{
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();
	var LatLongArray = new Array();
	for (var i = 0; i < markers.length; i++)
	{
		var sites = markers[i];
		var siteLatLng = new google.maps.LatLng(sites[2].trim(),sites[3].trim());

		var CheckLatLong=LatLongArray.indexOf(sites[2].trim()+":"+sites[3].trim()); /* Check duplicate marker for exact same location */
		LatLongArray.push(sites[2].trim()+":"+sites[3].trim());

		if(CheckLatLong<0)
		{

			/* Ajax for getting similar jobs based on postal code and to show listing of all jobs of that postal code in INFOWINDOW - Starts */
			var Content='';
			$.ajax({
				url: baseUrl+"/job/getsimilarjobs/ids/"+markers[i][7]+"/location/"+markers[i][8],
				async:false,
				json:true,
				success: function (data)
				{
					Content=JSON.parse(data);
				}
			});
			/* Ajax for getting similar jobs based on postal code and to show listing of all jobs of that postal code in INFOWINDOW - Starts */

			var marker = new RichMarker({
				  position: siteLatLng,
				  map: map,
				  flat: true,
				  content: '<div style="background-color:#1281AE;color:#ffffff; border-radius:50%; width:35px; height:35px;font-weight: 400;font-size: 15px;padding-top: 8px;text-align: center;cursor:pointer">'+Content.length+'</div>',
				});


			/*var marker = new google.maps.Marker({
				position: siteLatLng,
				map: map,
				title: markers[i][0],
				icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			});*/
			gmarkers.push(marker);
			var desc=markers[i][4].substr(0,200);
			var job_title=markers[i][0];
			//var loc=markers[i][4].substr(0,15);
			//var jobImage=baseUrl+"/public/resources/jobs_images/300/"+markers[i][1];
			var closingDate = getDateDifference(new Date(),markers[i][6]);

			var InfoContent="<div style='font-weight:600; font-size:17px;'>"+Content[0].job_location+"</div><hr style='margin:10px 0px;' />";
			for(var k=0;k<Content.length;k++)
			{

				var closingDate = getDateDifference(new Date(),Content[k].job_closing_date);
				InfoContent=InfoContent+"<div class='col-sm-12 padding-0'><span></span><span onclick='chooseHire("+userId+","+Content[k].job_id+");' style='font-weight:500; font-size:16px; cursor:pointer;'>"+Content[k].job_title+"</span>&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='btn hire' style='padding: 5px 10px;color: #fff !important; font-weight: 500;font-size: 13px;border-radius: 3px !important;float:right' onclick='chooseHire("+userId+","+Content[k].job_id+");'>Invite</button><br/><span>Completed before: "+closingDate+"</span><hr style='margin:10px 0px;' />";
			}
			InfoContent="<div id='infobox'><div style='max-width:350px;width:auto;position:relative;margin-bottom:20px;max-height:200px;overflow-y:auto;text-align:left;'>"+InfoContent+"</div></div>";
			marker.contentString = InfoContent;
			/*marker.contentString =
		   "<div style='max-width:350px;width:auto;overflow:hidden;position:relative;margin-bottom:20px;'><div class='col-sm-12 PaddingLeftZero'><span onclick='showJob("+markers[i][5]+");' style='font-weight:500; font-size:16px; cursor:pointer;'>"+job_title+"</span>&nbsp;&nbsp;&nbsp;&nbsp;<span style='color: #1281AE;float: right;margin-top: 3px; cursor:pointer;'  onclick='showJob("+markers[i][5]+");'>View Preview</span><br/><hr style='margin:10px 0px;' /><span>"+desc+"</span><br/><hr style='margin:10px 0px;' /><span>Closing in: "+closingDate+"</span></div>";*/

			/*google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
			return function() {
			infowindow.setContent(this.contentString);
			infowindow.open(map, marker);
			}
			})(marker, i));*/

			infobox = new InfoBox({
				//scontent: InfoContent,
				disableAutoPan: false,
				pixelOffset: new google.maps.Size(-140, 0),
				zIndex: null,
				boxStyle: {
							background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
							opacity: 1,
							//width: "360px"
							width: "220px"
					},
				closeBoxMargin: "12px 4px 2px 2px",
				closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
				infoBoxClearance: new google.maps.Size(1, 1)
			});
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infobox.setContent(this.contentString);
					infobox.open(map, marker);
				}
			})(marker, i));

			bounds.extend(marker.position);
		}
	}
	//map.fitBounds(bounds);
	var sites = InfoWindow;
	//map.setCenter(sites[0][2],sites[0][3]);
	//center the map to the geometric center of all markers
	map.setCenter(bounds.getCenter());
	map.fitBounds(bounds);
	//remove one zoom level to ensure no marker is on the edge.
	//map.setZoom(map.getZoom()-1);
	// set a minimum zoom
	// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
	/*if(map.getZoom()> 15){
	  map.setZoom(15);
	}*/
	//map.fitBounds(circle.getBounds());
	var listener = google.maps.event.addListener(map, "idle", function () {
		//map.setZoom(3);
		google.maps.event.removeListener(listener);
	});
	var markerCluster = new MarkerClusterer(map, gmarkers);
}

function chooseHire(userId,jobId)
{
	window.location=baseUrl+'/job/hireinvite/user_id/'+userId+'/job_id/'+jobId;
}

function getWorkType(workTypeId)
{
	if(workTypeId == 0)
	{
		return 'Full Time';
	}
	else if(workTypeId == 1)
	{
		return 'Part Time';
	}
	else if(workTypeId == 2)
	{
		return 'Casual';
	}
	else
	{
		return '';
	}

}

function setMarkers(map, markers)
{
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();
	var LatLongArray = new Array();
	var ua = navigator.userAgent.toLowerCase();
	var mobile = ua.indexOf("mobile") > -1;

	for (var i = 0; i < markers.length; i++)
	{
		var sites = markers[i];
		var siteLatLng = new google.maps.LatLng(sites[2].trim(),sites[3].trim());
		var CheckLatLong=LatLongArray.indexOf(sites[2].trim()+":"+sites[3].trim()); /* Check duplicate marker for exact same location */
		LatLongArray.push(sites[2].trim()+":"+sites[3].trim());

		if(CheckLatLong < 0)
		{
            /*var marker = new google.maps.Marker({
				position: siteLatLng,
				map: map,
				title: markers[i][0],
				//icon: baseUrl + '/public/resources/bg_images/'+mapIcon
				content: '<div>Hello</div>'
			});*/

			/* Ajax for getting similar jobs based on postal code and to show listing of all jobs of that postal code in INFOWINDOW - Starts */
			var Content='';
			$.ajax({
				url: baseUrl+"/job/getsimilarjobs/ids/"+markers[i][7]+"/location/"+markers[i][8],
				async:false,
				json:true,
				success: function (data)
				{
					Content=JSON.parse(data);
				}
			});
			/* Ajax for getting similar jobs based on postal code and to show listing of all jobs of that postal code in INFOWINDOW - Starts */

			//Placing text in place of marker icon
			if(sites[12] == '2' && sites[13] == '1') {
				var backgroundImage = baseUrl + '/public/img/pin.png';
				// console.log("this is sponsor job");
			}
			else
				var backgroundImage = baseUrl+'/public/img/cog.png';

			var marker = new RichMarker({
			  position: siteLatLng,
			  map: map,
			  flat: true,
			  length: Content.length,
			  /*content: '<div style="background-color:#1281AE;color:#ffffff; border-radius:50%; width:35px; height:35px;font-weight: 400;font-size: 15px;padding-top: 8px;text-align: center;cursor:pointer">'+Content.length+'</div>',*/
			  content: '<div style="background-image:url('+backgroundImage+'); background-size:cover; width:40px; height:40px;text-align: center;color: #ffffff;font-size: 16px;   padding-top: 9px;">'+Content.length+'</div>',
			});

			gmarkers.push(marker);
			var desc=markers[i][4].substr(0,200);
			var job_title=markers[i][0];
			var closingDate = getDateDifference(new Date(),markers[i][6]);

			if(mobile)
			{
				var InfoContent="<div style='font-weight:600; font-size:14px;text-align:left;'>"+Content[0].job_location+"</div><hr style='margin:10px 0px;' />";
			}else{
				var InfoContent="<div style='font-weight:600; font-size:17px;'>"+Content[0].job_location+"</div><hr style='margin:10px 0px;' />";
			}

			for(var k=0;k<Content.length;k++)
			{

				// console.log(Content[k].user_image);

				var jobType="";
				var perimiumText="";
				var jobType2="";
				if(Content[k].subscription_id == '2' && Content[k].status == '1') {
					jobType = "<label style='    color: #06ae4e;    position: absolute;    bottom: 5%;    right: 1%;    border: 2px solid #06ae4e;    padding: 3px;    border-radius: 12%;    font-weight: bolder;'>Premium </label>";
					jobType2 = "<label style='    color: #06ae4e;    position: absolute;    top: 2%;    right: 1%;    border: 2px solid #06ae4e;      border-radius: 12%; font-size: 10px;'>Premium </label>";
					perimiumText="style='border: 4px solid #07AE4D; padding: 10px; '";

				}

				if(Content[k].user_image!=null){
					var url = imgUrl+Content[k].user_image;
					$.ajax({
						url:url,
						type:'HEAD',
						async:false,
						error:
							function(){
								url = imgUrl+'/thumb/default.jpg';
								//do something depressing
							},
						success:
							function(){
								url = imgUrl+'/thumb/'+Content[k].user_image;
								//do something cheerful :)
							}
					});
				}else{
					url = imgUrl+'/thumb/default.jpg';
				}
				console.log(url);

				if(Content[k].job_type==0){
					var JobType="<i class='fa fa-globe' style='padding-left:0px;'></i>Online Job";
				}else{
					var JobType="<i class='fa fa-user' style='padding-left:0px;'></i>Physical Job";
				}

				if(Content[k].job_budget_type==0){
						if(Content[k].job_fix_type==0){
							var Budget = "<i class='fa fa-money' style='padding-left:0px;'></i> <b>BUDGET : </b><i class='fa fa-dollar'></i> "+Content[k].job_budget;
						}else{
							var Budget = "<i class='fa fa-money' style='padding-left:0px;'></i> <b>BUDGET : </b><i class='fa fa-dollar'></i> "+Content[k].job_budget+' per hour';
						}
				}else{
					var Budget = "<i class='fa fa-money' style='padding-left:0px;'></i> <b>BUDGET : </b><i class='fa fa-dollar'></i> "+Content[k].job_min_price+" to <i class='fa fa-dollar'></i> "+Content[k].job_max_price;
				}

				var closingDate = getDateDifference(new Date(),Content[k].job_closing_date);

				if(mobile)
				{
					var InfoBoxwidth = "200px";
					InfoContent=InfoContent
					+
					"<div style='margin-right:0px;'><div class='col-sm-12 padding-0' "+perimiumText+">"+jobType2+"<div class='col-sm-3 col-xs-12 text-left paddingLeft-0 text-center' style='margin-bottom:0px;'><img src='"
					+
					url
					+
					"' style='width:50px; height:50px; border-radius:50%;' /><p class='text-center' style='font-size:12px;color:#1281AE;cursor:pointer;text-decoration:underline;margin-bottom:0px;' onclick='showProfile("
					+
					Content[k].user_id
					+
					");'>"
					+
					Content[k].user_first_name
					+
					"</p></div><div class='col-sm-9 col-xs-12 padding-0 text-left overflow'><div onclick='showJob("
					+
					Content[k].job_id
					+
					","
					+
					Content[k].job_app_user_id
					+
					","
					+
					Content[k].job_app_status
					+
					");' style='font-weight:400; font-size:12px;font-style:italic; cursor:pointer; color:#347CB9;margin-top:0px;'>"
					+
					Content[k].job_title
					+
					"</div><div style='margin-top:2px;'>Completed before: "
					+
					closingDate
					+
					"</div><div style='margin-top:2px;font-style:italic;'>"
					+
					JobType
					+
					"</div></div><div class='col-sm-12 padding-0' style='font-size:11px;'><div style='margin-top:5px;font-style:italic;'>"
					+
					Budget
					+
					"</div></div><button type='button' style='padding: 5px 10px;font-size: 14px;font-weight:400;  margin:10px 0px;border-radius:3px !important;width:50%;padding: 5px 10px 3px 10px !important;font-size: 12px !important;margin-left: 30% !important;margin-top: 10px !important;' class='hire btn' onclick='showJob("
					+
					Content[k].job_id
					+
					","
					+
					Content[k].job_app_user_id
					+
					","
					+
					Content[k].job_app_status
					+
					");'>View Job</button></div><hr style='margin:10px 0px;' />";
				}
				else
				{
					var InfoBoxwidth = "420px";
					InfoContent=InfoContent

					+

					"<div style='margin-right:10px;'><div class='col-sm-12 padding-0' "+perimiumText+"><div class='col-sm-9 padding-0'><div onclick='showJob("
					+
					Content[k].job_id
					+
					","
					+
					Content[k].job_app_user_id
					+
					","
					+
					Content[k].job_app_status
					+
					");' style='font-weight:400; font-size:14px;font-style:italic; cursor:pointer; color:#347CB9;margin-top:8px;'>"
					+
					Content[k].job_title
					+
					"</div><div style='margin-top:5px;'>Completed before: "
					+
					closingDate
					+
					"</div><div style='margin-top:5px;font-style:italic;'>"
					+
					JobType
					+
					"</div></div><div class='col-sm-3 paddingLeft-0 text-center'><img src='"
					+
					url
					+
					"' style='width:80px; height:80px; border-radius:50%;' /><p class='text-center' style='font-size:12px;color:#1281AE;cursor:pointer;text-decoration:underline;' onclick='showProfile("
					+
					Content[k].user_id
					+
					");'>"
					+
					Content[k].user_first_name
					+
					"</p></div><div class='col-sm-12 padding-0 overflow'><div style='margin-top:5px;font-style:italic;'><i class='fa fa-sitemap' style='padding-left:0px;'></i> <b>CATEGORY : </b>"
					+
					Content[k].category_title
					+
					"</div><div style='margin-top:5px;font-style:italic;'><i class='fa fa-cube' style='padding-left:0px;'></i> <b>SKILLS : </b>"
					+
					Content[k].skills.substr(0,200)
					+
					"....</div><div style='margin-top:5px;font-style:italic;'><i class='fa fa-briefcase' style='padding-left:0px;'></i> <b>TYPE OF WORK : </b>"
					+
					getWorkType(Content[k].job_type_of_work)
					+
					"</div><div style='margin-top:5px;font-style:italic;'>"
					+jobType+
					Budget
					+
					"</div></div><button type='button' style='padding: 5px 10px;font-size: 14px;font-weight:400;  margin:10px 0px;border-radius:3px !important;width:100%;' class='hire btn' onclick='showJob("
					+
					Content[k].job_id
					+
					","
					+
					Content[k].job_app_user_id
					+
					","
					+
					Content[k].job_app_status
					+
					");'>View Job</button></div><hr style='margin:10px 0px;' />";
				}

			}

			if(mobile)
			{
				InfoContent="<div id='infobox'><div style='width:190px;position:relative;margin-bottom:5px;max-height:400px;overflow-y:auto;'>"+InfoContent+"</div></div>";
			}else{
				InfoContent="<div id='infobox'><div style='width:400px;position:relative;margin-bottom:5px;max-height:350px;overflow-y:auto;'>"+InfoContent+"</div></div>";
			}

			marker.contentString = InfoContent;
			infobox = new InfoBox({
				//scontent: InfoContent,
				disableAutoPan: false,
				pixelOffset: new google.maps.Size(-140, 0),
				zIndex: null,
				boxStyle: {
							background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
							opacity: 1,
							width: InfoBoxwidth
					},
				closeBoxMargin: "12px 4px 2px 2px",
				closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
				infoBoxClearance: new google.maps.Size(1, 1)
			});

			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infobox.setContent(this.contentString);
					infobox.open(map, marker);
				}
			})(marker, i));

			bounds.extend(marker.position);
		}
	}
	//map.fitBounds(bounds);
	//console.log(LatLongArray);
	var sites = InfoWindow;
	//map.setCenter(sites[0][2],sites[0][3]);
	//center the map to the geometric center of all markers
	map.setCenter(bounds.getCenter());


	//remove one zoom level to ensure no marker is on the edge.
	//map.setZoom(map.getZoom()-1);
	// set a minimum zoom
	// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
	/*if(map.getZoom()> 15){
	  map.setZoom(15);
	}*/
	//map.fitBounds(circle.getBounds());

	if(job_area!='1' && job_area!='' && (job_country!='' || job_location!='')){
		//alert('radius'+job_area);
		var circleOptions = {
			center: bounds.getCenter(),
			fillOpacity: 0,
			strokeOpacity:0,
			map: map,
			radius: job_area*1000 /* 20 miles */
		}
		var myCircle = new google.maps.Circle(circleOptions);
		map.fitBounds(myCircle.getBounds());
	}else{
		map.fitBounds(bounds);
	}

	var listener = google.maps.event.addListener(map, "idle", function () {
		//map.setZoom(3);
		//map.setZoom(Math.round(14-Math.log(5)/Math.LN2));
		google.maps.event.removeListener(listener);
	});
//	var markerCluster = new MarkerClusterer(map, gmarkers);
		var markerCluster = new MarkerClusterer(map, gmarkers, {
				  styles: [{
				height:40,
				url:  baseUrl + '/public/img/cog.png',
				width: 40,
				textColor : '#ffffff'
		}]
        });
}

function enablezipcode1(val)
{
	
	if(val!=''){
		$('#job_location').removeAttr('readonly');
		getsearchlatlong($('#job_location').val());

	}else{
		$('#job_location').attr('readonly','readonly');
		$('#job_location').val('');
	}
}

function CheckLatLong()
{

	if($("#job_country").find(":selected").text()=="Choose country"){

		$("#job_country").after("<label style='color: red;'>This field is required</label>");
		return false;
	}
	/*if($('#job_latitude').val()!='' && $('#job_longitude').val()!='')
	{
		$('#job_search_form').submit();
		return true;
	}else{*/
	if( $('#MapCanvas').length && ($('#job_location').val()!='' || $('#job_country').val()!='')){

		getsearchlatlong($('#job_location').val(),'submit');
	}
	else{//list view
		getsearchlatlong($('#job_location').val(),'submit');
		//setTimeout( $('#job_search_form').submit(), 1000);
	}
}

function getsearchlatlong(value,type)
{
	var country = $('#job_country').val();
	$.ajax({
		url: baseUrl+"/job/getlatlong/zipcode/"+value+"/country/"+country,
		success: function (data)
		{
			data=JSON.parse(data);
			//console.log(data);
			if(data){
				var dataArray = data.split(",");
				var latitude = dataArray[0];
				var longitude = dataArray[1];
				if( $('#MapCanvas').length && ($('#job_location').val()!='' || $('#job_country').val()!='')) {
					var mapOptions = {
						zoom: 9,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: new google.maps.LatLng(latitude, longitude)
					};
					var map = new google.maps.Map(document.getElementById("MapCanvas"), mapOptions);
					var siteLatLng = new google.maps.LatLng(latitude, longitude);
					var marker = new google.maps.Marker({
						position: siteLatLng,
						map: map,
						icon: baseUrl + '/public/resources/bg_images/' + mapIcon
					});

					map.setCenter(mapOptions.center);
					marker.setPosition(mapOptions.center);
					marker.setVisible(true);
					var geocoder = new google.maps.Geocoder();
				}

				$("#job_latitude").val(latitude);
				$("#job_longitude").val(longitude);
			}
			if(type=='submit'){

				setTimeout( $('#job_search_form').submit(),1000);
			}
			//setTimeout( $('#job_search_form').submit(), 10000);
			//getJobs();
			//$('#job_search_form').submit();
		}
	});
}

function populateCategories(value)
{
	$.ajax({
		url: baseUrl+"/job/getcategories/value/"+value,
		success: function (data){
			var Categories = JSON.parse(data);

			var x = document.getElementById("job_category");
			$('#job_category').html('<option value="">Choose Category</option>');

			for(var p=0;p<Categories.length;p++)
			{
				$('#job_category').append('<option value='+Categories[p].category_id+'>' + Categories[p].category_title + '</option>');
				$('#job_category').trigger("chosen:updated");
			}
		}
	});
}

/*$('#job_budget').change(function(e) {
	$('#job_search_form').submit();
});
$('#job_min_price').change(function(e) {
	$('#job_search_form').submit();
});
$('#job_max_price').change(function(e) {
	$('#job_search_form').submit();
});*/

function getJobs()
{
	$.ajax({
		url: baseUrl+"/job/getjobs",
		data: $('#job_search_form').serializeArray(),
		success: function (data)
		{
			//alert(data);
		}
	});
}

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}

function addParam(url, param, value) {
   var a = document.createElement('a'), regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/g;
   var match, str = []; a.href = url; param = encodeURIComponent(param);
   while (match = regex.exec(a.search))
       if (param != match[1]) str.push(match[1]+(match[2]?"="+match[2]:""));
   str.push(param+(value?"="+ encodeURIComponent(value):""));
   a.search = str.join("&");
   return a.href;
}
window.onpopstate = function(e){
	var first = getUrlVars()["jobId"];
	if(!first){
		$('a[href=#step-1]').trigger('click');
	}else{
		$('a[href=#step-'+first+']').trigger('click');
	}

	TrackStep=first-1;
};

var jobId,jobSaved,TrackStep;

function showJob(id,jobappUserId,jobAppStatus)
{
	/*alert(jobappUserId+"::"+userId);
	if(jobappUserId==userId)
	{
		alert("You have Already applied for this job");
	}
	else{*/

	jobId=id;
	/* Changing url without page load-Ajax page transitions-Starts */
	url = window.location;
	newurl = addParam(url, "jobId", "2");
	TrackStep=2;
	window.history.pushState('page2', 'Title', newurl);
	/* Changing url without page load-Ajax page transitions-Ends */

		$.ajax({
			url: baseUrl+"/job/jobpreview/id/"+jobId,
			success: function (data)
			{
				if(data==0){
					alert("Sorry! You already applied for this job. Please select a new one");
				}else{
					$('#step-1 .nextBtn').trigger('click');
					var DataArray = data.split('||');
					$('#JobContent').html(DataArray[0]);
					var latitude = DataArray[1];
					var longitude = DataArray[2];
					jobSaved = DataArray[3];

					initializeJobMap(latitude,longitude);
					$('#savejobmessage').css('display','none');
					if(jobSaved==1){
						$('#saveJobBtn').html('<i class="fa fa-star"></i>&nbsp;Saved Job');
					}else{
						$('#saveJobBtn').html('<i class="fa fa-star-o"></i>&nbsp;Save Job');
					}
				}
			}
		});
	//}
}

function SaveJob()
{
	if(jobSaved!=1){
		$.ajax({
			url: baseUrl+"/job/savejob/jobId/"+jobId,
			success: function (data)
			{
				$('#savejobmessage').css('display','block');
				$('#saveJobBtn').html('<i class="fa fa-star"></i>&nbsp;Saved Job');
			}
		});
	}
}

$('.SearchJobDiv #step-2 .nextBtn').click(function(e) {
	/* Changing url without page load-Ajax page transitions-Starts */
	url = window.location;
	newurl = addParam(url, "jobId", "3");
	TrackStep=1;
	window.history.pushState('page2', 'Title', newurl);
	/* Changing url without page load-Ajax page transitions-Ends */

	$.ajax({
		url: baseUrl+"/job/applyjob/id/"+jobId,
		success: function (data)
		{
			$('#ApplyJobContent').html(data);
			TrackStep=3;
		}
	});
});

$('.SearchJobDiv #step-3 .nextBtn').click(function(e) {
	if($('#apply_form').valid()!=false){
		/* Changing url without page load-Ajax page transitions-Starts */
		url = window.location;
		newurl = addParam(url, "jobId", "4");
		TrackStep=1;
		window.history.pushState('page2', 'Title', newurl);
		/* Changing url without page load-Ajax page transitions-Ends */

		$.ajax({
			url: baseUrl+"/job/applyjob/id/"+jobId,
			data: $('#apply_form').serializeArray(),
			success: function (data)
			{
				//alert(data);
				TrackStep=4;
			}
		});
	}else{
		return false;
	}
});

function initializeJobMap(latitude,longitude)
{
	var mapOptions = {
		zoom:9,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: new google.maps.LatLng(latitude,longitude)
	};
	var map = new google.maps.Map(document.getElementById("job-map-canvas"),mapOptions);
	var siteLatLng = new google.maps.LatLng(latitude,longitude);
	var marker = new google.maps.Marker({
			position: siteLatLng,
			map: map,
			icon: baseUrl + '/public/resources/bg_images/'+mapIcon
			//draggable:false,
			//title: userlocation,
	});

	//map = new google.maps.Map(document.getElementById('job-map-canvas'),mapOptions);
	//marker = new google.maps.Marker({map: map});
	//marker.set("draggable", "false");
	map.setCenter(mapOptions.center);
	marker.setPosition(mapOptions.center);
	marker.setVisible(true);
	var geocoder = new google.maps.Geocoder();
}

function updateRadius(rad){

	//alert(rad);
	//rad = rad*100;
	rad = rad*1600;
	circle.setRadius(rad);
}

function getDateDifference(date11,date22)
{
	var monthNames = [
		"January", "February", "March",
		"April", "May", "June", "July",
		"August", "September", "October",
		"November", "December"
	];

	var newEndDate=date22.split("-");
	var date2=new Date(newEndDate[0],newEndDate[1],newEndDate[2]);

	var day = date2.getDate();
	var monthIndex = date2.getMonth();
	var year = date2.getFullYear();

	console.log(day, monthNames[monthIndex], year);
	return monthNames[monthIndex-1]+' '+day + ', ' + year;
	/*
	var newEndDate=date22.split("-");
	var date1=new Date(date11.getFullYear(),date11.getMonth(),date11.getDate());
	var date2=new Date(newEndDate[0],newEndDate[1],newEndDate[2]);


	var one_day=1000*60*60*24;
	var date1_ms = date1.getTime();
	var date2_ms = date2.getTime();

	var difference_ms = date2_ms - date1_ms;
	difference_ms = difference_ms/1000;
	var seconds = Math.floor(difference_ms % 60);
	difference_ms = difference_ms/60;
	var minutes = Math.floor(difference_ms % 60);
	difference_ms = difference_ms/60;
	var hours = Math.floor(difference_ms % 24);
	var days = Math.floor(difference_ms/24);
	var months = (days/30);

	var finalTimeArr=new Array();
	finalTimeArr[0]=months;
	finalTimeArr[2]=days;
	finalTimeArr[3]=hours;
	finalTimeArr[3]=minutes;
	finalTimeArr[4]=seconds;

	if(Math.floor(months)!='0')
		return Math.floor(months)+' months';
	else if(Math.floor(days)!='0')
		return Math.floor(days)+' days';
	else if(Math.floor(hours)!='0')
		return Math.floor(hours)+' hours';
	else if(Math.floor(minutes)!='0')
		return Math.floor(minutes)+' days';
	else if(Math.floor(seconds)!='0')
		return Math.floor(seconds)+' seconds';
	*/
	//return finalTimeArr;
}

function ViewApplicants(jobId)
{
	$.ajax({
	url: baseUrl+"/job/jobapplicants/job_id/"+jobId,
	success: function (data)
	{
		$('#ApplicantsModal .modal-body').html(data);
		$('#ApplicantsModal').modal('show');
				var a=$('.text-left a').html();
		var str=a.split("<small>");
		var href=$('.text-left a').attr('href');
		var res = href.split('/');
		var user_id=res[2];
			$(".reply").after(a);
			$(".AppMessage").after('<div class="reply_container"><a class="reply" href=""></a></div>');
			$('.reply').attr('href','https://thejobmill.com/compose?profile_user_id='+user_id);
			$('.reply').text('Reply to'+str[0]);
	}
	});
}

function checkReplyForm(num) {

	$('#message_form'+num).validate({
		rules:{
			message_text:{required:true},
			},
	});

	$('#message_form'+num).validate();

	if($('#message_form'+num).valid()){
		$('#message_form'+num).submit();
	}
	else
		return false;
}

function postComment(num)
{

	if($('#commentSection'+num).hasClass('hide'))
		$('#commentSection'+num).removeClass('hide');
	else
		$('#commentSection'+num).addClass('hide')	;
}

function reportPost(num,forumid)
{
	$('#reportModal').modal('show');
	$('#report_thread_id').val(num);
	$('#report_forum_id').val(forumid);
}

function suggestPost(num,forumid)
{
	$('#improvementModal').modal('show');
	$('#improvement_thread_id').val(num);
	$('#improvement_forum_id').val(forumid);
}

$('#reportBtn').click(function(e) {

	if($('#reportModal form').valid()){
		$('#reportModal form').submit();
	}
});

$('#improvementBtn').click(function(e) {
	if($('#improvementModal form').valid()){
		$('#improvementModal form').submit();
	}
});

$('.addSkillBtn').click(function(e) {
   $('#skillModal').modal('show');
});

$('#skillBtn').click(function(e) {

	if($('#skillModal form').valid()){
		$('#skillModal form').submit();
	}
});



/*back button js(apply jobs)*/

$(document).ready(function(){

$(".steps_redirect").click(function(){
          $('#step-4').css('display','none');
          $('#step-1').css('display','block');
		  $(".but_remove").removeClass("btn-primary");
		  $(".but_add").addClass("btn-primary");


                      });
	
});



$(".dropdown-toggle").on("click",function(){
	// alert("hello");
	// $('.dropdown-toggle').dropdown("toggle");
	$(this).next("ul").toggle();
});