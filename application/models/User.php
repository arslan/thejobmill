<?php
class Application_Model_User extends Application_Model_SuperModel
{
 	protected $_name = 'users' , $primary ;
	private $modelEmail ;
	
	
	public function init(){		
 		
		$this->primary = "user_id";
		$this->modelStatic = new Application_Model_Static();
		$this->modelEmail = new Application_Model_Email();
		
	}
	
 	/* 	Add / Update User Information 
	 *	@
	   
	 */
 	public function add($data , $id = false){	

		try{
			
			if($id){
				$updated_records = $this->update($data , $this->primary."=".$id);
				return (object)array("success"=>true,"error"=>false,"message"=>"Record Successfully Updated","row_affected"=>$updated_records) ;
			}
			$password = $data['password'];
			unset($data['password']);
			$insertedId = $this->insert($data); 
			
 			$reset_password_key = md5($insertedId."!@#$%^$%&(*_+".time());
			
			$data_to_update = array("user_reset_status"=>"1","pass_resetkey"=>$reset_password_key);
			
			$this->update($data_to_update, 'user_id = '.$insertedId);
			
 			$data['pass_resetkey'] = $reset_password_key ;
			$data['user_reset_status'] = "1" ;
			$data['last_inserted_id'] = $insertedId ;
			$data['password'] = $password;
			$isSend = $this->modelEmail->sendEmail('registration_email',$data);
 			
 			return (object)array("success"=>true,"error"=>false,"message"=>"Record Successfully Inserted","inserted_id"=>$insertedId) ;
 		}
		catch(Zend_Exception  $e) {/* Handle Exception Here  */
			
			return (object)array("success"=>false,"error"=>true,"message"=>$e->getMessage(),"exception"=>true,"exception_code"=>$e->getCode()) ;
 		}
	}
	

	
	
	
	/* 	Check Email Address Existance 
	 *	@
	   
	 */
	public function checkEmail($email,$id=false){	
	
		$query =  $this->select()->where("user_email = '".$email."'"); 
		
		if(!$id)
			return  $query->query()->fetch();	 	

 		    return  $query->where("user_id != '".$id."'")->query()->fetch(); 	
 	}
	
	
	
	
	
	/* 	Reset Password Email 
	 *	@
	   
	 */
  	public function resetPassword($user_email){
 		/* Update Reset Status */
		
		$user = $this->get(array("where"=>"user_email='$user_email'")) ;
		
 		
		$reset_password_key = md5($user['user_id']."!@#$%^".$user['user_created'].time());
		$data_to_update = array(
							"user_reset_status"=>"1",
							"pass_resetkey"=>$reset_password_key,
 		    			  );
		
 	  
        $this->update($data_to_update, 'user_id = '.$user['user_id']);
		
		$user['pass_resetkey'] = $reset_password_key ;
 		$user['user_reset_status'] = "1" ;
		
		$email = $this->modelEmail->sendEmail('reset_password',$user);
		
		if($email->success)
			return true;
	
 		return false ;  
	}
	
	
		##-------------------------##
	## Check Twt User exittance
	##--------------------------##	 
	public function checkTwtUserExistance($uid){	
		
 		
		if($uid)		
		return  $this->select()->where("oauth_uid = '".$uid."' and oauth_provider = '2'")->query()->fetch(); 	
 	}
	
	
 	/* 	Reset Password Email 
	 *	@
	   
	 */
	 public function get($param = false ){
		 if(is_array($param)){
			 
			 if(isset($param['key'])){
				$result = $this->fetchAll("pass_resetkey='".$param['key']."'");
				if($result->count()){
					return $result->current()->toArray();
				}
				return false ;
 			 }
			 
			 if(isset($param['where'])){
				$result = $this->fetchAll($param['where']);
				if($result->count()){
					return $result->current()->toArray();
				}
				return false ;
 			 }   			 
		 }
		 
 		 $user = $this->find($param) ;
		 return $user->count()?$user->current()->toArray():false;
		  		 
	 }

	public function getAll($param = false)
	{
		if(is_array($param)){

			if(isset($param['key'])){
				$result = $this->fetchAll("pass_resetkey='".$param['key']."'");
                                if($result->count()){
					return $result->current()->toArray();
				}
				return false ;
			}

			if(isset($param['where'])){
				$result = $this->fetchAll($param['where']);
                                
				if($result->count()){
					return $result->toArray();
				}
				return false ;
			}
		}
                $user = $this->fetchAll();
		//$user = $this->find($param) ;
				
		return $user->count()?$user->toArray():false;
	}
	 
	 
	 
	 /* Get User Counts */
	 public function getCount($param = array()){

		 $this->_name = isset($param['table'])? $param['table']: "users"; 
		 
		 $field_name = isset($param['key'])?$param['key']:"user_id";
		 
		 $where = isset($param['where'])?$param['where']:"1";
		  
		 
		 $data = $this->getAdapter()->select()->from($this->_name,new Zend_Db_Expr(" count($field_name) as count"))->where($where)->query()->fetch(); 
		 return $data['count'];
	 }
	 
	 
	 
	 
	 
	 
	 
	 /*  Update the Auth Storage for the Logged User
	 *	@
	   
	 */
	 private function updateAuth($userID)
	 {	
		$userData = $this->getUserData($userID);
		$authStorage=Zend_Auth::getInstance()->getStorage();
		$auth_updated= $authStorage->write((object)$userData);
		return true;
 	}
 	public function deleteUsers($user_ids)
	{
		$uId = $this->delete('user_id IN('.$user_ids.')');
		return $uId;
	}
 
 
 	public function getAllNotifications($user,$type,$site_configs)
	{
		$this->db = Zend_Registry::get("db");
		
		$jobData=$ratingData=$jobAppData=$jobApprovalData=$jobConfirmData=array();
		if(isset($user->user_id) && !empty($user->user_id)){
			if($user->user_type=='employer'){}
				
			else{}
		
			/* MESSAGES NOTIFICATIONS DATA */	
			//$messageWhere="msg_to=".$user->user_id." and notify_type='0' and notify_for_user_id='".$user->user_id."' and notify_status='0'";
			$messageWhere="msg_to=".$user->user_id." and notify_type='0' and notify_for_user_id='".$user->user_id."'";
			if($type==1)	
				$messageWhere=$messageWhere." and (msg_status='0')";
			
			$messageData=$this->db->query('SELECT msg_id,msg_job,notify_id,notify_type,notify_date,user_first_name as user_name, job_title, job_id, notify_status, user_image, user_first_name,user_last_name FROM notifications join messages on msg_id=notify_refer_id join job on job_id=msg_job join users on user_id=msg_from where '.$messageWhere.' group by notify_id')->fetchAll();
			
			
			/* RATING NOTIFICATIONS DATA */	
			//$ratingWhere="(job_user_id=".$user->user_id." or job_app_user_id=".$user->user_id.") and notify_for_user_id='".$user->user_id."' and job_rate_for='".$user->user_id."' and notify_type='4' and notify_status='0'";
			$ratingWhere="(job_user_id=".$user->user_id." or job_app_user_id=".$user->user_id.") and notify_for_user_id='".$user->user_id."' and job_rate_for='".$user->user_id."' and notify_type='4'";
			if($type==1)	
				$ratingWhere=$ratingWhere." and ";
			
			$ratingData=$this->db->query('SELECT job_rating_id,notify_id,notify_type,notify_date,user_first_name as user_name,job_title,job_id,notify_status, user_image, user_first_name,user_last_name,job_rate FROM notifications join job_rating on job_rating_id=notify_refer_id join job on job_id=job_rating_job_id left join job_applications on job_id=job_app_job_id join users on user_id=job_rate_by where '.$ratingWhere.' group by notify_id')->fetchAll();
			
			
			/* JOB APPLICATION NOTIFICATIONS DATA */	
			//$jobAppWhere="job_user_id=".$user->user_id." and notify_for_user_id='".$user->user_id."' and notify_type='1' and notify_status='0'";
			$jobAppWhere="job_user_id=".$user->user_id." and notify_for_user_id='".$user->user_id."' and notify_type='1'";
			if($type==1)	
				$jobAppWhere=$jobAppWhere." and ";
			
			$jobAppData=$this->db->query('SELECT job_app_id,notify_id,notify_type,notify_date,user_first_name as user_name,job_title,job_id,notify_status, user_image,user_first_name,user_last_name FROM notifications join job_applications on job_app_id=notify_refer_id join job on job_id=job_app_job_id join users on user_id=job_app_user_id where '.$jobAppWhere.' group by notify_id')->fetchAll();
			
			
			/* JOB APPROVAL NOTIFICATIONS DATA */	
			//$jobAppovalWhere="notify_for_user_id='".$user->user_id."' and notify_type='2' and notify_status='0'";
			$jobAppovalWhere="notify_for_user_id='".$user->user_id."' and notify_type='2'";
			if($type==1)	
				$jobAppovalWhere=$jobAppovalWhere." and ";
			
			$jobApprovalData=$this->db->query('SELECT job_app_id,notify_id,notify_type,notify_date,user_first_name as user_name,job_title,job_id,notify_status,
			user_image,user_first_name,user_last_name FROM notifications join job_applications on job_app_id=notify_refer_id join job on job_id=job_app_job_id join users on user_id=job_app_user_id where '.$jobAppovalWhere.' group by notify_id')->fetchAll();
			
			
			/* JOB CONFIRMATION NOTIFICATIONS DATA */	
			//$jobConfirmWhere="job_app_user_id='".$user->user_id."' and notify_type='3' and notify_status='0'";
			$jobConfirmWhere="job_app_user_id='".$user->user_id."' and notify_type='3'";
			if($type==1)	
				$jobConfirmWhere=$jobConfirmWhere." and ";
			
			$jobConfirmData=$this->db->query('SELECT job_app_id,notify_id,notify_type,notify_date,user_first_name as user_name,job_title,job_id,notify_status, user_image,user_first_name,user_last_name FROM notifications join job_applications on job_app_id=notify_refer_id join job on job_id=job_app_job_id join users on user_id=job_app_user_id where '.$jobConfirmWhere.' group by notify_id')->fetchAll();
			
			
			$notifyData=array_merge($jobData,$messageData,$ratingData,$jobAppData,$jobApprovalData,$jobConfirmData);
			if(count($notifyData)>0){
				$notifyData=subval_sort($notifyData,'notify_date'); 
				$notifyData = array_reverse($notifyData);	
			}
			
		}else
			$notifyData=array();
			
		
		return $notifyData;
		
	}

	public function getUserBySkills($skills)
	{

		$users = array();
		$joinArr = array(
			'0' => array('0' => 'users', '1' => 'users.user_id = user_details.ud_user_id', '2' => 'inner', '3' => array('users.*')),
		);
		foreach ($skills as $skill) {
			$where = " FIND_IN_SET('$skill', ud_intrested_in) > 0";
			$user = $this->modelStatic->Super_Get('user_details', $where, "fetchAll",
				$extra = array('fields' => array('*')), $joinArr);
			foreach ($user as $tempUser){

				$users[$tempUser["ud_id"]]=$tempUser;

			}

		}

		return $users;
	}
		public function getCsvByUserSkills($users){

			$fileName = "./data/csv/" . date('dmy-hms') . ".csv";
			$handler = fopen($fileName, "w");
			fputcsv($handler, ["#", "First Name", "Last Name", "Cell", "Email","Skills"]);
			$counter = 0;
			$skillsArray=array();
			foreach ($users as $user) {
					unset($skillsArray);

				$joinArr = array(
					'0' => array('0'=>'skills_endorsed',
						'1' => "skill_id=s_skill_id and s_user_id='".$user['ud_user_id']."'",
						'2' => 'left',
						'3' => array('s_user_id','s_skill_id','s_by_user_id')),
				);
				$extra = array(
					'fields' => array('skill_id','skill_title','endorse_count'=>new Zend_Db_Expr('COUNT(s_id)')),
					'group' => 'skill_id',
				);
				$where = "skill_id IN (".$user['ud_intrested_in'].") ";
				$UserSkills = $this->modelStatic->Super_Get('skills',$where,"fetchAll",$extra,$joinArr);
				foreach($UserSkills as $skills){
					$skillsArray[]=$skills['skill_title'];
				}

				$counter++;
				fputcsv($handler, [$counter, $user["user_first_name"], $user["user_last_name"], $user["ud_contact_no"], $user["user_email"], implode(",",$skillsArray)]);

			}
			

			return $fileName;
		}
	public function sendEmailToAdmin($data,$file)
	{
		 $html="<table cellpadding='5' style='    border: 1px solid #ddd;    width: 40%;
					max-width: 40%;
					margin-bottom: 20px;    border-collapse: collapse;
					border-spacing: 0;display: table;     border: 1px solid #ddd;'>
				<tr>
					<td style='    border: 1px solid #eceeef;'><strong style='color: #8a8a8a;'>Job title</strong></td><td style='    border: 1px solid #eceeef;'> <h3 style='    font-family: Open Sans;
					font-size: 16px;
					font-weight: 600;
					line-height: 20px;
					color: #007F7F;
					display: block;
					text-decoration: none;
					outline: 0;'>
					". $data["job_title"]. "</h3>
					</td>
					</tr>
				<tr>
					
				
					<td style='    border: 1px solid #eceeef;'><strong style='color: #8a8a8a;'>Category :</strong></td><td style='    border: 1px solid #eceeef;'>
					<ul style='    display: inline-block;
    list-style: outside none none;
    margin: 0;
    padding: 0;'>
					 <li style='    background: #1281AE none repeat scroll 0 0;
    border-radius: 2px;
    color: #fff;
    cursor: pointer;
    font-family: OPEN SANS;
    font-size: 12px;
    font-weight: 400;
    line-height: 14px;
    padding: 1px 5px;
    text-decoration: none;
    transition: all .3s ease-in-out 0s;'> ". $data["job_category"]. "</li>
					 </ul>
					 </td>
					 </tr>
               <tr>
                 
                 
				<td style='    border: 1px solid #eceeef;'>
				 <strong style='color: #8a8a8a;'>Type of Work :</strong>  
				</td><td style='    border: 1px solid #eceeef;'><ul style='    display: inline-block;
    list-style: outside none none;
    margin: 0;
    padding: 0;'>
					 <li style='    background: #1281AE none repeat scroll 0 0;
    border-radius: 2px;
    color: #fff;
    cursor: pointer;
    font-family: OPEN SANS;
    font-size: 12px;
    font-weight: 400;
    line-height: 14px;
    padding: 1px 5px;
    text-decoration: none;
    transition: all .3s ease-in-out 0s;'>". $data["type_of_work"]. "</li>
					 </ul></td>
					 </tr><tr>
				<td style='    border: 1px solid #eceeef;'>
				 <strong style='color: #8a8a8a;'>Budget :  </strong> 
				</td><td style='    border: 1px solid #eceeef;'><p style='color: #8a8a8a;'>";



	 if($data['job_budget_type']=='0'){
		if($data['job_fix_type']=='0'){

			$html = $html." $".$data['job_budget'];
		}
		else{

			$html = $html." $".$data['job_budget'] . "per hour";
		}
	 }
	else
	{
//		echo '<i class="fa fa-dollar"></i>'.$data['job_min_price'].' to <i class="fa fa-dollar"></i>'.$data['job_max_price']
		$html = $html." $".$data['job_min_price'] . " to $" .$data['job_max_price'] ;
	}



		$html = $html."
				</p></td>
				</tr>
				<tr>
				<td style='    border: 1px solid #eceeef;'>
				 <strong style='color: #8a8a8a;'>Completed before :</strong>    
				</td>
				<td style='    border: 1px solid #eceeef;'>
				<p style='color: #8a8a8a;'>". $data['dead_line'] ."</p>      
				</td>
				</tr>
				<tr>
				<td style='    border: 1px solid #eceeef;'>
				<strong style='color: #8a8a8a;'>Postcode:</strong>      
				</td><td style='    border: 1px solid #eceeef;'>
				<p style='color: #8a8a8a;'> ". $data['post_code'] ."</p>      
				</td>

				</tr>
				<tr>
				<td style='    border: 1px solid #eceeef;'>
				<strong style='color: #8a8a8a;'>Skills:</strong>      
				</td><td style='    border: 1px solid #eceeef;'>
				<p style='color: #8a8a8a;'> ". implode(",",$data['skill_titles']) ."</p>      
				</td>

				</tr>
				<tr>
				<td style='    border: 1px solid #eceeef;'>
				<strong style='color: #8a8a8a;'>Hirer email:</strong>      
				</td><td style='    border: 1px solid #eceeef;'>
				<p style='color: #8a8a8a;'> ".$data['hirer_email'] ."</p>      
				</td>

				</tr>
				<tr>
				<td style='    border: 1px solid #eceeef;'>
				<strong style='color: #8a8a8a;'>Hirer phone:</strong>      
				</td><td style='    border: 1px solid #eceeef;'>
				<p style='color: #8a8a8a;'> ".$data['hirer_phone'] ."</p>      
				</td>

				</tr>
				<tr>
				<td style='    border: 1px solid #eceeef;'>
				<strong style='color: #8a8a8a;'>Job link:</strong>      
				</td><td style='    border: 1px solid #eceeef;'>
				<p style='color: #8a8a8a;'> <a href='" . SITE_HTTP_URL ."/job-details/".$data['job_link']. "'>". $data["job_title"] . "</a></p>      
				</td>

				</tr>
				</table>";
		

		$mail             = new PHPMailer(); // defaults to using php "mail()"




//		$mail->AddReplyTo("name@yourdomain.com","First Last");

		$mail->SetFrom('admin@thejobmill.com', 'TheJobMill');
		// $address = "rob@rnwood.co.uk";
//		$address = "dev3@shayansolutions.com";
		$address = "info@thejobmill.com";
		$mail->AddAddress($address, "Admin Jobmill");


		$mail->Subject    = "An Urgent job is posted";

		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

		$mail->MsgHTML($html);

		$mail->AddAttachment($file);      // attachment

		if(!$mail->Send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			echo "Message sent!";
		}



	}

}