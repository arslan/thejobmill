<?php
class Application_Model_Static extends Application_Model_SuperModel
{
	 
	
	/* Add / Edit Page Information */
	public function add($table_name,$data,$id=false){
 		
		$this->_name= $table_name;
		
		try{
 			
 			if($id){

				$rows_affected = $this->update($data ,$id);

				return (object)array("success"=>true,"error"=>false,"message"=>"Content Successfully Updated","rows_affected"=>$rows_affected) ;
			}
 			
			$inserted_id = $this->insert($data);
			
			return (object)array("success"=>true,"error"=>false,"message"=>"New Page Successfully Added to the database","inserted_id"=>$inserted_id) ;
			
 		}catch(Zend_Exception $e){
			return (object)array("success"=>false,"error"=>true,"message"=>$e->getMessage(),"exception"=>true,"exception_code"=>$e->getCode()) ;
 		}
	}
	 
	 
	public function getPage($id){
		
		$this->_name = "pages";
		
		return $this->select()->where("page_id = ? ",$id)->query()->fetch();
		
		
		
		
	}
	
	public function getContentBlock($id = false){
		
		$this->_name = "content_block";
		
		if($id){
			return $this->select()->where("content_block_id = ? ",$id)->query()->fetch();
		}
		
		return $this->select()->query()->fetchAll();
		
 	}
	
	
	
	/* ================= Static Functions Related To Site Config Table =============================== */
	
	public function getConfigs($type=false){
		$this->_name = "config";
		$where = "1";
		if($type){
			$where = "config_group='".strtoupper($type)."'";
		}
		return $this->select()->where($where)->order('config_id asc')->query()->fetchAll();
	}
	
	
	
	
	/* /////////////////////////////////// END === > Static Functions Related To Site Config Table \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
	
	
	
	/* ================= Static Functions Related To Graphic Media Table =============================== */
	
	public function getMedia($option = false){
		
		$this->_name = "graphic_media";
		
		$result = $this->select();
		
 		if($option){
			$where = "media_id=$option";
			return  $result->where(" media_id = ? ",$option )->query()->fetch();
		}
		 
		return $result->query()->fetchAll();
	}
	
	
	
	
	/* /////////////////////////////////// END === > Static Functions Related To Site Config Table \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
	
	
	
	/* ================= Static Functions Related To Email Templates Table =============================== */
   	public function  getTemplate($param = false){
		$this->_name = "email_templates";
 		if($param){
			if(is_array($param)){
			}else{
				$result = $this->find($param);
				if($result->count())
					return $result->current()->toArray();
				return false ;
			}
		}
	}

	public function getLookingToHireJobs($user_id)
	{
		$joinArr=array(
			'0' => array('0'=>'job_applications','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_app_id','job_app_user_id')),
		);

		/* Getting all the jobs user has posted(LOOKING For job) - Starts */
		$HireJobs = $this->Super_Get('job',"job_user_id='".$user_id."' and (job_app_status is null or job_app_status ='0')","fecthAll", $extra=array(), $joinArr);
		//print_r($HireJobs);
		//exit;

		/* Getting all the jobs user has posted(LOOKING For job) - Ends */

		/* Getting all previous hirings(PREVIOUS HIRINGS) - Starts */

		$PreviousHirings = $this->Super_Get('job',"job_user_id='".$user_id."' and job_app_status='1'","fecthAll",$extra=array(),$joinArr);

		/**
		 * Loop through all HireJobs
		 * if matching record found in Previous Hiting
		 * then remove that record from array
		 */
		foreach($HireJobs as $key=>$hiring)
		{
			foreach($PreviousHirings as $previous)
			{
				if($previous['job_id'] == $hiring['job_id'])
				{
					unset($HireJobs[$key]);
				}
			}
		}
		return $HireJobs;

	}
	
	/* /////////////////////////////////// END === > Static Functions Related To Email Templates Table \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
	

	
	 
	
	   
	
}
