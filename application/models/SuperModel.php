<?php
class Application_Model_SuperModel extends Zend_Db_Table_Abstract
{
 	protected $_name = "";
	
	public function init()
	{		
	}
	
  	/* Insert/Update Record to the DataBase */
 	public function Super_Insert($table_name ,$data ,$where = false)
	{
		$this->_name = $table_name;
		try
		{			
			if($where)
			{
				$updated_records = $this->getAdapter()->update($table_name,$data,$where);
				return (object)array("success"=>true,"error"=>false,"message"=>"Record Successfully Updated","row_affected"=>$updated_records) ;
			}
			
			$insertedId = $this->getAdapter()->insert($table_name,$data); 
 			return (object)array("success"=>true,"error"=>false,"message"=>"Record Successfully Inserted","inserted_id"=>$this->getAdapter()->lastInsertId()) ;
 		}		
		catch(Zend_Exception  $e) 
		{	/* Handle Exception Here */
			return (object)array("success"=>false,"error"=>true,"message"=>$e->getMessage(),"exception"=>true,"exception_code"=>$e->getCode()) ;
 		}
	}

	public function Super_Raw($sql){
		$result = $this->getAdapter()->query($sql);
		return $result->fetchAll();
	}
	
  	/* Get Records */
 	public function Super_Get($table_name , $where = 1, $fetchMode = 'fetch', $extra = array(),$joinArr=array())
	{
		$this->_name = $table_name;
		$fields = array('*');
		
		if(isset($extra['fields']) and  $extra['fields'])
		{
			if(is_array($extra['fields']))
			{
				$fields = $extra['fields'];
			}
			else
			{
				$fields = explode(",",$extra['fields']);
			}
		}
		
		$query  = $this->getAdapter()->select()->from($this->_name,$fields)->where($where);
		/*** Join Conditions ***/
		/*** 0 = table, 1 = join condition, 2 = full join or left join, 3 = columns ***/
		if(isset($joinArr)){
			foreach($joinArr as $newCondition){ 
				if($newCondition[2]=='full')
				{
					$query->join($newCondition[0],$newCondition[1],$newCondition[3]);
				}
				else
					$query->joinLeft($newCondition[0],$newCondition[1],$newCondition[3]);	
			}
		}

		//echo $query.'<br><br>';
		if(isset($extra['group']) and  $extra['group'])
		{
			$query = $query->group($extra['group']);
		}
		
		if(isset($extra['having']) and  $extra['having'])
		{
			$query = $query->having($extra['having']);
		}
		
		if(isset($extra['order']) and  $extra['order'])
		{
			$query = $query->order($extra['order']);
		}
		
		if(isset($extra['limit']) and  $extra['limit'])
		{
			$query = $query->limit($extra['limit'][0],$extra['limit'][1]);
		}
 		/* If Pagging is Required */
		if(isset($extra['pagination']) and  $extra['pagination'])
		{
			return $query;
		}

		return $fetchMode=='fetch'? $query->query()->fetch():$query->query()->fetchAll();
	 }
	 
	public function SuperJoin($tablename1,$tablename2,$join,$where)
	{
		 $record=$this->getAdapter()->select()->from(array('t1'=>$tablename1))
		 ->joinLeft(array('t2'=>$tablename2),$join)
		 ->where($where)
		 ->query()
		 ->fetch();

		 return $record;
	}
	 
	 /* Insert  / Update Record to the DataBase */
 	public function Super_Delete($table_name , $where = "1")
	{	
   		try{
			
			$deleted_records = $this->getAdapter()->delete($table_name ,  $where);
 			return (object)array("success"=>true,"error"=>false,"message"=>"Record Successfully Deleted","deleted_records"=>$deleted_records) ;
  		}
		catch(Zend_Exception  $e) 
		{/* Handle Exception Here  */
			return (object)array("success"=>false,"error"=>true,"message"=>$e->getMessage(),"exception"=>true,"exception_code"=>$e->getCode()) ;
 		}
	}
}
