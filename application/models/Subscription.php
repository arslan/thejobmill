<?php
class Application_Model_Subscription extends Zend_Db_Table_Abstract
{

    protected $_name = 'subscription';
    public $primary ="" , $modelStatic;
    const HOME_GALLERY_SUBSCRIPTION_NAME = 'Home Page Gallery';
    const DISCOUNT_PERCENTAGE = 2;
    const DISCOUNT_VALUE = 1;

    public function init()
    {
        $table_info = $this->info('primary');

        $this->primary = $table_info ['1'];
        $this->modelStatic = new Application_Model_Static();
    }

    public function getHomeGalleryJobs(){
        $modelStatic = new Application_Model_Static();


//        $joinArr=array(
//            '0' => array('0'=>'job','1'=>'job_subscriptions.job_id = job.job_id','2'=>'full','3'=>array('job.*')),
//            '1' => array('0'=>'subscription','1'=>'subscription.sub_id = job_subscriptions.subscription_id','2'=>'full','3'=>array()),
//            '2' => array('0'=>'categories','1'=>'categories.category_id = job.job_category','2'=>'full','3'=>array('categories.category_title')),
//        );
        $sql = "SELECT `job_subscriptions`.*, `job`.*, `categories`.`category_title` FROM `job_subscriptions`
INNER JOIN `job` ON job_subscriptions.job_id = job.job_id
INNER JOIN `subscription` ON subscription.sub_id = job_subscriptions.subscription_id
INNER JOIN `categories` ON categories.category_id = job.job_category WHERE (subscription.sub_id = 4 AND  job.job_closing_date>=CURDATE() AND job.job_id NOT IN (SELECT job_applications.job_app_job_id FROM `job_applications` WHERE job_app_status = '1' AND job_app_complete_status='1'))
";
        //return $this->modelStatic->Super_Get('job_subscriptions',"subscription.sub_name = 'Home Page Gallery'",null, [],$joinArr);
        return $this->modelStatic->Super_Raw($sql);
    }

    public function getNearJobs($user=NULL){
        $modelStatic = new Application_Model_Static();
        /* Getting user details-Starts */
        $distanceQuery="";
        $extra="";

        if ($user!=NULL){
            
            $user->user_id;

            $user_details = $this->modelStatic->Super_Get('user_details',"user_details.ud_user_id = ".$user->user_id,null);

            $filter_lat = $user_details[0]["ud_latitude"];
            $filter_long = $user_details[0]["ud_longitude"];

            if (!empty($filter_lat) && !empty($filter_long)) {

                $distanceQuery = "(((ACOS(SIN(" . $filter_lat . " * PI() / 180) * SIN(job_latitude * PI() / 180) + COS(" . $filter_lat . " * PI() / 180) * COS(job_latitude * PI() / 180) * COS((" . $filter_long . "-job_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344))";
                $extra = array('having' => "distance<=5");


                $joinArr = array(
                    '0' => array('0' => 'job', '1' => 'job_subscriptions.job_id = job.job_id', '2' => 'full', '3' => array('job.*', 'distance' => new Zend_Db_Expr($distanceQuery))),
                    '1' => array('0' => 'subscription', '1' => 'subscription.sub_id = job_subscriptions.subscription_id', '2' => 'full', '3' => array()),
                    '2' => array('0' => 'categories', '1' => 'categories.category_id = job.job_category', '2' => 'full', '3' => array('categories.category_title')),
                );
                return $this->modelStatic->Super_Get('job_subscriptions', "subscription.sub_name = 'Home Page Gallery'", null, $extra, $joinArr);

            }
            else {return false;}
        }
    }


}