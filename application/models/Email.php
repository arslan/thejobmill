<?php
class Application_Model_Email extends Zend_Db_Table_Abstract
{
	
	protected $_name = 'email_templates';
	public $primary ="" , $modelStatic; 
	
	public function init()
	{
   		$table_info = $this->info('primary');
		
		$this->primary = $table_info ['1'];
		$this->modelStatic = new Application_Model_Static();
 	}
	
	 public function sendEmail($type = false ,$data = false)
	 {
		$mail = new Zend_Mail();
		$site_config = Zend_Registry::get("site_config");
		$site_link = SITE_HTTP_URL;
		$site_name = $site_config['site_title']; 
		$SenderName = ""; $SenderEmail = "";$ReceiverName = ""; $ReceiverEmail = "";
		
		$admin_info = $this->modelStatic->getAdapter()->select()->from("users")->where("user_id =1")->query()->fetch();
		
		if(!$type)
		{
		 return  (object) array("error"=>true , "success"=>false , "message"=>" Please Define Type of Email");
		}
 		 
 		switch($type)
		{
			case  'reset_password' :  /* begin  : Reset Password Email */
				
				$template = $this->modelStatic->getTemplate('reset_password');
				$ReceiverEmail = $data['user_email'];
				$ReceiverName =  $data['user_email'];
				
				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				if($data['user_type']=="1" or $data['user_type']=="2")
				{
 					$resetlink = SITE_HTTP_URL."/admin/resetpassword?key=".$data['pass_resetkey'];
 					//$resetlinkhtml='<a href="'.$resetlink.'" >'.$resetlink.'</a>';
				}
				else
				{
					$resetlink = SITE_HTTP_URL."/user/resetpassword/key/".$data['pass_resetkey'];
	 				$resetlinkhtml='<a href="'.$resetlink.'" >'.$resetlink.'</a>';
				}
   				
				$MESSAGE = str_ireplace(array("{user_name}","{verification_link}","{website_link}","{site_name}" ), array( $data['user_first_name'] , $resetlink,APPLICATION_URL,$site_name),$template['emailtemp_content']);
 				
   			break; /* end : Reset Password Email */
			
			case 'registration_email':/* begin : Registration Email */
				
				$template = $this->modelStatic->getTemplate('registration_email');
 				
				$ReceiverEmail = $data['user_email'];
				$ReceiverName = $data['user_email'];
				//$password = $data['password'];
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				
  				$resetlink = SITE_HTTP_URL."/user/activate/key/".$data['pass_resetkey'];
 
  				//$resetlinkhtml='<a href="'.$resetlink.'" >'.$resetlink.'</a>';
 				$MESSAGE = str_ireplace(array("{user_name}","{verification_link}","{website_link}","{site_name}"), array($data['user_first_name'],$resetlink,APPLICATION_URL,$site_name),$template['emailtemp_content']);
			break ;/* end : Registration Email */
			
 			/* Email For Verification of new Email Address */
			case 'email_verification': /* begin :  email_verification */
 				 
				$template = $this->modelStatic->getTemplate('email_verification');
 				
				$ReceiverEmail = $data['user_email'];
				$ReceiverName = $data['user_email'];
				
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				
   				$resetlink = SITE_HTTP_URL."/user/verifyemail/key/".$data['user_email_key'];
 				 
 				//$resetlinkhtml='<a href="'.$resetlink.'" >'.$resetlink.'</a>';
  				
				$MESSAGE = str_ireplace(array("{user_name}","{verification_link}","{website_link}","{site_name}"), array( $data['user_first_name']." ".$data['user_last_name'],$resetlink,APPLICATION_URL,$site_name),$template['emailtemp_content']);
   				
  			break ;/* end : email_verification*/
			
			case 'subadmin_registration_email':/* begin : Registration Email */
				
  				 
				$template = $this->modelStatic->getTemplate('subadmin_registration_email');
				$ReceiverEmail = $data['user_email'];
				$ReceiverName = $data['user_email'];
				
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				
  				$resetlink = SITE_HTTP_URL."/admin/login";
 				$logoLink=HTTP_PROFILE_IMAGES_PATH.'/logo/'.$logoData['config_value'];
  				//$resetlinkhtml='<a href="'.$resetlink.'" >'.$resetlink.'</a>';
 				$MESSAGE = str_ireplace(array("{user_name}","{user_password}","{verification_link}","{website_link}","{site_name}"), array($data['user_first_name'],$data['user_new_password'],$resetlink,APPLICATION_URL,$SenderName),$template['emailtemp_content']);
 				
			break ;/* end : Registration Email */
			
			case 'contact_us':
			{
 	 			$template = $this->modelStatic->getTemplate("contact_us_user");
	 			
 				$sender_email = $data['guest_email'];
				$sender_name = $data['guest_name'];
				$sender_phone = $data['guest_phone'];
				$message = $data['guest_message'];
				$subject = $site_config['site_title']." - ".$template['emailtemp_subject']; 
		 
				/*$mail_content = str_ireplace(
									array( "{SITE_TITLE}" ,"{site_admin}","{guest_name}","{sender_email}","{sender_phone}","{sender_message}","{website_link}","{site_name}"), 
									array(	$site_config['site_title'] ,$site_config['site_title'] ,$sender_name,$sender_email,$sender_phone,$message,APPLICATION_URL,$site_name),
									$template['emailtemp_content']
									);
									
				$mail = new Zend_Mail();
				$mail->setBodyHtml($mail_content)
				->setFrom($site_config["register_mail_id"], $site_config['site_title'])
				->addTo($sender_email , $sender_name)
				->setSubject($subject);
		
				if(!TEST){
					$mail->send() ;
				}*/
				
				// Mail To Admin  		
				$template =$this->modelStatic->getTemplate("contact_us_admin");
						
				$mail_content = str_ireplace(
									array( "{SITE_TITLE}" ,"{site_admin}","{guest_name}","{guest_email}","{guest_phone}","{guest_message}","{website_link}","{site_name}"), 
									array(	$site_config['site_title'] ,$site_config['site_title'] ,$sender_name,$sender_email,$sender_phone,$message,APPLICATION_URL,$site_name),
									$template['emailtemp_content']
									);
				
 				$mail = new Zend_Mail();
				$mail->setBodyHtml($mail_content)
					->setFrom($site_config["register_mail_id"], $site_config['site_title'])
					->addTo($site_config["register_mail_id"],$admin_info['user_first_name']." ".$admin_info['user_last_name'])
					->setSubject($subject);
				 
				if(!TEST and $mail->send()){ return true;} else {return false;}
			}
			break;
			
			/* Email For Paypal Subscription */
			case 'paypal_subscription': 

				$template = $this->modelStatic->getTemplate("paypal_subscription");
				$ReceiverEmail = $data['email'];
				$ReceiverName = $data['name'];
				$subscription_id = $data['subscription_id'];
				$subscription_amount = $data['subscription_amount'];
				
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				$site_link="<br><a href=".SITE_HTTP_URL.">".$site_link."</a>";
				$website_link=SITE_HTTP_URL;
  				$MESSAGE = str_ireplace(array("{content}","{site_name}","{site_link}","{website_link}","{subscription_id}","{subscription_amount}","{user_name}"), array( $data['message'],$site_name,$site_link,$website_link,$subscription_id,$subscription_amount.' $',$ReceiverName),$template['emailtemp_content']);
   				
  			break ;
			/* Email For Job Application */
			case 'job_application_notification': 
				
				$template = $this->modelStatic->getTemplate("job_application_notification");
				$ReceiverEmail = $data['email'];
				$ReceiverName = $data['name'];
				
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				$site_link="<br><a href=".SITE_HTTP_URL.">".$site_link."</a>";
				$website_link=SITE_HTTP_URL;
				//$jobLink="<br><a href=".SITE_HTTP_URL.'/job-details/'.$data['job_id'].">".SITE_HTTP_URL.'/job-details/'.$data['job_id']."</a>";
				$jobLink="<br><a href=".SITE_HTTP_URL.'/job-list#myjobs'.">".SITE_HTTP_URL.'/job-list#myjobs'."</a>";
				
  				$MESSAGE = str_ireplace(array("{site_name}","{site_link}","{website_link}","{user_name}","{job_title}","{job_link}"), array($site_name,$site_link,$website_link,$ReceiverName,$data['job_title'],$jobLink),$template['emailtemp_content']);
   				
  			break ;
			/* Email For Job Invite */
			case 'invitation_for_job': 

				$template = $this->modelStatic->getTemplate("invitation_for_job");
				$ReceiverEmail = explode(",",$data['email']);
				$ReceiverName = $data['name'];
				
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				$site_link="<br><a href=".SITE_HTTP_URL.">".$site_link."</a>";
				$website_link=SITE_HTTP_URL;
				$jobLink="<br><a href=".SITE_HTTP_URL.'/job-details/'.$data['job_id'].">".SITE_HTTP_URL.'/job-details/'.$data['job_id']."</a>";
  				$MESSAGE = str_ireplace(array("{site_name}","{site_link}","{website_link}","{user_name}","{job_title}","{job_link}"), array($site_name,$site_link,$website_link,$ReceiverName,$data['job_title'],$jobLink),$template['emailtemp_content']);
  			break ;
			
			/* Email For Job Awarded */
			case 'job_awarded': 

				$template = $this->modelStatic->getTemplate("job_awarded");
				$ReceiverEmail = $data['email'];
				$ReceiverName = $data['name'];
				
 				$SenderEmail = $site_config['register_mail_id']; 
				$SenderName = $site_config['site_title']; 
				$site_link="<br><a href=".SITE_HTTP_URL.">".$site_link."</a>";
				$website_link=SITE_HTTP_URL;
				$jobLink="<br><a href=".SITE_HTTP_URL.'/job-details/'.$data['job_id'].">".SITE_HTTP_URL.'/job-details/'.$data['job_id']."</a>";
  				$MESSAGE = str_ireplace(array("{site_name}","{site_link}","{website_link}","{user_name}","{job_title}","{job_link}"), array($site_name,$site_link,$website_link,$ReceiverName,$data['job_title'],$jobLink),$template['emailtemp_content']);
  			break ;
			
 			default:return  (object)array("error"=>true , "success"=>false , "message"=>" Please Define Proper Type for  Email");
		}
		$mail->setBodyHtml($MESSAGE)
			 ->setFrom($SenderEmail, $SenderName)
			 ->addTo($ReceiverEmail,$ReceiverName)
			 ->setSubject($template['emailtemp_subject']);

		if(!TEST)
		{
 			$mail->send();
			return (object)array("error"=>false , "success"=>true , "message"=>" Mail Successfully Sent");
		}
		return (object)array("error"=>false , "success"=>true , "message"=>" Unable To Send Email ");	
	 }
	 
	 private function _registration()
	 {
	 }
         public function sendUsersEmail($type = false ,$email = false,$name =false)
	 {
		$mail = new Zend_Mail();
		$site_config = Zend_Registry::get("site_config");
		$site_link = SITE_HTTP_URL;
		$site_name = $site_config['site_title'];
                $site_link="<br><a href=".SITE_HTTP_URL.">".$site_link."</a>";
                $website_link=SITE_HTTP_URL;
                $ReceiverEmail = $email;
                $ReceiverName = $name;
                
		$SenderName = ""; $SenderEmail = "";$ReceiverName = ""; $ReceiverEmail = "";
		
		$admin_info = $this->modelStatic->getAdapter()->select()->from("users")->where("user_id =1")->query()->fetch();
		
		if(!$type)
		{
		 return  (object) array("error"=>true , "success"=>false , "message"=>" Please Define Type of Email");
		}
 		 
 		//Template Content 		
                $template = $this->modelStatic->getTemplate($type);
                $ReceiverEmail = $email;
                $ReceiverName =  $email;

                $SenderEmail = $site_config['register_mail_id']; 
                $SenderName = $site_config['site_title']; 
                

                $MESSAGE = $template['emailtemp_content'];
                $MESSAGE = str_ireplace(array("{site_name}","{site_link}","{website_link}","{user_name}","{user_email}"), array($site_name,$site_link,$website_link,$ReceiverName,$ReceiverEmail),$template['emailtemp_content']);
 			
		$mail->setBodyHtml($MESSAGE)
			 ->setFrom($SenderEmail, $SenderName)
			 ->addTo($ReceiverEmail,$ReceiverName)
			 ->setSubject($template['emailtemp_subject']);

		if(!TEST)
		{
 			$mail->send();
			return (object)array("error"=>false , "success"=>true , "message"=>" Mail Successfully Sent");
		}
		return (object)array("error"=>false , "success"=>true , "message"=>" Unable To Send Email ");	
	 }
}