<?php
class Application_Form_StaticForm extends Twitter_Bootstrap_Form_Vertical
{
 
 	public function init(){
		$this->setMethod('post');
		$this->setAction(self::METHOD_POST);
		$this->setAttribs(array(
			'id' => 'validate',
			"role"=>"form",
			'class' => 'default-form  validate',
			"novalidate"=>"novalidate"
		));
	}
 	/* 
	 *	Static Page Form
	 */
	public function page(){
		 ## --- Page Title ---##	
 		$this->addElement('text', 'page_title', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"placeholder" => "Page Title" ,
			"label" => "Page Title" ,
			"filters"    => array("StringTrim","StripTags"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Page Title is required ")),
 							),
 		));
	 
        ## ---- Page Content ---##
 		$this->addElement('textarea', 'page_content', array (
			"required" => TRUE,
			'class' => 'form-control ckeditor',
			'id' => 'cleditor',
			'rows'=>'6',
			"label" => "Page Content",
			 "validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Page Content is required")),
 							),
		));
 		$this->submitButton();
  	}
	
	public function expertise(){
		 $categoryType = array('0'=>' Online Job','1'=>' Physical Job');
		 $this->addElement('radio', 'category_type', array (
			"required" => TRUE,
			'class' => 'required',
			"label" => "Category Type " ,
			"multioptions"=>$categoryType,
 		));
 		$this->addElement('text', 'category_title', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Category Title " ,
			"filters"    => array("StringTrim","StripTags"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Expertise Title is required ")),
 							),
 		));
 		$this->submitButton();
  	}
	
	public function skills(){
		 ## --- Page Title ---##	
		 
		 

		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll",$extra=array('order'=>'category_title','fields'=>array('category_id','category_title')));
		//$CategoryArray = array(''=>'');
		$CategoryArray = array();
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
		
 		$this->addElement('select', 'skill_category', array(
			//'class'      => 'form-control required chosen-select',
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'&nbsp;&nbsp;Category',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray,
			//'filters'    => array('StringTrim','StripTags','HtmlEntities'),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->skill_category->setDecorators($simpleElementDecorators);
		$this->skill_category->setRegisterInArrayValidator(false);
		
		 
		 
		 
 		$this->addElement('text', 'skill_title', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Skill Title " ,
			"filters"    => array("StringTrim","StripTags"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Skill Title is required ")),
 							),
 		));
 		$this->submitButton();
  	}
	
	public function background_images(){
		/*0=home page,1=sign up page,2=sign in page,3=forgot password,4=myaccount page,5=my profile page*/
		$this->addElement('file', 'bg_0', array (
 			"class" => "form-control",
			"label"=>"Upload Home Page Image"
		));
 		$this->bg_0->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280)
			);
			
/*			$upload->addValidator('ImageSize', false,
                      array('minwidth' => 40,
                            'maxwidth' => 80,
                            'minheight' => 100,
                            'maxheight' => 200)
                      );
*/		$this->addElement('file', 'bg_1', array (
 			"class" => "form-control",
			"label"=>"Upload Signup Page Image"
		));
 		$this->bg_1->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));
			
		$this->addElement('file', 'bg_2', array (
 			"class" => "form-control",
			"label"=>"Upload Signin Page Image"
		));
 		$this->bg_2->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));
			
		$this->addElement('file', 'bg_3', array (
 			"class" => "form-control",
			"label"=>"Upload Forgot Password Page Image"
		));
 		$this->bg_3->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));
			
		$this->addElement('file', 'bg_4', array (
 			"class" => "form-control",
			"label"=>"Upload Myaccount Page Image"
		));
 		$this->bg_4->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));
			
		$this->addElement('file', 'bg_5', array (
 			"class" => "form-control",
			"label"=>"Upload Myprofile Page Image"
		));
 		$this->bg_5->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));
			
		$this->addElement('file', 'bg_6', array (
 			"class" => "form-control",
			"label"=>"Upload Contact Us Page Image"
		));
 		$this->bg_6->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));	
			
		$this->addElement('file', 'bg_7', array (
 			"class" => "form-control",
			"label"=>"Upload Static Page Image"
		));
 		$this->bg_7->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));	
			
		$this->addElement('file', 'bg_10', array (
 			"class" => "form-control",
			"label"=>"Upload Job Post Background Image"
		));
 		$this->bg_10->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB")
			->addValidator('ImageSize', false,array('minwidth' => 1280));	
			
		$this->addElement('file', 'mapicon_8', array (
			"class" => "form-control",
			"label"=>"Upload Map Icon"
		));
		$this->mapicon_8->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB");	
			
		$this->addElement('file', 'processingicon_9', array (
			"class" => "form-control",
			"label"=>"Upload Processing Icon"
		));
		$this->processingicon_9->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF")
			->addValidator('Size', false, "15MB");	
			
		$this->addElement('file', 'favicon_11', array (
			"class" => "form-control",
			"label"=>"Upload Favicon Icon"
		));
		$this->favicon_11->setDestination(BG_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG,gif,GIF,ico,ICO")
			->addValidator('Size', false, "15MB");	
			
   		$this->submitButton();		
  	}

	public function voucher()
	{
		$this->addElement('text', 'code', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Code" ,
			"validators" =>  array(
				array("NotEmpty",true,array("messages"=>" Voucher Code is required")),
			),
		));

		$this->addElement('text', 'value', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Value" ,
			"validators" =>  array(
				array("NotEmpty",true,array("messages"=>" Value is required")),
			),
		));

		$this->addElement('text', 'expire_date', array(
			'class'      => 'form-control required date-picker',
			'required'   => TRUE,
			'label'=>'Expire Date',
			'validators' => array('NotEmpty')
		));

		$this->addElement('select', 'discount_type', array (
			"required" => TRUE,
			'class' => 'form-control required selector_class',
			"label" => "Discount Type" ,
			"validators" =>  array(
				array("NotEmpty"),
			),
			'multioptions' => array(
				'1' => 'Percentage',
				'2' => 'Flat',
			),
		));

		$this->addElement('select', 'status', array(
			'class'      => 'form-control required selector_class',
			'required'   => TRUE,
			'label'=>'Status',
			'validators' => array('NotEmpty'),
			'multioptions' => array(
				'1' => 'Active',
				'2' => 'Inactive',
			),
		));

		$this->addElement('button', 'bttnsubmit', array (
			'class' => 'btn blue ',
			'ignore'=>true,
			'type'=>'submit',
			'label'=>'<i class="icon-ok"></i> Save',
			'escape'=>false
		));
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-right'))	));
	}

	public function subscription()
	{
		$this->addElement('text', 'sub_name', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Name" ,
			 "validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Content Block Title is required")),
 							),
 		));
		
		$this->addElement('text', 'sub_desc', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Description" ,
			 "validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Content Block Title is required")),
 							),
 		));
		
		$this->addElement('text', 'sub_price', array (
			"required" => TRUE,
			'class' => 'form-control required number',
			"label" => "Price" ,
			 "validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Content Block Title is required")),
 							),
 		));
		$this->addElement('select', 'sub_status', array(
			'class'      => 'form-control required',
			'required'   => TRUE,
			'label'=>'Status',
			'validators' => array('NotEmpty'),
			'multioptions' => array(
				'1' => 'Active',
				'2' => 'Incactive',
			),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-md'))
		);
		$this->sub_status->setDecorators($simpleElementDecorators);
		$this->sub_status->setRegisterInArrayValidator(false);

		$this->addElement('button', 'bttnsubmit', array (
				'class' => 'btn blue ',
				'ignore'=>true,
				'type'=>'submit',
 				'label'=>'<i class="icon-ok"></i> Save',
				'escape'=>false
		));
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-right'))	));
	}
	
 	public function content_block(){
   		$this->addElement('text', 'content_block_title', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"placeholder" => "Content Block Title" ,
			"label" => "Content Block Title" ,
			 "validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Content Block Title is required")),
 							),
 		));
		
        ## ---- Page Content ---##
 		$this->addElement('textarea', 'content_block_content', array (
			"required" => TRUE,
			'class' => 'form-control ckeditor',
			'id' => 'cleditor',
			'rows'=>'9',
			"label" => "Content",
			 "validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Content is required")),
 							),
		));
		
		$this->addElement('button', 'bttnsubmit', array (
				'class' => 'btn blue ',
				'ignore'=>true,
				'type'=>'submit',
 				'label'=>'<i class="icon-ok"></i> Save',
				'escape'=>false
		));
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-right'))	));
		
  	} 		 
 
 	public function navigation(){
		$modelUser = new Application_Model_User();
		$modelNavigation = new Application_Model_Navigation();
		$modelContent = new Application_Model_Content();
 		
 		$this->addElement('text', 'menu_title', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"placeholder" => "Menu Title" ,
			"label" => "Menu Title" 
 		));
		
		/* Page Drop Down For the Menu */
		$parentPages = $modelNavigation->getParentMenuList();
  		$this->addElement('select', 'menu_parent_id', array(
			'class'      => 'form-control ',
			'label'=>'Select Menu Parent',
			'validators' => array('NotEmpty'),
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			'multiOptions' => $parentPages
		));
 		
		$getPageList = $modelContent->getPageList();
   		$this->addElement('select', 'menu_page_id', array(
			  'class'      => ' form-control required',
			  'label'=>' Select Page For Menu  ',
			  'validators' => array('NotEmpty'),
			  "filters"    => array("StringTrim","StripTags","HtmlEntities"),
			  'multiOptions' => $getPageList
		));
		
		$post=  array("Header"=>"Show in Header","Footer"=>"Show in Footer","Both"=>"Show in Both Positions");		
		$this->addElement('select', 'menu_show', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Menu Show on ',
			'validators' => array('NotEmpty'),
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			'multiOptions' => $post
		));
		
  		$this->addElement('text', 'menu_permalink', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"placeholder" => "Menu Peramlink" ,
			"label" => "Menu Peramlink" 
 		));
 		
 		$status=  array("0"=>"NO Index","1"=>"Index");		
		$this->addElement('select', 'menu_index', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Menu Index',
			'validators' => array('NotEmpty'),
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			'multiOptions' => $status
		));
 		

		$status=  array("0"=>"No Follow","1"=>"Follow");		
		$this->addElement('select', 'menu_follow', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Menu Follow',
			'validators' => array('NotEmpty'),
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			'multiOptions' => $status
		));
		
		$status=  array("0"=>"Draft","1"=>"Publish");		
		$this->addElement('select', 'menu_status', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Menu Status',
			'validators' => array('NotEmpty'),
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			'multiOptions' => $status
		));
		
		$this->addElement('textarea', 'menu_meta_keywords', array (
			'class' => 'form-control',
 			'rows'=>'6',
			"label" => "Meta Keywords",
			"placeholder" => "Meta Keywords"
		));
		
		$this->addElement('textarea', 'menu_meta_description', array (
 			'class' => 'form-control  ',
 			'rows'=>'6',
			"label" => "Meta Description",
			"placeholder" => "Meta Description"
		));
		
		$this->addElement('textarea', 'menu_google_code', array (
 			'class' => 'form-control   ',
 			'rows'=>'6',
			"label" => "Google Analytics Code For Page",
			"placeholder" => "Google Analytics Code For Page"
		));
		
		$getRequestAdminList = $modelUser->getRequestAdminList();
  		$this->addElement('select', 'request_admin', array(
					  'class'      => ' form-control',
 					  'label'=>' Assign Request Admin ',
					  'validators' => array('NotEmpty'),
					  "filters"    => array("StringTrim","StripTags","HtmlEntities"),
					  'multiOptions' => $getRequestAdminList
		));
		
		$this->addElement('button', 'bttnsubmit', array (
				'class' => 'btn blue ',
				'ignore'=>true,
				'type'=>'submit',
 				'label'=>'<i class="icon-ok"></i> Save',
				'escape'=>false
		));
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-right'))	));
 	}
	
	/* Site Configuration Form */
	public function configuration($type=false){
		$modelStatic = new Application_Model_Static() ;
		$fields = $modelStatic->getConfigs($type);
 		foreach($fields as $key=>$values){
			if($values['config_key']=='subscription_mode'){
				$subscriptionArray = array('0'=>'Off','1'=>'On');
				$this->addElement('radio',$values['config_key'], array (
					"required" => TRUE,
					'class' => 'required',
					"multioptions"=>$subscriptionArray,
					"label" => 'Subscription Mode' ,
					"value"=>$values['config_value']
				));
			}else if($values['config_key']=='forum_state'){
				$forumArray = array('0'=>'Off','1'=>'On');
				$this->addElement('radio',$values['config_key'], array (
					"required" => TRUE,
					'class' => 'required',
					"multioptions"=>$forumArray,
					"label" => 'Forum State' ,
					"value"=>$values['config_value']
				));
			}
			else if($values['config_key']=='maintenance_mode'){
                $maintenanceOptions = array('0'=>'Off','1'=>'On');
                $this->addElement('radio',$values['config_key'], array (
                    "required" => TRUE,
                    'class' => 'required',
                    "multioptions"=>$maintenanceOptions,
                    "label" => 'Maintenance Mode',
                    "value"=>$values['config_value']
                ));
			}else{
				$this->addElement('text',$values['config_key'], array (
					"required" => TRUE,
					'class' => 'form-control required',
					"placeholder" => $values['config_title'] ,
					"label" => $values['config_title'] ,
					"value"=>$values['config_value']
				));
			}
		}
		$this->submitButton();
 	}

	public function add_email_template(){
		## ---- EMAIL TEMPLETS TITEL  ---- ##
		$this->addElement('text', 'emailtemp_title', array (
			'class' => 'form-control required',
			"placeholder" => "Input Email Title",
			"label" => "Input Email Title",
			'validators' => array( array('NotEmpty', true, array ("messages" => "Please enter email title")))
		));

		## ---- EMAIL TEMPLETS SUBJECT  ---- ##
		$this->addElement('text', 'emailtemp_subject', array (
			'class' => 'form-control required',
			"placeholder" => "Input Email Subject",
			"label" => "Input Email Subject"
		));
                ## ---- EMAIL TYPE  ---- ##
		$this->addElement('select', 'email_type', array(
			'class'      => 'form-control required',
			'required'   => true,
			'label'=>'Recipients',
			'validators' => array('NotEmpty'),
			'multioptions' => array(
				'1' => 'New Users (Last 2 months)',
				'2' => 'All Users',
				'3' => 'Individual Email Address'
			),
		));
                ## ---- EMAIL IDs  ---- ##
                $this->addElement('textarea', 'selected_email', array (
			'class' => 'form-control',
			"placeholder" => "Input Email Ids",
			"label" => "Input Email Ids",
                        'attribs'    => array('data-toggle' => 'tooltip','title' => 'use ; to separate email addresses','rows'=>'3')
		));

		## ---- EMAIL TEMPLETS CONTENT  ---- ##
		$this->addElement('textarea', 'emailtemp_content', array (
			'class' => 'ckeditor form-control',
			'id' => 'cleditor',
			"placeholder" => "Email Content Here " ,
			"label" => "Input Email Content",

		));

		$this->addElement('hidden', 'emailtemp_key',array(
			'value'=>'auto-generated-'.time()
		));

		$this->submitButton();
	}
	
	public function email_template($type = -1){
	 	## ---- EMAIL TEMPLETS TITEL  ---- ##
		$this->addElement('text', 'emailtemp_title', array (
			'class' => 'form-control required',
			"placeholder" => "Input Email Title",
			"label" => "Input Email Title",
			'validators' => array( array('NotEmpty', true, array ("messages" => "Please enter email title")))
		));

		## ---- EMAIL TEMPLETS SUBJECT  ---- ##
		$this->addElement('text', 'emailtemp_subject', array (
			'class' => 'form-control required',
			"placeholder" => "Input Email Subject",
			"label" => "Input Email Subject"
		));
		if($type != 0) {
			## ---- EMAIL TYPE  ---- ##
			$this->addElement('select', 'email_type', array(
				'class' => 'form-control required',
				'required' => true,
				'label' => 'Recipients',
				'validators' => array('NotEmpty'),
				'multioptions' => array(
					'1' => 'New Users (Last 2 months)',
					'2' => 'All Users',
					'3' => 'Individual Email Address'
				),
				'attribs' => array('disabled' => 'disabled')
			));
			## ---- EMAIL IDs  ---- ##
			$this->addElement('textarea', 'selected_email', array(
				'class' => 'form-control',
				"placeholder" => "Input Email Ids",
				"label" => "Input Email Ids",
				'attribs' => array('disabled' => 'disabled', 'rows' => '3')
			));
		}
                ## ---- EMAIL TEMPLETS CONTENT  ---- ##
		$this->addElement('textarea', 'emailtemp_content', array (
			'class' => 'ckeditor form-control ',
			'id' => 'cleditor',
			"placeholder" => "Email Content Here " ,
			"label" => "Input Email Title"
		 
		));
		$this->submitButton();
	}
	
	public function graphic_media(){
		$this->addElement('text', 'media_title', array (
			'class' => 'form-control required',
			"placeholder" => " Title",
			"required"=>true,
			"label"=>' Title : <span class="required">*</span>',
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>" This field is required ")),
							),
		));
		
		$this->addElement('text', 'media_alt', array (
			'class' => 'form-control required',
			"placeholder" => " Alternate Text",
			"required"=>true,
			"label"=>' Alternate : <span class="required">*</span>',
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>" This field is required ")),
							),
		));
		
		/* User Video Data */
 		$this->addElement('file', 'media_path', array (
			"placeholder" => " Upload  ",
			"id" => "media_path_image",
			"required"=>true,
 			"class" => "form-control",
			"label"=>"Upload Photo "
		));
 		$this->media_path->setDestination(MEDIA_IMAGES_PATH)
			->addValidator('Extension', false,"jpg,JPG,png,PNG,jpeg,JPEG")
			->addValidator('Size', false, "15MB");
   		$this->submitButton();		
	}
	
	public function subadmin($user_id=false){
		
		
		$this->setAttribs(array(
 			'class' => 'subadmin_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			'enctype'=>'multipart/form-data'
		));
		
		
 		
		global $roleArr;
 		$this->addElement('text', 'user_first_name', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "First Name" ,
			"filters"    => array("StringTrim","StripTags"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"First Name is required ")),
 							),
 		));
	 
		$this->addElement('text', 'user_last_name', array (
			"required" => TRUE,
			'class' => 'form-control required',
			"label" => "Last Name" ,
			"filters"    => array("StringTrim","StripTags"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Last Name is required ")),
 							),
 		));
		
		$this->addElement('text', 'user_email', array (
			"required" => TRUE,
			'class' => 'form-control required email',
			"label" => "Email Address" ,
			"filters"    => array("StringTrim","StripTags"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Email Address is required ")),
 							),
 		));
		
		if($user_id==false){
			$this->user_email->setAttrib("class","form-control required checkemail email  ");
			
			$validator = new Zend_Validate_Db_NoRecordExists(array('table' => 'users','field' => 'user_email'));
			$validator->setMessage("`%value%`  already exists , please enter any other email address");	
			$this->user_email->addValidator($validator);
			
		}
		
		$this->addElement('password', 'user_password', array(
 			"class"      => "form-control ",
			"label"   => "Enter Password<span class='required'>*</span>",
			"autocomplete" =>"off",
 			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
							),
		));
		
 		$this->addElement('password', 'user_rpassword', array(
 			"class"      => "form-control ",
			"label"   => "Re Type  Password<span class='required'>*</span>",
			"autocomplete" =>"off",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
							),
		));
		
		if($user_id==''){
			$this->user_password->setAttrib('class','form-control required');
			$this->user_rpassword->setAttrib('class','form-control required');
			
			$validator = new Zend_Validate_Identical(array('token' =>"user_rpassword"));
			$validator->setMessage(" Password Mismatch ,please enter correct password");	
			$this->user_password->addValidator($validator);
			
			$validator = new Zend_Validate_Identical(array('token' =>"user_password"));
			$validator->setMessage(" Password Mismatch ,please enter correct password");	
			$this->user_rpassword->addValidator($validator);
			
		}
		else{
			$this->addElement('hidden', 'user_id');
		}
		
		
		
		$this->addElement('radio', 'user_role', array(
 			"class"      => "form-control required ",
			"required"   => true,
			"label"   => "Assign Roles",
			"multiOptions" =>$roleArr,
			"value" =>1,
		));
		
		
		
 		$this->submitButton();
 
  	}
	
	public function submitButton(){
		$this->addElement('button', 'bttnsubmit', array (
				'class' => 'btn blue ',
				'ignore'=>true,
				'type'=>'submit',
 				'label'=>'<i class="fa fa-check"></i> Save',
				'escape'=>false
		));
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-right'))	));
	}
}
?>
