<?php
class Application_Form_User extends Twitter_Bootstrap_Form_Vertical
{
	public function init(){
		global $user_info;
  		$this->setMethod('post');
		$this->setAttribs(array(
 			'class' => 'profile_form',
 			'id' => 'register_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			'enctype'=>'multipart/form-data'
		));
		
		$logged_identity = Zend_Auth::getInstance()->getInstance();
		if($logged_identity->hasIdentity()){
			$model = new Application_Model_User();
			$logged_identity = $logged_identity->getIdentity();
			$user_info = (object) $model->get($logged_identity->user_id);
		}
  	}
	
	public function subscription()
	{
		//$subsArray=  array("0"=>"Quarterly (20 $)","1"=>"Half Yeary (30 $)","2"=>"Yearly (75 $)");		
 		$subsArray=  array("0"=>"","1"=>"","2"=>"");		
		$this->addElement('radio', 'user_subscription', array(
			'class'      => 'required',
			'required'   => true,	
			/*'label'=>'Choose a subscription <span href="#" class="badge badge-danger" style="font-size: 12px !important; cursor:pointer;" data-toggle="modal"
			 data-target="#subscriptionModal">?</span>',*/
			'validators' => array('NotEmpty'),
			'multiOptions' => $subsArray
		));
	}
	
	public function user_details(){
		global $user_info;		
		$this->addElement('text', 'user_first_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_first_name,
			"label" => "&nbsp;&nbsp;First Name",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-user EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_first_name->setDecorators($simpleElementDecorators);
		
		
		$this->addElement('text', 'user_last_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_last_name,
			"label" => "&nbsp;&nbsp;Last Name",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-user EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_last_name->setDecorators($simpleElementDecorators);		
		
		/*$this->addElement('text', 'user_last_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_last_name,
			"label" => "&nbsp;&nbsp;User Last Name",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-user EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_last_name->setDecorators($simpleElementDecorators);*/
		
		$this->addElement('text', 'user_email', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_email,
			"label" => "&nbsp;&nbsp;Email Address",
			"readonly"=>"readonly",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-envelope-square EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_email->setDecorators($simpleElementDecorators);
		/*
		$this->addElement('file', 'user_image', array (
							"label" => " Profile Image ",
							"id" => "user_image",
							"class" => "form-control",
							"accept"=>"image/*"
							));
		$this->user_image->setDestination(PROFILE_IMAGES_PATH)
			 ->addValidator('Extension', false,IMAGE_VALID_EXTENTIONS)
			 ->addValidator('Size', false, IMAGE_VALID_SIZE);
		*/
		
		$functionName="match_old_password_front";
		$this->addElement('password', 'user_old_password', array(
 			"class" => "form-control",
			/*"required" => true,*/
			"label" => "Enter Old Password",
			"ignore" => true,
 			"filters" => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								//array("NotEmpty",true, array("messages"=>" Old Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
								array("Callback" , true, array($functionName,'messages'=>"Old Password Mismatch,")),
							),
		));
		 
		$this->addElement('password', 'user_password', array(
 			"class"      => "form-control",
			/*"required"   => true,*/
			"label"   => "Enter Password",
			"autocomplete" =>"off",
 			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								//array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
								array("Identical" , true,array('token' => "user_rpassword", 'messages'=>"Password mismatch, please ender correct same password "))
							),
		));
		
 		$this->addElement('password', 'user_rpassword', array(
 			"class"      => "form-control",
			/*"required"   => true,*/
			"label"   => "Re Type  Password",
			"ignore"=>true,
			"autocomplete" =>"off",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								//array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
								array("Identical" , true,array('token' => "user_password", 'messages'=>"Password mismatch, please ender correct same password "))
							),
		));
   		//$this->user_image->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
		
 		/*$this->addElement('text', 'sp_title', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Profile Title",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Title is Required ")),
 							),
  		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-user ServiceProviderAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->sp_title->setDecorators($simpleElementDecorators);*/
		
 		/*$this->addElement('text', 'sp_rate', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Rate per hour",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Rate is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-money ServiceProviderAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->sp_rate->setDecorators($simpleElementDecorators);*/
		
		$this->addElement('text', 'ud_location', array (
			'class' => 'form-control required',
			"required"=>true,
			"onkeyup"=>"initialize();",
			"label" => "&nbsp;&nbsp;Location",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Location is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-map-marker EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_location->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'ud_contact_no', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Contact Number",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Contact Number is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-phone EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_contact_no->setDecorators($simpleElementDecorators);
		
		$contact_via = array(0=>'Phone',1=>'Email',2=>'Both');
		$this->addElement('select', 'ud_contact_via', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Contact me via",
			'multioptions' => $contact_via,
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Contact Number is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-chevron-circle-right EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_contact_via->setDecorators($simpleElementDecorators);
		
		/*$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll",$extra=array('fields'=>array('category_id','category_title')));
		$CategoryArray = array(''=>'');
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
 		$this->addElement('select', 'sp_expertise', array(
			'class'      => 'form-control required chosen-select',
			'required'   => true,	
			'label'=>'&nbsp;&nbsp;Choose Expertise',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap ServiceProviderAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->sp_expertise->setDecorators($simpleElementDecorators);
		$this->sp_expertise->setRegisterInArrayValidator(false);*/
		$model = new Application_Model_Static();
		$SkillData = $model->Super_Get('skills','skill_status="1"',"fetchAll",$extra=array('order'=>'skill_title','fields'=>array('skill_id','skill_title')));
		$SkillArray = array();
		foreach($SkillData as $skills)
		{
			$SkillArray[$skills['skill_id']]=$skills['skill_title'];
		}
		$user_type=  array("service_provider"=>"Service Provider","employer"=>"Employer");		
 		$this->addElement('select', 'ud_intrested_in', array(
			'class'      => 'form-control chosen-select',
			'required'   => false,	
			"multiple"=>"multiple",
			'label'=>'&nbsp;&nbsp;Skills',
			//'validators' => array('NotEmpty'),
			'multiOptions' => $SkillArray
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_intrested_in->setDecorators($simpleElementDecorators);
		$this->ud_intrested_in->setRegisterInArrayValidator(false);
		
		$this->addElement('select', 'ud_looking_for', array(
			'class'      => 'form-control chosen-select',
			'required'   => false,	
			"multiple"=>"multiple",
			'label'=>'&nbsp;&nbsp;Looking For',
			//'validators' => array('NotEmpty'),
			'multiOptions' => $SkillArray
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_looking_for->setDecorators($simpleElementDecorators);
		$this->ud_looking_for->setRegisterInArrayValidator(false);
		
		$this->addElement('text', 'ud_experience', array (
			'class' => 'form-control',
			"required"=>false,
			"label" => "&nbsp;&nbsp;Experience in years",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-list EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_experience->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'ud_exp_required', array (
			'class' => 'form-control',
			"required"=>false,
			"label" => "&nbsp;&nbsp;Experience required in years",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-list EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_exp_required->setDecorators($simpleElementDecorators);
		
		/*$this->addElement('text', 'ud_worker_company', array (
			'class' => 'form-control url',
			"required"=>false,
			"label" => "&nbsp;&nbsp;Link to Company Page",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-link EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_worker_company->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'ud_hirer_company', array (
			'class' => 'form-control url',
			"required"=>false,
			"label" => "&nbsp;&nbsp;Link to Company Page",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-link EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->ud_hirer_company->setDecorators($simpleElementDecorators);*/
		
		$this->addElement('textarea', 'ud_pitch', array (
			'class' => 'form-control ',
			"required"=>false,
			"style"=>"height:100px;",
			"label" => "Elevator Pitch",
		));
		
		$this->addElement('textarea', 'ud_worker_about', array (
			'class' => 'form-control',
			"required"=>false,
			"style"=>"height:100px;",
			"label" => "Write something about your worker profile",
		));
		
		$this->addElement('textarea', 'ud_hirer_about', array (
			'class' => 'form-control',
			"required"=>false,
			"style"=>"height:100px;",
			"label" => "Write something about your hirer profile",
		));
		
		$this->addElement('hidden', 'ud_latitude', array (
		));
		
		$this->addElement('hidden', 'ud_longitude', array (
		));
 		 
 		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Save',
			'class'    => 'work SiteButton'
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 	}
	
	public function addskill()
	{
		
		/*
		$this->addElement('text', 'skill_category', array (
			'class' => 'form-control required',
			"label"=>"Enter Category ID",
			"required"=>true,
		));

		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll",$extra=array('fields'=>array('category_id','category_title')));
		$CategoryArray = array(''=>'');
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
 		$this->addElement('select', 'skill_categories', array(
			'class'      => 'form-control required chosen-select',
			'required'   => true,	
			'label'=>'&nbsp;&nbsp;Select Category',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray
		));
		
		*/
		
		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll",$extra=array('order'=>'category_title','fields'=>array('category_id','category_title')));
		//$CategoryArray = array(''=>'');
		$CategoryArray = array();
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
		
 		$this->addElement('select', 'skill_category', array(
			//'class'      => 'form-control required chosen-select',
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'&nbsp;&nbsp;Category',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray,
			//'filters'    => array('StringTrim','StripTags','HtmlEntities'),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->skill_category->setDecorators($simpleElementDecorators);
		$this->skill_category->setRegisterInArrayValidator(false);
		
		
		$this->addElement('text', 'skill_title', array (
			'class' => 'form-control required',
			"label"=>"Enter Skill Title",
			"required"=>true,
		));
		
		
	}
	
	public function employer(){
		global $user_info;
		
		$this->addElement('text', 'user_first_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_first_name,
			"label" => "&nbsp;&nbsp;First Name",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-user EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_first_name->setDecorators($simpleElementDecorators);
		
		
		
		$this->addElement('text', 'user_last_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_last_name,
			"label" => "&nbsp;&nbsp;Last Name",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-user EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_last_name->setDecorators($simpleElementDecorators);
				
		
		$this->addElement('text', 'user_email', array (
			'class' => 'form-control required',
			"required"=>true,
			"value"=>$user_info->user_email,
			"label" => "&nbsp;&nbsp;Email Address",
			"readonly"=>"readonly",
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-envelope-square EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->user_email->setDecorators($simpleElementDecorators);
		
		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll",$extra=array('order'=>'category_title','fields'=>array('category_id','category_title')));
		$CategoryArray = array(''=>'');
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
 		$this->addElement('select', 'e_industry', array(
			'class'      => 'form-control required chosen-select',
			'required'   => true,	
			'label'=>'&nbsp;&nbsp;Looking For',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->e_industry->setDecorators($simpleElementDecorators);
		$this->e_industry->setRegisterInArrayValidator(false);
		
		$this->addElement('text', 'e_experience', array (
			'class' => 'form-control',
			"required"=>false,
			"label" => "&nbsp;&nbsp;Experience in years",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-list EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->e_experience->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'e_company_link', array (
			'class' => 'form-control url',
			"required"=>false,
			"label" => "&nbsp;&nbsp;Company Link",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-link EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->e_company_link->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'e_location', array (
			'class' => 'form-control required',
			"required"=>true,
			"onkeyup"=>"initialize()",
			"label" => "&nbsp;&nbsp;Location",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Location is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-map-marker EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->e_location->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'e_contact_number', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Contact Number",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Contact Number is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-phone EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->e_contact_number->setDecorators($simpleElementDecorators);
		
		$contact_via = array(1=>'Phone',2=>'Email',3=>'Both');
		$this->addElement('select', 'e_contact_via', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Contact me via",
			'multioptions' => $contact_via,
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Contact Number is Required ")),
 								),
		));
		$simpleElementDecorators = array(
		array('ViewHelper'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-chevron-circle-right EmployerAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->e_contact_via->setDecorators($simpleElementDecorators);
		
		$this->addElement('textarea', 'e_pitch', array (
			'class' => 'form-control ',
			"required"=>false,
			"style"=>"height:100px;",
			"label" => "Elevator Pitch",
		));
		
		$this->addElement('textarea', 'e_about', array (
			'class' => 'form-control',
			"required"=>false,
			"style"=>"height:100px;",
			"label" => "Write something about your hirer profile",
		));
		
		$this->addElement('hidden', 'e_latitude', array (
		));
		
		$this->addElement('hidden', 'e_longitude', array (
		));
 		 
 		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Save',
			'class'    => 'hire SiteButton'
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 	}
	
	
	/* Front User Registration Form */
	public function register(){
		/*$user_type=  array("service_provider"=>"Service Provider","employer"=>"Employer");		
 		$this->addElement('radio', 'user_type', array(
			'class'      => 'required',
			'required'   => true,	
			'label'=>'Register as : ',
			'value'=>'service_provider',
			'validators' => array('NotEmpty'),
			'multiOptions' => $user_type
		));*/
		
 		$this->addElement('text', 'user_first_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "First Name",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" First Name is Required ")),
 							),
  		));
		
 		$this->addElement('text', 'user_last_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "Last Name",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Last Name is Required ")),
 							),
  		));		

		/*$this->addElement('text', 'user_last_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "Last Name",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Last Name is Required ")),
 								),
		));*/
 	
 		$this->loginElements();
 		
  		$this->addElement('password', 'user_rpassword', array(
 			"class"      => "form-control required ",
			"required"   => true,
			"label"   => "Confirm Password",
			"ignore"=>true,
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 50, 'messages'=>"Password must between 6 to 16 characters "))
							),
		));
		
		$model = new Application_Model_User();
		$secretQuestions = $model->Super_Get("secret_questions","1","fetchAll");
		
		$secretQuestionArray=array(''=>'Choose a secret question');
		foreach($secretQuestions as $question){
			$secretQuestionArray[$question['secret_question_id']]=$question['secret_question_text'];
		}
		$this->addElement('text', 'user_mobile_no', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "Mobile Number",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
				array("NotEmpty",true,array("messages"=>" First Name is Required ")),
			),
		));
        /*
		$this->addElement('button', 'confirm_button', array(
			'label' => 'Confirm Mobile',
			'class' => 'nice_button form-control',
//			'data-toggle' => 'modal',
//			'data-target' => '#myModal',
		));*/

		$this->addElement('select', 'user_secret_question', array(
 			"class"      => "form-control required ",
			"required"   => true,
			"label"   => "Choose a secret question",
			"multioptions"=>$secretQuestionArray,
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Secret question is required")),
							),
		));
		
		$this->addElement('text', 'user_secret_answer', array(
 			"class"      => "form-control required ",
			"required"   => true,
			"label"   => "Answer your secret question",
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Secret question is required")),
							),
		));

		
		$this->addElement('text', 'user_terms_link', array(
 			"class"      => "form-control required hidden-element ",
			"required"   => true,
		));
		
		
		$this->addElement('text', 'user_privacy_link', array(
 			"class"      => "form-control required hidden-element ",
			"required"   => true
		));
		
		
		$this->addElement('text', 'user_registration_ip_address', array(
 			"class"      => "form-control required hidden-element ",
			"required"   => true,
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Secret question is required")),
							),
		));
		
		$this->addElement('checkbox', 'user_terms_agreement', array(
				"label"   => "&nbsp; I agree to the <a class='agreement-link' href='/term-and-conditions' target='_blank'>Terms & Conditions</a> and <a class='agreement-link' href='/privacy-policy' target='_blank'>Privacy Policy</a>.",
  		));
		
		/*
		$this->addElement('checkbox', 'cb_agree', array(
 			"class"      => "required ",
			"required"   => true
		));		
		*/
		
		$this->user_email->setAttrib("class","form-control required checkemail email  ");
		$validator = new Zend_Validate_Db_NoRecordExists(array('table' => 'users','field' => 'user_email'));
		$validator->setMessage("`%value%`  already exists , please enter any other email address");	
		$this->user_email->addValidator($validator);
		
		$validator = new Zend_Validate_Identical(array('token' =>"user_rpassword"));
		$validator->setMessage(" Password Mismatch ,please enter correct password");	
  		$this->user_password->addValidator($validator);
		
		$validator = new Zend_Validate_Identical(array('token' =>"user_password"));
		$validator->setMessage(" Password Mismatch ,please enter correct password");	
  		$this->user_rpassword->addValidator($validator);
 		
 		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Register',
			'class'    => 'hire SiteButton'
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 	}
	
	public function  loginElements(){
		$this->addElement('text', 'user_email', array(
			"class"      => "form-control required email ",
			'autocomplete'=>'on',
			"required"   => true,
			"label"   => "Email Address",
			"filters"    => array("StringTrim","StripTags","HtmlEntities","StringToLower"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>" Email address is required ")),
								array("EmailAddress" , true,array("messages"=>" Please enter valid email address "))
							),
		 ));
		
 		$this->addElement('password', 'user_password', array(
 			"class"      => "form-control    required ",
			"required"   => true,
			"label"   => "Password",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters "))
							),
		));
	}
	
	public function twitter_email(){
		$this->addElement('text', 'user_email', array(
			"class"      => "form-control required email ",
			'autocomplete'=>'on',
			"required"   => true,
			"label"   => "Email Address",
			"filters"    => array("StringTrim","StripTags","HtmlEntities","StringToLower"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>" Email address is required ")),
								array("EmailAddress" , true,array("messages"=>" Please enter valid email address "))
							),
		 ));
		
		$this->user_email->setAttrib("class","form-control required checkemail email  ");
		$validator = new Zend_Validate_Db_NoRecordExists(array('table' => 'users','field' => 'user_email'));
		$validator->setMessage("`%value%`  already exists , please enter any other email address");	
		$this->user_email->addValidator($validator);
		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Continue',
			'class'    => 'btn btn-lg btn-primary btn-block '
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	/* Login Form */
	public function login(){
		
		$this->loginElements();
		
		
 		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Login',
			'class'    => 'btn btn-custom-dark row-fluid button-margin-form '
		));
  	}
	
	public function login_front($isAdmin = false){
 		
		if($isAdmin){
			$this->loginElements();
			
			$this->user_email->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
			$this->user_password->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
		}else{
			$this->loginElements();
			$this->addElement('checkbox', 'remember_me', array(
				"label"   => "&nbsp; Remember Me",
  		));
			/*$user_type=  array("service_provider"=>"Service Provider","employer"=>"Employer");		
			$this->addElement('radio', 'user_type', array(
				'class'      => 'required',
				'required'   => true,	
				'label'=>'Register as : ',
				'value'=>'service_provider',
				'validators' => array('NotEmpty'),
				'multiOptions' => $user_type
			));*/
			
			
		}
		
 		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'label'    => 'Login',
			'type'=>'submit',
			'class'    => 'hire SiteButton'
		));
		
		//gcm($this->submit);
		//prd($this->submit->getAttrib('buttons'));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	public function forgotPassword(){
		$this->addElement('text', 'user_email', array(
			"class"      => "form-control required email emailexists",
			'autocomplete'=>'on',
			"required"   => true,
			"label"   => "Email Address",
			"filters"    => array("StringTrim","StripTags","HtmlEntities","StringToLower"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>" Email address is required ")),
								array("EmailAddress" , true,array("messages"=>" Please enter valid email address ")),
								array("Db_RecordExists" , true,array('table' => 'users','field' => 'user_email' ,
								"messages"=>"`%value%` is not registered , please enter valid email address "))
							),
		));
		//$this->user_email->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 		
  		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Submit',
			'class'    => 'hire SiteButton'
		));
		
		$this->btnsubmit->setAttrib("type",'submit');
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
 	
	public function contact_us(){
		$this->setMethod('post');
		/*$this->setAction(self::METHOD_POST);*/
		
		/*$this->setAttribs(array(
			'id' => 'validate',
			'class' => 'form-vertical',
 			'novalidate'=>'novalidate',
			'enctype'=>'multipart/form-data'
 		));*/
		
		/*  Name  */	
		$this->addElement('text', 'guest_name', array(
				"class"      => "form-control top-element required",
				"required"   => true,
 				"placeholder"   => "Enter Your Full Name",
				"label"   => "Full Name",
				"filters"    => array("StringTrim",'StripTags'),
				'validators' => array( array('NotEmpty', true, array ("messages" => " Full Name cannot be empty ")))
  		));
		//$this->guest_name->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
  		
		/* Email */
 		$this->addElement('text', 'guest_email', array(
						'class' => 'form-control middle-element required email',
						'required'   => true,
						'placeholder'   => 'Enter Your Email Address',
						'label'   => 'Email Address',
						'filters'    => array('StringTrim','StripTags'),
						'validators' => array( array('NotEmpty', true, array ("messages" => "Email Address cannot be empty")), 'EmailAddress')
        ));
		//$this->guest_email->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
		
		$this->addElement('text', 'guest_phone', array(
						'class' => 'form-control  middle-element required ',
						'required'   => true,
						'placeholder'   => 'Enter Your Phone Number',
						'label'   => 'Phone Number',
						'filters'    => array('StringTrim','StripTags'),
						'validators' => array( array('NotEmpty', true, array ("messages" => "Phone Number cannot be empty")))
        ));
		//$this->guest_phone->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
  
		/*  User Address   */	
		$this->addElement('textarea', 'guest_message', array(
				"class"      => "form-control bottom-element required",
 				"rows"=>5, 
				'required'   => true,
  				"placeholder"   => " Enter Message Here",
				"label"   => "Message ",
				"filters"    => array("StringTrim",'StripTags') ,
				'validators' => array( array('NotEmpty', true, array ("messages" => "Message field cannot be empty.")))
  		));
		//$this->guest_message->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Send',
			'class'    => 'btn btn-lg btn-primary btn-block '
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
  	}
 
	public function profile(){
 		$this->addElement('text', 'user_name', array (
			'class' => 'm-wrap span6 required',
			"label" => "Admin Name",
			'validators' => array( array('NotEmpty', true, array ("messages" => "Please enter name")))
		));
		$this->user_name->addValidator("NotEmpty", true, array ("messages" => "Please enter name"));
	
		/*$this->addElement('text', 'user_last_name', array (
			'class' => 'm-wrap span6 required',
			"placeholder" => "Admin Last Name",
			"label" => "Admin Last Name",
			 'validators' => array( array('NotEmpty', true, array ("messages" => "Please enter last name")))
		));
		$this->user_last_name->addValidator("NotEmpty", true, array ("messages" => "Please enter last name"));*/
 
 		##--------------- Admin Email Address -------##
		$this->addElement('text', 'user_email', array(
			'label'      => 'Email',
			'class' => 'm-wrap span6 required email',
			'required'   => true,
			'filters'    => array('StringTrim','StripTags'),
			'validators' => array('NotEmpty')
		));
		
		$this->user_email->addValidator('NotEmpty',true,array('messages' =>'Email is required.'))
		->addValidator('EmailAddress',true)->addErrorMessage('Please enter a valid Email-Id');
		
		##--------------- Admin  PaypaL Email Address -------##
		$this->addElement('text', 'user_paypal_email', array(
							'label'      => 'Paypal Email',
							'class' => 'm-wrap span6 required email',
							'required'   => true,
							'filters'    => array('StringTrim','StripTags'),
							'validators' => array('NotEmpty')
		));
		$this->user_paypal_email->addValidator('NotEmpty',true,array('messages' =>'Payal Email is required.'))
		->addValidator('EmailAddress',true)->addErrorMessage('Please enter a valid Email-Id');
		
		$this->addElement('file', 'user_image', array (
							"label" => " Please Select Image ",
							"id" => "user_image",
							"class" => "default",
							));
  		$this->user_image->setDestination(ROOT_PATH.PROFILE_IMAGES);
		$this->submitBtn();
	}

	public function profile_admin($user_id=false){
		$this->setAttrib('id','user_profile');
 
  		$salutations=  array("0"=>"Mr","1"=>"Mrs","2"=>"Ms");		
 		$this->addElement('select', 'user_salutation', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Salutation',
			'validators' => array('NotEmpty'),
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			'multiOptions' => $salutations
		));
  		
 		$this->addElement('text', 'user_first_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "First Name",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" First Name is Required ")),
 							),
  		));
 	
		$this->addElement('text', 'user_last_name', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "Last Name",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Last Name is Required ")),
 							),
		));
 	
		$this->addElement('text', 'user_email', array(
 			'class' => 'form-control required checkemail_exclude email',
			'required'   => true,
			"readonly"=>"readonly",
 			'label'      => 'Email',
  			"filters"    => array("StringTrim","StripTags","HtmlEntities","StringToLower"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>" Email address is required ")),
								array("EmailAddress" , true,array("messages"=>" Please enter valid email address "))
							),
		));
		
		$validator = new Zend_Validate_Db_NoRecordExists(array('table' => 'users','field' => 'user_email',
			'exclude' => array(
			'field' => 'user_id',
			'value' => $user_id
        	)
		));
		$validator->setMessage("`%value%`  already exists , please enter any other email address");	
		$this->user_email->addValidator($validator);
			
		/*$this->addElement('text', 'user_address', array(
 			'class' => 'form-control required ',
			'required'   => true,
			"onchange"=>"deliveryAddress()",
			'label'      => 'Address',
   			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Address is Required ")),
 							),
		));
		
 		 $this->addElement('text', 'user_state', array(
 			'class' => 'form-control required',
			'required'   => true,
			'label'      => 'State',
			"onchange"=>"deliveryAddress()",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" State is Required ")),
 							),
  		));
		
		 $this->addElement('text', 'user_postal_code', array(
 			'class' => 'form-control required ',
			'required'   => true,
			'label'      => 'Postal Code',
  			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Postal Code  is Required ")),
 							),
		));
		
 		 $this->addElement('text', 'user_country', array(
 			'class' => 'form-control required',
			'required'   => true,
			'label'      => 'Country',
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Country is Required ")),
 							),
  		));*/
   		$this->_submitButton(false,"profile_submit");
	}
 	 
	public function image(){
		$this->addElement('file', 'user_image', array (
							"label" => " Profile Image ",
							"id" => "user_image",
							"class" => "default required",
							"accept"=>"image/*"
							));
		$this->user_image->setDestination(PROFILE_IMAGES_PATH)
			 ->addValidator('Extension', false,IMAGE_VALID_EXTENTIONS)
			 ->addValidator('Size', false, IMAGE_VALID_SIZE);
   		$this->user_image->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	 
	/* Change Password Form  */
	public function changePassword($isAdmin = false){
		/*	Admin Old Passwork Form	*/	
		
		$functionName="match_old_password_front";
		if($isAdmin){
			$functionName="match_old_password";
		}
		
 		$this->addElement('password', 'user_old_password', array(
 			"class" => "form-control  required ",
			"required" => true,
			"label" => "Enter Old Password",
			"ignore" => true,
 			"filters" => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true, array("messages"=>" Old Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
								array("Callback" , true, array($functionName,'messages'=>"Old Password Mismatch,")),
							),
			));
			$this->resetPassword($isAdmin);
	}

	/* Reset Password Form  */
 	public function resetPassword($isAdmin = false ){
  		$this->addElement('password', 'user_password', array(
 			"class"      => "form-control  required ",
			"required"   => true,
			"label"   => "Enter Password",
			"autocomplete" =>"off",
 			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
								array("Identical" , true,array('token' => "user_rpassword", 'messages'=>"Password mismatch, please ender correct same password "))
							),
		));
		
 		$this->addElement('password', 'user_rpassword', array(
 			"class"      => "form-control  required ",
			"required"   => true,
			"label"   => "Re Type  Password",
			"ignore"=>true,
			"autocomplete" =>"off",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>" Password is required ")),
								array("StringLength" , true,array('min' => 6, 'max' => 16, 'messages'=>"Password must between 6 to 16 characters ")),
								array("Identical" , true,array('token' => "user_password", 'messages'=>"Password mismatch, please ender correct same password "))
							),
		));
		
		$model = new Application_Model_User();
		$secretQuestions = $model->Super_Get("secret_questions","1","fetchAll");
		
		$secretQuestionArray=array(''=>'Your Secret Question');
		foreach($secretQuestions as $question){
			$secretQuestionArray[$question['secret_question_id']]=$question['secret_question_text'];
		}

		$this->addElement('select', 'user_secret_question', array(
 			"class"      => "form-control required ",
			"disabled"   => "disabled",
			"label"   => "Your Secret Question",
			"multioptions"=>$secretQuestionArray,
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Secret question is required")),
							),
		));
		
		$this->addElement('text', 'user_secret_answer', array(
 			"class"      => "form-control required ",
			"required"   => true,
			"label"   => "Answer your secret question",
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Secret question is required")),
							),
		));
		
		global $user_info;
		/*if($user_info->user_type=='service_provider'){
			$this->addElement('button', 'bttnsubmit', array(
				'ignore'   => true,
				'type'=>'submit',
				'label'    => 'Reset Password',
				'class'    => 'work SiteButton'
			));
		}else{*/
			$this->addElement('button', 'bttnsubmit', array(
				'ignore'   => true,
				'type'=>'submit',
				'label'    => 'Reset Password',
				'class'    => 'hire SiteButton'
			));
		//}
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-left'))	));
		if(!$isAdmin){
 			//$this->user_password->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
			//$this->user_rpassword->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
			$this->bttnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
		}
	}
	
 	public function submitBtn($class=false){
		global $user_info;
		if($user_info->user_type=='service_provider'){
			$this->addElement('button', 'bttnsubmit', array (
				'class' => 'work SiteButton',
				'ignore'=>true,
				'type'=>'submit',
				'label'=>'<i class="icon-ok"></i> Save',
				'escape'=>false
			));
		}else{
			$this->addElement('button', 'bttnsubmit', array (
				'class' => 'hire SiteButton',
				'ignore'=>true,
				'type'=>'submit',
				'label'=>'<i class="icon-ok"></i> Save',
				'escape'=>false
			));
		}
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' => $class))	));
	}
	 
	private function _submitButton(){
		global $user_info;
		if($user_info->user_type=='service_provider'){
			$this->addElement('button', 'bttnsubmit', array (
				'class' => 'work SiteButton',
				'ignore'=>true,
				'type'=>'submit',
 				'label'=>'Save',
				'escape'=>false
			));
		}else{
			$this->addElement('button', 'bttnsubmit', array (
				'class' => 'hire SiteButton',
				'ignore'=>true,
				'type'=>'submit',
 				'label'=>'Save',
				'escape'=>false
			));
		}
		
		$this->bttnsubmit->setDecorators(array('ViewHelper',array(array('controls' => 'HtmlTag'), array('tag' => 'div', 'class' =>'form-actions text-left'))	));
	}
}
