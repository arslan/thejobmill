<?php
class Application_Form_Blog extends Twitter_Bootstrap_Form_Vertical
{
	public function init(){
		global $user_info;
  		$this->setMethod('post');
		$this->setAttribs(array(
 			'class' => 'profile_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			'enctype'=>'multipart/form-data'
		));
  	}
 	
	public function addblog(){
 		$this->addElement('text', 'blog_title', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "Blog Title",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Job Title is Required ")),
 							),
  		));
		
		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('blog_category','1',"fetchAll",$extra=array('fields'=>array('blog_cat_id','blog_cat_title')));
		$CategoryArray = array(''=>'Select Category');
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['blog_cat_id']]=$categories['blog_cat_title'];
		}
 		$this->addElement('select', 'blog_category', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Choose Category',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray
		));
		$this->blog_category->setRegisterInArrayValidator(false);
		
		$this->addElement('textarea', 'blog_desc', array (
			'class' => 'form-control ckeditor required',
			"required"=>true,
			"style"=>"height:100px;",
			"label" => "Blog Description",
		));
		
		$this->addElement('file', 'blog_image', array (
			'class'=> 'form-control',
			"label" => "Blog Image",
		));
		$this->blog_image->setDestination(BLOG_IMAGES_PATH)
			 ->addValidator('Extension', false,IMAGE_VALID_EXTENTIONS)
			 ->addValidator('Size', false, IMAGE_VALID_SIZE);
		
		$this->addElement('button', 'submit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Save',
			'class'    => 'hire SiteButton'
		));
		$this->submit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 	}
	
	public function addblogcategory(){
 		$this->addElement('text', 'blog_cat_title', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Blog Category Title",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>"Blog Category Title is Required ")),
 							),
  		));
		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Save',
			'class'    => 'hire SiteButton'
		));
		
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	public function blogcomments(){
		$this->addElement('textarea', 'blog_comment', array (
			'class' => 'form-control required',
			"required"=>true,
			"style"=>"height:100px;",
			"label" => "Add a comment",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Comment is Required ")),
 							),
  		));
		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Post Comment',
			'class'    => 'hire SiteButton'
		));
		
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
}