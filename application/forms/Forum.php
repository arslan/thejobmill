<?php
class Application_Form_Forum extends Twitter_Bootstrap_Form_Vertical
{
	public function init(){
		global $user_info;
  		$this->setMethod('post');
		$this->setAttribs(array(
 			'class' => 'profile_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			'enctype'=>'multipart/form-data'
		));
  	}
 	
	public function addtopic($moduleType){
		
		$ForumCategoryArray = array(''=>'Select Category','Lounge'=>'Lounge','Questions'=>'Questions','Feedback'=>'Feedback');
		$this->addElement('select', 'forum_cat_id', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "Choose a category",
			"multioptions"=>$ForumCategoryArray,
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Category is Required ")),
 							),
  		));
		
		if($moduleType=='default')
			$moduleType="text";
		else
			$moduleType="textarea";	
 		$this->addElement($moduleType, 'forum_topic', array (
			'class' => 'form-control required',
			"required"=>true,
			"rows"=>4,
			"label" => "Topic Name",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Topic Name is Required ")),
 							),
  		));
		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Save',
			'class'    => 'work SiteButton'
		));
		
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	
	
	
	public function addthread(){
		$this->addElement('textarea', 'f_thread', array (
			'class' => 'form-control required',
			"required"=>true,
			"style"=>"height:100px;",
		//	"label" => "Add a comment",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Comment is Required ")),
 							),
  		));
		
		$this->addElement('hidden', 'f_forum_id');
		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'SUBMIT POST',
			'class'    => 'work SiteButton'
		));
		
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	public function sendreport()
	{
		
		$this->addElement('hidden', 'report_thread_id');
		$this->addElement('hidden', 'report_forum_id');
		
		$this->addElement('text', 'report_email', array (
			'class' => 'form-control required email',
			"required"=>true,
			"label" => "Email Address",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Email Address is Required ")),
 							),
  		));
		
		$this->addElement('textarea', 'report_text', array (
			'class' => 'form-control required',
			"required"=>true,
			"style"=>"height:100px;",
			"label" => "Your Comment",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Comment is Required ")),
 							),
  		));
	}
	
	public function sendimprovement()
	{
		
		$this->addElement('hidden', 'improvement_thread_id');
		$this->addElement('hidden', 'improvement_forum_id');
		
		$this->addElement('text', 'improvement_email', array (
			'class' => 'form-control required email',
			"required"=>true,
			"label" => "Email Address",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Email Address is Required ")),
 							),
  		));
		
		$this->addElement('textarea', 'improvement_text', array (
			'class' => 'form-control required',
			"required"=>true,
			"style"=>"height:100px;",
			"label" => "Your Comment",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Comment is Required ")),
 							),
  		));
	}
}