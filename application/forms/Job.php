<?php
class Application_Form_Job extends Twitter_Bootstrap_Form_Vertical
{
	public function init(){
		global $user_info;
		global $user_detail;
		$this->setMethod('post');
		$this->setAttribs(array(
 			'class' => 'profile_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			'enctype'=>'multipart/form-data'
		));
		
		$logged_identity = Zend_Auth::getInstance()->getInstance();
		if($logged_identity->hasIdentity()){
			$model = new Application_Model_User();
			$logged_identity = $logged_identity->getIdentity();
			$user_info = (object) $model->get($logged_identity->user_id);
			$user_detail = (object) $model->Super_Get('user_details','ud_user_id="'.$logged_identity->user_id.'"');
		}
  	}
 	
	public function postjob(){
		global $user_info;
		global $user_detail;
		$this->addElement('text', 'job_title', array (
			'class' => 'form-control required',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Job Title",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Job Title is Required ")),
 							),
  		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-align-justify JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_title->setDecorators($simpleElementDecorators);
		
		$JobTypeArray = array('0'=>'Online Job','1'=>'Physical Job');
		$this->addElement('select', 'job_type', array(
			'class'      => 'form-control required',
			"required"=>true,
			"label"=>"&nbsp;Choose Type",
			"onchange"=>"populateCategories(this.value);",
			'multiOptions' => $JobTypeArray,
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Job Type is Required ")),
 							),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_type->setDecorators($simpleElementDecorators);
		$this->job_type->setRegisterInArrayValidator(false);
		
		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll",$extra=array('order'=>'category_title','fields'=>array('category_id','category_title')));
		$CategoryArray = array(''=>'');
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
 		$this->addElement('select', 'job_category', array(
			'class'      => 'form-control required chosen-select',
			'required'   => true,	
			'label'=>'&nbsp;&nbsp;Choose Category',
			'validators' => array('NotEmpty'),
			'multioptions' => $CategoryArray
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-sitemap JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_category->setDecorators($simpleElementDecorators);
		$this->job_category->setRegisterInArrayValidator(false);
		
		/*
		$SkillData = $model->Super_Get('skills',"skill_status='1'","fetchAll",$extra=array('order'=>'skill_title','fields'=>array('skill_id','skill_title')));
		$SkillArray = array();
		foreach($SkillData as $skills)
		{
			$SkillArray[$skills['skill_id']]=$skills['skill_title'];
		}
		*/
		
 		$this->addElement('select', 'job_skills', array(
			'class'      => 'form-control required chosen-select',
			'required'   => true,	
			"multiple"=>"multiple",
			'label'=>'&nbsp;&nbsp;Choose job skills',
			'validators' => array('NotEmpty')
			//,
			//'multiOptions' => $SkillArray
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag'=>'<span>','tagClass'=>'input-group-addon fa fa-sitemap JobAddon','escape'=>false)),
			array(array('row'=>'HtmlTag'),array('tag'=>'div','class'=>'input-group input-group-sm'))
		);
		$this->job_skills->setDecorators($simpleElementDecorators);
		$this->job_skills->setRegisterInArrayValidator(false);
		
		//$CountryData = $model->Super_Get('countries','1',"fetchAll",$extra=array('fields'=>array('country_id','country_name')));
		$CountryData = $model->Super_Get('countries','1',"fetchAll",$extra=array('fields'=>array('country_id','country_name'), 'order'=>'country_order'));
		$CountryArray = array(''=>'Choose country');
		foreach($CountryData as $countries)
		{
			$CountryArray[trim($countries['country_name'])]=trim($countries['country_name']);
		}
		$country = array_reverse(explode(',', $user_detail->ud_location));
		if(isset($country))
		{
			$country = trim($country[0]);
		}
		else
		{
			$country = '';
		}
		$this->addElement('select', 'job_country', array (
			'class' => 'form-control required',
			//'id' => 'job_country_id1',
			'value' => $country,
			"onchange"=>"enablezipcode(this.value);",
			"label" => "&nbsp;&nbsp;Choose country",
			'multiOptions' => $CountryArray,
			'validators' => array('NotEmpty'),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-align-justify JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_country->setDecorators($simpleElementDecorators);

		$this->addElement('text', 'job_location', array (
			'class' => 'form-control required digits',
			//"required"=>true,
			"autocomplete"=>false,
			"onkeyup"=>"getlatlog(this.value);",
			"label" => "&nbsp;&nbsp;Enter Postcode",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Postal Code is Required ")),
 								),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-align-justify JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_location->setDecorators($simpleElementDecorators);
		
		$BudgetArray = array('0'=>'Enter fixed amount','1'=>'Enter range');
		$this->addElement('radio', 'job_budget_type', array(
			'class'      => '',
			'validators' => array('NotEmpty'),
			'multiOptions' => $BudgetArray
		));
		
		$BudgetTypeArray = array('0'=>'Fixed amount','1'=>'Per hour');
		$this->addElement('select', 'job_fix_type', array(
			'class'      => 'form-control',
			'multiOptions' => $BudgetTypeArray,
			"readonly"=>"readonly",
		));
		$this->addElement('select', 'job_range_type', array(
			'class'      => 'form-control',
			'multiOptions' => $BudgetTypeArray,
			"readonly"=>"readonly",
		));
		$this->addElement('text', 'job_budget', array (
			'class' => 'form-control required number',
			/*"required"=>true,*/
			"label" => "&nbsp;",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"readonly"=>"readonly",
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Budget is Required ")),
 								),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-dollar JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_budget->setDecorators($simpleElementDecorators);
		
		$this->addElement('text', 'job_min_price', array (
			'class' => 'form-control required number',
			/*"required"=>true,*/
			"readonly"=>"readonly",
			"value"=>"0",
			"label" => "Min <i class='fa fa-dollar'></i>",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Min price is Required ")),
 								),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_min_price->setDecorators($simpleElementDecorators);

		$this->addElement('text', 'job_max_price', array (
			'class' => 'form-control required number',
			/*"required"=>true,*/
			"readonly"=>"readonly",
			"value"=>"0",
			"label" => "Max <i class='fa fa-dollar'></i>",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Max price is Required ")),
 								),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_max_price->setDecorators($simpleElementDecorators);
		
		/*$this->addElement('file', 'job_attachment', array (
							'class' => 'form-control required',
							"required"=>true,
							"label" => "&nbsp;&nbsp;Job Attachment",
		));
		$simpleElementDecorators1 = array(
		array('File'),
		array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-align-justify JobAddon', 'escape' => false)),
		array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
  		$this->job_attachment->setDestination(JOB_ATTACHMENTS);
		$this->job_attachment->setDecorators($simpleElementDecorators1);*/
		
		$this->addElement('text', 'job_closing_date', array (
			'class' => 'form-control required date-picker',
			"required"=>true,
			"label" => "&nbsp;&nbsp;Job Closing Date",
			"readonly"=>"readonly",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Closing Date is Required ")),
 								),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-align-justify JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_closing_date->setDecorators($simpleElementDecorators);
		$typeofworkAray = array('0'=>'Full Time','1'=>'Part Time','2'=>'Casual');
			$this->addElement('select', 'job_type_of_work', array (
			'class' => 'form-control required',
			//"required"=>true,
			"onchange"=>"enablezipcode(this.value);",
			"label" => "&nbsp;&nbsp;Choose Type of Work",
			'multiOptions' => $typeofworkAray,
			'validators' => array('NotEmpty'),
		));
		$simpleElementDecorators = array(
			array('ViewHelper'),
			array('Label',array('tag' => '<span>','tagClass' => 'input-group-addon fa fa-align-justify JobAddon', 'escape' => false)),
			array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-group input-group-sm'))
		);
		$this->job_type_of_work->setDecorators($simpleElementDecorators);
		
		$this->addElement('textarea', 'job_description', array (
			'class' => 'form-control ckeditor',
			"required"=>false,
			"style"=>"height:100px;",
			"label" => "Job Description",
		));
		
		$this->addElement('hidden', 'job_latitude', array ());
		
		$this->addElement('hidden', 'job_longitude', array ());
 		
		$this->addElement('button', 'submit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Save',
			'class'    => 'hire SiteButton'
		));
		$this->submit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 	}
	
	public function searchjobs()
	{
		global $user_detail;
		$this->addElement('text', 'job_budget', array (
			'class' => 'form-control',
			"label" => "&nbsp;",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Budget is Required ")),
 								),
		));
		
		$JobTypeArray = array(''=>'All Jobs','0'=>'Online Jobs','1'=>'Physical Jobs');
		$this->addElement('radio', 'job_type', array(
			'class'      => 'required',
			"required"=>true,
			'multiOptions' => $JobTypeArray,
			"validators" =>  array(
								array("NotEmpty",true,array("messages"=>"Job Type is Required ")),
 							),
		));
		
		$this->addElement('text', 'job_min_price', array (
			'class' => 'form-control',
			/*"required"=>true,*/
			"label" => "Min <i class='fa fa-dollar'></i>",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>"Budget is Required ")),
 							),
		));
		
		$this->addElement('text', 'job_max_price', array (
			'class' => 'form-control',
			/*"required"=>true,*/
			"label" => "Max <i class='fa fa-dollar'></i>",
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>"Budget is Required ")),
 							),
		));
		
		$BudgetTypeArray = array('0'=>'Fixed amount','1'=>'Per hour');
		$this->addElement('radio', 'job_fix_type', array(
			'class'      => '',
			'value'=>'0',
			'multiOptions' => $BudgetTypeArray,
		));
		
		$model = new Application_Model_Static();
		
		/*
		$SkillData = $model->Super_Get('skills',"skill_status='1'","fetchAll",$extra=array('order'=>'skill_title','fields'=>array('skill_id','skill_title')));
		$SkillArray = array();
		foreach($SkillData as $skills)
		{
			$SkillArray[$skills['skill_id']]=$skills['skill_title'];
		}
		*/
		
 		$this->addElement('select', 'job_skills', array(
			'class'      => 'form-control chosen-select',
			"multiple"=>"multiple",
			'label'=>'Choose job skills'
			//,
			//'multiOptions' => $SkillArray
		));
		$this->job_skills->setRegisterInArrayValidator(false);
		
		$model = new Application_Model_Static();
		$CategoryData = $model->Super_Get('categories','1',"fetchAll", $extra=array('order'=>'category_title','fields'=>array('category_id','category_title')));
		$CategoryArray = array(''=>'Choose category');
		foreach($CategoryData as $categories)
		{
			$CategoryArray[$categories['category_id']]=$categories['category_title'];
		}
		
		
 		$this->addElement('select', 'job_category', array(
			'class'      => 'form-control chosen-select',
			'label'=>'Choose job category',
			'multiOptions' => $CategoryArray
		));
		$this->job_category->setRegisterInArrayValidator(false);
		
		//$CountryData = $model->Super_Get('countries','1',"fetchAll",$extra=array('fields'=>array('country_id','country_name')));
		$CountryData = $model->Super_Get('countries','1',"fetchAll",$extra=array('fields'=>array('country_id','country_name'), 'order'=>'country_order'));
		$CountryArray = array(''=>'Choose country');
		foreach($CountryData as $countries)
		{
			$CountryArray[trim($countries['country_name'])]=trim($countries['country_name']);
		}
		if(property_exists($user_detail,'ud_location')){
			$country = array_reverse(explode(',', $user_detail->ud_location));
		}

		if(isset($country))
		{
			$country = trim($country[0]);
		}
		else
		{
			$country = '';
		}
		$this->addElement('select', 'job_country', array (
			'class' => 'form-control',
			//'id' => 'job_country_id',
			'value' => $country,
			//"onchange"=>"enablezipcode1(this.value);",
			"label" => "Choose country",
			'multiOptions' => $CountryArray,
			'validators' => array('NotEmpty'),
		));
		
		$AreaArray = array('5'=>'5 Km','10'=>'10 Km','15'=>'15 Km','25'=>'25 Km','50'=>'50 Km','100'=>'100 Km','250'=>'250 Km','500'=>'500 Km','800'=>'800 Km','1'=>'Everywhere');
		$this->addElement('select', 'job_area', array (
			'class' => 'form-control',
			"label" => "Show jobs within",
			"value"=>"50",
			'multiOptions' => $AreaArray,
		));
		
		
		
		$typeofworkAray = array('0'=>'Full Time','1'=>'Part Time','2'=>'Casual');
			$this->addElement('select', 'job_type_of_work', array (
			'class' => 'form-control required',
			//"required"=>true,
			"onchange"=>"enablezipcode(this.value);",
			"label" => "&nbsp;&nbsp;Choose Type of Work",
			'multiOptions' => $typeofworkAray,
			'validators' => array('NotEmpty'),
		));
		
		
		
		
		
		$this->addElement('text', 'job_location', array (
			'class' => 'form-control digits',
			"autocomplete"=>false,
			"onchange"=>"getsearchlatlong(this.value,'0');",
			"label" => "Enter Postcode",
			"filters" => array("StringTrim","StripTags","HtmlEntities"),
		));
		
		$this->addElement('text', 'job_closing_date', array (
			'class' => 'form-control date-picker',
			"label" => "Jobs needing completion by",
			'id' => 'jcd',
			/*"readonly"=>"readonly",*/
			"filters"    => array("StringTrim","StripTags","HtmlEntities"),
			"validators" => array(
								array("NotEmpty",true,array("messages"=>"Location is Required ")),
 							),
		));
		
		$this->addElement('hidden', 'job_latitude', array (
		));
		
		$this->addElement('hidden', 'job_longitude', array (
		));
 		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'button',
			'label'    => 'Get Results',
			"onclick"=>"CheckLatLong();",
			'class'    => 'hire SiteButton'
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	public function searchserviceproviders()
	{	
		$this->addElement('text', 'keyword', array (
			'class' => 'form-control',
			"label" => "Enter Keyword",
			"filters" => array("StringTrim","StripTags","HtmlEntities"),
		));
		
		$model = new Application_Model_Static();
		$SkillData = $model->Super_Get('skills','1',"fetchAll",$extra=array('order'=>'skill_title','fields'=>array('skill_id','skill_title')));
		$SkillArray = array();
		foreach($SkillData as $skills)
		{
			$SkillArray[$skills['skill_id']]=$skills['skill_title'];
		}
		
		$user_type=  array("service_provider"=>"Service Provider","employer"=>"Employer");		
 		$this->addElement('select', 'skills', array(
			'class'      => 'form-control chosen-select',
			"multiple"=>"multiple",
			'label'=>'Choose skills',
			'multiOptions' => $SkillArray
		));
		$this->skills->setRegisterInArrayValidator(false);
		
		
		
		$this->addElement('text', 'location', array (
			'class' => 'form-control',
			"autocomplete"=>false,
			"label" => "Location",
			"filters" => array("StringTrim","StripTags","HtmlEntities"),
		));
 		
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Get Results',
			'class'    => 'hire SiteButton'
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
	}
	
	public function rate()
	{
		$rate=array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"");
		$this->addElement('radio', 'review_star', array(
			"class"      => "required star",
			"required"   => true,
			"label"   => "Rate :",
			"value"=>'1',
			"multioptions"=>$rate,
			"validators" => array(
								array("NotEmpty",true,array("messages"=>"Rating is required ")),
							),
		 ));
		 
	}
}