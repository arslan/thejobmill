<?php
class Application_Form_Message extends Twitter_Bootstrap_Form_Vertical
{
	
	public function init(){
		global $user_info;
  		$this->setMethod('post');
		$this->setAttribs(array(
 			'class' => 'profile_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			'enctype'=>'multipart/form-data'
		));
		
		$logged_identity = Zend_Auth::getInstance()->getInstance();
		if($logged_identity->hasIdentity()){
			$model = new Application_Model_User();
			$logged_identity = $logged_identity->getIdentity();
			$user_info = (object) $model->get($logged_identity->user_id);
		}
  	}
	
	

	public function compose($draft_id=false){
		
		
		$profile_user_id = Zend_Controller_Front::getInstance()->getRequest()->getParam('profile_user_id');
		$profile_user_id_string = "";
		
		if ($profile_user_id != ""){
			$profile_user_id_string = "?profile_user_id=" . $profile_user_id;
		}
		
		$this->setAttribs(array(
 			'class' => 'profile_form',
 			'novalidate'=>'novalidate',
			"role"=>"form",
			//"action"=>SITE_HTTP_URL."/message/compose/DraftId/".$draft_id . $profile_user_id_string,
			"action"=>SITE_HTTP_URL."/message/compose/DraftId/".$draft_id,
			'enctype'=>'multipart/form-data'
		));
		global $user_info;
		$model = new Application_Model_Static();
		
		//$user_id = $this->getRequest()->getParam('user_id');
		//$profile_user_id = $this->getRequest()->getParam('profile_user_id');
		
		
		
		
		$UserProfileData = $model->Super_Get('users',"user_type='user' and user_id = '".$profile_user_id."'","fetchAll",$extra=array('fields'=>array('user_id','CONCAT(user_first_name," ",user_last_name) as user_name','user_first_name', 'user_last_name')));
		
		
		/*
		$this->addElement('text', 'user_id', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'User ID :',
			'value'=>$UserProfileData[0],
			'validators' => array('NotEmpty'),
		));
		*/
		
		
		
		
		/* Getting all hirers of logged in user */
		$joinArr=array('0' => array('0'=>'job','1'=>'job_app_job_id = job_id','2'=>'left','3'=>array()));
		$UsersData1 = $model->Super_Get('job_applications',"job_app_user_id='".$user_info->user_id."'","fetch",$extra=array('fields'=>array('ids'=>new Zend_Db_Expr('GROUP_CONCAT(DISTINCT(job_user_id))'))),$joinArr);
		
		/* Getting all workers of logged in user */
		$joinArr=array('0' => array('0'=>'job','1'=>'job_app_job_id = job_id','2'=>'left','3'=>array()));
		$UsersData2 = $model->Super_Get('job_applications',"job_user_id='".$user_info->user_id."'","fetch",$extra=array('fields'=>array('ids'=>new Zend_Db_Expr('GROUP_CONCAT(DISTINCT(job_app_user_id))'))),$joinArr);
		
		if(!empty($UsersData1['ids'])){
			$userIds = $UsersData1['ids'].','.$UsersData2['ids'];
		}else{
			$userIds = $UsersData2['ids'];
		}
		$userIds = chop($userIds,",");

		if(!empty($userIds)){
			$UsersData = $model->Super_Get('users',"user_type='user' and user_id IN (".$userIds.")","fetchAll",$extra=array('fields'=>array('user_id','CONCAT(user_first_name," ",user_last_name) as user_name','user_first_name','user_last_name')));
			//$UsersData = $model->Super_Get('users',"user_type='user'","fetchAll",$extra=array('fields'=>array('user_id','CONCAT(user_first_name," ",user_last_name) as user_name','user_first_name')));
		}else{
			//$UsersData = $model->Super_Get('users',"user_type='user'","fetchAll",$extra=array('fields'=>array('user_id','CONCAT(user_first_name," ",user_last_name) as user_name','user_first_name')));
			$UsersData = array();
		}
		
		
		
		if(!empty($UserProfileData)){
			
			$UsersArray = array();
			
			foreach($UserProfileData as $users)
			{
				if(!empty($users['user_name'])){
					$UsersArray[$users['user_id']]=ucfirst($users['user_name']);
				}else{
					//$UsersArray[$users['user_id']]=ucfirst($users['user_first_name']);
					$UsersArray[$users['user_id']]=ucfirst($users['user_first_name']) . " " . ucfirst($users['user_last_name']);
				}
			}
		//}else if(!empty($UsersData)){
	      }else{
			
			$UsersArray = array(''=>'Select User');
			
			foreach($UsersData as $users)
			{
				if(!empty($users['user_name'])){
					$UsersArray[$users['user_id']]=ucfirst($users['user_name']);
				}else{
					//$UsersArray[$users['user_id']]=ucfirst($users['user_first_name']);
					$UsersArray[$users['user_id']]=$users['user_first_name'] . " " . $users['user_last_name'];
				}
			}
		}
		
		
		
			$this->addElement('select', 'msg_to', array(
				'class'      => 'form-control required chosen-select',
				'required'   => true,	
				'label'=>'To :',
				'validators' => array('NotEmpty'),
				'multiOptions' => $UsersArray
			));
			$this->msg_to->setRegisterInArrayValidator(false);

		
		//$JobData = $model->Super_Get('job',"job_status='1' and job_user_id='".$user_info->user_id."'","fetchAll",$extra=array('fields'=>array('job_id','job_title')));
		//$JobData = $model->Super_Get('job',"job_status='1' and (job_user_id='".$user_info->user_id."' or job_user_id IN (".$userIds."))","fetchAll",$extra=array('fields'=>array('job_id','job_title')));
		
		/*
		$joinArr=array(
			'0' => array('0'=>'job_applications','1'=>'job_app_user_id IN (' . $userIds . ")",'2'=>'left','3'=>array('job_app_user_id')),
		);
		*/
		
		if(!empty($UserProfileData)){
		
			/*
		
			$JobArray = array('1'=>'No Job');
			$this->addElement('select', 'msg_job', array(
				'class'      => 'form-control hidden-element',
				'required'   => false,	
				'validators' => array('NotEmpty'),
				'multiOptions' => $JobArray
			));
			$this->msg_job->setRegisterInArrayValidator(false);
			
			*/
			
			$this->addElement('text', 'msg_job', array(
				'class'      => 'form-control required hidden-element',
				'required'   => true,
				'value'=>'1'
			));			
			
		
		}else{
			
			$JobData = $model->Super_Get('job',"job_status='1' and job_user_id='".$user_info->user_id."'","fetchAll",$extra=array('fields'=>array('job_id','job_title')));
			
			$JobArray = array(''=>'Select Job');
			
			/*
			foreach($JobData as $job)
			{
				$JobArray[$job['job_id']]=$job['job_title'];
			}
			*/
			
			$this->addElement('select', 'msg_job', array(
				'class'      => 'form-control',
				'required'   => false,	
				'label'=>'Select Job :',
				'validators' => array('NotEmpty'),
				'multiOptions' => $JobArray
			));
			$this->msg_job->setRegisterInArrayValidator(false);
			
		}
		
		$this->addElement('text', 'msg_subject', array(
			'class'      => 'form-control required',
			'required'   => true,	
			'label'=>'Subject :',
			'validators' => array('NotEmpty'),
		));
		
		$this->addElement('textarea', 'msg_text', array(
			'class'      => 'form-control required',
			'required'   => true,	
			"style"=>"height:100px;",
			'label'=>'Message :',
			'validators' => array('NotEmpty'),
		));
		
		$this->addElement('file', 'msg_file', array (
			'class'=> '',
			"label" => "Attachment",
		));
		$this->msg_file->setDestination(MESSAGES_PATH)
			->addValidator('Extension', false,"txt,xlsx,csv,doc,docx,pdf,TXT,XLSX,CSV,DOC,DOCX,PDF")
			->addValidator('Size', false, MESSAGE_VALID_SIZE);
		 
		$this->addElement('button', 'btnsubmit', array(
			'ignore'   => true,
			'type'=>'submit',
			'label'    => 'Send',
			'class'    => 'hire SiteButton'
		));
		$this->btnsubmit->removeDecorator('label')->removeDecorator('HtmlTag')->removeDecorator('Wrapper');
 	}
}