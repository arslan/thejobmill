s<?php
class Zend_View_Helper_Nav extends Zend_Navigation_Container
{
 	public function __construct(){
		/*$this->setContainer($this->getNavArray());*/
	}

	public function getNavArray(){
		
		$auth   = Zend_Auth::getInstance();	
		$userData=$auth->getIdentity();
		$getvariable = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		
		if(isset($getvariable['blog_cat_id'])){
			$blog_cat_id = $getvariable['blog_cat_id'];
		}else{
			$blog_cat_id = '';
		}
		
		if(isset($getvariable['id'])){
			$blog_id = $getvariable['id'];
		}else{
			$blog_id = '';
		}
		
		if($userData->user_role=='2')	
		{
			$pages = array (		
				array(
					'label' => 'Dashboard',
					'icon' =>'icon-home',
					'module' => 'admin',
					'controller' => 'index',
					'action' => 'index',
					'privilege' => 'index',
					'route'=>'default',
				),
				
				/* Admin Navigation - Manage Profile */
				array(
					'label' => 'Manage Profile',
					'icon' =>'icon-user',
					'uri' => 'javascript:void(0)',
					'pages' =>array(
						array(
							'label'=>'Update Profile',
							'icon' =>'fa fa-edit',
							'route'=>'update_profile_admin'
						),
						array(
							'label'=>'Profile Image',
							'icon' =>'fa fa-edit',
							'route'=>'update_image_admin'
						),
						array(
							'label'=>'Change Password',
							'icon' =>'fa fa-edit',
							'route'=>'update_password_admin'
						),
					)
					),
					
					/*array(
					'label'=>'Manage Blog Categories',
					'icon' =>'fa fa-sitemap',
					'route'=>'admin_blog_category',
					'pages' =>array(
						array(
							'icon' => 'fa fa-sitemap ',
							'label' => 'Blog Categories',
							'route'=>'admin_blog_category',
							'pages' =>array(
								array(
									'label'=>'Category',
									'icon' =>'fa fa-sitemap',
									'route'=>'admin_blog_category',
								),
								array(
									'label'=>'Add Category',
									'icon' =>'fa fa-sitemap',
									'route'=>'admin_add_blog_category',
								),
								array(
									'label'=>'Add Category',
									'icon' =>'fa fa-sitemap',
									'route'=>'admin_add_blog_category',
									'params'=>array('blog_cat_id'=>$blog_cat_id),
								),
							)
						),
					)
				),*/
				/* (END) Blog Category Management */
				
				/* Admin Navigation - Blog Management */
				/*array(
					'label'=>'Manage Blogs',
					'icon' =>'fa fa-certificate fa-spin ',
					'route'=>'admin_blog',
					'pages' =>array(
						array(
							'icon' => 'fa fa-certificate fa-spin',
							'label' => 'All Blogs',
							'route'=>'admin_blog',
							'pages' =>array(
								array(
									'label'=>'All Blogs',
									'icon' =>'fa fa-certificate',
									'route'=>'admin_blog',
								),
								array(
									'label'=>'Add Blogs',
									'icon' =>'fa fa-certificate',
									'route'=>'admin_add_blog',
								),
								array(
									'label'=>'Add Blogs',
									'icon' =>'fa fa-certificate',
									'route'=>'admin_view_blog',
									'params'=>array('id'=>$blog_id),
								),
								array(
									'label'=>'Blog Comments',
									'icon' =>'fa fa-certificate',
									'module'=>'admin',
									'controller'=>'blog',
									'action'=>'blogcomments',
								),
							)
						),
					)
				),*/
				
				array(
					'label'=>'Manage Forum Topics',
					'icon' =>'fa fa-comment',
					'route'=>'admin_forum_topic',
					'pages' =>array(
						array(
							'icon' => 'fa fa-comment',
							'label' => 'Manage Forum Topics',
							'route'=>'admin_forum_topic',
							'pages' =>array(
								array(
									'route'=>'admin',
									'controller'=>'forum',
									'action'=>'threads',
								),
								array(
									'route'=>'admin',
									'controller'=>'forum',
									'action'=>'comments',
								),
							)
						),
						array(
							'label'=>'User Topic Requests',
							'icon' =>'fa fa-warning',
							'route'=>'admin_forum_users',
						),
						array(
							'label'=>'Reported Posts',
							'icon' =>'fa fa-warning',
							'route'=>'admin_forum_reports',
						),
						array(
							'label'=>"Posts for Improvement",
							'icon' =>'fa fa-warning',
							'route'=>'admin_forum_improvement',
						),
					)
				),
				);
		}
		else{
  			$pages = array (
			/* Dashboard */
			array(
				'label' => 'Dashboard',
				'icon' =>'icon-home',
				'module' => 'admin',
				'controller' => 'index',
 				'action' => 'index',
				'privilege' => 'index',
				'route'=>'default',
     		),
			
			/* Admin Navigation - Manage Profile */
			array(
				'label' => 'Manage Profile',
				'icon' =>'icon-user',
				'uri' => 'javascript:void(0)',
 				'pages' =>array(
					array(
						'label'=>'Update Profile',
						'icon' =>'fa fa-edit',
						'route'=>'update_profile_admin'
  					),
					array(
						'label'=>'Profile Image',
						'icon' =>'fa fa-edit',
						'route'=>'update_image_admin'
  					),
					array(
						'label'=>'Change Password',
						'icon' =>'fa fa-edit',
						'route'=>'update_password_admin'
  					),
   				)
     		),
			/* (END) Manage Profile */
			 
			/* Admin Navigation-Site Setting */ 
			array(
				'label' => 'Site Configurations',
				'icon' =>'icon-settings fa-spin',
				'uri' => 'javascript:void(0)',
 				'pages' =>array(
					array(
						'label'=>'Site Configuration',
						'icon' =>'fa icon-settings',
 						'route'=>'admin_site_configs',
 					),
   				)
     		),
			
			array(
				'label' => 'Manage Sub Admin',
				'icon' =>'fa fa-group',
				'uri' => 'javascript:void(0)',
 				'pages' =>array(
					array(
						'label' => 'Manage Sub Admin',
						'icon' =>'fa fa-group',
						'route'=>'manage_subuser',
						'pages'=>array(
 							array(
								'label'=>'Add Sub Admin',
								'icon' =>'fa fa-group',
								'route'=>'add_subuser',
							),
							array(
								'label'=>'Edit Sub Admin',
								'icon' =>'fa fa-group',
								'route'=>'edit_subuser',
							),
						)
  					),
   				)
     		),
			
			/* (END) Site Setting */
			 
			/* Admin Navigation - Slider Images */ 
			/*array(
				'label' => 'Slider Images',
				'icon' =>'fa fa-file-image-o',
				'uri' => 'javascript:void(0)',
				'route'=>'default',
 				'pages' =>array(
					array(
						'label'=>'Slider Images',
						'icon' =>'fa fa-file-image-o',
						'module'=>'admin',
						'controller'=>'slider',
						'action'=>'index',
						'route'=>'default',
						'pages'=>array(
 							array(
								'label'=>'Slider Images',
								'icon' =>'fa fa-file-image-o',
								'module'=>'admin',
								'controller'=>'slider',
								'action'=>'index',
								'route'=>'default',
							),
							array(
								'label'=>'Add Slider Image ',
								'icon' =>'icon-edit',
								'module'=>'admin',
								'controller'=>'slider',
								'action'=>'add',
								'route'=>'default',
							),
							array(
								'label'=>'Edit Static Pages',
								'icon' =>'icon-edit',
								'module'=>'admin',
								'controller'=>'slider',
								'action'=>'edit',
								'route'=>'default',
							),
						)
 					),
   				)
     		),*/
			/* (END) Site Setting */
			
			/* Manage Background images -Starts */
			array(
				'label' => 'Manage Images',
				'icon' =>'fa fa-file-image-o',
				'uri' => 'javascript:void(0)',
				'route'=>'default',
 				'pages' =>array(
					array(
						'label'=>'Manage Images',
						'icon' =>'fa fa-file-image-o',
						'module'=>'admin',
						'controller'=>'static',
						'action'=>'addimages',
						'route'=>'default',
						'pages'=>array(
 							array(
								'label'=>'Add Background Images',
								'icon' =>'fa fa-file-image-o',
								'module'=>'admin',
								'controller'=>'static',
								'action'=>'addimages',
								'route'=>'default',
							),
							array(
								'label'=>'Add Background Images',
								'icon' =>'fa fa-file-image-o',
								'module'=>'admin',
								'controller'=>'static',
								'action'=>'addimages',
								'route'=>'default',
							),
						)
 					),
   				)
     		),
			/* Manage Background images -Ends */
			
			/* Admin Navigation - Static Content */ 
 			array(
				'label' => 'Static Content',
				'icon' =>'fa fa-file-text',
 				'uri' => 'javascript:void(0)',
  				'pages' =>array(
					array(
						'label'=>'Manage Pages',
						'icon' =>'fa fa-file-text-o',
 						'route'=>'admin_static_pages',
						'pages' =>array(
							array(
								'label'=>'Static Pages',
								'icon' =>'icon-paste',
 								'route'=>'admin_static_pages',
							),
							array(
								'label'=>'Edit Static Pages',
								'icon' =>'icon-edit',
 								'module' => 'admin',
								'controller' => 'static',
								'action' => 'edit',
							),
							array(
								'label'=>'Edit Static Pages',
								'icon' =>'icon-edit',
 								'module' => 'admin',
								'controller' => 'static',
								'action' => 'add',
							),
							 array(
									'label'=>'View Page Info',
									'icon' =>'icon-edit',
									'module' => 'admin',
									'controller' => 'static',
									'action' => 'viewpage',
								),
						)
   					),
					array(
						'label'=>'Content Blocks',
						'icon' =>'fa fa-copy',
 						'route'=>'admin_content_block',
						'pages' =>array(
							array(
								'label'=>'Content Blocks',
								'icon' =>'icon-paste',
 								'route'=>'admin_content_block',
							),
							array(
								'label'=>'Add Content Block',
								'icon' =>'icon-edit',
 								'module' => 'admin',
								'controller' => 'static',
								'action' => 'addblock',
							),
							array(
								'label'=>'Edit Content Block',
								'icon' =>'icon-edit',
 								'module' => 'admin',
								'controller' => 'static',
								'action' => 'editcontentblock',
							),
							array(
								'label'=>'View Content Block',
								'icon' =>'icon-eye-open',
 								'module' => 'admin',
								'controller' => 'static',
								'action' => 'viewblock',
							),
						)
   					),
					/*array(
						'label' => 'Graphic Media',
						'icon' =>'icon-picture',
						'route'=>'admin_graphic_media',
						'pages' =>array(
							array(
								'label' => 'Graphic Media',
								'icon' =>'icon-camera',
 								'route'=>'admin_graphic_media',
							),
							array(
								'icon' =>'icon-edit',
								'label' => 'Edit Graphic Media',
								'module' => 'admin',
								'controller' => 'static',
								'action' => 'editgraphicmedia',
						),
							array(
								'icon' =>'fa fa-plus',
								'label' => 'Add New  Graphic Media',
 								'route'=>'admin_add_graphic_media'
							),
						)
					),*/
					
					array(
						'label'=>'Email Templates',
						'icon' =>'icon-envelope-letter',
						'route'=>'admin_email_templates',
						'pages' =>array(
							array(
								'icon' =>'icon-envelope-alt',
								'label' => 'Email Templates',
 								'module' => 'admin',
								'controller' => 'email',
								'action' => 'index',
							),
							array(
								'icon' =>'icon-edit',
 								'label' => 'Edit Template ',
								'module' => 'admin',
								'controller' => 'static',
								'action' => 'editmailtemplate',
							),
 						)
  					),
   				)
     		),
  			/* (END) Site Setting */
 			
			/* Admin Navigation - Expertise Management */
			array(
				'label'=>'Manage Categories',
				'icon' =>'fa fa-sitemap ',
				'route'=>'admin_expertise',
				'pages' =>array(
					array(
						'icon' => 'fa fa-sitemap ',
						'label' => 'Categories',
						'route'=>'admin_expertise',
						'pages' =>array(
							array(
								'label'=>'Categories',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_expertise',
							),
							array(
								'label'=>'Categories',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_add_expertise',
							),
  						)
					),
				)
  			),
			/* (END) Expertise Management */
			
			/* Admin Navigation - Skill Management */
			array(
				'label'=>'Manage Skills',
				'icon' =>'fa fa-sitemap ',
				'route'=>'admin_skill',
				'pages' =>array(
					array(
						'icon' => 'fa fa-sitemap ',
						'label' => 'Skills',
						'route'=>'admin_skill',
						'pages' =>array(
							array(
								'label'=>'Skills',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_skill',
							),
  						)
					),
					array(
						'icon' => 'fa fa-warning ',
						'label' => 'Pending Skills',
						'route'=>'admin_pending_skill',
					),
				)
  			),
			/* (END) Skill Management */
			
			/* Admin Navigation - Job Management */
			array(
				'label'=>'Manage Jobs',
				'icon' =>'fa fa-gear fa-spin ',
				'route'=>'admin_jobs',
				'pages' =>array(
					array(
						'icon' => 'fa fa-gear fa-spin',
						'label' => 'All Jobs',
						'route'=>'admin_jobs',
						'pages' =>array(
							array(
								'label'=>'All Jobs',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_jobs',
							),
							array(
								'label'=>'All Jobs',
								'icon' =>'fa fa-sitemap',
								'module' => 'admin',
								'controller' => 'job',
								'action' => 'jobdetails',
								'route'=>'default',
							),
							array(
								'label'=>'All Job Applications',
								'icon' =>'fa fa-sitemap',
								'module' => 'admin',
								'controller' => 'job',
								'action' => 'jobapplications',
								'route'=>'default',
							),
  						)
					),
					array(
						'icon' => 'fa fa-gear fa-spin',
						'label' => 'Sponsored Jobs',
						'route'=>'admin_sponsored_jobs',
						'pages' =>array(
							array(
								'label'=>'Sponsored Jobs',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_sponsored_jobs',
							),
							array(
								'label'=>'Sponsored Jobs',
								'icon' =>'fa fa-sitemap',
								'module' => 'admin',
								'controller' => 'job',
								'action' => 'sponsoredjobdetails',
								'route'=>'default',
							),

						)
					),
				)
  			),
			/* (END) Job Management */
			
			/* Admin Navigation - Blog Category Management */
		/*	array(
				'label'=>'Manage Blog Categories',
				'icon' =>'fa fa-sitemap',
				'route'=>'admin_blog_category',
				'pages' =>array(
					array(
						'icon' => 'fa fa-sitemap ',
						'label' => 'Blog Categories',
						'route'=>'admin_blog_category',
						'pages' =>array(
							array(
								'label'=>'Category',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_blog_category',
							),
							array(
								'label'=>'Add Category',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_add_blog_category',
							),
							array(
								'label'=>'Add Category',
								'icon' =>'fa fa-sitemap',
								'route'=>'admin_add_blog_category',
								'params'=>array('blog_cat_id'=>$blog_cat_id),
							),
  						)
					),
				)
  			),*/
			/* (END) Blog Category Management */
			
			/* Admin Navigation - Blog Management */
			/*array(
				'label'=>'Manage Blogs',
				'icon' =>'fa fa-certificate fa-spin ',
				'route'=>'admin_blog',
				'pages' =>array(
					array(
						'icon' => 'fa fa-certificate fa-spin',
						'label' => 'All Blogs',
						'route'=>'admin_blog',
						'pages' =>array(
							array(
								'label'=>'All Blogs',
								'icon' =>'fa fa-certificate',
								'route'=>'admin_blog',
							),
							array(
								'label'=>'Add Blogs',
								'icon' =>'fa fa-certificate',
								'route'=>'admin_add_blog',
							),
							array(
								'label'=>'Add Blogs',
								'icon' =>'fa fa-certificate',
								'route'=>'admin_view_blog',
								'params'=>array('id'=>$blog_id),
							),
							array(
								'label'=>'Blog Comments',
								'icon' =>'fa fa-certificate',
								'module'=>'admin',
								'controller'=>'blog',
								'action'=>'blogcomments',
							),
  						)
					),
				)
  			), */
			
			/* (END) Blog Management */

			/* Admin Navigation - Subscription Management */
			array(
				'label'=>'Add Ons Management',
				'icon' =>'fa fa-money',
				'uri' => 'javascript:void(0)',
				'pages' =>array(
					array(
						'icon' => 'fa fa-money',
						'label' => 'Add Ons',
						'module' => 'admin',
						'controller' => 'user',
						'action' => 'subscription',
						'route'=>'default',
						'pages' =>array(
							array(
								'label'=>'Subscriptions',
								'icon' =>'fa fa-money',
 								'module' => 'admin',
								'controller' => 'user',
								'action' => 'subscription',
								'route'=>'default',
							),
  						)
					),
				)
  			),
			/* (END) Voucher Management */

				/* Admin Navigation - Subscription Management */
				array(
					'label'=>'Manage Voucher',
					'icon' =>'fa fa-barcode',
					'uri' => 'javascript:void(0)',
					'pages' =>array(
						array(
							'icon' => 'fa fa-barcode',
							'label' => 'Vouchers',
							'module' => 'admin',
							'controller' => 'user',
							'action' => 'voucher',
							'route'=>'default',
							'pages' =>array(
								array(
									'label'=>'Vouchers',
									'icon' =>'fa fa-barcode',
									'module' => 'admin',
									'controller' => 'user',
									'action' => 'voucher',
									'route'=>'default',
								),
							)
						),
					)
				),
				/* (END) Subscription Management */
				/* Admin Navigation - Subscription Management */
				array(
					'label'=>'Transaction Management',
					'icon' =>'fa fa-money',
					'uri' => 'javascript:void(0)',
					'pages' =>array(
						array(
							'icon' => 'fa fa-money',
							'label' => 'Transaction History',
							'module' => 'admin',
							'controller' => 'user',
							'action' => 'transaction',
							'route'=>'default',
							'pages' =>array(
								array(
									'label'=>'Subscriptions',
									'icon' =>'fa fa-money',
									'module' => 'admin',
									'controller' => 'user',
									'action' => 'transaction',
									'route'=>'default',
								),
								array(
									'label'=>'All Users',
									'icon' =>'icon-user',
									'module' => 'admin',
									'controller' => 'user',
									'action' => 'viewtransaction',
								),
							)
						),
					)
				),
 			/* Admin Navigation - User Management */
			array(
				'label' => 'User Management',
				'icon' =>'fa fa-users',
				'uri' => 'javascript:void(0)',
 				'pages' =>array(
					array(
						'label'=>'All Users',
						'icon' =>'fa  fa-users',
 						'module' =>'admin',
						'controller' =>'user',
						'action' =>'index',
						'route'=>'default',
						'pages' =>array(
							array(
								'label'=>'All Users',
								'icon' =>'icon-user',
 								'module' => 'admin',
								'controller' => 'user',
								'action' => 'index',
							),
							array(
								'label'=>'User Information ',
								'icon' =>'icon-zoom-in',
 								'module' => 'admin',
								'controller' => 'user',
								'action' => 'account',
							),
							array(
								'label'=>'User Information ',
								'icon' =>'icon-zoom-in',
 								'module' => 'admin',
								'controller' => 'user',
								'action' => 'socialdetails',
							),
							array(
								'label'=>'User Image ',
								'icon' =>'icon-zoom-in',
 								'module' => 'admin',
								'controller' => 'user',
								'action' => 'image',
							),
							array(
								'label'=>'Reset User Password ',
								'icon' =>'icon-zoom-in',
 								'module' => 'admin',
								'controller' => 'user',
								'action' => 'password',
							),
  						)
 					),
					array(
						'label'=>'Verified User',
						'icon' =>'fa fa-check-circle',
 						'module' =>'admin',
						'controller' =>'user',
						'action' =>'verified',
						'route'=>'default',
					 
 					),
					array(
						'label'=>'Blocked Users',
						'icon' =>'fa fa-warning ',
 						'module' =>'admin',
						'controller' =>'user',
						'action' =>'blocked',
						'route'=>'default',
  					),
   				)
     		),
			
				array(
				'label'=>'Manage Forum Topics',
				'icon' =>'fa fa-comment',
				'route'=>'admin_forum_topic',
				'pages' =>array(
					array(
						'icon' => 'fa fa-comment',
						'label' => 'Manage Forum Topics',
						'route'=>'admin_forum_topic',
						'pages' =>array(
							array(
								'route'=>'admin',
								'controller'=>'forum',
								'action'=>'threads',
							),
							array(
								'route'=>'admin',
								'controller'=>'forum',
								'action'=>'comments',
							),
						)
						
					),
					array(
						'label'=>'User Topic Requests',
						'icon' =>'fa fa-warning',
						'route'=>'admin_forum_users',
					),
					array(
						'label'=>'Reported Posts',
						'icon' =>'fa fa-warning',
						'route'=>'admin_forum_reports',
					),
					array(
						'label'=>"Posts for Improvement",
						'icon' =>'fa fa-warning',
						'route'=>'admin_forum_improvement',
					),
				)
  			),
			
			
			/* (END) User Management */
		);
		}
		return $pages;
	}
}
?>