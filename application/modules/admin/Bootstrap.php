<?php
class Admin_Bootstrap extends Zend_Application_Module_Bootstrap	
{
   
 	function _initApplication(){ 
	 
	}

	protected function _initNavigation() {
		// make sure the layout is loaded
		$this->bootstrap('layout');
		// get the view of the layout
		$layout = $this->getResource('layout');		
		$view = $layout->getView();
		//load the navigation xml
		$config = new Zend_Config_Xml(ROOT_PATH.'/private/navigation.xml','nav');
		// pass the navigation xml to the zend_navigation component
		$nav = new Zend_Navigation($config);
		// pass the zend_navigation component to the view of the layout 
		$view->navigation($nav);
	}
    /**
     * return the default bootstrap of the app
     * @return Zend_Application_Bootstrap_Bootstrap
     */
    protected function _getBootstrap()
    {
        $frontController = Zend_Controller_Front::getInstance();
        $bootstrap =  $frontController->getParam('bootstrap');	//deb($bootstrap);
        return $bootstrap;
    }
	
	public function _initSession(){
		Zend_Session::start();
		global $mySession;
		$mySession = new Zend_Session_Namespace('admin');
	}
 
	public function _initRouter()
	{
		$this->FrontController = Zend_Controller_Front::getInstance();
		$this->router = $this->FrontController->getRouter();  
		$this->appRoutes=array();
 	}

	protected  function _initSiteRouters(){	
		$this->appRoutes['admin_dashboard']= new Zend_Controller_Router_Route('/admin',array('module'=>'admin','controller'=>'index','action'=>'index'));
		$this->appRoutes['admin_profile']= new Zend_Controller_Router_Route('/admin/profile',array('module'=>'admin','controller'=>'profile','action'=>'index'));
		$this->appRoutes['admin_logout']= new Zend_Controller_Router_Route('/admin/logout',array('module'=>'admin','controller'=>'index','action'=>'logout'));
 		$this->appRoutes['admin_site_configs']= new Zend_Controller_Router_Route('/admin/site-configurations',array('module'=>'admin','controller'=>'static','action'=>'siteconfigs'));	
		$this->appRoutes['admin_email_templates']= new Zend_Controller_Router_Route('/admin/email-templates',array('module'=>'admin','controller'=>'static','action'=>'showmailtemplates'));	
		$this->appRoutes['admin_static_pages']= new Zend_Controller_Router_Route('/admin/static-pages',array('module'=>'admin','controller'=>'static','action'=>'index'));	
		$this->appRoutes['admin_content_block']= new Zend_Controller_Router_Route('/admin/content-blocks',array('module'=>'admin','controller'=>'static','action'=>'contentblock'));	
		
		$this->appRoutes['admin_expertise']= new Zend_Controller_Router_Route('/admin/expertise',array('module'=>'admin','controller'=>'static','action'=>'expertise'));
		$this->appRoutes['admin_add_expertise']= new Zend_Controller_Router_Route('/admin/add-expertise/:id',array('module'=>'admin','controller'=>'static','action'=>'addexpertise',
		'id'=>'\d+'));
		$this->appRoutes['admin_delete_expertise']= new Zend_Controller_Router_Route('/admin/delete-expertise',array('module'=>'admin','controller'=>'static',
		'action'=>'removeexpertise'));
		
		$this->appRoutes['admin_skill']= new Zend_Controller_Router_Route('/admin/skills',array('module'=>'admin','controller'=>'static','action'=>'skills','type'=>1));
		
		$this->appRoutes['admin_add_skill']= new Zend_Controller_Router_Route('/admin/add-skill/:id',array('module'=>'admin','controller'=>'static','action'=>'addskill',
		'id'=>'\d+'));
		$this->appRoutes['admin_delete_skill']= new Zend_Controller_Router_Route('/admin/delete-skill/:type',array('module'=>'admin','controller'=>'static','action'=>'removeskills','type'=>'/d+'));
		$this->appRoutes['admin_pending_skill']= new Zend_Controller_Router_Route('/admin/pending-skills',array('module'=>'admin','controller'=>'static','action'=>'skills','type'=>2));
		
		
		$this->appRoutes['admin_jobs']= new Zend_Controller_Router_Route('/admin/alljobs',array('module'=>'admin','controller'=>'job','action'=>'index'));
		$this->appRoutes['admin_sponsored_jobs']= new Zend_Controller_Router_Route('/admin/sponsoredjobs',array('module'=>'admin','controller'=>'job','action'=>'sponsoredjobs'));

		$this->appRoutes['admin_blog_category']= new Zend_Controller_Router_Route('/admin/blog-category',array('module'=>'admin','controller'=>'blog','action'=>'categories'));
		$this->appRoutes['admin_add_blog_category']= new Zend_Controller_Router_Route('/admin/add-blog-category/:blog_cat_id',array('module'=>'admin','controller'=>'blog',
		'action'=>'addcategory','blog_cat_id'=>'\d+'));
		$this->appRoutes['admin_delete_blog_category']= new Zend_Controller_Router_Route('/admin/delete-blog-category',array('module'=>'admin','controller'=>'blog',
		'action'=>'removecategory'));
		
		$this->appRoutes['admin_blog']= new Zend_Controller_Router_Route('/admin/blogs',array('module'=>'admin','controller'=>'blog','action'=>'index'));
		$this->appRoutes['admin_view_blog']= new Zend_Controller_Router_Route('/admin/view-blog/:id',array('module'=>'admin','controller'=>'blog','action'=>'blogdetails',
		'id'=>'\d+'));
		$this->appRoutes['admin_add_blog']= new Zend_Controller_Router_Route('/admin/add-blog/:blog_id',array('module'=>'admin','controller'=>'blog','action'=>'addblog',
		'blog_id'=>'\d+'));
		$this->appRoutes['admin_delete_blog']= new Zend_Controller_Router_Route('/admin/delete-blog',array('module'=>'admin','controller'=>'blog','action'=>'removeblog'));

		/* Pages */
		$this->appRoutes['admin_delete_page']= new Zend_Controller_Router_Route('/admin/delete-pages',array('module'=>'admin','controller'=>'static','action'=>'removepages'));
		$this->appRoutes['admin_delete_contentblocks']= new Zend_Controller_Router_Route('/admin/delete-content-block',array('module'=>'admin','controller'=>'static','action'=>'removeblock')); 		
		
		/* Graphic Media */
		$this->appRoutes['admin_graphic_media']= new Zend_Controller_Router_Route('/admin/graphic-media',array('module'=>'admin','controller'=>'static','action'=>'graphicmedia'));
 		$this->appRoutes['admin_add_graphic_media']= new Zend_Controller_Router_Route('/admin/add-graphic-media',array('module'=>'admin','controller'=>'static','action'=>'addgraphicmedia'));
		$this->appRoutes['admin_edit_graphic_media']= new Zend_Controller_Router_Route('/admin/edit-graphic-media/:media_id',array('module'=>'admin','controller'=>'static','action'=>'editgraphicmedia','media_id'=>'\d+'));
		$this->appRoutes['admin_delete_graphic_media']= new Zend_Controller_Router_Route('/admin/delete-graphic-media/:media_id',array('module'=>'admin','controller'=>'static','action'=>'deletegraphicmedia','media_id'=>'\d+'));
 
	   /* Admin  Profile Controller Routings*/
	   $this->appRoutes['update_profile_admin'] = new Zend_Controller_Router_Route('/admin/profile-update',array('module'=>'admin','controller'=>'profile','action'=>'index'));
	   $this->appRoutes['update_image_admin'] = new Zend_Controller_Router_Route('/admin/profile-image',array('module'=>'admin','controller'=>'profile','action'=>'image'));
	   $this->appRoutes['update_password_admin'] = new Zend_Controller_Router_Route('/admin/change-password',array('module'=>'admin','controller'=>'profile','action'=>'password'));
	   $this->appRoutes['update_notification_admin'] = new Zend_Controller_Router_Route('/admin/notification-settings',array('module'=>'admin','controller'=>'profile','action'=>'notification'));
		$this->appRoutes['payment_refund'] = new Zend_Controller_Router_Route('/admin/payment/refund', array ('module' => 'admin','controller' => 'payment','action' => 'refund'));
		/*  End  */
		$this->appRoutes['admin_login']= new Zend_Controller_Router_Route('/admin/login',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'login')
		);	
		$this->appRoutes['admin']= new Zend_Controller_Router_Route('/admin',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'index')
		);	
		//forgotpassword
		$this->appRoutes['forgotpassword']= new Zend_Controller_Router_Route('/admin/forgot-password',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'forgotpassword')
		);
		$this->appRoutes['resetpassword']= new Zend_Controller_Router_Route('/admin/resetpassword',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'resetpassword')
		);
		$this->appRoutes['logout']= new Zend_Controller_Router_Route('/admin/logout',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'logout')
		);
		$this->appRoutes['changepassword']= new Zend_Controller_Router_Route('/admin/changepassword',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'changepassword')
		);
		$this->appRoutes['editemailtemps']= new Zend_Controller_Router_Route('/admin/static-content/edit/content_id/:content_id',
								 array('module'     => 'admin', 
										'controller' => 'pages',
										'action' => 'edit-template',"content_id")
		);
		/* End New Routers */
		$this->appRoutes['login']= new Zend_Controller_Router_Route('/admin/login',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'login')
		);	
		$this->appRoutes['logout']= new Zend_Controller_Router_Route('/admin/logout',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'logout')
		);
		$this->appRoutes['changepassword']= new Zend_Controller_Router_Route('/admin/changepassword',
								 array('module'     => 'admin', 
										'controller' => 'index',
										'action' => 'changepassword')
		);
	 	$this->appRoutes['editemailtemps']= new Zend_Controller_Router_Route('/admin/static-content/edit/content_id/:content_id',
								 array('module'     => 'admin', 
										'controller' => 'pages',
										'action' => 'edit-template',"content_id")
		);
		
		 /* Subadmin */
	$this->appRoutes['manage_subuser']= new Zend_Controller_Router_Route('/admin/manage-subuser',array('module'=>'admin','controller'=>'static','action'=>'subuser'));
	$this->appRoutes['add_subuser']= new Zend_Controller_Router_Route('/admin/add-subuser',array('module'=>'admin','controller'=>'static','action'=>'addsubuser'));
	$this->appRoutes['edit_subuser']= new Zend_Controller_Router_Route('/admin/edit-subuser/:user_id',array('module'=>'admin','controller'=>'static','action'=>'addsubuser','user_id'=>'\d+'));
	$this->appRoutes['remove_subuser']= new Zend_Controller_Router_Route('/admin/remove-subuser',array('module'=>'admin','controller'=>'static','action'=>'removesubuser'));
	
	
	/* FORUM TOPICS */
		
	$this->appRoutes['admin_forum_topic'] = new Zend_Controller_Router_Route('/admin/manage-forumtopics',array('module'=>'admin','controller'=>'forum','action'=>'index','type'=>'1'));
	$this->appRoutes['admin_forum_add'] = new Zend_Controller_Router_Route('/admin/add-forumtopic',array('module'=>'admin','controller'=>'forum','action'=>'add'));
	$this->appRoutes['admin_forum_edit'] = new Zend_Controller_Router_Route('/admin/edit-forumtopic/:forum_id',array('module'=>'admin','controller'=>'forum','action'=>'add','forum_id'=>'/d+'));
	$this->appRoutes['admin_forum_remove'] = new Zend_Controller_Router_Route('/admin/remove-forumtopic',array('module'=>'admin','controller'=>'forum','action'=>'removetopic'));
	$this->appRoutes['admin_forum_users'] = new Zend_Controller_Router_Route('/admin/users-forumtopics',array('module'=>'admin','controller'=>'forum','action'=>'index','type'=>2));
	$this->appRoutes['admin_view_threads'] = new Zend_Controller_Router_Route('/admin/view-posts/:forum_id',array('module'=>'admin','controller'=>'forum','action'=>'threads','forum_id'=>'/d+'));
	$this->appRoutes['admin_view_comments'] = new Zend_Controller_Router_Route('/admin/view-comments/:f_thread_id',array('module'=>'admin','controller'=>'forum','action'=>'comments','f_thread_id'=>'/d+'));
	$this->appRoutes['admin_forum_reports'] = new Zend_Controller_Router_Route('/admin/reported-posts',array('module'=>'admin','controller'=>'forum','action'=>'reportposts'));
	$this->appRoutes['admin_forum_improvement'] = new Zend_Controller_Router_Route('/admin/improvement-posts',array('module'=>'admin','controller'=>'forum','action'=>'improvement'));
	
		
	
	}
	 protected  function _initSetupRouting(){	
			foreach($this->appRoutes as $key=>$cRouter){
				$this->router->addRoute( $key,  $cRouter );
			}
	}
    /**
     * return the bootstrap object for the active module
     * @return Offshoot_Application_Module_Bootstrap
     */
    public function _getActiveBootstrap($activeModuleName)
    {
        $moduleList = $this->_getBootstrap()->getResource('modules');
        if (isset($moduleList[$activeModuleName])) {
        }
        return null;
    }
}