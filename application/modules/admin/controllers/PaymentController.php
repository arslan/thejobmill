<?php
use ZfrStripe\Exception\TransactionErrorException;
use ZfrStripe\Client;
class Admin_PaymentController extends Zend_Controller_Action
{

    public function init()
    {
        $this->modelStatic = new Application_Model_Static();
        $this->modelUser = new Application_Model_User();
        $this->pluginImage = new Application_Plugin_Image();
    }

    
    public function refundAction()
    {

        $user_id = 	$logged_identity = Zend_Auth::getInstance()->getInstance();
        $userInfo = $user_id->getIdentity();
        $totalPackages = 0;
        $request = $this->getRequest();
        $transaction_id = $request->getPost('transaction_id');
        $package = $request->getPost('package');
        $packageDetails = 0;
        $totalPrice = 0;
        $refundPrice = 0;


        $transactionDetails = $this->modelStatic->Super_Get('transactions_history', "id='" . $transaction_id . "'", 'fetch');

        $metaData = json_decode($transactionDetails['metadata']);


        if (isset($metaData->pkg1)) {
            $pkg1 = explode(':', $metaData->pkg1);
            if ($pkg1[0] != 'false') {
                $totalPackages++;

            }
        }


        if (isset($metaData->pkg2)) {
            $pkg2 = explode(':', $metaData->pkg2);
            if ($pkg2[0] != 'false') {
                $totalPackages++;

            }
        }


        if (isset($metaData->pkg3)) {
            $pkg3 = explode(':', $metaData->pkg3);
            if ($pkg3[0] != 'false') {
                $totalPackages++;

            }
        }
        
        $metaData = (array)$metaData;

        if(isset($metaData["discount_type"]) && isset($metaData["discount_value"])){
            if ($metaData["discount_type"] == 1) {
                $obj = explode(':', $metaData[$package]);
                $totalPrice = (float)str_replace('$', '', $obj[1]);
                $refundPrice = $totalPrice - ($totalPrice * $metaData["discount_value"] / 100);
            }
            if ($metaData["discount_type"] == 2) {
                $obj = explode(':', $metaData[$package]);
                echo $totalPrice = (float)str_replace('$', '', $obj[1]);
                //		echo "total packages".$totalPackages;
                $refundPrice = $totalPrice - ($metaData["discount_value"] / $totalPackages);
            }
        }
        else{

            $obj = explode(':', $metaData[$package]);
            $refundPrice = (float)str_replace('$', '', $obj[1]);

        }



        $refundPrice = (int)( $refundPrice*100);
        $obj = explode(':', $metaData[$package]);
        $subscriptionId=$obj[0];


        $client = new ZfrStripe\Client\StripeClient('sk_test_sI6SI0CngEoIauY03bzUlPcO', '2015-10-16');
        $reund = $client->refundCharge( array(

            "id" => $transactionDetails["transaction_id"],
            "amount" => $refundPrice
        ));

        $refmetadata=[

            "Voucher_code" => isset($metaData["Voucher_code"])?$metaData["Voucher_code"]:"",
            "discount"=> isset($metaData["discount"])?$metaData["discount"]:"",
            "discount_value" => isset($metaData["discount_value"])?$metaData["discount_value"]:"",
            "discount_type" => isset($metaData["discount_type"])?$metaData["discount_type"]:"",
            "subtotal"=>isset($metaData["subtotal"])?$metaData["subtotal"]:"",
            "pkg"=>$metaData[$package],
        ];


        $data = [
            "user_id"=>$userInfo->user_id,
            "transaction_id" => $reund['id'],
            "amount"=>$reund['amount'],
            "currency"=>$reund['currency'],
            "description"=> "Refound for ".$obj[2] . " : ".$transactionDetails["transaction_id"],
            "metadata"=> json_encode($refmetadata),
            "status"=>$reund["status"],
            "job_id" => $transactionDetails["job_id"]
        ];

        if($reund["status"]=="succeeded") {

            $result = $this->modelStatic->Super_Insert('transactions_history', $data);


            $refundPackage = $metaData[$package] = "false";

            //Update actual transaction after refund
            $updateData = array(

                "metadata" => json_encode($metaData),

            );
            $result = $this->modelStatic->Super_Insert('transactions_history', $updateData, "id='" . $transaction_id . "'");


            $updateData = array(

                "status" => "0"
            );
            $result = $this->modelStatic->Super_Insert('job_subscriptions', $updateData, "job_id='" . $transactionDetails["job_id"] . "' AND subscription_id='".$subscriptionId."'");




            echo $this->_helper->json($reund);

        }
        else{ echo $this->_helper->json(["status"=>"false"]);
        }
        exit;

    }


}