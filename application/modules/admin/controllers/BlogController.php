<?php
class Admin_BlogController extends Zend_Controller_Action
{
    public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->view->pageIcon = "fa  fa-users";
    }
 	
 	public function indexAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Manage All Blogs";
		$this->view->pageDescription = "manage all blogs ";
		$this->view->request_type = "all";
 	}
	
	public function blogcommentsAction(){
 		global $mySession; 
		$blog_id = $this->getRequest()->getParam('blog_id');
		$this->view->blog_id = $blog_id;
 		$this->view->pageHeading = "Manage All Blog Comments";
		$this->view->pageDescription = "manage all blog comments";
 	}
 	
	public function addblogAction()
	{
		global $mySession;
		$this->view->pageHeading = "Add Blog Category";
		$this->view->pageIcon = "fa fa-sitemap";
		$blog_id = $this->getRequest()->getParam('blog_id');
		$blog = $this->modelStatic->Super_Get('blogs',"blog_id='".$blog_id."'","fetch",$extra=array(),$joinArr=array());
		$this->view->blog = $blog;
		$form = new Application_Form_Blog();
		$form->addblog();
		if(!empty($blog)){
			$form->populate($blog);
		}
		$this->view->form = $form;
		
		if($this->getRequest()->isPost()) 
		{
 			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
				$data = $form->getValues();
				if(!empty($blog)){
					if($data['blog_image']==''){
						$data['blog_image']=$blog['blog_image'];
					}
					$this->modelStatic->Super_Insert('blogs',$data,"blog_id='".$blog_id."'");
				}else{
					$data['blog_date'] = date('Y-m-d H:i:s');
					$this->modelStatic->Super_Insert('blogs',$data);
				}
				$mySession->successMsg = 'Blog added successfuly';
				$this->_redirect('/admin/blogs');
			}
		}
	}
	
	public function blogdetailsAction()
	{
		global $objSession;
		$blog_id = $this->getRequest()->getParam('id');
		$joinArr=array(
			'0' => array('0'=>'blog_category','1'=>'blog_cat_id = blog_category','2'=>'left','3'=>array('blog_cat_title','blog_cat_id')),
		);
		$blog = $this->modelStatic->Super_Get('blogs',"blog_id='".$blog_id."'","fetch",$extra=array(),$joinArr);
		$this->view->blog = $blog;
		
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'user_id = blog_comm_user_id','2'=>'left','3'=>array('user_first_name','user_last_name','user_image')),
		);
		$blogComments = $this->modelStatic->Super_Get('blog_comments',"blog_comm_blog_id='".$blog_id."'","fetchAll",$extra=array('order'=>'blog_comm_date DESC'),$joinArr);
		$this->view->blogComments = $blogComments;
	}
	
 	/* Ajax Call For Get Users */
  	public function getblogsAction()
	{
		$this->dbObj = Zend_Registry::get('db');

 		$aColumns = array(
			'blog_id','blog_title','blog_category','blog_desc','blog_date','blog_cat_id','blog_cat_title'
  		);
		$sIndexColumn = 'blog_id';
		$sTable = 'blogs';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				//$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join blog_category on blog_cat_id=blog_category $sWhere $sOrder
		 $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join blog_category on blog_cat_id=blog_category $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			$row[] = '<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[] = $row1['blog_title']."<br><a href='".$this->view->url(array('module'=>'admin','controller'=>'blog','action'=>'blogcomments','blog_id'=>$row1[$sIndexColumn]),'default',true)."'>View Comments</a>";
   			$row[] = $row1['blog_cat_title'];
			$row[] = date('M d, Y',strtotime($row1['blog_date']));
			$row[] = '<a class="btn default btn-xs blue" href="'.$this->view->url(array('blog_id'=>$row1[$sIndexColumn]),'admin_add_blog').'"><i class="fa fa-edit"></i> Edit </a>';
			$row[] = '<a class="btn mini green-stripe btn-success btn-sm" href="'.$this->view->url(array('id'=>$row1[$sIndexColumn]),'admin_view_blog').'">View</a>';
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function getblogcommentsAction()
	{
		$this->dbObj = Zend_Registry::get('db');
		$blog_id = $this->getRequest()->getParam('blog_id');
 		$aColumns = array(
			'blog_comm_id','blog_comment','user_first_name','user_last_name','blog_comm_date','blog_comm_user_id','blog_comm_blog_id'
  		);
		$sIndexColumn = 'blog_comm_id';
		$sTable = 'blog_comments';
		/** Paging **/
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering **/
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		if($sWhere){
			$sWhere = $sWhere." and blog_comm_blog_id='".$blog_id."'";
		}else{
			$sWhere = " where blog_comm_blog_id='".$blog_id."'";
		}
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join users on user_id=blog_comm_user_id $sWhere $sOrder $sLimit";
		//echo $sQuery;die;
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join users on user_id=blog_comm_user_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			$row[] = '<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[] = $row1['blog_comment'];
			$row[] = $row1['user_first_name'].' '.$row1['user_last_name'];
			$row[] = date('M d, Y',strtotime($row1['blog_comm_date']));
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function removeblogAction()
	{
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if(isset($formData['blogs']) and count($formData['blogs'])){
				 foreach($formData['blogs'] as $key=>$values){
   					 $user_info = $this->modelUser->get($values);
					
					 /*if(empty($user_info))
						continue ;*/
						// prd($this->modelUser->Super_Delete("subscription","sub_id IN ($values)"));
					$removed = $this->modelUser->Super_Delete("blogs","blog_id IN ($values)");
				 }
 				$mySession->successMsg = " Blog Deleted Successfully ";
				
 			}else{
				$mySession->errorMsg = " Invalid Request to Delete Blog(s) ";
			}
 			$this->_redirect('/admin/blogs?removed=1');	 
		} 
 	}
	
	public function removeblogcommentAction()
	{
		global $mySession;
		$blog_id = $this->getRequest()->getParam('blog_id');
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if(isset($formData['blog_comments']) and count($formData['blog_comments'])){
				 foreach($formData['blog_comments'] as $key=>$values){
   					 $user_info = $this->modelUser->get($values);
					
					 /*if(empty($user_info))
						continue ;*/
						// prd($this->modelUser->Super_Delete("subscription","sub_id IN ($values)"));
					$removed = $this->modelUser->Super_Delete("blog_comments","blog_comm_id IN ($values)");
				 }
 				$mySession->successMsg = " Blog Comment(s) Deleted Successfully ";
				
 			}else{
				$mySession->errorMsg = " Invalid Request to Delete Blog Comment(s) ";
			}
 			$this->_redirect('/admin/blog/blogcomments/blog_id/'.$blog_id.'?removed=1');	 
		} 
 	}
	
	public function categoriesAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Manage All Blog Categories";
		$this->view->pageDescription = "manage all blog categories";
 	}
 	
	public function addcategoryAction()
	{
		global $mySession;
		$this->view->pageHeading = "Add Blog Category";
		$this->view->pageIcon = "fa fa-sitemap";
		$blog_cat_id = $this->getRequest()->getParam('blog_cat_id');
		$blog_category = $this->modelStatic->Super_Get('blog_category',"blog_cat_id='".$blog_cat_id."'","fetch",$extra=array(),$joinArr=array());
		$this->view->blog_category = $blog_category;
		$form = new Application_Form_Blog();
		$form->addblogcategory();
		if(!empty($blog_category)){
			$form->populate($blog_category);
		}
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()) 
		{
 			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
				$data = $form->getValues();
				if(!empty($blog_category)){
					$this->modelStatic->Super_Insert('blog_category',$data,"blog_cat_id='".$blog_cat_id."'");
				}else{
					$this->modelStatic->Super_Insert('blog_category',$data);
				}
				$mySession->successMsg = 'Category added successfuly';
				$this->_redirect('/admin/blog-category');
			}
		}
	}
	
 	/* Ajax Call For Get Users */
  	public function getcategoriesAction(){
		$this->dbObj = Zend_Registry::get('db');

 		$aColumns = array(
			'blog_cat_id','blog_cat_title'
  		);
		$sIndexColumn = 'blog_cat_id';
		$sTable = 'blog_category';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				//$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[]=$row1['blog_cat_title'];
			$row[]='<a class="btn default btn-xs blue" href="'.$this->view->url(array('blog_cat_id'=>$row1[$sIndexColumn]),'admin_add_blog_category').'"><i class="fa fa-edit"></i> Edit </a>';
 			$output['aaData'][] = $row;
			$j++;
		$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function removecategoryAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if(isset($formData['blog_category']) and count($formData['blog_category'])){
				 foreach($formData['blog_category'] as $key=>$values){
					$removed = $this->modelUser->Super_Delete("blog_category","blog_cat_id IN ($values)");
				 }
 				$mySession->successMsg = "Blog Category Deleted Successfully";
				
 			}else{
				$mySession->errorMsg = "Invalid Request to Delete Blog Category(s)";
			}
 			$this->_redirect('/admin/blog-category?removed=1');	 
		} 
 	}
}