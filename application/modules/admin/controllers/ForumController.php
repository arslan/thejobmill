<?php
class Admin_ForumController extends Zend_Controller_Action
{
    public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->view->pageIcon = "fa  fa-comments";
    }
 	
 	public function indexAction(){
 		global $mySession; 
		$type=$this->_getParam('type');
		$this->view->type=$type;
		
		if($type==1){
			$this->view->pageHeading = "Manage Forum Topics";
			$this->view->pageDescription = "manage forum topics";
		}
		else{
			$this->view->pageHeading = "User Topic Requests";
			$this->view->pageDescription = "user topic requests";
		}
 	}
	
	public function threadsAction(){
 		global $mySession; 
		$forum_id=$this->_getParam('forum_id');
		$this->view->forum_id=$forum_id;
		$forumData = $this->modelStatic->Super_Get('forum_topics',"forum_id='".$forum_id."'","fetch");
		if(empty($forumData)){
			$mySession->errorMsg = 'Invalid Request';
			$this->_redirect("/admin/manage-forumtopics");
		}
 		$this->view->pageHeading = "View All Posts";
		$this->view->pageDescription = "view all posts";
 	}
	
	public function commentsAction(){
 		global $mySession; 
		$f_thread_id=$this->_getParam('f_thread_id');
		$this->view->f_thread_id=$f_thread_id;
		$forumData = $this->modelStatic->Super_Get('forum_threads',"f_thread_id='".$f_thread_id."'","fetch");
		if(empty($forumData)){
			$mySession->errorMsg = 'Invalid Request';
			$this->_redirect("/admin/manage-forumtopics");
		}
		
 		$this->view->pageHeading = "View All Comments";
		$this->view->pageDescription = "view all coments";
 	}
	
	public function reportpostsAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Reported Posts";
		$this->view->pageDescription = "reported posts";
		$this->view->pageIcon = "fa  fa-warning";
 	}
	
	public function improvementAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Posts for Improvement";
		$this->view->pageDescription = "posts for improvewent";
		$this->view->pageIcon = "fa  fa-warning";
 	}
		
 	
	public function addAction()
	{
		global $mySession;
		$this->view->pageHeading = "Add Forum Topic";
		$this->view->pageIcon = "fa fa-plus";
		$forum_id = $this->getRequest()->getParam('forum_id');
		$form = new Application_Form_Forum();
		$form->addtopic($this->view->current_module);
		if($forum_id!='/d+' && $forum_id!=''){
			$forumData = $this->modelStatic->Super_Get('forum_topics',"forum_id='".$forum_id."'","fetch",$extra=array(),$joinArr=array());
			if(!empty($forumData)){
				$this->view->pageHeading = "Edit Forum Topic";
				$this->view->pageIcon = "fa fa-edit";
				$form->populate($forumData);
			}
			else{
				$mySession->errorMsg = 'Invalid Request';
				$this->_redirect("/admin/manage-forumtopics");
			}
		}
		$this->view->form = $form;
		
		if($this->getRequest()->isPost()) 
		{
 			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
				$data = $form->getValues();
				
				$posted_data['forum_user_id'] =$this->view->user->user_id;
				$posted_data['forum_added_date'] = date('Y-m-d H:i:s');
				unset($posted_data['btnsubmit']);
				if(!empty($forumData)){
					$msgType="Updated";
					$this->modelStatic->Super_Insert('forum_topics',$posted_data,"forum_id='".$forum_id."'");
				}else{
					$msgType="Added";
					$this->modelStatic->Super_Insert('forum_topics',$posted_data);
				}
				$mySession->successMsg = 'Forum Topic '.$msgType.' successfuly';
				$this->_redirect('/admin/manage-forumtopics');
			}
		}
		
		$this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
	}
	
	
 	/* Ajax Call For Get Users */
  	public function getforumtopicsAction()
	{
		$this->dbObj = Zend_Registry::get('db');

		$type=$this->_getParam('type');
 		$aColumns = array(
			'forum_id','forum_topic','forum_added_date','user_type','user_first_name','user_last_name','user_image','forum_approved_status','forum_status','forum_cat_id'
  		);
		$sIndexColumn = 'forum_id';
		$sTable = 'forum_topics';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		
		if($sWhere){
			if($type==2){ /* TOPICS ADDED BY USESR */
				$sWhere.=" and forum_user_id!='".$this->view->user->user_id."' and forum_approved_status='0'";
			}
			else{
				$sWhere.=" and forum_approved_status!='0'";
			}
		}
		else{
			if($type==2){ /* TOPICS ADDED BY USESR */
				$sWhere.=" where forum_user_id!='".$this->view->user->user_id."' and forum_approved_status='0'";
			}
			else{
				$sWhere.=" where forum_approved_status!='0'";
			}

		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns)).", ifnull(count(f_thread_id),0) as totalThreads FROM  $sTable join users on user_id=forum_user_id left join forum_threads on f_forum_id=forum_id $sWhere group by forum_id $sOrder $sLimit";
		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM  $sTable join users on user_id=forum_user_id left join forum_threads on f_forum_id=forum_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		$ForumCategoryArray = array(''=>'N/A','Lounge'=>'Lounge','Questions'=>'Questions','Feedback'=>'Feedback');
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			$row[] = '<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[] = $ForumCategoryArray[$row1['forum_cat_id']];
			$row[] = $row1['forum_topic'];
			$row[]='<img src="'.getUserImage($row1['user_image'],60).'" class="avatar" /><br/>'.$row1['user_first_name'];
			switch($row1['user_type']){
				case 'admin': $labelText="ADMIN";$labelClass="warning";break; 
				case 'user': $labelText="USER";$labelClass="success";break; 
			}
			$row[] ="<label class='label label-".$labelClass."'>".$labelText."</label>";
			$row[] = date('M d, Y',strtotime($row1['forum_added_date']));
			
			
			if($type==1) {
			if($row1['totalThreads']!=0)
				$row[] ="<a href='".$this->view->url(array('forum_id'=>$row1[$sIndexColumn]),'admin_view_threads')."'><label class='label label-warning' style='cursor:pointer;'>".$row1['totalThreads']."</label>";
			else
				$row[] ="<label class='label label-warning'>".$row1['totalThreads']."</label>";		
			
			
				$status2 = $row1['forum_status']!=1?"checked='checked'":" ";
				$forum_status_div='<div class="danger-toggle-button-2">
							<input type="checkbox" class="toggle status-'.(int)$row1['forum_status'].' "  '.$status2.'  id="forum_topics-'.$row1[$sIndexColumn].'" onChange="globalStatus(this)" />
						</div>';
				$row[]=$forum_status_div;					
			}
			if($type==2) {
				
				$status3 = $row1['forum_approved_status']!=1?"checked='checked'":" ";
				$approved_status_div='<div class="danger-toggle-button-3">
							<input type="checkbox" class="toggle status-'.(int)$row1['forum_approved_status'].' "  '.$status3.'  id="forum_approved_topics-'.$row1[$sIndexColumn].'" onChange="globalStatus(this)" />
						</div>';
				$row[]=$approved_status_div;
			}
			if($type==1) {
				$row[] = '<a class="btn default btn-xs blue" href="'.$this->view->url(array('forum_id'=>$row1[$sIndexColumn]),'admin_forum_edit').'"><i class="fa fa-edit"></i> Edit </a>';
			}
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function getforumpostsAction()
	{
		$this->dbObj = Zend_Registry::get('db');

		$forum_id=$this->_getParam('forum_id');
 		$aColumns = array(
			'f_thread_id','f_forum_id','f_user_id','user_type','user_first_name','user_last_name','user_image','f_added_date','f_thread'
  		);
		$sIndexColumn = 'f_thread_id';
		$sTable = 'forum_threads';
		
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		
		if($sWhere){
			$sWhere.=" and f_forum_id='".$forum_id."'";
		}
		else{
			$sWhere.=" where f_forum_id='".$forum_id."'";
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns)).", ifnull(count(fc_comment_id),0) as totalComments FROM  $sTable join users on user_id=f_user_id left join forum_comments on fc_thread_id=f_thread_id $sWhere group by f_thread_id $sOrder $sLimit";
		
		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join users on user_id=f_user_id left join forum_comments on fc_thread_id=f_thread_id $sWhere group by f_thread_id ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
			$row[] = $row1['f_thread'];
			$row[]='<img src="'.getUserImage($row1['user_image'],60).'" class="avatar" /><br/>'.$row1['user_first_name'];
			$row[] = date('M d, Y',strtotime($row1['f_added_date']));
			if($row1['totalComments']!=0)
				$row[] ="<a href='".$this->view->url(array('f_thread_id'=>$row1[$sIndexColumn]),'admin_view_comments')."'><label class='label label-warning' style='cursor:pointer;'>".$row1['totalComments']."</label>";
		   else
				$row[] ="<label class='label label-warning'>".$row1['totalComments']."</label>";		
			
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function getcommentsAction()
	{
		$this->dbObj = Zend_Registry::get('db');

		$f_thread_id=$this->_getParam('f_thread_id');
 		$aColumns = array(
			'fc_comment_id','fc_thread_id','fc_user_id','user_type','user_first_name','user_last_name','user_image','fc_comment','fc_added_date'
  		);
		$sIndexColumn = 'fc_comment_id';
		$sTable = 'forum_comments';
		
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		
		if($sWhere){
			$sWhere.=" and fc_thread_id='".$f_thread_id."'";
		}
		else{
			$sWhere.=" where fc_thread_id='".$f_thread_id."'";
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join users on user_id=fc_user_id $sWhere $sOrder $sLimit";
		
		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join users on user_id=fc_user_id  $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
			$row[] = $row1['fc_comment'];
			$row[]='<img src="'.getUserImage($row1['user_image'],60).'" class="avatar" /><br/>'.$row1['user_first_name'];
			$row[] = date('M d, Y',strtotime($row1['fc_added_date']));
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function getreportsAction()
	{
		$this->dbObj = Zend_Registry::get('db');

 		$aColumns = array(
			'report_id','report_thread_id','report_email','user_first_name','user_last_name','user_image','report_text','report_added_date'
  		);
		$sIndexColumn = 'report_id';
		$sTable = 'forum_reports';
		
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join forum_threads on report_thread_id=f_thread_id join users on users.user_id=f_user_id $sWhere $sOrder $sLimit";
		
		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join forum_threads on report_thread_id=f_thread_id join users on users.user_id=f_user_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
			$row[] ="<label class='label label-primary'>".$row1['report_email']."</label>";
			$row[] = $row1['report_text'];
			$row[]='<img src="'.getUserImage($row1['user_image'],60).'" class="avatar" /><br/>'.$row1['user_first_name'];
			$row[] = date('M d, Y',strtotime($row1['report_added_date']));
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function getsuggestionsAction()
	{
		$this->dbObj = Zend_Registry::get('db');

 		$aColumns = array(
			'improvement_id','improvement_thread_id','improvement_email','user_first_name','user_last_name','user_image','improvement_text','improvement_added_date'
  		);
		$sIndexColumn = 'improvement_id';
		$sTable = 'forum_improvement';
		
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".mysql_real_escape_string($_GET["sSearch"])."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join forum_threads on improvement_thread_id=f_thread_id join users on users.user_id=f_user_id $sWhere $sOrder $sLimit";
		
		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join forum_threads on improvement_thread_id=f_thread_id join users on users.user_id=f_user_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
			$row[] ="<label class='label label-primary'>".$row1['improvement_email']."</label>";
			$row[] = $row1['improvement_text'];
			$row[]='<img src="'.getUserImage($row1['user_image'],60).'" class="avatar" /><br/>'.$row1['user_first_name'];
			$row[] = date('M d, Y',strtotime($row1['improvement_added_date']));
 			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	
	
	
	
	
	public function removetopicAction()
	{
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if(isset($formData['forum_topics']) and count($formData['forum_topics'])){
				 foreach($formData['forum_topics'] as $key=>$values){
					$removed = $this->modelUser->Super_Delete("forum_topics","forum_id IN ($values)");
				 }
 				$mySession->successMsg = " Forum Topic Deleted Successfully ";
				
 			}else{
				$mySession->errorMsg = " Invalid Request to Delete Forum Topic(s) ";
			}
 			$this->_redirect('/admin/manage-forumtopics?removed=1');	 
		} 
 	}
	
	
	
}