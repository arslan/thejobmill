<?php
class Admin_StaticController extends Zend_Controller_Action
{
 	private $admin = "" , $modelStatic;
	
    public function init(){ 
  		$this->modelStatic  = new Application_Model_Static();
		$this->modelEmail  = new Application_Model_Email();
  	}
 	
	public function siteconfigsAction(){
		global $mySession; 

		$this->view->pageHeading = " Site Configuration  ";
 		$this->view->pageDescription = "manage site configuration  ";
		
		$form = new Application_Form_StaticForm();
		$form->configuration();
		
   		if ($this->getRequest()->isPost()) {
 			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
				$data = $form->getValues();
				
				foreach($data as $key=>$values){
 					try{
						$this->modelStatic->getAdapter()->update('config',array('config_value'=>$values),"config_key='".$key."'");
					}catch(Zend_Exception $e){
 						 $mySession->errorMsg = $e->getMessage();
						 $this->render("index");
						 break;
					} 
				}
				$mySession->successMsg = " Site Configuration Successfully Updated ";
				$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_site_configs");
   			}
		}
  		  $this->view->form = $form;
  		  $this->_helper->getHelper('viewRenderer')->renderScript('add.phtml');
    }
	
	public function addimagesAction()
	{
		global $mySession;
		$this->view->pageHeading = "Add images";
		$this->view->pageDescription = "add images";
		$this->view->pageIcon = "fa fa-image";
 		$form = new Application_Form_StaticForm();
		$form->background_images();
		
		$id = $this->getRequest()->getParam('id');
		$backgroundData = $this->modelStatic->Super_Get('background_images',"1",'fetchAll');
		$this->view->backgroundData = $backgroundData;
		
 		if($this->getRequest()->isPost()) {
			$data_form = $this->getRequest()->getPost();
   			if($form->isValid($data_form)){
				$data_to_insert = $form->getValues() ;
				foreach($data_to_insert as $key=>$values){
					if($values!=''){
						$type=explode("_",$key);
						$InsertData = array(
							'bg_type'=>$type[1],
							'bg_file'=>$values,
						);
						$this->modelStatic->Super_Insert('background_images',$InsertData,"bg_type='".$type[1]."'");
					}
				}
				$mySession->successMsg = " Images updated successfully ";
				$this->_redirect('/admin/static/addimages');
   			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
  		 $this->view->form =$form;
	}
	
	/* Show All Email Templates */
	public function showmailtemplatesAction(){
		$this->view->pageHeading = "Manage Email Templates";
		$this->view->pageDescription = "manage all email templates";
		$this->view->pageIcon = "icon-envelope-letter";
   	}

	/* Edit Email Template */
	public function addmailtemplateAction(){
		global $mySession;
		$this->view->pageHeading = "Add Email Templates";
		$this->view->pageDescription = "manage all email templates";
		$this->view->pageIcon = "icon-envelope-letter";

		$form = new Application_Form_StaticForm();
		$form->add_email_template();


		if($this->getRequest()->isPost()){
			if($form->isValid($this->getRequest()->getPost())){
				$data = $form->getValues();
                                $data['selected_email'] = json_encode(explode(';',$data['selected_email']));
                                $data['emailtemp_modified'] = Zend_Date::now()->toString('YYYY-MM-dd HH:mm:ss');
                                
				$this->modelStatic->add("email_templates",$data);
				$mySession->successMsg = "Email Template Updated Successfully";
				$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_email_templates");
			}
		}
		$this->view->form = $form;
		$this->view->action = 'add';
		$this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
	}
	
	/* Edit Email Template */
	public function editmailtemplateAction(){
		global $mySession; 
		$this->view->pageHeading = "Edit Email Templates";
		$this->view->pageDescription = "manage all email templates";
		$this->view->pageIcon = "icon-envelope-letter";
		$id =  $this->_getParam('email_key');
 		$record = $this->modelStatic->getTemplate($id);
 		if(!$record){
 			$mySession->errorMsg = "No Such Email Template Exists";
 			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_email_templates");
  		}
                //Check email type and autofill EmailIds
                if($record['email_type'] == 1){
                    $record['selected_email'] = 'New users';
                }
                elseif($record['email_type'] == 2){
                    $record['selected_email'] = 'All Users';
                }
                
 		$form = new Application_Form_StaticForm();
		$form->email_template($record['email_type']);
		$form->populate((array)$record);
		
		if($this->getRequest()->isPost()){
            $data = array_merge($record,$this->getRequest()->getPost());
			if($form->isValid($data)){
                $data = $form->getValues();
                $data['selected_email'] = json_encode(explode(';',$data['selected_email']));
                $data['emailtemp_modified'] = Zend_Date::now()->toString('YYYY-MM-dd HH:mm:ss');
				$this->modelStatic->add("email_templates",$data,"emailtemp_key='".$id."'");
				$mySession->successMsg = "Email Template Updated Successfully";
 				$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_email_templates");
			}
 		}
  		$this->view->form = $form;
		$this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
	}
	public function sendemailAction(){
            global $mySession;
            $id =  $this->_getParam('email_key');
            $record = $this->modelStatic->getTemplate($id);
            $emailType = $record['email_type'];
            //Email send according to selected email type
            if($emailType == 2){
                ini_set('memory_limit','1G');
                set_time_limit(0);
                $userModel = new Application_Model_User();
                $users = $userModel->getAll();
                foreach($users as $user)
                {
                        $response = $this->modelEmail->sendUsersEmail($id,$user['user_email'],$user['name']);
                }
                $mySession->successMsg = "Email Send Successfully To All User";
            }
            elseif($emailType == 1){
                $userModel = new Application_Model_User();
                $users = $userModel->getAll(array('where'=> "user_created >= DATE_SUB(CURDATE(), INTERVAL 2 MONTH) "));
                foreach($users as $user)
                {
                    $response = $this->modelEmail->sendUsersEmail($id,$user['user_email'],$user['name']);
                }
                $mySession->successMsg = "Email Send Successfully To New Users";
            }
            else{
                $users = $record['selected_email'];
                $emailIds = json_decode($users);
                foreach($emailIds as $email)
                {
                    $response = $this->modelEmail->sendUsersEmail($id,$email,'dev1');
                }
                
                $mySession->successMsg = "Email Send Successfully To Selected User";
            }
            $this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_email_templates");
        }
        public function deleteemailAction(){
            global $mySession;
            $id =  $this->_getParam('email_key');
            $records = $this->modelStatic->Super_Delete("email_templates","emailtemp_key='".$id."'");
            $mySession->successMsg = "Email Template Deleted Successfully";
            $this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_email_templates");
            
        }
        /* DataTable Get Email Template */
	public function gettemplatesAction(){
 		$this->dbObj = Zend_Registry::get('db');
		$aColumns = array( 'emailtemp_key','emailtemp_title' ,'emailtemp_subject','emailtemp_modified' ,'email_type');
		$sIndexColumn = 'emailtemp_key';
		$sTable = 'email_templates';
		
		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM   $sTable $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();

 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		
		/** Output */
 		 
		$output = array(
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		$j=0;
		foreach($qry as $row1)
		{
                    $sendBtn = '';
                    if(!empty($row1['email_type'])){
                        $sendBtn = '<a class="btn btn-xs blue" href="'.APPLICATION_URL.'/admin/static/sendemail/email_key/'.$row1[$sIndexColumn].'" onclick="return confirm(\'Do you really want to send this?\');"><i class="fa fa-arrow-right"></i> Send </a><a class="btn btn-xs blue" href="'.APPLICATION_URL.'/admin/static/deleteemail/email_key/'.$row1[$sIndexColumn].'" onclick="return confirm(\'Do you really want to delete this?\');"><i class="fa fa-trash-o"></i> Delete </a>';
                    }
			$row=array();
 			$row[] = $j+1;
  			$row[]=ucwords($row1['emailtemp_title']);
			$row[]=ucwords(substr($row1['emailtemp_subject'],0,40));
  			$row[]=ucwords(substr($row1['emailtemp_modified'],0,40));
			$row[]='<a class="btn btn-xs blue" href="'.APPLICATION_URL.'/admin/static/editmailtemplate/email_key/'.$row1[$sIndexColumn].'"><i class="fa fa-edit"></i> Edit </a>'.$sendBtn;
 			$output['aaData'][] = $row;
			$j++;
		}
		echo json_encode( $output );
		exit();
	}
	
	/* Show Pages  */
	public function indexAction(){
		global $mySession;
		$this->view->pageHeading = "Static Page Content";
		$this->view->pageDescription = "manage static page content";
		$this->view->pageIcon = "fa fa-save";
    }
	
	public function expertiseAction(){
		global $mySession;
		$this->view->pageHeading = "Categories";
		$this->view->pageDescription = "manage categories";
		$this->view->pageIcon = "fa fa-sitemap ";
    }
	public function addexpertiseAction(){
		global $mySession; 
  		$this->view->pageHeading = "Create Category";
		$this->view->pageDescription = "add category";
		$this->view->pageIcon = "fa fa-sitemap";
 		$form = new Application_Form_StaticForm();
		$form->expertise();
		
		$id = $this->getRequest()->getParam('id');
		$expertiseData = $this->modelStatic->Super_Get('categories',"category_id='".$id."'",'fetch');
		if(!empty($expertiseData)){
			$form->populate($expertiseData);
		}
 		if($this->getRequest()->isPost()) {
			$data_form = $this->getRequest()->getPost();
   			if($form->isValid($data_form)){
				$data_to_insert = $form->getValues() ;
				if(!empty($expertiseData)){
					$is_insert = $this->modelStatic->add("categories",$data_to_insert,"category_id='".$id."'");
					$mySession->successMsg  = " Expertise successfully updated ";
				}else{
					$is_insert = $this->modelStatic->add("categories",$data_to_insert);
					$mySession->successMsg  = " Expertise successfully added ";
				}
				
				if($is_insert->success){
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_expertise");
 				}
 				$mySession->errorMsg  = $is_insert->message;
   			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
  		 $this->view->form =$form;
  	}
	
	public function skillsAction(){
		global $mySession;
		
		$type=$this->_getParam('type');
		$this->view->type=$type;
		if($type==1){
			$this->view->pageHeading = "Skills";
			$this->view->pageDescription = "manage skills";
			$this->view->pageIcon = "fa fa-sitemap ";
		}
		else{
			$this->view->pageHeading = "Pending Skills";
			$this->view->pageDescription = "pending skills";
			$this->view->pageIcon = "fa fa-warning ";
		}
    }
	public function addskillAction(){
		global $mySession; 
  		$this->view->pageHeading = "Create Skill";
		$this->view->pageDescription = "add skills";
		$this->view->pageIcon = "fa fa-sitemap";
 		$form = new Application_Form_StaticForm();
		$form->skills();
		
		$id = $this->getRequest()->getParam('id');
		$skillsData = $this->modelStatic->Super_Get('skills',"skill_id='".$id."'",'fetch');
		if(!empty($skillsData)){
			$form->populate($skillsData);
		}
 		if($this->getRequest()->isPost()) {
			$data_form = $this->getRequest()->getPost();
   			if($form->isValid($data_form)){
				$data_to_insert = $form->getValues() ;
				if(!empty($skillsData)){
					$is_insert = $this->modelStatic->add("skills",$data_to_insert,"skill_id='".$id."'");
					$mySession->successMsg  = " Skill successfully updated ";
				}else{
					$is_insert = $this->modelStatic->add("skills",$data_to_insert);
					$mySession->successMsg  = " Skill successfully added ";
				}
				
				if($is_insert->success){
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_skill");
 				}
 				$mySession->errorMsg  = $is_insert->message;
   			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
  		 $this->view->form =$form;
  	}
	
  	/* Create a New Page   */
	public function addAction(){
		global $mySession; 
  		$this->view->pageHeading = "Create Page";
		$this->view->pageDescription = "add new page";
		$this->view->pageIcon = "fa fa-save";
		$this->view->showEditorUpload = "page";
 		$form = new Application_Form_StaticForm();
		$form->page();
		  		
 		if($this->getRequest()->isPost()) {
			$data_form = $this->getRequest()->getPost();
   			if($form->isValid($data_form)){
				$data_to_insert = $form->getValues() ;
				$is_insert = $this->modelStatic->add("pages",$data_to_insert);
				if($is_insert->success){
					$mySession->successMsg  = " Page Successfully Created ";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_static_pages");
 				}
 				$mySession->errorMsg  = $is_insert->message;
   			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
  		 $this->view->form =$form;
 		 $this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
  	}
	
 	/* Edit Page Information */
 	public function editAction(){
		global $mySession; 
		$this->view->pageHeading = "Edit Page Content";
		$this->view->pageDescription = "edit page content";
 		$this->view->pageIcon = "fa fa-save";
		$this->view->showEditorUpload = "page";
 		$page_id = $this->_getParam('page_id') ;
		$content =  $this->modelStatic->getPage($page_id);
		
  		if(!$content){
			$mySession->errorMsg = "No Such Page Found ";
			$this->_redirect("admin/pages");
		}
		
 		$form = new Application_Form_StaticForm();
		$form->page();
 		$form->populate($content);
 		if($this->getRequest()->isPost()) { 
			$posted_data = $this->getRequest()->getPost();
   			if($form->isValid($posted_data)){
  				$is_update = $this->modelStatic->add("pages",$form->getValues() ,"page_id=".$page_id);
				if($is_update->success){
					$mySession->successMsg  = " Page Content Successfully Updated ";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_static_pages");
				}
				$mySession->errorMsg  = $is_update->message;
    			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
		 
		 $this->view->page_content = $content;
		 $this->view->form =$form;
		 $this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
	}
	
	/* View Page Information */
	public function viewpageAction(){
		global $mySession; 
 		$this->view->pageHeading = " Page Information  ";
		$this->view->pageDescription = "view page information and delete page  ";
 		$this->view->pageIcon = "fa fa-save";
		$page_id = $this->_getParam('page_id') ;
		$content =  $this->modelStatic->getPage($page_id);
		
		if(!$content){
			$mySession->errorMsg = "No Such Page Found ";
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_static_pages");
		}
  		if($this->getRequest()->isPost()) { 
			$posted_data = $this->getRequest()->getPost();
 			if($form->isValid($posted_data)){
				$this->modelStatic->getAdapter()->delete("pages","page_id=".$page_id);
				$mySession->successMsg = " Page Successfully Deleted";
				$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_static_pages");
			}else{
				$mySession->errorMsg = "Please assign Request Admin";
			}
		 }
		 $this->view->page_content = $content;
	}
	
	public function getexpertiseAction()
	{
  		$this->dbObj = Zend_Registry::get('db');
		$aColumns = array('category_id','category_title','category_type');
		$sIndexColumn = 'category_id';
		$sTable = 'categories';
		/*Table Setting*/{
		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		}/* End Table Setting */
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM   $sTable 
		$sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/*
		 * Output
		 */
 		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
		$j=0;
		foreach($qry as $row1)
		{
			$row=array();
			/* Page Author Image */
 			$row[] = $j+1;
			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
  			$row[]=ucwords($row1['category_title']);
			if($row1['category_type']==0){
				$row[]='<label class="label label-success">Online Job</label>';
			}else{
				$row[]='<label class="label label-warning">Physical Job</label>';
			}
   			$row[]='<a class="btn default btn-xs blue" href="'.$this->view->url(array('id'=>$row1[$sIndexColumn]),'admin_add_expertise').'"><i class="fa fa-edit"></i> Edit </a>';
  			$output['aaData'][] = $row;
			$j++;
		}	
		echo json_encode( $output );
		exit();
 	
	}
	
	public function getskillsAction()
	{
  		$this->dbObj = Zend_Registry::get('db');
		
		$type=$this->_getParam('type');
		
		$aColumns = array('skill_id','skill_title','skill_status');
		$sIndexColumn = 'skill_id';
		$sTable = 'skills';
		/*Table Setting*/{
		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		}/* End Table Setting */
		
		
		if($type==1){
			$sWhere="where skill_status='1'";
		}
		else{
			$sWhere="where skill_status='0'";
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM   $sTable 
		$sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/*
		 * Output
		 */
 		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
		$j=0;
		foreach($qry as $row1)
		{
			$row=array();
			/* Page Author Image */
 			$row[] = $j+1;
			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
  			$row[]=ucwords($row1['skill_title']);
   			if($type==1){
				$row[]='<a class="btn default btn-xs blue" href="'.$this->view->url(array('id'=>$row1[$sIndexColumn]),'admin_add_skill').'"><i class="fa fa-edit"></i> Edit </a>';
			}
  			$output['aaData'][] = $row;
			$j++;
		}	
		echo json_encode( $output );
		exit();
 	
	}
	
 	/* Datatables Get Pages */
	public function getpagesAction(){
  		$this->dbObj = Zend_Registry::get('db');
		$aColumns = array('page_id','page_title','page_updated');
		$sIndexColumn = 'page_id';
		$sTable = 'pages';
		/*Table Setting*/{
		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		}/* End Table Setting */
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM   $sTable 
		$sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/*
		 * Output
		 */
 		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
		$j=0;
		foreach($qry as $row1)
		{
			$row=array();
			/* Page Author Image */
 			$row[] = $row1[ $aColumns[0] ];
			//$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
  			$row[]=ucwords($row1['page_title']);
			$row[]=date('d-F , Y',strtotime($row1['page_updated']));
   			$row[]='<a class="btn default btn-xs blue" href="'.APPLICATION_URL.'/admin/static/edit/page_id/'.$row1[$sIndexColumn].'"><i class="fa fa-edit"></i> Edit </a>';
			$row[]='<a class="btn default btn-xs blue" href="'.APPLICATION_URL.'/admin/static/viewpage/page_id/'.$row1[$sIndexColumn].'"><i class="fa fa-search"></i> View </a>';
  			$output['aaData'][] = $row;
			$j++;
		}	
		echo json_encode( $output );
		exit();
 	} 
	
	/* Remove Pages */
	public function  removepagesAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$formData = $this->getRequest()->getPost();
		if ($this->getRequest()->isPost() &&  isset($formData['pages']) && count($formData['pages'])) {
			$pages = implode(",",$formData['pages']) ;
  			$removed = $this->modelStatic->getAdapter()->delete('pages',"page_id IN (".$pages.")");
 			$mySession->successMsg=" Recored(s) Deleted Successfuly for the database.. ";
 		}
  		$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_static_pages");
 	}
	
	/* Remove Expertise */
	public function  removeexpertiseAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$formData = $this->getRequest()->getPost();
		if ($this->getRequest()->isPost() &&  isset($formData['categories']) && count($formData['categories'])) {
			$categories = implode(",",$formData['categories']) ;
  			$removed = $this->modelStatic->getAdapter()->delete('categories',"category_id IN (".$categories.")");
 			$mySession->successMsg=" Recored(s) Deleted Successfuly for the database.. ";
 		}
  		$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_expertise");
 	}
	
	/* Remove Expertise */
	public function  removeskillsAction(){
		global $mySession;
		
		$type=$this->_getParam('type');
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$formData = $this->getRequest()->getPost();
		if ($this->getRequest()->isPost() &&  isset($formData['skills']) && count($formData['skills'])) {
			$skills = implode(",",$formData['skills']) ;
  			$removed = $this->modelStatic->getAdapter()->delete('skills',"skill_id IN (".$skills.")");
 			$mySession->successMsg=" Recored(s) Deleted Successfuly from the database.. ";
 		}
		if($type==1)
  			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_skill");
		else
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_pending_skill");	
 	}
	
	public function  approveskillAction(){
		global $mySession;
		
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$formData = $this->getRequest()->getPost();
		if ($this->getRequest()->isPost() &&  isset($formData['skills']) && count($formData['skills'])) {
			$skills = implode(",",$formData['skills']) ;
  			$removed = $this->modelStatic->getAdapter()->update('skills',array("skill_status"=>1),"skill_id IN (".$skills.")");
 			$mySession->successMsg=" Skills Approved Successfuly";
 		}
		$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_pending_skill");	
 	}
	
	/* Manange All Content Blocks  */
	public function contentblockAction(){
		global $mySession;
		$this->view->pageHeading = "Content Block";
		$this->view->pageDescription = "manage content blocks";
		$this->view->pageIcon = "fa fa-copy";
	}
	
	/* Add Content Block */
  	public function addblockAction(){
		global $mySession; 
 		$this->view->pageHeading = "Add Content Block ";
		$this->view->pageDescription = "add new content block";
		$this->view->showEditorUpload = "contentBlock";
		$this->view->pageIcon = "fa fa-copy";
 		$form = new Application_Form_StaticForm();
		$form->content_block();
 		if($this->getRequest()->isPost()) {
			$data_form = $this->getRequest()->getPost();
  			if($form->isValid($data_form)){
				$data_to_insert = $form->getValues() ;
 				$is_insert = $this->modelStatic->add("content_block",$data_to_insert);
				if($is_insert->success){
					$mySession->successMsg  = " Content Block Successfully Created ";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_content_block");
 				}
				$mySession->errorMsg  = $is_insert->message;
  			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
 		 $this->view->form =$form;
		 $this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
 	}
  	
 	/* Edit Content Blocks  */
	public function editcontentblockAction(){
		global $mySession; 
		$this->view->pageHeading = "Add Content Block ";
		$this->view->pageDescription = "add new content block";
		$this->view->pageIcon = "fa fa-copy";
		$this->view->showEditorUpload = "contentBlock";
		
		$content_block_id = $this->_getParam('content_block_id') ;
		$content =  $this->modelStatic->getContentBlock($content_block_id);
		if(!$content){
			$mySession->errorMsg = "No Such Page Found ";
			$this->_redirect("admin/pages/contentblock");
		}
  		$form = new Application_Form_StaticForm();
		$form->content_block();
		$form->populate($content);
		
 		if($this->getRequest()->isPost()) { 
			$posted_data = $this->getRequest()->getPost();
  			if($form->isValid($posted_data)){
				$data_to_insert = $form->getValues();
				$is_update = $this->modelStatic->add('content_block' , $data_to_insert ,"content_block_id =".$content_block_id);
				if($is_update->success){
					$mySession->successMsg  = " Content Block Successfully Updated ";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_content_block");
				}
 				$mySession->errorMsg  = $is_update->message;
   			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
 		 $this->view->page_content = $content;
 		 $this->view->form =$form;
		 $this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
 	}
	
	/* View Content Block  */
	public function viewblockAction(){
		global $mySession; 
 		$this->view->pageHeading = " Content Block  ";
		$this->view->pageDescription = "view content block information ";
		$this->view->pageIcon = "fa fa-copy";
		$content_block_id = $this->_getParam('content_block_id') ;
		$content =  $this->modelStatic->getContentBlock($content_block_id);
		if(!$content){
			$mySession->errorMsg = "No Such Page Found ";
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_content_block");
		}		 
 		if($this->getRequest()->isPost()) { 
			$posted_data = $this->getRequest()->getPost();
			$approval_required = false; 
			if($content['content_block_status']){
				/* Direct Delete */
				$form->request_admin->setRequired(true);
				$approval_required = true;
			}
 			if($form->isValid($posted_data)){
				if(!$approval_required){
  					$this->modelStatic->getAdapter()->delete("content_block","content_block_id=".$content_block_id);
					$mySession->successMsg = " Content Block Successfully Deleted";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_content_block");
				}else{
					/* Approval is needed */
					$modelRequest = new Application_Model_Request();
 					$data = $form->getValues();
 					$content['request_admin'] = $data['request_admin']; 
		 			$content['block_to_delete'] = $content['content_block_id']; /* Which Block Needs to Update*/ ;
 					$is_added = $modelRequest->addRequest($content , DELETE_CONTENT_BLOCK , $this->view->admin->user_id /* Author */ );
					 if($is_added->success){
						 $mySession->successMsg = " Request is Successfully Sent to Request Admin. ";
						 $this->_redirect('admin/pages/contentblock');
					 }else{
						  $mySession->errorMsg = $is_added->message;
					 }
  				}
				$mySession->errorMsg  = $is_update->message;
				
 				if(false){
 					/* Approval is needed */
					$modelRequest = new Application_Model_Request();
 					$data = $form->getValues();
 					$content['request_admin'] = $data['request_admin']; 
		 			$content['block_to_delete'] = $content['content_block_id']; /* Which Block Needs to Update*/ ;
 					$is_added = $modelRequest->addRequest($content , DELETE_CONTENT_BLOCK , $this->view->admin->user_id /* Author */ );
					 if($is_added->success){
						 $mySession->successMsg = " Request is Successfully Sent to Request Admin. ";
						 $this->_redirect('admin/pages/contentblock');
					 }else{
						  $mySession->errorMsg = $is_added->message;
					 }
				}/* end false */
			}else{
				
				$mySession->errorMsg = "Please assign Request Admin";
			}
		 }
 		 $this->view->page_content = $content;
   	}
	
 	/* Remove removeblock */
	public function  removeblockAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$formData = $this->getRequest()->getPost();
		if ($this->getRequest()->isPost() &&  isset($formData['content_block']) && count($formData['content_block'])) {
			$pages = implode(",",$formData['content_block']) ;
  			$removed = $this->modelStatic->getAdapter()->delete('content_block',"content_block_id IN (".$pages.")");
 			$mySession->successMsg=" Recored(s) Deleted Successfuly for the database.. ";
 		}
  		$this->_helper->getHelper("Redirector")->gotoRoute(array(),"admin_content_block");
 	}
	
	/* Datatable Get Content Blocks */
	public function getblocksAction(){
  		$this->dbObj = Zend_Registry::get('db');
		$aColumns = array('content_block_id','content_block_title','content_block_updated');
		$sIndexColumn = 'content_block_id';
		$sTable = 'content_block';
		/*Table Setting*/{
		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		}/* End Table Setting */
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		
		/** Output*/
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
		
		$j=1;
		foreach($qry as $row1)
		{
			$row=array();
			/* Page Author Image */
			$row[] = $j.".";
			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
  			$row[]=ucwords($row1['content_block_title']);
			$row[]=date('d-F , Y',strtotime($row1['content_block_updated']));
   			$row[]='<a class="btn default btn-xs blue" href="'.APPLICATION_URL.'/admin/static/editcontentblock/content_block_id/'.$row1[$sIndexColumn].'"><i class="fa fa-edit"></i> Edit </a>';
			$row[]='<a class="btn default btn-xs blue" href="'.APPLICATION_URL.'/admin/static/viewblock/content_block_id/'.$row1[$sIndexColumn].'"><i class="fa fa-search"></i> View </a>';
  			$output['aaData'][] = $row;
			$j++;
		}	
		
		echo json_encode( $output );
		exit();
 	} 
	/*
	 *	Graphic Media
	*/
	public function graphicmediaAction(){
 		global $mySession; 
		$this->view->pageHeading = " Manage Graphic Media ";
		$this->view->pageDescription = "manage all images ";
		$all_media = $this->modelStatic->getMedia();
		$this->view->all_media = $all_media ;
  	}
	/*
	 * Add Graphic Media
 	 */
	public function addgraphicmediaAction(){
		global $mySession;
		$this->view->pageHeading = "Add Graphic Media";
		$this->view->pageDescription = "add new graphic media";
		$form = new Application_Form_StaticForm();
		$form->graphic_media();

   		if($this->getRequest()->isPost()){
			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
  				$media_path = $this->_handle_uploaded_image(); /* Receive Uploaded File */
				if(is_object($media_path) and $media_path->success){
					$inserted_data = $form->getValues();
					$inserted_data['media_path'] = $media_path->media_path;
					$insertedMedia = $this->modelStatic->add("graphic_media",$inserted_data); // Save Style in db
					if(is_object($insertedMedia) and $insertedMedia->success){
						$mySession->successMsg = "Graphic Media Item Successfully added "; // Sucessss
						$this->_redirector = $this->_helper->getHelper('Redirector')->gotoRoute(array(),'admin_graphic_media');
 					}
 				}
 				$mySession->errorMsg = $media_path->message; 
			}else{
 				$mySession->errorMsg = " Please Enter Correct Information ..! ";
 			}
		}
		$this->view->form = $form;
		$this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
	}
 	/*
	 * Edit Graphic Media
	 */
	public function editgraphicmediaAction(){
		global $mySession;
 		$this->view->pageHeading = "Edit Media Information ";
		$this->view->pageDescription = "Edit Media information ";
		$media_id =$this->_getParam('media_id') ;
		$form = new Application_Form_StaticForm();
		$form->graphic_media();
  		$media_info = $this->modelStatic->getMedia($media_id);
 		if(!$media_info){
			$mySession->infoMsg = "No Such Media Item  Exists in the database...!";
			$this->_redirect('admin/Media');
		}
		
		$form->populate($media_info);
 		 if($this->getRequest()->isPost()){
			 $posted_data = $this->getRequest()->getPost();
 			 if($form->isValid($posted_data)){
 				$media_path = $this->_handle_uploaded_image();
				if($media_path->success){
					$data_array = $form->getValues();
					if($media_path->media_path){
 						$data_array['media_path'] = $media_path->media_path ;
					}else{
						unset($data_array['media_path']);
					}
 					$is_update = $this->modelStatic->add("graphic_media",$data_array,"media_id=".$media_id); // Save Style in db
					if(is_object($is_update) and $is_update->success){
						if(isset($data_array['media_path'])){
							$this->_unlink_media_image($media_info['media_path']);
						}
						$mySession->successMsg = "Media Information Successfully Updated "; // Sucessss
						$this->_helper->getHelper('Redirector')->gotoRoute(array(),'admin_graphic_media');
					} 
 					$mySession->errorMsg = $is_update->message; 
 				}
				$mySession->errorMsg = $media_path->message; 
			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
		 $this->view->form =  $form;
		 /* User Add Style Page to Render */
		$this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
 	} 	
 	/* 
	 *	Remove Graphic Media 
	 */
 	public function deletegraphicmediaAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$media_id  = $this->_getParam("media_id");
		$media_info = $this->modelStatic->getMedia($media_id);
		if(empty($media_info)){
			$mySession->errorMsg = "No Such Graphic Media Found in the database";
			$this->_helper->getHelper('Redirector')->gotoRoute(array(),'admin_graphic_media');
 		}
		$this->_unlink_media_image($media_info['media_path']);
 		$removed = $this->modelStatic->getAdapter()->delete("graphic_media","media_id IN (".$media_id.")");
		$mySession->successMsg = "Graphic Media Successfully Deleted From the Database ";
		$this->_helper->getHelper('Redirector')->gotoRoute(array(),'admin_graphic_media');
	}
	/* 
	 *	Delete graphic Media Images 
	 */
	private function _unlink_media_image($image){
  		if($image!="" and file_exists(MEDIA_IMAGES_PATH."/".$image)){
			unlink(MEDIA_IMAGES_PATH."/".$image);
			unlink(MEDIA_IMAGES_PATH."/300/".$image);
		}
		return true ;
	}
	/* 
	 *	Handle The Uploaded Images For Graphic Media  
	 */
	private function _handle_uploaded_image(){
 		global $mySession; 
		$uploaded_image_names = array();
		$adapter = new Zend_File_Transfer_Adapter_Http();
		$files = $adapter->getFileInfo();
		$uploaded_image_names = array();
		$new_name = false; 
  		/*prd($adapter);*/
		foreach ($files as $file => $info) { /* Begin Foreach for handle multiple images */
  			$name_old = $adapter->getFileName($file);
			if(empty($name_old)){
				continue ;			
			}
			$file_title  = $adapter->getFileInfo($file);
			$file_title = $file_title[$file]['name']; 
  			$uploaded_image_extension = getFileExtension($name_old);
 			$file_title  = str_replace(".".$uploaded_image_extension,"",$file_title);
			$file_title = formatImageName($file_title);
 			$new_name = $file_title."-".time()."-".rand(1,100000).".".$uploaded_image_extension;
 			$adapter->addFilter('Rename',array('target' => MEDIA_IMAGES_PATH."/".$new_name));
			try{
				$adapter->receive($file);
			}
			catch(Zend_Exception $e){
				return (object) array('success'=>false,"error"=>true,'exception'=>true,'message'=>$e->getMessage(),'exception_code'=>$e->getCode()) ;
			}
			$thumb_config = array("source_path"=>MEDIA_IMAGES_PATH,"name"=> $new_name);
			Application_Plugin_ImageCrop :: uploadThumb(array_merge($thumb_config,array("destination_path"=>MEDIA_IMAGES_PATH."/300","crop"=>true ,"size"=>300,"ratio"=>false)));
 			//$uploaded_image_names[]=array('media_path'=>$new_name); => For Multiple Images
		}/* End Foreach Loop for all images */
		return (object)array("success"=>true,'error'=>false,"message"=>"Image(s) Successfully Uploaded","media_path"=>$new_name) ;
 	}
	
	
	
		/* Subadmin  */
	public function subuserAction(){
		global $mySession,$BlockedArray;
		$this->view->pageHeading = "Manage Sub Admin";
		$this->view->pageDescription = "manage sub admin";
		$this->view->pageIcon = "fa fa-group";
		if($this->view->user->user_type=='subadmin' && in_array('Manage Categories',$BlockedArray)){
				$this->_redirect('admin/index/index');
		}
    }
	
	
	public function addsubuserAction(){
		
		global $mySession; 
 
  		$form = new Application_Form_StaticForm();
		$user_id =$this->_getParam('user_id') ;
		$this->view->user_id=$user_id;
		if($user_id!=''){
			$form->subadmin($user_id);
			$Type="Edit";$page_type="edit";$msgType="Updated";
			$option_info = $this->modelStatic->Super_Get("users","user_id='".$user_id."'","fetchAll");
			if(!$option_info){
				$mySession->infoMsg = "No Such Sub Admin Exists in the database...!";
				$this->_redirect('/admin/manage-subuser');
			}
			$form->populate($option_info[0]);
		}
		else{
			$form->subadmin();
			$Type="Add";$page_type="add";$msgType="Added";
		}
		$this->view->pageHeading = $Type." Sub Admin";
		$this->view->pageDescription = $page_type." sub admin";
		$this->view->pageIcon = "fa fa-group";
		
		
		  		
 		if($this->getRequest()->isPost()) {
 			
			$data_form = $this->getRequest()->getPost();
			//prd($data_form);
			
   			if($form->isValid($data_form)){
				$data_to_insert = $form->getValues() ;
				$password=$data_to_insert['user_password'];
				if($data_to_insert['user_password']!='')
					$data_to_insert['user_password']=md5($data_to_insert['user_password']);
				else
					$data_to_insert['user_password']=($option_info[0]['user_password']);			
				
				
				$data_to_insert['user_status']=1;			 
				$data_to_insert['user_created']=date('Y-m-d H:i:s');
				$data_to_insert['user_type']='admin';
				
				unset($data_to_insert['user_rpassword']);
				unset($data_to_insert['bttnsubmit']);
				
				//prd($data_to_insert);
				if($user_id==''){
					$is_insert = $this->modelStatic->Super_Insert("users",$data_to_insert);
					$reset_password_key = md5($is_insert->inserted_id."!@#$%^$%&(*_+".time());
					$data_to_update = array("user_reset_status"=>"1","pass_resetkey"=>$reset_password_key);
					$this->modelStatic->Super_Insert('users',$data_to_update, 'user_id = '.$is_insert->inserted_id);
					
					$data_to_insert['pass_resetkey'] = $reset_password_key ;
					$data_to_insert['user_reset_status'] = "1" ;
					$data_to_insert['last_inserted_id'] = $is_insert->inserted_id;
					$data_to_insert['user_new_password'] = $password;
					//prd($data_to_insert);
					
					$isSend = $this->modelEmail->sendEmail('subadmin_registration_email',$data_to_insert);
				}
				else{
					$this->modelStatic->Super_Insert('users',$data_to_insert, "user_id = '".$user_id."'");
					
				}
				//prd($data_form);	
					$mySession->successMsg  = "Sub Admin Successfully ".$msgType." ";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"manage_subuser");
 				
   			}else{
				$mySession->errorMsg = " Please Check Information Again ... ! ";
 			}
		 }
  		 $this->view->form =$form;
 		 $this->_helper->getHelper('viewRenderer')->renderScript("add.phtml");
  	}
	
	
	public function getsubadminsAction(){
		
  		$this->dbObj = Zend_Registry::get('db');
  
		$aColumns = array('user_id','user_first_name','user_last_name','user_email','user_created','user_status','user_role');

		$sIndexColumn = 'user_id';
		$sTable = 'users';
  		
		
		/*Table Setting*/{
		
		/* 
		 * Paging
		 */
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		
		/*
		 * Ordering
		 */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		
		
		}/* End Table Setting */
		
 		
		if(!$sWhere)
		   $sWhere="where user_role!='0' and user_email!='".$this->view->user->user_email."'";
		  else 
		   $sWhere.=" and user_role!='0' and user_email!='".$this->view->user->user_email."'"; 
   
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable $sWhere ";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		
		/*
		 * Output
		 */
		 
		 
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
		
		$j=0;
		foreach($qry as $row1)
		{
			$row=array();
			
			/* Page Author Image */
 		 
			$row[] =($j+1).'.';
			
			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
						
  			$row[]=ucwords($row1['user_first_name']);
			$row[]=ucwords($row1['user_last_name']);
			$row[]=ucwords($row1['user_email']);
			switch($row1['user_role']){
				case 1: $roleText="FULL ADMIN";$roleClass="primary";break;
				case 2: $roleText="FORUM ADMIN";$roleClass="warning";break;
			}
			
			$row[]="<label class='label label-".$roleClass."'>".$roleText."</label>";
			$row[]=date('d F, Y',strtotime($row1['user_created']));
			
			$status = $row1['user_status']!=1?"checked='checked'":" ";
 			$row[]='<div class="danger-toggle-button">
						<input type="checkbox" class="toggle status-'.(int)$row1['user_status'].' "  '.$status.'  id="'.$sTable.'-'.$row1[$sIndexColumn].'" onChange="globalStatus(this)" />
					</div>';
			
			
     	 		 
   			$row[]='<a class="btn default btn-xs purple" href="'.$this->view->url(array('user_id'=>$row1[$sIndexColumn]),'edit_subuser',true).'"><i class="fa fa-edit"></i> Edit </a>';
			
  			$output['aaData'][] = $row;
			$j++;
		}	
		
		echo json_encode( $output );
		exit();
 	} 
	
	/* Remove Pages */
	public function  removesubuserAction(){
		
		global $mySession;
		
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		 
		$formData = $this->getRequest()->getPost();
 	 
		if ($this->getRequest()->isPost() &&  isset($formData['users']) && count($formData['users'])) {
			
			$pages = implode(",",$formData['users']) ;
			
  			$removed = $this->modelStatic->getAdapter()->delete('users',"user_id IN (".$pages.")");
 			$mySession->successMsg=" Sub Admin(s) Deleted Successfuly for the database.. ";
 		}
  		$this->_helper->getHelper("Redirector")->gotoRoute(array(),"manage_subuser");
 	}
}