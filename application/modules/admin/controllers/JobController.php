<?php
class Admin_JobController extends Zend_Controller_Action
{
    public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->view->pageIcon = "fa  fa-users";
    }
 	
 	public function indexAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Manage All Jobs";
		$this->view->pageDescription = "manage all jobs ";
		$this->view->request_type = "all";
 	}

	public function sponsoredjobsAction(){
		global $mySession;
		$this->view->pageHeading = "Manage Sponsored Jobs";
		$this->view->pageDescription = "manage sponsored jobs ";
		$this->view->request_type = "all";

	}
	public function jobapplicationsAction()
	{
		global $mySession; 
		$job_id = $this->getRequest()->getParam('job_id');
		$this->view->job_id = $job_id;
 		$this->view->pageHeading = "Manage All Job Applications";
		$this->view->pageDescription = "manage all job applications ";
	}

	public function sponsoredjobdetailsAction()
	{
		global $objSession;
		$job_id = $this->getRequest()->getParam('job_id');
		$joinArr=array(
			'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
			'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('')),
			'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
			'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name")),
		);
		$job = $this->modelStatic->Super_Get('job',"job.job_id='".$job_id."'","fetch",$extra=array('group'=>'job.job_id'),$joinArr);
		$this->view->job = $job;

		/* Check if user has already applied for this job - Starts */
		$getAppData = $this->modelStatic->Super_Get('job_applications',"job_app_job_id='".$job_id."' and job_app_user_id='".$this->view->user->user_id."'",'fetch');
		$this->view->getAppData = $getAppData;
		/* Check if user has already applied for this job - Ends */
	}

	public function jobdetailsAction()
	{
		global $objSession;
		$job_id = $this->getRequest()->getParam('job_id');
		$joinArr=array(
			'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
			'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('')),
			'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
			'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name")),
		);
		$job = $this->modelStatic->Super_Get('job',"job.job_id='".$job_id."'","fetch",$extra=array('group'=>'job.job_id'),$joinArr);
		$this->view->job = $job;
		
		/* Check if user has already applied for this job - Starts */
			$getAppData = $this->modelStatic->Super_Get('job_applications',"job_app_job_id='".$job_id."' and job_app_user_id='".$this->view->user->user_id."'",'fetch');
			$this->view->getAppData = $getAppData;
		/* Check if user has already applied for this job - Ends */
	}
	
 	/* Ajax Call For Get Users */
  	public function getjobsAction(){
		$this->dbObj = Zend_Registry::get('db');

 		$aColumns = array(
			'job_id','job_user_id','job_title','job_closing_date','job_added_date','job_status' ,'user_first_name','user_last_name'
  		);
		$sIndexColumn = 'job_id';
		$sTable = 'job';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join users on user_id=job_user_id $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable  join users on user_id=job_user_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[]=$row1['job_title']."<br><a href='".$this->view->url(array('module'=>'admin','controller'=>'job','action'=>'jobapplications','job_id'=>$row1[$sIndexColumn]),'default',true)."'>View Applications</a>";
			
   			$row[]=$row1['user_first_name']."&nbsp;".$row1['user_last_name'];
			$row[]=date('M d, Y',strtotime($row1['job_added_date']));
			$row[]=date('M d, Y',strtotime($row1['job_closing_date']));
			$status = $row1['job_status']!=1?"checked='checked'":" ";
 			$row[]='<div class="danger-toggle-button"><input type="checkbox" class="toggle status-'.(int)$row1['job_status'].' "  '.$status.'  id="'.$sTable.'-'.$row1[$sIndexColumn].'" onChange="globalStatus(this)" /></div>';
			$row[] = '<a class="btn mini green-stripe btn-success btn-sm" href="'.APPLICATION_URL.'/admin/job/jobdetails/job_id/'.$row1[$sIndexColumn].'" style="margin-top: 4px;">
			View</a>';
 			$output['aaData'][] = $row;
			$j++;
		$i++;
		}
		echo json_encode( $output );
		exit();
  	}

	public function getsponsoredjobsAction(){
		$this->dbObj = Zend_Registry::get('db');

		$aColumns = array(
			'job.job_id','job_user_id','job_title','job_closing_date','job_added_date','job_status' ,'user_first_name','user_last_name'
		);
		$sIndexColumn = 'job_id';
		$sTable = 'job';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}

			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/*
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}

		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		if(empty($sWhere))
		$sWhere="where job_subscriptions.subscription_id=2";

		//$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join users on user_id=job_user_id $sWhere $sOrder $sLimit";
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join users on user_id=job_user_id join job_subscriptions on job_subscriptions.job_id = job.job_id $sWhere  $sOrder $sLimit";
		$qry = $this->dbObj->query($sQuery)->fetchAll();
		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll();
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(job.job_id) as cnt FROM $sTable  join users on user_id=job_user_id join job_subscriptions on job_subscriptions.job_id=job.job_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll();
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
			$row=array();
			$row[] = $i;
			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[]=$row1['job_title'];

			$row[]=$row1['user_first_name']."&nbsp;".$row1['user_last_name'];
			$row[]=date('M d, Y',strtotime($row1['job_added_date']));
			$row[]=date('M d, Y',strtotime($row1['job_closing_date']));
			$status = $row1['job_status']!=1?"checked='checked'":" ";
			$row[]='<div class="danger-toggle-button"><input type="checkbox" class="toggle status-'.(int)$row1['job_status'].' "  '.$status.'  id="'.$sTable.'-'.$row1[$sIndexColumn].'" onChange="globalStatus(this)" /></div>';
			$row[] = '<a class="btn mini green-stripe btn-success btn-sm" href="'.APPLICATION_URL.'/admin/job/sponsoredjobdetails/job_id/'.$row1[$sIndexColumn].'" style="margin-top: 4px;">
			View</a>';
			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
	}

	public function getjobapplicationsAction(){
		
		$this->dbObj = Zend_Registry::get('db');
		$job_id = $this->getRequest()->getParam('job_id');
 		$aColumns = array(
			'job_app_id','job_app_message','job_app_status','job_app_date','job_app_user_id' ,'user_first_name','user_last_name'
  		);
		$sIndexColumn = 'job_app_id';
		$sTable = 'job_applications';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
		if($sWhere){
			$sWhere = $sWhere." and job_app_job_id='".$job_id."'";
		}else{
			$sWhere = " where job_app_job_id='".$job_id."'";
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable join users on user_id=job_app_user_id $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable join users on user_id=job_app_user_id $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
			$row[]=$row1['job_app_message'];
			
   			$row[]=$row1['user_first_name'];
			$row[]=date('M d, Y',strtotime($row1['job_app_date']));
			if($row1['job_app_status']=='0'){
				$row[] = '<label class="btn mini green-stripe btn-warning btn-sm" style="margin-top: 4px;">Not Assigned</label>';
			}else{
				$row[] = '<label class="btn mini green-stripe btn-success btn-sm" style="margin-top: 4px;">Assigned</label>';
			}
			
 			$output['aaData'][] = $row;
			$j++;

		$i++;
		}
		echo json_encode( $output );
		exit();
  	
	}
	
	public function removejobAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if(isset($formData['job']) and count($formData['job'])){
				 foreach($formData['job'] as $key=>$values){
   					 $user_info = $this->modelUser->get($values);
					
					 /*if(empty($user_info))
						continue ;*/
						// prd($this->modelUser->Super_Delete("subscription","sub_id IN ($values)"));
					$removed = $this->modelUser->Super_Delete("job","job_id IN ($values)");
				 }
 				$mySession->successMsg = " Job Deleted Successfully ";
				
 			}else{
				$mySession->errorMsg = " Invalid Request to Delete job(s) ";
			}
 			$this->_redirect('/admin/job/index?removed=1');	 
		} 
 	}

	public function removesponsoredjobAction(){
		global $mySession;
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if(isset($formData['job']) and count($formData['job'])){
				foreach($formData['job'] as $key=>$values){
					$user_info = $this->modelUser->get($values);

					/*if(empty($user_info))
                       continue ;*/
					// prd($this->modelUser->Super_Delete("subscription","sub_id IN ($values)"));
					$removed = $this->modelUser->Super_Delete("job","job_id IN ($values)");
				}
				$mySession->successMsg = " Job Deleted Successfully ";

			}else{
				$mySession->errorMsg = " Invalid Request to Delete job(s) ";
			}
			$this->_redirect('/admin/job/sponsoredjobs?removed=1');
		}
	}
}