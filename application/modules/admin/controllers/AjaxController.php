<?php
class Admin_AjaxController extends Zend_Controller_Action
{
 
    public function init(){ 
		
	}
	
 
  
  
	public function setstatusAction() {
		 
		 $params = $this->getRequest()->getParams() ;
 		 
 		 $db=Zend_Registry::get("db") ;
		 
		 $data = array(			
					"users"=>array(
					'table'      => "users",
					'field_id'      => "user_id",
					'field_status'      => 'user_status'),
					
					"job"=>array(
					'table'      => "job",
					'field_id'      => "job_id",
					'field_status'      => 'job_status'),
					
					"slider_images"=>array(
					'table'      => "slider_images",
					'field_id'      => "slider_image_id",
					'field_status'      => 'slider_image_status'),
					
					"forum_topics"=>array(
					'table'      => "forum_topics",
					'field_id'      => "forum_id",
					'field_status'      => 'forum_status'),
					
					"forum_approved_topics"=>array(
					'table'      => "forum_topics",
					'field_id'      => "forum_id",
					'field_status'      => 'forum_approved_status'),
					
					"users_role"=>array(
					'table'      => "users",
					'field_id'      => "user_id",
					'field_status'      => 'user_role'),
					
					
					
					
					
  			);
			
		 	if(isset($data[$params['type']])){
				$update_data=array($data[$params['type']]['field_status']=>$params['status']) ;
				try{
					$updated = $db->update($data[$params['type']]['table'], $update_data, $data[$params['type']]['field_id'].' = '.$params['id']);	
					echo json_encode(array("success"=>true,"error"=>false,"message"=> /*" Status of ".*/ucwords(str_replace("_"," ",$data[$params['type']]['field_status']))." Successfully Updated "  ));
				}catch(Zend_Exception $e){
					echo json_encode(array("success"=>false,"error"=>true,"exception"=>true,"exception_code"=>$e->getCode(),"message"=>$e->getMessage() ));
				}
			}else{
				echo json_encode(array("success"=>false,"error"=>true,"exception"=>false,"message"=>"Table Not Defined for the Current Request" ));
			}
			

				
			
 			exit();
	}
	
	public function userrightsAction()
	{
		$params = $this->getRequest()->getParams() ;
 		 
 		 $db=Zend_Registry::get("db") ;
		 
		 $data = array(			
					"users_role"=>array(
					'table'      => "users",
					'field_id'      => "user_id",
					'field_status'      => 'user_role'),
  			);
			
		 	if(isset($data[$params['type']])){
				$update_data=array($data[$params['type']]['field_status']=>$params['status'],'admin_rights'=>1) ;
				try{
					$updated = $db->update($data[$params['type']]['table'], $update_data, $data[$params['type']]['field_id'].' = '.$params['id']);	
					echo json_encode(array("success"=>true,"error"=>false,"message"=> /*" Status of ".*/ucwords(str_replace("_"," ",$data[$params['type']]['field_status']))." Successfully Updated "  ));
				}catch(Zend_Exception $e){
					echo json_encode(array("success"=>false,"error"=>true,"exception"=>true,"exception_code"=>$e->getCode(),"message"=>$e->getMessage() ));
				}
			}else{
				echo json_encode(array("success"=>false,"error"=>true,"exception"=>false,"message"=>"Table Not Defined for the Current Request" ));
			}
		
		$update_data=array($data[$params['type']]['field_status']=>$params['status']) ;
		exit();
	}
	
 	
}

