<?php
class Admin_UserController extends Zend_Controller_Action
{
    public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->view->pageIcon = "fa  fa-users";
    }
 	
 	public function indexAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Manage All Users";
		$this->view->pageDescription = "manage all site users ";
		$this->view->request_type = "all";
 	}
 	
	public function verifiedAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Manage All Users";
		$this->view->pageDescription = "manage all site users ";
		$this->view->request_type = "verified";
 		$this->render("index");
 	}
 	
	public function blockedAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Manage All Users";
		$this->view->pageDescription = "manage all site users ";
		$this->view->request_type = "blocked";
		$this->render("index");
 	}
	
	public function subscriptionAction(){
 		global $mySession; 
 		$this->view->pageHeading = "Add Ons Management";
		$this->view->pageDescription = "add ons management";
 	}

	public function voucherAction(){
		global $mySession;
		$this->view->pageHeading = "Manage Voucher";
		$this->view->pageDescription = "manage voucher";
	}

	public function transactionAction(){
		global $mySession;
		$this->view->pageHeading = "Transaction Management";
		$this->view->pageDescription = "view transaction history";
	}

	/* View Page Information */
	public function viewtransactionAction(){
		global $mySession;
		$this->view->pageHeading = "Transaction Management";
		$this->view->pageDescription = "view transaction history";
		$this->view->pageIcon = "fa fa-money";
		$transaction_id = $this->_getParam('transaction_id') ;

		$transactionDetails = $this->modelStatic->Super_Get('transactions_history',"id='".$transaction_id."'", "fetch");
		$user = $this->modelStatic->Super_Get('users',"user_id='".$transactionDetails['user_id']."'", "fetch");
		$transactionDetails['user_id'] = ucwords($user['user_last_name'].' '.$user['user_first_name']);
		$transactionDetails['metadata'] = json_decode($transactionDetails['metadata']);
		$this->view->transactionDetails = $transactionDetails;
	}

	public function viewsubscriptionAction()
	{
		global $objSession;
		$this->_helper->layout->disableLayout();
		$userId = $this->getRequest()->getParam('userId');
		$joinArr = array(
			'0' => array(
				'0'=>'subscriptions',
				'1'=>'users.user_subscription = subscriptions.subscription_id',
				'2'=>'left',
				'3'=>array('subscription_id','subscription_title','subscription_desc','subscription_price','subscription_added_date')
				),
		);
		
		$Data = $this->modelStatic->Super_Get('users',"user_id='".$userId."'",'fetch',$extra=array(),$joinArr);
		$this->view->Data = $Data;
	}

	public function addvoucherAction()
	{
		global $mySession;
		$form = new Application_Form_StaticForm();
		$form->voucher();
		$id = $this->getRequest()->getParam('id');
		$VoucherData = $this->modelUser->Super_Get('voucher',"id='".$id."'");
		if(!empty($VoucherData)){
			$this->view->pageHeading = "Edit Voucher";
			$form->populate($VoucherData);
		}
		else{
			$this->view->pageHeading = "Add Voucher";
		}
		$this->view->form = $form;

		if($this->getRequest()->isPost()){
			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
				$data = $form->getValues();
				$data['created_at']=date('Y-m-d H:i:s');
				$data['expire_date'] = date("Y-m-d", strtotime($data['expire_date']));
				if($id!=''){
					$mySession->successMsg = "Voucher updated Successfully";
					$this->modelUser->Super_Insert('voucher',$data,"id='".$id."'");
				}else{
					$mySession->successMsg = "Voucher added Successfully";
					$this->modelUser->Super_Insert('voucher',$data);
				}

				$this->_redirect('admin/user/voucher');
			}
		}
	}
	public function addsubscriptionAction()
	{
		global $mySession;
		$form = new Application_Form_StaticForm();
		$form->subscription();
		$sub_id = $this->getRequest()->getParam('sub_id');
		$SubscriptionData = $this->modelUser->Super_Get('subscription',"sub_id='".$sub_id."'");
		if(!empty($SubscriptionData)){
			$form->populate($SubscriptionData);
		}
		$this->view->form = $form;
		
		if($this->getRequest()->isPost()){
			$posted_data = $this->getRequest()->getPost();
  			if($form->isValid($posted_data)){
				$data = $form->getValues();
				$data['sub_added_date']=date('Y-m-d H:i:s');
				if($sub_id!=''){
					$mySession->successMsg = "Update Successfully";
					$this->modelUser->Super_Insert('subscription',$data,"sub_id='".$sub_id."'");
				}else{
					$mySession->successMsg = "Add Successfully";
					$this->modelUser->Super_Insert('subscription',$data);
				}
				
				$this->_redirect('admin/user/subscription');
			}
		}
	}

	public function accountAction(){
		global $mySession; 
		$user_id =  $this->_getParam("user_id");
		/* Getting user details-Starts */
		$joinArr=array(
			'0' => array('0'=>'user_details','1'=>'users.user_id = user_details.ud_user_id','2'=>'left',
			'3'=>array('ud_user_id','ud_contact_no','ud_contact_via','ud_location','ud_intrested_in','ud_experience','ud_worker_company','ud_worker_about','ud_looking_for',
			'ud_exp_required','ud_hirer_company','ud_hirer_about','ud_pitch')),
		);
		$UserDetails = $this->modelStatic->Super_Get('users',"user_id='".$user_id."'","fetch",$extra=array(),$joinArr);
		$this->view->UserDetails = $UserDetails;
		/* Getting user details-Ends */
		if(empty($UserDetails)){
			$mySession->errorMsg = "No Such User Found , Invalid Request .";
			$this->_redirect("admin");
		}
		$this->view->pageHeading = ucwords($UserDetails['user_first_name']."'s Profile ");
		$this->view->pageDescription = ucwords("View All Information about ".$UserDetails['user_first_name']);
		
		/* Getting all the jobs user has applied for - Starts */
		$joinArr=array(
			'0' => array('0'=>'job','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_id','job_title')),
		);
		$WorkJobs = $this->modelStatic->Super_Get('job_applications',"job_app_user_id='".$user_id."'","fecthAll",$extra=array(),$joinArr);
		$this->view->WorkJobs = $WorkJobs;
		/* Getting all the jobs user has applied for - Ends */
		
		/* Getting all the jobs user has posted - Starts */
		$HireJobs = $this->modelStatic->Super_Get('job',"job_user_id='".$user_id."'","fecthAll");
		$this->view->HireJobs = $HireJobs;
		/* Getting all the jobs user has posted - Ends */
		
		/* Getting user rating and votes - Starts */
		$extra=array('fields'=>array('rate'=>'SUM(job_rate)','count'=>'COUNT(job_rating_id)'));
		$RateData = $this->modelStatic->Super_Get('job_rating',"job_rate_for='".$user_id."'",'fetch',$extra);
		$this->view->RateData = $RateData;
		
		$extra=array('fields'=>array('thumbsUp'=>'COUNT(job_vote)'));
		$ThumbUpData = $this->modelStatic->Super_Get('job_rating',"job_rate_for='".$user_id."' and job_vote='1'",'fetch',$extra);
		$this->view->ThumbUpData = $ThumbUpData;
		
		$extra=array('fields'=>array('thumbsDown'=>'COUNT(job_vote)'));
		$ThumbDownData = $this->modelStatic->Super_Get('job_rating',"job_rate_for='".$user_id."' and job_vote='-1'",'fetch',$extra);
		$this->view->ThumbDownData = $ThumbDownData;
		/* Getting user rating and votes - Ends */
		
		/*if($user_information['user_type']=='service_provider'){
			$SubscriptionInfo = $this->modelUser->Super_Get('subscription',"sub_id='".$user_information['user_subscription']."'",'fetch');
			$this->view->SubscriptionInfo=$SubscriptionInfo;
			$SPdata = $this->modelUser->Super_Get('service_provider',"sp_user_id='".$user_information['user_id']."'",'fetch');
			$this->view->SPdata=$SPdata;
		}else{
			$EmployerData = $this->modelUser->Super_Get('employer',"e_user_id='".$user_information['user_id']."'",'fetch');
			$this->view->EmployerData=$EmployerData;
		}
		
		$form = new Application_Form_User();
		$form->profile_admin($user_id );
		$form->populate($user_information);
 		
		if($this->getRequest()->isPost()){
			$posted_data = $this->getRequest()->getPost();
  			if($form->isValid($posted_data)){
				$data = $form->getValues();
 				$is_update = $this->modelUser->add($data,$user_id);
				if(is_object($is_update) and $is_update->success){
					$mySession->successMsg = "User Information Successfully Updated";
					$this->_redirect("admin/user/account/user_id/".$user_id);
				}
				$mySession->errorMsg = "Please Check Information Again";
			}
 		}
		
  		$this->view->form = $form ;	
		$this->view->user_information = $user_information ;*/
	}
	
	public function socialdetailsAction()
	{
		global $objSession;
		$user_id = $this->getRequest()->getParam('user_id');
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = social_user_id','2'=>'left','3'=>array('user_first_name','user_image')),
		);
		$socialData = $this->modelStatic->Super_Get('user_socialdata',"social_user_id='".$user_id."' and social_type='0'","fetch",array(),$joinArr);
		//prd($socialData);
		$this->view->socialData = $socialData;
	}
	
	/*
	* User Account Information 
	*/
	public function imageAction(){
		
		global $mySession; 
		$user_id =  $this->_getParam("user_id");
		$user_information = $this->modelUser->find($user_id);
		if(!$user_information->count()){
			$mySession->errorMsg = "No Such User Found , Invalid Request .";
			$this->_redirect("admin");
		}
		$user_information = $user_information->current()->toArray();
		if($user_information['user_tpye']=="admin"){
 			$mySession->errorMsg = " Invalid Operation .";
			$this->_redirect("admin/user");
 		}
		$this->view->pageHeading = ucwords($user_information['user_name']."'s Profile ");
		$this->view->pageDescription = ucwords("View All Information about ".$user_information['user_name']);
		$form = new Application_Form_User();
		$form->image();
		if($this->getRequest()->isPost()){
 			$data_post = $this->getRequest()->getPost();
			if($form->isValid($data_post)){
 				$is_uploaded = $this->_handle_profile_image();
				if(is_object($is_uploaded) and $is_uploaded->success){
					if(empty($is_uploaded->media_path)){
						/* Not Image is Uploaded  */
						$objSession->defaultMsg = "No image selected ...";
						$this->_redirect("admin/user/image/user_id/".$user_id);
					}
 					$is_updated = $this->modelUser->add(array("user_image"=>$is_uploaded->media_path),$user_id);
					if(is_object($is_updated) and $is_updated->success){
						/* Remove Old User Images*/
						$this->_unlink_user_image($user_information['user_image']); 
						$objSession->successMsg = " Image Successfully Updated";
						$this->_redirect("admin/user/image/user_id/".$user_id);
 					}
				}
 			}
		}
  		$this->view->form = $form ;	
		$this->view->user_information = $user_information ;
	}
	
	/*
	 * User Account Information 
	 */
	public function passwordAction(){
		global $mySession; 
		$user_id =  $this->_getParam("user_id");
		$user_information = $this->modelUser->find($user_id);
		if(!$user_information->count()){
			$mySession->errorMsg = "No Such User Found , Invalid Request .";
			$this->_redirect("admin");
		}
		$user_information = $user_information->current()->toArray();
		$this->view->pageHeading = ucwords($user_information['user_name']."'s Profile ");
		$this->view->pageDescription = ucwords("View All Information about ".$user_information['user_name']);
		$form = new Application_Form_User();
		$form->resetPassword();
		$form->populate($user_information);
		
		if($this->getRequest()->isPost()){
			$posted_data = $this->getRequest()->getPost();
  			if($form->isValid($posted_data)){
				$data = $form->getValues();
				$data['user_password'] = md5($data['user_password']);
 				$is_update = $this->modelUser->add($data,$user_id);
				if(is_object($is_update) and $is_update->success){
					$mySession->successMsg = "User Information Successfully Updated";
					$this->_redirect("admin/user/account/user_id/".$user_id);
				}
				$mySession->errorMsg = "Please Check Information Again";
			}
 		}
  		$this->view->form = $form ;	
		$this->view->user_information = $user_information ;
		$this->render("account");
	}
 
 	/* Ajax Call For Get Users */
  	public function getusersAction(){
		$this->dbObj = Zend_Registry::get('db');
		$request_type = $this->_getParam('type');
 		$aColumns = array(
			//'user_id','user_type','user_image','user_first_name','user_email','user_status','user_salutation','user_last_name','user_verified_status', 'admin_rights','user_role'
			'user_id','user_type','user_image','user_first_name','user_last_name','user_email','user_status','user_salutation','user_last_name','user_verified_status', 'admin_rights','user_role', 'user_terms_link', 'user_privacy_link', 'user_registration_ip_address', 'user_created'
  		);
		$sIndexColumn = 'user_id';
		$sTable = 'users';
		/** Paging */
		$sLimit = "";
		if( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".addslashes(trim($_GET["sSearch"]))."%' OR CONCAT(user_first_name,' ',user_last_name) LIKE '%".addslashes(trim($_GET["sSearch"]))."%' OR "; // NEW CODE
				//echo $sWhere;
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".addslashes(trim($_GET["sSearch"]))."%' ";
			}
		}
		
		if($sWhere){
			$sWhere.=" and (user_type='user')";
		}else{
			$sWhere.=" where (user_type='user')";
		}
		
		if($request_type!=""){
			switch($request_type){
				case 'all': $sWhere.=" "; break;	
				case 'verified': $sWhere.=" and user_status = '1' "; break;	
				case 'blocked': $sWhere.=" and user_status = '0' "; break;
				default : break ;	
			}
		}
		
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		/** Output */
		$output = array(
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[]='<img src="'.getUserImage($row1['user_image'],60).'" />';
			switch($row1['user_status']){
				case '0':$verification_status ="<span class='badge badge-danger badge-roundless'>Unverified</span>";break;
				default :$verification_status ="<span class='badge badge-success badge-roundless'>Verified</span>";break;
			}
			$row[]=$row1['user_first_name'];
			$row[]=$row1['user_last_name'];
			
			/*if($row1['user_type']=='service_provider'){
				$row[]='<label class="label label-info">Service Provider</label>';
			}else{
				$row[]='<label class="label label-danger">Employer</label>';
			}
			*/
			/*if($row1['admin_rights']=='1'){
				if($row1['user_role']=='1')
				{
					$row[]=$row1['user_email'].'<p></p>'.'<input type="radio" name="admin_rights[]" id="admin_rights'.$row1[$sIndexColumn].'1" onclick="adminRights('.$row1[$sIndexColumn].',1);" checked="checked" value="1" />&nbsp;<label for="admin_right'.$row1[$sIndexColumn].'1">Full admin rights</label>'.'<br/>'.'<input type="radio" name="admin_rights[]" id="admin_rights'.$row1[$sIndexColumn].'2" onclick="adminRights('.$row1[$sIndexColumn].',2);" value="2" />&nbsp;<label for="admin_right'.$row1[$sIndexColumn].'2">Forum admin rights</label>';
				}
				else
				{
					$row[]=$row1['user_email'].'<p></p>'.'<input type="radio" name="admin_rights[]" id="admin_rights'.$row1[$sIndexColumn].'1" onclick="adminRights('.$row1[$sIndexColumn].',1);" value="1"  />&nbsp;<label for="admin_right'.$row1[$sIndexColumn].'1">Full admin rights</label>'.'<br/>'.'<input type="radio" name="admin_rights[]" id="admin_rights'.$row1[$sIndexColumn].'2" onclick="adminRights('.$row1[$sIndexColumn].',2);" checked="checked" value="2"  />&nbsp;<label for="admin_right'.$row1[$sIndexColumn].'2">Forum admin rights</label>';
				}
			}
			else
			{
   				$row[]=$row1['user_email'].'<p></p>'.'<input type="radio" name="admin_rights[]" id="admin_rights'.$row1[$sIndexColumn].'1" onclick="adminRights('.$row1[$sIndexColumn].',1);" value="1"  />&nbsp;<label for="admin_right'.$row1[$sIndexColumn].'1">Full admin rights</label>'.'<br/>'.'<input type="radio" name="admin_rights[]" id="admin_rights'.$row1[$sIndexColumn].'2" onclick="adminRights('.$row1[$sIndexColumn].',2);" value="2"  />&nbsp;<label for="admin_right'.$row1[$sIndexColumn].'2">Forum admin rights</label>';
			}*/
			
			
			/*if($row1['admin_rights']=='1'){
				if($row1['user_role']==2)
				$status1 ="checked='checked'";
				else
				$status1="";	
					
				$row[]=$row1['user_email'].'<br/><br/><div class="danger-toggle-button-4"><input type="checkbox" class="toggle status-'.(int)$row1['user_role'].' "  '.$status1.'  id="users_role-'.$row1[$sIndexColumn].'" onChange="globalRightStatus(this)" /></div>';
			
		   }else{
				$row[]=$row1['user_email'].'<p></p>'.'<input type="checkbox" name="admin_rights" id="admin_rights" onclick="adminRights('.$row1[$sIndexColumn].');" />&nbsp;<label>Provide admin rights</label>';
		   }*/
			if($row1['user_role']==2)
			$status1 ="checked='checked'";
			else
			$status1="";
			
			if($row1['admin_rights']!='1'){
				$style1="display:block";
				$style2="display:none";
				$status2 ="";
			}
			else{
				$style1="display:block";
				$style2="display:block";
				$status2 ="checked='checked'";
			}
			
			$row[]=$row1['user_email'].'<p></p><div style='.$style1.' class="AdminRights'.$row1['user_id'].'"><input type="checkbox" name="admin_rights" id="admin_rights'.$row1[$sIndexColumn].'" onclick="adminRights('.$row1[$sIndexColumn].');"'.$status2.' />&nbsp;<label>Provide admin rights</label></div><div style='.$style2.' class="UserRole'.$row1['user_id'].'"><div class="danger-toggle-button-4"><input type="checkbox" class="toggle status-'.(int)$row1['user_role'].' "  '.$status1.'  id="users_role-'.$row1[$sIndexColumn].'" onChange="globalRightStatus(this)" /></div></div>';
			
			$status = $row1['user_status']!=1?"checked='checked'":" ";
 			$row[]='<div class="danger-toggle-button"><input type="checkbox" class="toggle status-'.(int)$row1['user_status'].' "  '.$status.'  id="'.$sTable.'-'.$row1[$sIndexColumn].'" onChange="globalStatus(this)" /></div>';
			
			if($row1['user_verified_status']=='0'){
				$row[]='<button type="button" class="btn btn-danger btn-xs">Not Verified</button>';
			}else{
				$row[]='<button type="button" class="btn btn-info btn-xs">Verified</button>';
			}
			
			$row[]= 'Registration Date: ' . $row1['user_created'] . ' <br />' . 'Terms: ' . '<a href="https://thejobmill.com' . $row1['user_terms_link'] . '" target="_blank">Preview</a>' . '<br />' . 'Privacy Policy: ' . '<a href="https://thejobmill.com' . $row1['user_privacy_link'] . '" target="_blank">Preview</a>' . '<br />' . 'IP Address: ' . $row1['user_registration_ip_address'] . '<br />';
			
			$row[] =  '<a href="'.APPLICATION_URL.'/admin/user/account/user_id/'.$row1[$sIndexColumn].'" class="btn btn-xs purple"> View <i class="fa fa-search"></i></a>';
			//$row[] = '<a class="btn mini green-stripe" href="'.APPLICATION_URL.'/admin/user/account/user_id/'.$row1[$sIndexColumn].'">View</a>';
 			$output['aaData'][] = $row;
			$j++;
		$i++;
		}
		echo json_encode( $output );
		exit();
  	}
	
	public function adminrightsAction()
	{
		/*$user_id = $this->getRequest()->getParam('user_id');
		$role = $this->getRequest()->getParam('role');
		$updateData = array(
			'admin_rights'=>'1',
			'user_role'=>$role,
		);
		$this->modelUser->Super_Insert("users",$updateData,"user_id='".$user_id."'");
		exit;*/
		$user_id = $this->getRequest()->getParam('user_id');
		$type = $this->getRequest()->getParam('type');
		$updateData = array(
			'admin_rights'=>$type,
			'user_role'=>1
		);
		//prn($updateData);
		$this->modelUser->Super_Insert("users",$updateData,"user_id='".$user_id."'");
		exit;
	}



	public function getvoucherAction()
	{
		$this->dbObj = Zend_Registry::get('db');

		$aColumns = array(
			'id',
			'code',
			'expire_date',
			'value',
			'discount_type',
			'status',
			'created_at',
		);

		$sIndexColumn = 'id';
		$sTable = 'voucher';
		/** Paging */

		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}

		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/*
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}


		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
		$qry = $this->dbObj->query($sQuery)->fetchAll();

		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll();
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];

		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll();
		$iTotal = $rResultTotal[0]['cnt'];

		/** Output */
		$output = array(
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
			$row=array();
			$row[] = $i;
			$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
			$row[]=$row1['code'];
			if($row1['discount_type'] == 1)
			{
				$row[] = 'Percentage';
				$row[]=$row1['value'].'%';
			}
			else
			{
				$row[] = 'Flat';
				$row[]='$'.$row1['value'];
			}

			$row[]= date("m/d/Y", strtotime($row1['expire_date']));
			if($row1['status'] == 1)
				$row[] = 'Active';
			else
				$row[] = 'Inactive';
			$row[] =  '<a href="'.APPLICATION_URL.'/admin/user/addvoucher/id/'.$row1[$sIndexColumn].'" class="btn btn-xs blue"> Edit <i class="fa fa-search"></i></a>';
			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
	}

	public function gettransactionsAction()
	{
		$this->dbObj = Zend_Registry::get('db');
		$aColumns = array(
			'id',
			'user_id',
			'transaction_id',
			'amount',
			'currency',
			'description',
			'job_id'
		);

		$sIndexColumn = 'id';
		$sTable = 'transactions_history';
		/** Paging */

		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}

		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/*
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}


		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
		$qry = $this->dbObj->query($sQuery)->fetchAll();

		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll();
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];

		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll();
		$iTotal = $rResultTotal[0]['cnt'];

		/** Output */
		$output = array(
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$j=0;
		$i=1;
		foreach($qry as $row1){
			$row=array();
			$row[] = $i;
			$row[]= $row1['transaction_id'];
			$user = $this->modelStatic->Super_Get('users',"user_id='".$row1['user_id']."'", "fetch");
			$row[] = ucwords($user['user_last_name'].' '.$user['user_first_name']);
			$row[]= '$'. number_format($row1['amount']/100, 2);
			$row[]= strtoupper($row1['currency']);
			$row[]=$row1['description'];
			if(substr($row1['transaction_id'], 0, 2) == 'ch')
				$row[]='Charged';
			else
				$row[]='Refunded';
			$row[] =  '<a href="'.APPLICATION_URL.'/admin/job/jobdetails/job_id/'.$row1['job_id'].'" class="btn btn-xs blue"> Job <i class="fa fa-search"></i></a>'.'<a href="'.APPLICATION_URL.'/admin/user/viewtransaction/transaction_id/'.$row1['id'].'" class="btn btn-xs purple"> View  <i class="fa fa-search"></i></a>';

			$output['aaData'][] = $row;
			$j++;
			$i++;
		}
		echo json_encode( $output );
		exit();
	}
	public function getsubscriptionsAction()
	{
		$this->dbObj = Zend_Registry::get('db');
 
 		$aColumns = array(
			'sub_id',
			'sub_name',
			'sub_desc',
			'sub_price',
			'sub_status',
			'sub_added_date',
  		);

		$sIndexColumn = 'sub_id';
		$sTable = 'subscription';
		/** Paging */
		 
		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
		{
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		
		/** Ordering */
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
			{
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= "".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
						($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
				}
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		/* 
		 * Filtering
		 * NOTE this does not match the built-in DataTables filtering which does it
		 * word by word on any field. It's possible to do here, but concerned about efficiency
		 * on very large tables, and MySQL's regex functionality is very limited
		 */
		$sWhere = "";
		if ( isset($_GET['sSearch']) and $_GET['sSearch'] != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET["sSearch"]."%' OR "; // NEW CODE
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		/* Individual column filtering */
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($_GET['bSearchable_'.$i]) and $_GET['bSearchable_'.$i] == "true" and $_GET['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				//$sWhere .= "".$aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
				$sWhere .= "".$aColumns[$i]." LIKE '%".$_GET['sSearch_'.$i]."%' ";
			}
		}
		
	
		$sQuery = " SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." FROM  $sTable $sWhere $sOrder $sLimit";
 		$qry = $this->dbObj->query($sQuery)->fetchAll();
 
 		/* Data set length after filtering */
		$sQuery = "SELECT FOUND_ROWS() as fcnt";
		$aResultFilterTotal =  $this->dbObj->query($sQuery)->fetchAll(); 
		$iFilteredTotal = $aResultFilterTotal[0]['fcnt'];
		
		/* Total data set length */
		$sQuery = "SELECT COUNT(`".$sIndexColumn."`) as cnt FROM $sTable $sWhere";
		$rResultTotal = $this->dbObj->query($sQuery)->fetchAll(); 
		$iTotal = $rResultTotal[0]['cnt'];
		
		/** Output */
		$output = array(
 				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
		$j=0;
		$i=1;
		foreach($qry as $row1){
 			$row=array();
			$row[] = $i;
  			//$row[]='<input class="elem_ids checkboxes"  type="checkbox" name="'.$sTable.'['.$row1[$sIndexColumn].']"  value="'.$row1[$sIndexColumn].'">';
   			$row[]=$row1['sub_name'];
			$row[]=$row1['sub_desc'];
			$row[]=$row1['sub_price'].' $';
			if($row1['sub_status'] == 1)
				$row[] = 'Active';
			else
				$row[] = 'Inactive';
			$row[] =  '<a href="'.APPLICATION_URL.'/admin/user/addsubscription/sub_id/'.$row1[$sIndexColumn].'" class="btn btn-xs blue"> Edit <i class="fa fa-search"></i></a>';
 			$output['aaData'][] = $row;
			$j++;
		$i++;
		}
		echo json_encode( $output );
		exit();
	}
	
 	public function removeAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if(isset($formData['users']) and count($formData['users'])){
				 foreach($formData['users'] as $key=>$values){
   					 $user_info = $this->modelUser->get($values);
					 if(empty($user_info))
						continue ;
 					$this->_unlink_user_image($user_info['user_image']);
					$removed = $this->modelUser->delete("user_id IN ($values)");
				 }
 				$mySession->successMsg = " User Deleted Successfully ";
				
 			}else{
				$mySession->errorMsg = " Invalid Request to Delete User(s) ";
			}
 			$this->_redirect('/admin/user/?removed=1');	 
		} 
 	}

	public function removevoucherAction(){
		global $mySession;
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();

			if(isset($formData['voucher']) and count($formData['voucher'])){
				foreach($formData['voucher'] as $key=>$values){
					$user_info = $this->modelUser->get($values);

					/*if(empty($user_info))
                       continue ;*/
					// prd($this->modelUser->Super_Delete("subscription","sub_id IN ($values)"));
					$removed = $this->modelUser->Super_Delete("voucher","id IN ($values)");
				}
				$mySession->successMsg = " Voucher Deleted Successfully ";

			}else{
				$mySession->errorMsg = " Invalid Request to Delete voucher(s) ";
			}
			$this->_redirect('/admin/user/voucher?removed=1');
		}
	}

	public function removesubAction(){
		global $mySession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			
			if(isset($formData['subscription']) and count($formData['subscription'])){
				 foreach($formData['subscription'] as $key=>$values){
   					 $user_info = $this->modelUser->get($values);
					
					 /*if(empty($user_info))
						continue ;*/
						// prd($this->modelUser->Super_Delete("subscription","sub_id IN ($values)"));
					$removed = $this->modelUser->Super_Delete("subscription","sub_id IN ($values)");
				 }
 				$mySession->successMsg = " subscription Deleted Successfully ";
				
 			}else{
				$mySession->errorMsg = " Invalid Request to Delete subscription(s) ";
			}
 			$this->_redirect('/admin/user/subscription?removed=1');	 
		} 
 	}
	
	public function checksubadminemailAction(){

 		$email_address = strtolower($this->_getParam('user_email'));
		$exclude = strtolower($this->_getParam('exclude'));
		$getvariable=Zend_Controller_Front::getInstance()->getRequest()->getParams();
		
		$user_id = false ;
		if(!empty($getvariable['id'])){
			 $user_id =$getvariable['id'];
			
		}
		$email = $this->modelUser->checkEmail($email_address,$user_id);
		$rev = $this->_getParam("rev");
		
		if($email)
			echo json_encode("`$email_address` already exists , please enter any other email address ");
		else
			echo json_encode("true");
		exit();
	}

	private function _unlink_user_image($image){
		if(empty($image))
			return true; 
	 
  		if($image!="" and file_exists(PROFILE_IMAGES_PATH."/".$image)){
			unlink(PROFILE_IMAGES_PATH."/".$image);
 		}
		
		if($image!="" and file_exists(PROFILE_IMAGES_PATH."/thumb/".$image)){
			unlink(PROFILE_IMAGES_PATH."/thumb/".$image);
 		}
		
		if($image!="" and file_exists(PROFILE_IMAGES_PATH."/60/".$image)){
			unlink(PROFILE_IMAGES_PATH."/60/".$image);
 		}
		
		if($image!="" and file_exists(PROFILE_IMAGES_PATH."/160/".$image)){
			unlink(PROFILE_IMAGES_PATH."/160/".$image);
 		}
	}
	
	/* Handle The Uploaded Images For Graphic Media  */
	private function _handle_profile_image(){
 		global $objSession; 
		$uploaded_image_names = array();
		$adapter = new Zend_File_Transfer_Adapter_Http();
		$files = $adapter->getFileInfo();
		$uploaded_image_names = array();
		$new_name = false; 
  		/*prd($adapter);*/
		foreach ($files as $file => $info) { /* Begin Foreach for handle multiple images */
  			$name_old = $adapter->getFileName($file);
			if(empty($name_old)){
				continue ;			
			}
			$file_title  = $adapter->getFileInfo($file);
			$file_title = $file_title[$file]['name']; 
  			$uploaded_image_extension = getFileExtension($name_old);
 			$file_title  = str_replace(".".$uploaded_image_extension,"",$file_title);
			$file_title = formatImageName($file_title);
 			$new_name = $file_title."-".time()."-".rand(1,100000).".".$uploaded_image_extension;
  			$adapter->addFilter('Rename',array('target' => PROFILE_IMAGES_PATH."/".$new_name));
			try{
				$adapter->receive($file);
			}
			catch(Zend_Exception $e){
				return (object) array('success'=>false,"error"=>true,'exception'=>true,'message'=>$e->getMessage(),'exception_code'=>$e->getCode()) ;
			}
			$thumb_config = array("source_path"=>PROFILE_IMAGES_PATH,"name"=> $new_name);
			Application_Plugin_ImageCrop :: uploadThumb(array_merge($thumb_config,array("size"=>300)));
			Application_Plugin_ImageCrop :: uploadThumb(array_merge($thumb_config,array("destination_path"=>PROFILE_IMAGES_PATH."/60","crop"=>true ,"size"=>60,"ratio"=>false)));
			Application_Plugin_ImageCrop :: uploadThumb(array_merge($thumb_config,array("destination_path"=>PROFILE_IMAGES_PATH."/160","crop"=>true ,"size"=>160,"ratio"=>false)));
			
  			//$uploaded_image_names[]=array('media_path'=>$new_name); => For Multiple Images
   		
		}/* End Foreach Loop for all images */
		
		
		return (object)array("success"=>true,'error'=>false,"message"=>"Image(s) Successfully Uploaded","media_path"=>$new_name) ;
 	}
}