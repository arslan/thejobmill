<?php
use ZfrStripe\Exception\TransactionErrorException;
use ZfrStripe\Client;
class PaymentController extends Zend_Controller_Action
{

	public function init()
	{
		$this->modelStatic = new Application_Model_Static();
		$this->modelUser = new Application_Model_User();
		$this->pluginImage = new Application_Plugin_Image();
	}

	public function cronrefundAction(){
		$today = new DateTime();
		$oneDaysEarlier = date_format($today->sub(new \DateInterval("P1D")), 'Y-m-d H:i:s'  );
		$sql = "SELECT job.*, transactions_history.*
		FROM job
		JOIN job_subscriptions ON job_subscriptions.job_id = job.job_id
		JOIN transactions_history ON transactions_history.job_id=job.job_id
		WHERE ((job_added_date <= '".$oneDaysEarlier."'
		AND job.job_id NOT IN (SELECT job_applications.job_app_job_id FROM job_applications) 
		AND job_subscriptions.subscription_id = 3 AND job_subscriptions.status !=0)
		OR (job_closing_date <= '".$oneDaysEarlier."' 
			AND job_subscriptions.subscription_id = 3 AND job_subscriptions.status !=0)) GROUP BY job.job_id";

		$jobs = $this->modelStatic->Super_Raw($sql);

echo $sql."<br>";

		if ($jobs) {
		foreach ($jobs as $job) {
			echo $job['job_id']. "Refunded by cron job <br>";
				$transaction_id = $job['id'];
				$user_id = $job['job_user_id'];
				$job_id=$job['job_id'];
			    $packageDetails = 0;
				$totalPrice = 0;
				$refundPrice = 0;
				$totalPackages = 0;
				$package="pkg2"; //Package for Urgent job

			   $metaData = json_decode($job["metadata"]);
        	    $metaArray = (array) json_decode($job["metadata"]);

			if (isset($metaData->pkg1)) {
				$pkg1 = explode(':', $metaData->pkg1);
				if ($pkg1[0] != 'false') {
					$totalPackages++;

				}
			}


			if (isset($metaData->pkg2)) {
				$pkg2 = explode(':', $metaData->pkg2);
				if ($pkg2[0] != 'false') {
					$totalPackages++;

				}
			}


			if (isset($metaData->pkg3)) {
				$pkg3 = explode(':', $metaData->pkg3);
				if ($pkg3[0] != 'false') {
					$totalPackages++;

				}
			}

			$metaData = (array)$metaArray;

//			var_dump($metaData);

			if(isset($metaData["discount_type"]) && isset($metaData["discount_value"])){
				if ($metaData["discount_type"] == 1) {
					$obj = explode(':', $metaData[$package]);
					$totalPrice = (float)str_replace('$', '', $obj[1]);
					$refundPrice = $totalPrice - ($totalPrice * $metaData["discount_value"] / 100);
				}
				if ($metaData["discount_type"] == 2) {
					$obj = explode(':', $metaData[$package]);
					echo $totalPrice = (float)str_replace('$', '', $obj[1]);
					//		echo "total packages".$totalPackages;
					$refundPrice = $totalPrice - ($metaData["discount_value"] / $totalPackages);
				}
			}
			else{

				$obj = explode(':', $metaData[$package]);
				$refundPrice = (float)str_replace('$', '', $obj[1]);

			}


			$refundPrice = (int)( $refundPrice*100);
			$obj = explode(':', $metaData[$package]);
			$subscriptionId=$obj[0];

			$client = new ZfrStripe\Client\StripeClient('sk_test_sI6SI0CngEoIauY03bzUlPcO', '2015-10-16');
			$reund = $client->refundCharge( array(

				"id" => $job["transaction_id"],
				"amount" => $refundPrice
			));

			$refmetadata=[

				"Voucher_code" => isset($metaData["Voucher_code"])?$metaData["Voucher_code"]:"",
				"discount"=> isset($metaData["discount"])?$metaData["discount"]:"",
				"discount_value" => isset($metaData["discount_value"])?$metaData["discount_value"]:"",
				"discount_type" => isset($metaData["discount_type"])?$metaData["discount_type"]:"",
				"subtotal"=>isset($metaData["subtotal"])?$metaData["subtotal"]:"",
				"pkg"=>$metaData[$package],
			];


			$data = [
				"user_id"=>$job['user_id'],
				"transaction_id" => $reund['id'],
				"amount"=>$reund['amount'],
				"currency"=>$reund['currency'],
				"description"=> "Cron job Refunded amount of ".$obj[2] . " : ".$job["transaction_id"],
				"metadata"=> json_encode($refmetadata),
				"status"=>$reund["status"],
				"job_id" => $job["job_id"]
			];

			if($reund["status"]=="succeeded") {

				$result = $this->modelStatic->Super_Insert('transactions_history', $data);


				$refundPackage = $metaData[$package] = "false";

				//Update actual transaction after refund
				$updateData = array(

					"metadata" => json_encode($metaData),

				);
				$result = $this->modelStatic->Super_Insert('transactions_history', $updateData, "id='" . $transaction_id . "'");


				$updateData = array(

					"status" => "0"
				);
				$result = $this->modelStatic->Super_Insert('job_subscriptions', $updateData, "job_id='" . $job["job_id"] . "' AND subscription_id='" . $subscriptionId . "'");



			}
			
			








			} // End of foreach jobs loop
		} else {
			echo "No job to refund";
		}

		exit();
	}
	
	public function voucherAction(){

		$user_id = 	$logged_identity = Zend_Auth::getInstance()->getInstance();
		$userInfo = $user_id->getIdentity();


	$code = $this->getRequest()->getParam('code');
	if (isset($code)) {
		$Data = $this->modelStatic->Super_Get('voucher', "code='" . $code . "' AND status=1 AND expire_date>='" . date('Y-m-d') . "'", 'fetch');
		if ($Data) {
			//where is user id ?
			$voucher_history = $this->modelStatic->Super_Get('transactions_history', "voucher_id='" . $Data['id'] . "' AND user_id='".$userInfo->user_id."'", 'fetch');
			if ($voucher_history) {
				echo $this->_helper->json(["status" => 2 , "message" => "Voucher already used"]);
			} else {
				echo $this->_helper->json($Data);
			}


		} else {
			echo $this->_helper->json(["status" => 2, "message"=>"Invalid voucher code"]);
		}
	}
}

	public function dataAction()
	{

		$request = $this->getRequest();
		$name = $request->getPost('name');
		$cardNo = $request->getPost('cardNo');
		$expMonth = $request->getPost('expMonth');
		$expYear = $request->getPost('expYear');
		$cvv = $request->getPost('cvv');
		$street = $request->getPost('street');
		$city = $request->getPost('city');
		$state = $request->getPost('state');
		$country = $request->getPost('country');

		$zip = $request->getPost('zip');
		$voucher_code = $request->getPost('voucherCode');
		$amount =(int) ( $request->getPost('amount')*100);

		$pkg1 = $request->getPost('pkg1');
		$pkg2 = $request->getPost('pkg2');
		$pkg3 = $request->getPost('pkg3');
		$subtotal = $request->getPost('subtotal');
		$discount = $request->getPost('discount');
		$discount_value = $request->getPost('discount_value');
		$discount_type = $request->getPost('discount_type');
		$Data=NULL;
		$data=NULL;

		if (!empty($voucher_code)) {
			$Data = $this->modelStatic->Super_Get('voucher', "code='" . $voucher_code . "' && status=1 ", 'fetch');
		}
		$client = new ZfrStripe\Client\StripeClient('sk_test_sI6SI0CngEoIauY03bzUlPcO', '2015-10-16');
		$token = [ 'card' => ["number" => $cardNo,"cvc"	 =>  $cvv,"exp_month" => $expMonth,"exp_year"  => $expYear,
			"name"  => $name,"address_line1" => $street,"address_city" => $city,"address_zip" =>  $zip,
			"address_state" => $state,"address_country" => $country]];
		$token = $client->createCardToken($token);
		$details = $client->createCharge(['amount'   => $amount ,'currency' => 'AUD','source' => $token['id'],
			'description' => "Charge payment from user for packages","metadata" => ["Voucher_code" => $voucher_code,"discount"=>$discount,"discount_value" => $discount_value,"discount_type" => $discount_type,"subtotal"=>$subtotal,"pkg1"=>$pkg1,"pkg2"=>$pkg2,"pkg3"=>$pkg3	 ]]);

		if($details["status"]="succeeded" && !empty($details["id"])){
			$user_id = 	$logged_identity = Zend_Auth::getInstance()->getInstance();
			$userInfo = $user_id->getIdentity();
			$metadata = json_encode($details['metadata']);
			$data = ["user_id"=>$userInfo->user_id,"transaction_id" => $details['id'],"amount"=>$details['amount'],
				"currency"=>$details['currency'],"description"=>$details['description'],"metadata"=>$metadata,"status"=>$details["status"]];

			if(!empty($Data)) {
				$voucher_history = $this->modelStatic->Super_Get('transactions_history', "voucher_id='" . $Data['id'] . "'", 'fetch');
				if (!$voucher_history) {
					$data["voucher_id"]=$Data['id'];
				}
			}
			  $result = $this->modelStatic->Super_Insert('transactions_history',$data);

			$details["tb_transaction_id"]=$result->inserted_id;
			  echo $this->_helper->json($details);
		}
		else {
			echo $this->_helper->json(["status"=>false]);
		}

	}
}