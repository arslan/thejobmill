<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{	
 	protected function _initLoaderResources()
    {
        $this->getResourceLoader()->addResourceType('controller', 'controllers/', 'Controller');
    }
	
	protected function _initAutoloader()
 	{
 	   new Zend_Application_Module_Autoloader(array(
 	      'namespace' => 'Application',
 	      'basePath'  => APPLICATION_PATH,
 	   ));
 	}
	
	protected function _initHelperPath() 
	{
		$view = $this->bootstrap('view')->getResource('view');
		$view->setHelperPath(APPLICATION_PATH . '/views/helpers', 'Application_View_Helper');
    }
	
	protected function _initDoctype()
	{
		$this->bootstrap('view');
 		$view = $this->getResource('view');
  		$view->setEncoding('UTF-8');
		$view->doctype('HTML5');
 		$view->headMeta()->appendHttpEquiv('Content-Type',  'text/html;charset=utf-8');
	}
	
	protected function _initDB() {
		$dbConfig = new Zend_Config_Ini(ROOT_PATH.'/private/db.ini',APPLICATION_ENV);
		$dbConfig =$dbConfig->resources->db;
	 
       	$dbAdapter = Zend_Db::factory($dbConfig->adapter, array(
            'host'     => $dbConfig->params->hostname,
            'username' => $dbConfig->params->username,
            'password' => $dbConfig->params->password,
            'dbname'   => $dbConfig->params->dbname
         ));
		//$dbAdapter->exec("SET time_zone='".$dbConfig->params->timezone."'");
        Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
        Zend_Registry::set('db', $dbAdapter);
		Zend_Session::start();
		global $objSession;
		$objSession = new Zend_Session_Namespace('default');
    }
 	
	protected function _initAppKeysToRegistry()
	{
		$appkeys = new Zend_Config_Ini(ROOT_PATH . '/private/appkeys.ini');
		Zend_Registry::set('keys', $appkeys);
	}
	
	public function _initPlugins()
	{ // Add Plugin path
		$front = Zend_Controller_Front::getInstance();
		$front->registerPlugin(new Application_Plugin_SetLayout());
	}

	protected  function _initApplication()
	{   
		$this->FrontController=Zend_Controller_Front::getInstance();
		$this->FrontController->setControllerDirectory(array(
			'default' => '../application/controllers',
			'admin'    => '../application/admin/controllers'
		));
		// $this->FrontController->setDefaultControllerName('login'); 
		//	$this->FrontController->throwExceptions(false);
		$registry = Zend_Registry::getInstance();
		$registry->set("flash_error",false);
		// Add a 'foo' module directory:
		// $this->FrontController->setParam('prefixDefaultModule', true);
		// $this->FrontController->setDefaultModule('publisher');
		// $this->FrontController->setDefaultAction("index") ;
		// $this->FrontController->addControllerDirectory('../modules/foo/controllers', 'foo');
	}
	
	public function _initRouter()
	{
		$this->FrontController = Zend_Controller_Front::getInstance();
		$this->router = $this->FrontController->getRouter();
		$this->appRoutes = array ();
	}
	
	/* Site Routers */
	protected function _initSiteRouters(){
		/* Fixed Front Redirects */
		$this->appRoutes['front_page'] = new Zend_Controller_Router_Route('/:type', array ('module' => 'default','controller' => 'index','action' => 'index','type'=>'\s+'));
		$this->appRoutes['front_login'] = new Zend_Controller_Router_Route('login', array ('module' => 'default','controller' => 'user','action' => 'login'));
		$this->appRoutes['post_topic'] = new Zend_Controller_Router_Route('post-topic', array ('module' => 'default','controller' => 'user','action' => 'login','type'=>'1'));
		$this->appRoutes['submit_post'] = new Zend_Controller_Router_Route('submit-post/:forum_id', array ('module' => 'default','controller' => 'user','action' => 'login','forum_id'=>'1'));
		
		
		$this->appRoutes['front_changepassword'] = new Zend_Controller_Router_Route('change-password', array ('module' => 'default','controller' => 'user',
		'action' => 'changepassword'));
		$this->appRoutes['front_logout'] = new Zend_Controller_Router_Route('logout', array ('module' => 'default','controller' => 'user','action' => 'logout'));
		$this->appRoutes['front_register'] = new Zend_Controller_Router_Route('register', array ('module' => 'default','controller' => 'user','action' => 'register'));
		$this->appRoutes['front_forgotpassword'] = new Zend_Controller_Router_Route('forgot-password', array ('module' => 'default','controller' => 'user',
		'action' => 'forgotpassword'));
		$this->appRoutes['facebook_signup'] = new Zend_Controller_Router_Route('social/fblogin', array ('module' => 'default','controller' => 'social','action' => 'fblogin'));
		$this->appRoutes['twitter_signup'] = new Zend_Controller_Router_Route('social/twitterlogin',array('module'=>'default','controller'=>'social','action'=>'twitterlogin'));
		$this->appRoutes['about_us'] = new Zend_Controller_Router_Route('about-us', array ('module'=>'default','controller'=>'static','action'=>'index','page_id'=>'1'));
		$this->appRoutes['how_it_works'] = new Zend_Controller_Router_Route('how-it-works', array ('module'=>'default','controller'=>'static','action'=>'index','page_id'=>'2'));
		
		$this->appRoutes['template'] = new Zend_Controller_Router_Route('template', array ('module'=>'default','controller'=>'static','action'=>'index','page_id'=>'7'));		
		$this->appRoutes['home'] = new Zend_Controller_Router_Route('home', array ('module'=>'default','controller'=>'static','action'=>'index','page_id'=>'8'));
		
		$this->appRoutes['term_and_conditions'] = new Zend_Controller_Router_Route('term-and-conditions', array ('module'=>'default','controller'=>'static','action'=>'index',
		'page_id'=>'5'));
		$this->appRoutes['privacy_policy'] = new Zend_Controller_Router_Route('privacy-policy', array ('module'=>'default','controller'=>'static','action'=>'index','page_id'=>'4'));
		$this->appRoutes['contact_us'] = new Zend_Controller_Router_Route('contact-us', array ('module'=>'default','controller'=>'static','action'=>'contact'));
		$this->appRoutes['front_profile'] = new Zend_Controller_Router_Route('profile', array ('module'=>'default','controller'=>'profile','action'=>'index'));
		$this->appRoutes['edit_profile'] = new Zend_Controller_Router_Route('edit-profile/:image', array ('module'=>'default','controller'=>'profile','action'=>'editprofile','image'=>'\d+'));
		$this->appRoutes['user_profile'] = new Zend_Controller_Router_Route('user-profile/:user_id', array ('module'=>'default','controller'=>'profile','action'=>'userprofile',
		'user_id'=>'\d+'));
		$this->appRoutes['view_notifications'] = new Zend_Controller_Router_Route('view-notifications', array ('module'=>'default','controller'=>'profile','action'=>'viewnotifications'));
		
		$this->appRoutes['front_image'] = new Zend_Controller_Router_Route('change-avatar', array ('module'=>'default','controller'=>'profile','action'=>'image'));
		$this->appRoutes['profile_picture'] = new Zend_Controller_Router_Route('profile-picture', array ('module'=>'default','controller'=>'profile','action'=>'image'));
		$this->appRoutes['front_image_crop'] = new Zend_Controller_Router_Route('crop-image', array ('module'=>'default','controller'=>'profile','action'=>'cropimage'));
		$this->appRoutes['profile_image_crop'] = new Zend_Controller_Router_Route('profile-crop-image', array ('module'=>'default','controller'=>'profile','action'=>'cropimage'));
		$this->appRoutes['change_password'] = new Zend_Controller_Router_Route('change-password', array ('module'=>'default','controller'=>'profile','action'=>'password'));
		
		$this->appRoutes['post_job'] = new Zend_Controller_Router_Route('post-job/:job_id',array('module'=>'default','controller'=>'job','action'=>'index','job_id'=>'\d+'));
		$this->appRoutes['job_list'] = new Zend_Controller_Router_Route('job-list/:type/:page',array('module'=>'default','controller'=>'job','action'=>'joblist','type'=>'\s+','page'=>'\d+'));
		$this->appRoutes['job_details'] = new Zend_Controller_Router_Route('job-details/:job_id',array('module'=>'default','controller'=>'job','action'=>'jobdetails',
		'job_id'=>'\d+'));
		$this->appRoutes['job_board'] = new Zend_Controller_Router_Route('job-board/:job_id',array('module'=>'default','controller'=>'job','action'=>'jobboard','job_id'=>'\d+'));
		$this->appRoutes['jobs'] = new Zend_Controller_Router_Route('jobs/:type/:user_id/:page',array('module'=>'default','controller'=>'job','action'=>'jobs','type'=>'\s+','user_id'=>'\d+','page'=>'\d+'));
		$this->appRoutes['service_providers']=new Zend_Controller_Router_Route('workers/:type/:page',array('module'=>'default','controller'=>'job','action'=>'serviceproviders','type'=>'\s+','page'=>'\d+'));
		$this->appRoutes['search_jobs'] = new Zend_Controller_Router_Route('search-jobs/:type',array('module'=>'default','controller'=>'job','action'=>'searchjobs','type'=>'\s+'));
		$this->appRoutes['hire'] = new Zend_Controller_Router_Route('hire/:user_id/:type',array('module'=>'default','controller'=>'job','action'=>'jobhire','user_id'=>'\d+','type'=>'\s+'));
		
		$this->appRoutes['compose'] = new Zend_Controller_Router_Route('compose/:type/:message_id/:user_id/:draft_id',array('module'=>'default','controller'=>'message','action'=>'compose','type'=>'\d+','message_id'=>'\d+','user_id'=>'\d+','draft_id'=>'\d+'));
		$this->appRoutes['inbox'] = new Zend_Controller_Router_Route('inbox',array('module'=>'default','controller'=>'message','action'=>'index'));
		$this->appRoutes['from_hirers'] = new Zend_Controller_Router_Route('from-hirers',array('module'=>'default','controller'=>'message','action'=>'hirers'));
		$this->appRoutes['from_workers'] = new Zend_Controller_Router_Route('from-workers',array('module'=>'default','controller'=>'message','action'=>'workers'));
		$this->appRoutes['sent'] = new Zend_Controller_Router_Route('sent',array('module'=>'default','controller'=>'message','action'=>'sent'));
		$this->appRoutes['drafts'] = new Zend_Controller_Router_Route('drafts',array('module'=>'default','controller'=>'message','action'=>'drafts'));
		$this->appRoutes['message_details'] = new Zend_Controller_Router_Route('message-details/:message_id',array('module'=>'default','controller'=>'message','action'=>'details',
		'message_id'=>'\d+'));
		
		$this->appRoutes['blog_category'] = new Zend_Controller_Router_Route('blog-category',array('module'=>'default','controller'=>'blog','action'=>'categories'));
		$this->appRoutes['blog_listing'] = new Zend_Controller_Router_Route('blog-listing/:cat_id',array('module'=>'default','controller'=>'blog','action'=>'listing',
		'cat_id'=>'\d+'));
		$this->appRoutes['blog'] = new Zend_Controller_Router_Route('blog/:blog_id',array('module'=>'default','controller'=>'blog','action'=>'blogdetails','blog_id'=>'\d+'));
		
		/* FORUMS */
		
		$this->appRoutes['front_forums'] = new Zend_Controller_Router_Route('forums',array('module'=>'default','controller'=>'forum','action'=>'index'));
		$this->appRoutes['front_topic_detail'] = new Zend_Controller_Router_Route('topic-detail/:forum_id',array('module'=>'default','controller'=>'forum','action'=>'threads','forum_id'=>'/d+')); 
		$this->appRoutes['front_more_details'] = new Zend_Controller_Router_Route('more-profile-details/:user_id',array('module'=>'default','controller'=>'profile','action'=>'socialdetails','user_id'=>'/d+'));


		$this->appRoutes['cron_verifyemail'] = new Zend_Controller_Router_Route('cron/verifyemail',array('module'=>'default','controller'=>'user','action'=>'sendverificationemails'));
		
		
		
		
		$db = Zend_Registry::get('db');
		
		
	}
	
	protected function _initSetupRouting()
	{
		foreach ($this->appRoutes as $key => $cRouter)
		{
			$this->router->addRoute($key, $cRouter);
		}
		/*prd($this);*/
	}
	
	protected function _initTranslator()
	{
		$enLangData = require_once(ROOT_PATH.'/private/languages/en.php');
		$deLangData = require_once(ROOT_PATH.'/private/languages/fr.php');
 		$translate = new Zend_Translate(
			array(
				'adapter' => 'array',
				'content' => $enLangData,
				'locale'  => 'en',
			)
		);
		$translate->addTranslation(
			array(
				'content' => $deLangData,
				'locale'  => 'fr',
				'clear'   => true
			)
		);
		if(SITE_STAGE == "development"){
			$translate->setLocale('en');
		}else{
			$translate->setLocale('fr');
		}
		Zend_Registry::set('Zend_Translate', $translate);
	}
}
/* ------------------------------------------- Functions ---------------------------------  */
function prepareQuery($args){
	$sql=$args[0];
	$_sqlSplit = preg_split('/(\?|\:[a-zA-Z0-9_]+)/', $sql, -1, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
	$params=0;	
	foreach ($_sqlSplit as $key => $val) {
		if ($val == '?')
		{
			$_sqlSplit[$key]=$args[1][$params];
			$params++;
		}
	}
	$query=implode($_sqlSplit);	 
	return($query);
}