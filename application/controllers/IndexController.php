<?php
class IndexController extends Zend_Controller_Action
{
	public function init(){	
		$this->modelUser = new Application_Model_User();
  	}



 	public function indexAction(){
		global $objSession ;
		

		$type = $this->_getParam('type');
		$this->view->type = $type;
  		$this->view->pageHeading = "Home";
		$auth = Zend_Auth::getInstance();
		$user_id=NULL;
		$subscriptionModel = new Application_Model_Subscription();
		$homeGalleryNearJobs="";
		if($auth->hasIdentity()) {
			$user = $auth->getIdentity();
			$homeGalleryNearJobs = $subscriptionModel->getNearJobs($user);
			}
			if ($homeGalleryNearJobs){
				$this->view->gallery_jobs = $homeGalleryNearJobs;
			}else{
				$this->view->gallery_jobs = $subscriptionModel->getHomeGalleryJobs();
			}

				$modelSlider = new Application_Model_Slider();
				$images = $modelSlider->fetchImages();
				$this->view->slider_images = $images ;
  	}
	
	public function testAction(){	
 		global $objSession ; 
		exit();
		$this->view->pageHeading = "Select Interest Category";	
		$n = '0';
		for($i=1;$i<=1500;$i++){
			$n++;
			$messageInsert['user_referrer_code'] = "GETT".$n."D".rand(00,99);
			$messageInsert['user_type'] = "user";
			$messageInsert['user_salutation'] = '1';
			$messageInsert['user_email'] = 'user'.$n.'@test.com';
			$messageInsert['user_name'] = 'user'.$n;
			$messageInsert['user_password'] = 'e10adc3949ba59abbe56e057f20f883e';
			$messageInsert['user_status'] = '1';
			$messageInsert['user_login_type'] = 'normal';
			$messageInsert['user_address'] = 'Sra. Otilia Ramos Perez Urión 30, Col. Atlatilco';
			$messageInsert['user_postal_code'] = '77520';
			$messageInsert['user_state'] = 'South-Central Mexico';
			$messageInsert['user_country'] = 'United States';
			$messageInsert['user_email_verified'] = '1';
			$messageInsert['user_created'] = date("Y-m-d H:i:s");
			$messageInsert['user_updated'] = date("Y-m-d H:i:s");
			$jobSecurityUpdateStatus = $this->modelUser->Super_Insert("users",$messageInsert); 	
		}
		exit;
	}
}