<?php
class BlogController extends Zend_Controller_Action
{
    public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->view->pageIcon = "fa  fa-users";
    }
 	
 	public function categoriesAction(){
 		global $objSession; 
		$categories = $this->modelStatic->Super_Get('blog_category',"1","fetchAll");
		$this->view->categories = $categories;
 	}
 	
	public function listingAction(){
		global $objSession; 
		$cat_id = $this->getRequest()->getParam('cat_id');
		$catDetails = $this->modelStatic->Super_Get('blog_category',"blog_cat_id='".$cat_id."'",'fetch');
		$this->view->catDetails = $catDetails; 
		$blogs = $this->modelStatic->Super_Get('blogs',"blog_category='".$cat_id."'","fetchAll",$extra=array('pagination'=>true));
		
		$adapter= new Zend_Paginator_Adapter_DbSelect($blogs);
		$paginator = new Zend_Paginator($adapter);
		$page = $this->_getParam('page',9);
		$rec_counts = 10; // Item per page
		$paginator->setItemCountPerPage($rec_counts);
		$paginator->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator = $paginator;
	}
	
	public function blogdetailsAction(){
		global $objSession;
		$blog_id = $this->getRequest()->getParam('blog_id');
		$joinArr=array(
			'0' => array('0'=>'blog_category','1'=>'blog_cat_id = blog_category','2'=>'left','3'=>array('blog_cat_title','blog_cat_id')),
		);
		$blog = $this->modelStatic->Super_Get('blogs',"blog_id='".$blog_id."'","fetch",$extra=array(),$joinArr);
		$this->view->blog = $blog;
		
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'user_id = blog_comm_user_id','2'=>'left','3'=>array('user_first_name','user_last_name','user_image')),
		);
		$blogComments = $this->modelStatic->Super_Get('blog_comments',"blog_comm_blog_id='".$blog_id."'","fetchAll",$extra=array('order'=>'blog_comm_date DESC'),$joinArr);
		$this->view->blogComments = $blogComments;
		
		$form = new Application_Form_Blog();
		$form->blogcomments();
		$this->view->form = $form;
		if($this->getRequest()->isPost())
		{
			if(!empty($this->view->user))
			{
				$data_post = $this->getRequest()->getPost();
				if($form->isValid($data_post))
				{
					$data_insert = $form->getValues() ;
					$BlogAddData = array(
						'blog_comm_user_id' => $this->view->user->user_id,
						'blog_comment' => $data_insert['blog_comment'],
						'blog_comm_blog_id' => $blog_id,
						'blog_comm_date' => date('Y-m-d H:i:s'),
					);
					$this->modelStatic->Super_Insert('blog_comments',$BlogAddData);
					$objSession->successMsg = 'Comment added successfully';
					$this->_redirect('blog/'.$blog_id);
				}
			}else{
				$this->_redirect('user/login/blog_id/'.$blog_id);
				$objSession->errorMsg = 'Please login to continue.';
			}
		}
	}
	
	public function deletecommentAction()
	{
		global $objSession;
		$blog_comm_id = $this->getRequest()->getParam('blog_comm_id');
		$blog_id = $this->getRequest()->getParam('blog_id');
		
		$this->modelStatic->Super_Delete("blog_comments","blog_comm_id='".$blog_comm_id."'");
		$objSession->successMsg = 'Comment deleted successfully';
		$this->_redirect('blog/'.$blog_id);
	}
}