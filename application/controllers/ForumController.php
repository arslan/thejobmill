<?php
class ForumController extends Zend_Controller_Action
{
    public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->view->pageIcon = "fa  fa-users";
    }
 	
 	public function indexAction(){
 		global $objSession; 
		
		$topicForm=new Application_Form_Forum();
		$topicForm->addtopic($this->view->current_module);
		$topicForm->btnsubmit->setLabel("POST TOPIC");
		$this->view->topicForm=$topicForm;
		$forumTopicData = $this->modelUser->Super_Get('forum_topics',"forum_status='1' and forum_approved_status='1'","fetchAll",array('group'=>'forum_id'),array('0'=>array('0'=>'users','1'=>'user_id=forum_user_id','2'=>'full','3'=>array('user_id','user_first_name','user_last_name','user_image')),'1'=>array('0'=>'forum_threads','1'=>'f_forum_id=forum_id','2'=>'left','3'=>array('totalThreads'=> new Zend_Db_Expr('ifnull(count(f_thread_id),0)')))));
		$this->view->forumTopicData = $forumTopicData;
		$search = $this->getRequest()->getParam('search');
		
		if($this->getRequest()->isPost())
		{
			if($search=='1')
			{
				$data_post = $this->getRequest()->getPost();
				$db = Zend_Registry::get('db');
				$search_txt_val = $db->quote("%".$data_post['keyword']."%");
				$where = "forum_topic LIKE $search_txt_val or user_first_name LIKE $search_txt_val or user_last_name LIKE $search_txt_val or forum_cat_id LIKE $search_txt_val or CONCAT(user_first_name,' ',user_last_name) LIKE $search_txt_val";
				$forumTopicData = $this->modelUser->Super_Get('forum_topics',"forum_status='1' and forum_approved_status='1' and ".$where."","fetchAll",array('group'=>'forum_id'),array('0'=>array('0'=>'users','1'=>'user_id=forum_user_id','2'=>'full','3'=>array('user_id','user_first_name','user_last_name','user_image')),'1'=>array('0'=>'forum_threads','1'=>'f_forum_id=forum_id','2'=>'left','3'=>array('totalThreads'=> new Zend_Db_Expr('ifnull(count(f_thread_id),0)')))));
				$this->view->forumTopicData = $forumTopicData;
				$this->view->data_post = $data_post;
			}else{
				if(!empty($this->view->user))
				{
					$data_post = $this->getRequest()->getPost();
					if($topicForm->isValid($data_post))
					{
						$data_insert = $topicForm->getValues() ;
						$data_insert['forum_approved_status']=0;
						$data_insert['forum_user_id']=$this->view->user->user_id;
						$data_insert['forum_added_date']=date('Y-m-d H:i:s');
						$this->modelUser->Super_Insert('forum_topics',$data_insert);
						$objSession->successMsg = 'Topic Request is Sent to Administartor for Approval.';
						$this->_redirect('forums');
					}
				}else{
					$objSession->errorMsg = 'Please login to continue.';
					$this->_redirect('post-topic');
				}
			}
		}
		
 	}
	
 	
	public function threadsAction(){
		global $objSession; 
		$forum_id = $this->getRequest()->getParam('forum_id');
		
		$forumTopicData = $this->modelUser->Super_Get('forum_topics',"forum_id='".$forum_id."'","fetch",array('fields'=>array('forum_topic','forum_added_date','forum_id')));
		$this->view->forumTopicData = $forumTopicData;
		
		if(empty($forumTopicData)){
				$objSession->errorMsg = 'Invalid Request';
				$this->_redirect("forums");
		}
		
		
		$forumThreadData = $this->modelUser->Super_Get('forum_threads',"f_forum_id='".$forum_id."'","fetchAll",array('group'=>'f_thread_id','pagination'=>true),array('0'=>array('0'=>'users','1'=>'user_id=f_user_id','2'=>'full','3'=>array('user_first_name','user_last_name','user_image')),'1'=>array('0'=>'forum_comments','1'=>'fc_thread_id=f_thread_id','2'=>'left','3'=>array())));
		
		$forumThreadForm=new Application_Form_Forum();
		$forumThreadForm->addthread();
		$forumThreadForm->f_forum_id->setValue($forumTopicData['forum_id']);
		$this->view->forumThreadForm=$forumThreadForm;
		
		
		$reportForm=new Application_Form_Forum();
		$reportForm->sendreport();
		$this->view->reportForm=$reportForm;
		
		$improvementForm=new Application_Form_Forum();
		$improvementForm->sendimprovement();
		$this->view->improvementForm=$improvementForm;
		
		$adapter= new Zend_Paginator_Adapter_DbSelect($forumThreadData);
		$paginator = new Zend_Paginator($adapter);
		$page = $this->_getParam('page',9);
		$rec_counts = 10; // Item per page
		$paginator->setItemCountPerPage($rec_counts);
		$paginator->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->forumThreadData = $paginator;
		
		
		
		if($this->getRequest()->isPost())
		{
			$data_post = $this->getRequest()->getPost();
			
				if($reportForm->isValid($data_post))
				{
					$data_insert = $reportForm->getValues() ;
					$data_insert['report_added_date']=date('Y-m-d H:i:s');
					$forumId=$data_insert['report_forum_id'];
					unset($data_insert['report_forum_id']);
					$this->modelUser->Super_Insert('forum_reports',$data_insert);
					$objSession->successMsg = 'Post Reported Successfully.';
					$this->_redirect('topic-detail/'.$forumId);
				}
				
				else if($improvementForm->isValid($data_post))
				{
					$data_insert = $improvementForm->getValues() ;
					$data_insert['improvement_added_date']=date('Y-m-d H:i:s');
					$forumId=$data_insert['improvement_forum_id'];
					unset($data_insert['improvement_forum_id']);
					$this->modelUser->Super_Insert('forum_improvement',$data_insert);
					$objSession->successMsg = 'Improvemnt Request is Sent.';
					$this->_redirect('topic-detail/'.$forumId);
				}
				
			if(!empty($this->view->user))
			{
				if($forumThreadForm->isValid($data_post))
				{
					$data_insert = $forumThreadForm->getValues() ;
					
					$data_insert['f_user_id']=$this->view->user->user_id;
					$data_insert['f_added_date']=date('Y-m-d H:i:s');
					$this->modelUser->Super_Insert('forum_threads',$data_insert);
					$objSession->successMsg = 'Post Added Successfully.';
					$this->_redirect('topic-detail/'.$data_insert['f_forum_id']);
				}
				
				else if(isset($data_post['fc_comment']) && !empty($data_post['fc_comment']))
				{
					$data_insert = $forumThreadForm->getValues() ;
					$data_post['fc_comment']=$data_post['fc_comment'][0];
					$data_post['fc_user_id']=$this->view->user->user_id;
					$data_post['fc_added_date']=date('Y-m-d H:i:s');
					$forumId=$data_post['f_forum_id'];
					unset($data_post['f_forum_id']);
					$this->modelUser->Super_Insert('forum_comments',$data_post);
					$objSession->successMsg = 'Comment Added Successfully.';
					$this->_redirect('topic-detail/'.$forumId);
				}
				
			}else{
				$objSession->errorMsg = 'Please login to continue.';
				$this->_redirect('submit-post/'.$data_post['f_forum_id']);
			}
		}
	}
	
	
	public function deletethreadAction()
	{
		global $objSession;
		$f_thread_id = $this->getRequest()->getParam('f_thread_id');
		$forum_id = $this->getRequest()->getParam('forum_id');
		
		
		$this->modelUser->Super_Delete("forum_threads","f_thread_id='".$f_thread_id."'");
		$objSession->successMsg = 'Post deleted successfully';
		$this->_redirect('topic-detail/'.$forum_id);
	}
	
	public function deletecommentAction()
	{
		global $objSession;
		$fc_comment_id = $this->getRequest()->getParam('fc_comment_id');
		$forum_id = $this->getRequest()->getParam('forum_id');
		
		
		$this->modelUser->Super_Delete("forum_comments","fc_comment_id='".$fc_comment_id."'");
		$objSession->successMsg = 'Comment deleted successfully';
		$this->_redirect('topic-detail/'.$forum_id);
	}
}