<?php
class JobController extends Zend_Controller_Action
{
  	private $modelUser ,$modelContent; 
	 
	public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->modelEmail = new Application_Model_Email();
 	}
	
	

	public function retrieveskillsAction(){
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	
	
		$param1 = $this->_request->getParam('param1');
		$param2 = $this->_request->getParam('param2');
	
		$model = new Application_Model_User();
		$SkillData = $model->Super_Get('skills',"skill_category='".$param1."'","fetchAll",$extra=array('fields'=>array('skill_id','skill_title')));			
		
		$output = array();
		
		$JobArray = array(''=>'Select Skill');
		foreach($SkillData as $skill)
		{
			
			$row = array();
			$row[0] = $skill['skill_id'];
			$row[1] = $skill['skill_title'];
			$output[] = $row;	
			
		}
	
		
		echo json_encode($output);
		
	}	
	
	
	public function indexAction(){
		global $objSession;
		$job_id = $this->getRequest()->getParam('job_id');
		$job = $this->modelStatic->Super_Get('job',"job_id='".$job_id."'","fetch");
		$subscriptions = $this->modelStatic->Super_Get('subscription',"sub_status= 1", "All");
		$this->view->job = $job;
		$this->view->subscriptions = $subscriptions;
		$user_id = 	$logged_identity = Zend_Auth::getInstance()->getInstance();
		$userInfo = $user_id->getIdentity();
		$this->view->userInfo = $userInfo;
		$skillForm=new Application_Form_User();
		$skillForm->addskill();
		$this->view->skillForm=$skillForm;
		
		$form = new Application_Form_Job();
		$form->postjob();
		if(!empty($job))
		{
			if($job['job_user_id']!=$this->view->user->user_id){
				$objSession->successMsg = 'You are not allowed to modify this job.';
				$this->_redirect('job-list');
			}
			
			$form->populate($job);
			$locationArray = explode(",",$job['job_location']);
			//prn($locationArray);
			$form->job_description->setValue($job['job_desc']);
			
			$form->job_country->setValue($locationArray[0]);
			$form->job_location->setValue($locationArray[1]);
		}
		$form->job_closing_date->setValue(date('F d, Y',strtotime($job['job_closing_date'])));
		$form->job_skills->setValue(explode(",",$job['job_skills']));
		$this->view->form = $form;
		
		if ($this->getRequest()->isPost()){

 			$posted_data  = $this->getRequest()->getPost();

			if ($form->isValid($this->getRequest()->getPost()))
			{
				$Formdata  = $form->getValues();
				$Formdata['job_user_id'] = $this->view->user->user_id;
				$Formdata['job_closing_date'] = date('Y-m-d',strtotime($Formdata['job_closing_date']));
				$Formdata['job_added_date'] = date('Y-m-d H:i:s');
				$Formdata['job_skills'] = implode(",",$Formdata['job_skills']);
				$Formdata['job_desc'] = $Formdata['job_description'];
				unset($Formdata['job_description']);
				$Formdata['job_location'] = trim($Formdata['job_country']).', '.$Formdata['job_location'];
				unset($Formdata['job_country']);
				/*if((($Formdata['job_min_price']!='0') && ($Formdata['job_max_price']!='0')) || (($Formdata['job_min_price']=='0') && ($Formdata['job_max_price']!='0'))  || 
				(($Formdata['job_min_price']!='0') && ($Formdata['job_max_price']=='0')))*/
				if((strlen($Formdata['job_min_price'])>0 || strlen($Formdata['job_max_price'])>0) && $Formdata['job_budget']==''){
					$Formdata['job_budget_type']='1'; //For budget in range
				}else{
					$Formdata['job_budget_type']='0';  //For budget as fixed price
				}
				$Formdata['job_type_of_work']=$Formdata['job_type_of_work'];
				/* EDIT JOB CODE-Starts */
				if(!empty($job)){
					unset($Formdata['job_range_type']);
					$this->modelStatic->Super_Insert('job',$Formdata,"job_id='".$job_id."'");
					$this->modelStatic->Super_Delete('job_skills',"job_id='".$job_id."'");
					foreach(explode(",",$Formdata['job_skills']) as $skills){
						$skillData = array(
							'job_id'=>$job_id,
							'skill_id'=>$skills,
						);
						$this->modelStatic->Super_Insert('job_skills',$skillData);
					}
					$objSession->successMsg = 'Job updated successfully.';
				}
				/* EDIT JOB CODE-Ends */
				/* POST NEW JOB CODE-Starts */
				else
				{

					 unset($Formdata['job_range_type']);
					if($Formdata['job_min_price'] != 0 && $Formdata['job_max_price'])
						$Formdata['job_budget'] = 0;
					$inserted = $this->modelUser->Super_Insert('job',$Formdata);

					foreach(explode(",",$Formdata['job_skills']) as $skills){
						$skillData = array(
							'job_id'=>$inserted->inserted_id,
							'skill_id'=>$skills,
						);

						$this->modelStatic->Super_Insert('job_skills',$skillData);

					}

					$sponsored_job = $this->getRequest()->getPost("sponsored_job_");
					$urgent_job = $this->getRequest()->getPost("urgent_job");
					$home_page_gallery = $this->getRequest()->getPost("home_page_gallery");
					$tb_transaction_id = $this->getRequest()->getPost("tb_transaction_id");
					if ($tb_transaction_id!="false") {

						$updateData = array(

							"job_id" => $inserted->inserted_id
						);
						$result = $this->modelStatic->Super_Insert('transactions_history',$updateData,"id='".$tb_transaction_id."'");
					}


					$date = date("Y-m-d H:i:s");

					if ($sponsored_job!="false") {

						$job_subscriptions = array(
							'job_id'=>$inserted->inserted_id,
							"subscription_id" => $sponsored_job,
							'status' => 1,
							'created_at' => $date

						);
						$this->modelStatic->Super_Insert('job_subscriptions', $job_subscriptions);

					}

					if ($urgent_job!="false") {

						$job_subscriptions = array(
							'job_id'=>$inserted->inserted_id,
							"subscription_id" => $urgent_job,
							'status' => 1,
							'created_at' => $date

						);
						$this->modelStatic->Super_Insert('job_subscriptions', $job_subscriptions);
						$users = $this->modelUser->getUserBySkills($this->getRequest()->getPost('job_skills'));
						$filePath = $this->modelUser->getCsvByUserSkills($users);
						$this->emailToAdmin($inserted->inserted_id,$filePath,$Formdata['job_budget_type']);


					}
					if ($home_page_gallery!="false") {

						$job_subscriptions = array(
							'job_id'=>$inserted->inserted_id,
							"subscription_id" => $home_page_gallery,
							'status' => 1,
							'created_at' => $date

						);
						$this->modelStatic->Super_Insert('job_subscriptions', $job_subscriptions);

					}
					// exit();
					$objSession->successMsg = 'Job posted successfully.';
				}
				/* POST NEW JOB CODE-Ends */
				$this->_redirect('job-list#myjobs');
				exit;
			}
			else if($skillForm->isValid($posted_data))
			{
				$data_insert = $skillForm->getValues() ;
				$data_insert['skill_status']=0;
				$this->modelUser->Super_Insert('skills',$data_insert);
				$objSession->successMsg = 'Request is Sent to Administrator for Approval.';
				$this->_redirect('post-job');
			}
		}
 	}
	
	public function addskillsAction()
	{
		global $objSession;
		$skillForm=new Application_Form_User();
		$skillForm->addskill();
		$this->view->skillForm=$skillForm;
		if ($this->getRequest()->isPost()){ 
 			$posted_data  = $this->getRequest()->getPost();
			if($skillForm->isValid($posted_data))
			{
				$data_insert = $skillForm->getValues() ;
				$data_insert['skill_status']=0;
				$this->modelUser->Super_Insert('skills',$data_insert);
				$objSession->successMsg = 'Request is Sent to Administrator for Approval.';
				$this->_redirect('search-jobs/work');
			}
		}
	}
	
	public function getcategoriesAction()
	{
		$value = $this->getRequest()->getParam('value');
		$Categories = $this->modelStatic->Super_Get('categories',"category_type='".$value."'","fetchAll");
		print_r(json_encode($Categories));exit;
	}
	
	public function getlatlongAction(){
		$zipcode = $this->getRequest()->getParam('zipcode');
		$country = $this->getRequest()->getParam('country');
		$location = $country.','.$zipcode;
		print_r(json_encode(getLatLong($zipcode,trim($country))));
		exit;
	}
	
	public function getcurrentlocationAction()
	{
		$ipAddress=$_SERVER['REMOTE_ADDR'];
		//$ipAddress='115.70.81.252';
		//$locations = json_decode(file_get_contents("http://ipinfo.io/".$ipAddress."/json"));
		//$locations = json_decode(file_get_contents("http://api.ipaddresslabs.com/iplocation/v1.8/locateip?key=SAK7S7EF94HH8528EWKZ&ip=".$ipAddress."&format=JSON"));
		$locations = json_decode(file_get_contents("http://api.apigurus.com/iplocation/v1.8/locateip?key=SAK7S7EF94HH8528EWKZ&ip=".$ipAddress."&format=JSON"));
		//$locations = file_get_contents('http://freegeoip.net/json/'.$ipAddress);
		
		print_r(json_encode($locations));
		exit;
	}
	
	public function joblistAction()
	{
		global $objSession;
		
		$type = $this->getRequest()->getParam('type');
		if($type=='\s+'){
			$type='list';
		}
		$this->view->type = $type;
		
		/* All Jobs-Starts */
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name')),
		);
		$jobs = $this->modelStatic->Super_Get('job',"job_status='1'","fetchAll",$extra=array('pagination'=>true,'group'=>'job.job_id'),$joinArr);
		/* All Jobs-Ends  */
		
		/* Applied Jobs-Starts */
		$joinArr=array(
			'0' => array('0'=>'job','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_id','job_user_id','job_title','job_desc','job_budget','job_closing_date','job_latitude','job_longitude','job_location')),
			'1' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name')),
		);
		$Appliedjobs = $this->modelStatic->Super_Get('job_applications',"job_app_user_id='".$this->view->user->user_id."' and job_status='1'","fetchAll",
		$extra=array('pagination'=>true,'group'=>'job_applications.job_app_job_id'),$joinArr);
		/* Applied Jobs-Ends */
		
		/* My Jobs-Starts */
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name')),
			'1'=> array('0'=>'job_applications','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_app_id','job_app_job_id')),
		);
		$Myjobs = $this->modelStatic->Super_Get('job',"job_user_id='".$this->view->user->user_id."' and job_status='1'","fetchAll",$extra=array('pagination'=>true,
		'fields'=>array('*','COUNT(job_app_job_id) as Applicants'),'group'=>'job.job_id'),$joinArr);
		/* My Jobs-Ends */
		
		/* Saved Jobs-Starts */
		$joinArr=array(
			'0' => array('0'=>'job','1'=>'job.job_id = jobs_saved.jobs_saved_job_id','2'=>'left','3'=>array('job_id','job_user_id','job_title','job_desc','job_budget','job_closing_date','job_latitude','job_longitude','job_location')),
			'1' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name')),
		);
		$Savedjobs = $this->modelStatic->Super_Get('jobs_saved',"jobs_saved_user_id='".$this->view->user->user_id."' ","fetchAll",
		$extra=array('pagination'=>true,'group'=>'jobs_saved.jobs_saved_job_id'),$joinArr);
		/* Saved Jobs-Ends */
		
		/* PAGINATOR */
		$adapter= new Zend_Paginator_Adapter_DbSelect($jobs);
		$paginator = new Zend_Paginator($adapter);
		$page = $this->_getParam('page',1);
		$rec_counts = 10; // Item per page
		$paginator->setItemCountPerPage($rec_counts);
		$paginator->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator = $paginator;
		
		$adapter1= new Zend_Paginator_Adapter_DbSelect($Appliedjobs);
		$paginator1 = new Zend_Paginator($adapter1);
		$page = $this->_getParam('page',1);
		$rec_counts = 10; // Item per page
		$paginator1->setItemCountPerPage($rec_counts);
		$paginator1->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator1, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator1 = $paginator1;
		
		$adapter2= new Zend_Paginator_Adapter_DbSelect($Myjobs);
		$paginator2 = new Zend_Paginator($adapter2);
		$page = $this->_getParam('page',1);
		$rec_counts = 10; // Item per page
		$paginator2->setItemCountPerPage($rec_counts);
		$paginator2->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator2, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator2 = $paginator2;
		
		$adapter3= new Zend_Paginator_Adapter_DbSelect($Savedjobs);
		$paginator3 = new Zend_Paginator($adapter3);
		$page = $this->_getParam('page',1);
		$rec_counts = 10; // Item per page
		$paginator3->setItemCountPerPage($rec_counts);
		$paginator3->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator3, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator3 = $paginator3;
	}
	
	public function jobdetailsAction()
	{
		global $objSession;
		$job_id = $this->getRequest()->getParam('job_id');
		$joinArr=array(
			'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
			'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('')),
			'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
			'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name","user_image")),
			'4' => array('0'=>'job_applications','1'=>'job_applications.job_app_job_id=job.job_id','2'=>'left','3'=>array("job_app_user_id","job_app_status",'job_app_complete_status','job_app_id')),
		);
		$job = $this->modelStatic->Super_Get('job',"job.job_id='".$job_id."'","fetch",$extra=array('group'=>'job.job_id'),$joinArr);
		$this->view->job = $job;
		
		/* Getting user details-Starts */
			$joinArr=array(
				'0' => array('0'=>'user_details','1'=>'users.user_id = user_details.ud_user_id','2'=>'left',
				'3'=>array('ud_user_id','ud_contact_no','ud_contact_via','ud_location','ud_intrested_in','ud_experience','ud_worker_company','ud_worker_about','ud_looking_for',
				'ud_exp_required','ud_hirer_company','ud_hirer_about','ud_pitch')),
			);
			$UserDetails = $this->modelStatic->Super_Get('users',"user_id='".$job['user_id']."'","fetch",$extra=array(),$joinArr);
			$this->view->UserDetails = $UserDetails;
		/* Getting user details-Ends */
		
		/* Getting all the jobs user has posted(LOOKING For job) - Starts */
		$HireJobs = $this->modelStatic->Super_Get('job',"job_user_id='".$job['user_id']."'","fecthAll");
		$this->view->HireJobs = $HireJobs;
		/* Getting all the jobs user has posted(LOOKING For job) - Ends */
		
		/* Getting user rating and votes - Starts */
			$extra=array('fields'=>array('rate'=>'SUM(job_rate)','count'=>'COUNT(job_rating_id)'));
			$RateData = $this->modelStatic->Super_Get('job_rating',"job_rate_for='".$UserDetails['user_id']."'",'fetch',$extra);
			$this->view->RateData = $RateData;
		/* Getting user rating and votes - Ends */
		
		/* Check if user has already applied for this job - Starts */
			$getAppData = $this->modelStatic->Super_Get('job_applications',"job_app_job_id='".$job_id."' and job_app_user_id='".$this->view->user->user_id."'",'fetch');
			$this->view->getAppData = $getAppData;
		/* Check if user has already applied for this job - Ends */
		
		/* Testimonial Data - Starts */
			$TestiData = $this->modelStatic->Super_Get('testimonials',"testimonial_job_id='".$job_id."' and testimonial_by='".$this->view->user->user_id."'",'fetch');
			$this->view->TestiData = $TestiData;
		/* Testimonial Data - Ends */
		
		if ($this->getRequest()->isPost())
		{ 
			$posted_data  = $this->getRequest()->getPost();
			
			$myOffer = str_replace('$', '', $posted_data['job_app_offer']);
			$myOffer = str_replace(' ', '', $myOffer);
			
			$JobApplyData = array(
				'job_app_job_id' => $job_id,
				'job_app_user_id' => $this->view->user->user_id,
				'job_app_message' => $posted_data['ApplyMessage'],
				//'job_app_offer' => $posted_data['job_app_offer'],
				'job_app_offer' => $myOffer,
				'job_app_date' => date('Y-m-d H:i:s'),
			);
			
			if(!empty($getAppData)){
				$this->modelStatic->Super_Insert('job_applications',$JobApplyData,"job_app_id='".$getAppData['job_app_id']."'");
			}else{
				$this->modelStatic->Super_Insert('job_applications',$JobApplyData);
				
				$jobDetails = $this->modelStatic->Super_Get('job',"job_id='".$job_id."'",'fetch');
				$userDetails = $this->modelStatic->Super_Get('users',"user_id='".$jobDetails['job_user_id']."'",'fetch');
				$emailData = array(
					'name'=>$userDetails['user_first_name'].' '.$userDetails['user_last_name'],
					'email'=>$userDetails['user_email'],
					'job_title'=>$jobDetails['job_title'],
					'job_id'=>$jobDetails['job_id'],
				);
				
				$this->modelEmail->sendEmail('job_application_notification',$emailData);
			}
			$objSession->successMsg = 'Your application was submitted successfully.';
			$this->_redirect('job-details/'.$job_id);
		}	
	}
	
	public function jobapplicantsAction()
	{
		global $objSession;
		$this->_helper->layout->disableLayout();
		$job_id = $this->getRequest()->getParam('job_id');
		
		$joinArr=array(
			'0' => array('0'=>'job','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_id','job_user_id','job_title')),
			'1' => array('0'=>'users','1'=>'users.user_id = job_applications.job_app_user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
		);
		$Applicants = $this->modelStatic->Super_Get('job_applications',"job_app_job_id='".$job_id."'","fetchAll",$extra=array(),$joinArr);
		$this->view->Applicants = $Applicants;
		
		$job_app_id = $this->getRequest()->getParam('job_app_id');
		if(isset($job_app_id)){
			$updateData = array(
				'job_app_status'=>'1'
			);
			$this->modelStatic->Super_Insert('job_applications',$updateData,"job_app_id='".$job_app_id."'");
			
			$JobApplicationData = $this->modelStatic->Super_Get('job_applications',"job_app_id='".$job_app_id."'","fetch");
			$jobDetails = $this->modelStatic->Super_Get('job',"job_id='".$job_id."'",'fetch');
			$userDetails = $this->modelStatic->Super_Get('users',"user_id='".$JobApplicationData['job_app_user_id']."'",'fetch');
			$emailData = array(
				'name'=>$userDetails['user_first_name'].' '.$userDetails['user_last_name'],
				'email'=>$userDetails['user_email'],
				'job_title'=>$jobDetails['job_title'],
				'job_id'=>$jobDetails['job_id'],
			);
			
			$this->modelEmail->sendEmail('job_awarded',$emailData);
			
			
			/* SEND NOTIFICATION TO USER FOR RATING START */
			$notifyData=array(
						'notify_type'=>2,
						'notify_for_user_id'=>$userDetails['user_id'],
						'notify_refer_id'=>$job_app_id,
						'notify_date'=>date('Y-m-d H:i:s'),
					);
		    $this->modelStatic->Super_Insert("notifications",$notifyData);
			
			$objSession->successMsg = 'Job asssigned successfully.';
			$this->_redirect('job-list');
		}
	}
	
	public function jobratingAction()
	{
		global $objSession;
		$form = new Application_Form_Job();
		$form->rate();
		$this->view->form = $form;
		$job_id = $this->getRequest()->getParam('job_id');
		$edit = $this->getRequest()->getParam('edit');
		
		if ($this->getRequest()->isPost()){ 
 			$posted_data  = $this->getRequest()->getPost();
			if ($form->isValid($this->getRequest()->getPost()))
			{ 
			//prd($posted_data);
				if($edit==0){
					$RateData=array(
						'job_rating_job_id'=>$posted_data['job_rating_job_id'],
						'job_rate'=>$posted_data['review_star'],
						'job_vote'=>$posted_data['job_vote'],
						'job_rate_by'=>$posted_data['job_rate_by'],
						'job_rate_for'=>$posted_data['job_rate_for'],
						'job_feedback'=>$posted_data['job_feedback'],
					);
					$ratingData=$this->modelStatic->Super_Insert('job_rating',$RateData);
					
					/* SEND NOTIFICATION TO USER FOR RATING START */
					$notifyData=array(
								'notify_type'=>4,
								'notify_for_user_id'=>$posted_data['job_rate_for'],
								'notify_refer_id'=>$ratingData->inserted_id,
								'notify_date'=>date('Y-m-d H:i:s'),
							);
				   $this->modelStatic->Super_Insert("notifications",$notifyData);
				  
				}else{
					$RateData=array(
						'job_rate'=>$posted_data['review_star'],
						'job_vote'=>$posted_data['job_vote'],
						'job_feedback'=>$posted_data['job_feedback'],
					);
					$where= "job_rating_job_id='".$posted_data['job_rating_job_id']."' and job_rate_by='".$posted_data['job_rate_by']."' and 
					job_rate_for='".$posted_data['job_rate_for']."'";
					$this->modelStatic->Super_Insert('job_rating',$RateData,$where);
				}
				$objSession->successMsg='Rating done successfully.';
			}
		}
		
		$this->_redirect('job-details/'.$job_id);
	}
	
	public function savetestimonialAction()
	{
		global $objSession;
		$job_id = $this->getRequest()->getParam('job_id');
		$testedit = $this->getRequest()->getParam('testedit');
		
		if ($this->getRequest()->isPost()){ 
 			$posted_data  = $this->getRequest()->getPost();
			if($testedit==0){
				$TestData=array(
					'testimonial_text'=>$posted_data['testimonial'],
					'testimonial_by'=>$this->view->user->user_id,
					'testimonial_for'=>$posted_data['testimonial_for'],
					'testimonial_job_id'=>$job_id,
				);
				$this->modelStatic->Super_Insert('testimonials',$TestData);
				$objSession->successMsg='Testimonial added successfully.';
			}else{
				$TestData=array(
					'testimonial_text'=>$posted_data['testimonial'],
					'testimonial_by'=>$this->view->user->user_id,
					'testimonial_for'=>$posted_data['testimonial_for'],
					'testimonial_job_id'=>$job_id,
				);
				$where= "testimonial_by='".$this->view->user->user_id."' and testimonial_for='".$posted_data['testimonial_for']."' and testimonial_job_id='".$job_id."'";
				$this->modelStatic->Super_Insert('testimonials',$TestData,$where);
				$objSession->successMsg='Testimonial updated successfully.';
			}
		}
		
		$this->_redirect('job-details/'.$job_id);
	}
	
	public function jobboardAction()
	{
		global $objSession;
		$job_id = $this->getRequest()->getParam('job_id');
		$joinArr=array(
			'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
			'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('')),
			'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
			'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name")),
		);
		$job = $this->modelStatic->Super_Get('job',"job.job_id='".$job_id."'","fetch",$extra=array('group'=>'job.job_id'),$joinArr);
		$this->view->job = $job;
	}
	
	public function searchjobsAction()
	{
		global $objSession;
		$skillForm=new Application_Form_User();
		$skillForm->addskill();
		$this->view->skillForm=$skillForm;
		
		$form = new Application_Form_Job();
		$form->searchjobs();
		$type = $this->getRequest()->getParam('type');
		if(isset($_GET['tab'])){
			$this->view->tab = $_GET['tab'];
		}else{
			$this->view->tab = '';
		}
		
		if(isset($_GET['jobId'])){
			$this->view->jobId = $_GET['jobId'];
		}else{
			$this->view->jobId = '';
		}

		if($type=='\s+'){
			$type='map';
		}
		$this->view->type = $type;
		if($this->getRequest()->isPost()){ 
 			$posted_data  = $this->getRequest()->getPost();
			
			$data = $posted_data;
				//prn($posted_data);
				$joinArr=array(
					'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
					'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('skill_id')),
					'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
					'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name","user_image")),
					'4' => array('0'=>'job_subscriptions','1'=>'job.job_id = job_subscriptions.job_id','2'=>'left','3'=>array("job_subscriptions.subscription_id","job_subscriptions.status")),
					//'4' => array('0'=>'job_applications','1'=>'job_app_job_id= job_id','2'=>'left','3'=>array("job_app_user_id","job_app_status")),
				);
				$extra = array('group'=>'job.job_id');
				/*$joinArr = array(
					'0' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('job_skills_id')),
					'1' => array('0'=>'users','1'=>'job_user_id = user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
				);*/
				$distanceQuery='';
				$where = "job_user_id!='".$this->view->user->user_id."' and job_closing_date >= NOW()";
				
				if($data['job_latitude']!='' && ($data['job_location']!='' || $data['job_country']!='') && $data['job_area']!='1')
				{
					$filter_lat = $data['job_latitude'];
					$filter_long = $data['job_longitude'];
					$distanceQuery = "(((ACOS(SIN(".$filter_lat." * PI() / 180) * SIN(job_latitude * PI() / 180) + COS(".$filter_lat." * PI() / 180) * COS(job_latitude * PI() / 180) * COS((".$filter_long."-job_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344))";
					// for KM -> 60 * 1.1515 * 1.609344, for MILES -> 60 * 1.1515
					$extra = array('group'=>'job.job_id','fields'=>array('*' ,'distance'=>new Zend_Db_Expr($distanceQuery)) ,'having'=>"distance<='".$data['job_area']."'" );
				}
				else if($data['job_location']!='' && $data['job_country']!=''){
					$where="job_location LIKE '".$data['job_country']."%' and ".$where;
					$extra = array('group'=>'job.job_id','fields'=>array('*'));
				}
				else{
					$extra = array('group'=>'job.job_id','fields'=>array('*'));
				}
				
				$skills='';
				
				if(!empty($data['job_skills'])){
					$skills=implode(",",$data['job_skills']);
				}
				
				if($data['job_budget']!='')
				{
					$where = "job_budget='".number_format((float)$data['job_budget'], 2, '.', '')."'"." and job_fix_type='".$data['job_fix_type']."' and ".$where;
				}
				else if($data['job_min_price']!='')
				{
					$where = "(job_min_price <= '".$data['job_min_price']."' and job_max_price>='".$data['job_min_price']."') or 
					(job_min_price <= '".$data['job_max_price']."' and job_max_price>='".$data['job_max_price']."')"." or ".$where;
				}
				
				if($skills!='')
				{
					$where = "job_skills.skill_id IN ('".$skills."')"." and ".$where;
				}
				if($data['job_closing_date']!='')
				{
					$where = "job_closing_date >= '".date('Y-m-d',strtotime($data['job_closing_date']))."'"." and ".$where;
				}
				if($data['job_category']!='')
				{
					$where = "job_category = '".$data['job_category']."'"." and ".$where;
				}
				if($data['job_category']!='')
				{
					$where = "job_category = '".$data['job_category']."'"." and ".$where;
				}
				if($data['job_type_of_work']!='')
				{
					$where = "job_type_of_work = '".$data['job_type_of_work']."'"." and ".$where;
				}
				if(!empty($data['job_type']))
				{
					$where = "job_type = '".$data['job_type']."'"." and ".$where;
				}
			$extra['order'] = 'job.job_id desc';
			$where = chop($where," and ");
			$sql = "SELECT `job`.*, `categories`.`category_title`, `job_skills`.`skill_id`, GROUP_CONCAT(\" \",skill_title) AS `skills`, `users`.`user_id`, `users`.`user_first_name`, `users`.`user_last_name`, `users`.`user_image`, `subscriptions`.`subscription_id`, `subscriptions`.`status` FROM `job`
 LEFT JOIN `categories` ON job.job_category = categories.category_id
 LEFT JOIN `job_skills` ON job.job_id = job_skills.job_id
 LEFT JOIN `skills` ON skills.skill_id = job_skills.skill_id
 LEFT JOIN `users` ON users.user_id = job.job_user_id
 LEFT JOIN (SELECT * FROM job_subscriptions where job_subscriptions.subscription_id = 2) subscriptions ON job.job_id = subscriptions.job_id WHERE (".$where.") GROUP BY `job`.`job_id` ORDER BY `job`.`job_id` desc";

			//$jobs = $this->modelStatic->Super_Get('job',$where,"fetchAll",$extra,$joinArr);
			$jobs = $this->modelStatic->Super_Raw($sql);
			if(count($jobs) > 30)
			{
				$jobs = array_slice($jobs, 0, 30);
			}
			$sponsoredJobs = array();
			$otherJobs = array();
			foreach($jobs as $index => $job)
			{
				if(($job['subscription_id'] == 2) && ($job['status'] == 1))
					$sponsoredJobs[] = $job;
				else
					$otherJobs[] = $job;
			}
			$jobs = array_merge($sponsoredJobs, $otherJobs);
			$this->view->jobs = $jobs;
			$form->populate($posted_data);
			$this->view->posted_data = $posted_data;
		}
		else{
			if(!empty($_GET)){
				$posted_data  = $_GET;
				$data = $posted_data;
				//prn($posted_data);

				//SELECT * FROM job_subscriptions where job_subscriptions.subscription_id = 2
				$innerQuery = $this->modelStatic->getAdapter()->select()
					->from('job_subscriptions')
                    ->where('subscription_id = ?', 2);
				$joinArr=array(
					'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
					'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('skill_id')),
					'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
					'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name","user_image")),
					'4' => array('0'=>$innerQuery,'1'=>'job.job_id = t.job_id','2'=>'left','3'=>array("t.subscription_id","t.status")),
				);
				$extra = array('group'=>'job.job_id','pagination'=>1);

				/*$joinArr = array(
					'0' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('job_skills_id')),
					'1' => array('0'=>'users','1'=>'job_user_id = user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
				);*/
				$distanceQuery='';
				//$where = "job_user_id!='".$this->view->user->user_id."' and job_closing_date >= NOW()";
				$where = "job_user_id!='".$this->view->user->user_id."'and job_closing_date >= NOW()";
				/*if($data['job_latitude']!='' && ($data['job_location']!='' || $data['job_country']!='') && $data['job_area']!='1')*/
				if($data['job_latitude']!='' && ($data['job_location']!='') && $data['job_area']!='1')
				{
					$filter_lat = $data['job_latitude'];
					$filter_long = $data['job_longitude'];
					$distanceQuery = "(((ACOS(SIN(".$filter_lat." * PI() / 180) * SIN(job_latitude * PI() / 180) + COS(".$filter_lat." * PI() / 180) * COS(job_latitude * PI() / 180) * COS((".$filter_long."-job_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344))";
					// for KM -> 60 * 1.1515 * 1.609344, for MILES -> 60 * 1.1515
					$extra = array('group'=>'job.job_id','fields'=>array('*','distance'=>new Zend_Db_Expr($distanceQuery)),'having'=>"distance<='".$data['job_area']."'");
				}
				else if($data['job_location']!='' || $data['job_country']!=''){
					$where="job_location LIKE '".$data['job_country']."%' and ".$where;
					$extra = array('group'=>'job.job_id','fields'=>array('*'));
				}
				else{
					$extra = array('group'=>'job.job_id','fields'=>array('*'));
				}
				//$this->db->query('SELECT msg_id,msg_job,notify_id,notify_type,notify_date,user_first_name as user_name, job_title, job_id, notify_status, user_image, user_first_name,user_last_name FROM notifications join messages on msg_id=notify_refer_id join job on job_id=msg_job join users on user_id=msg_from where '.$messageWhere.' group by notify_id')->fetchAll();
				$skills='';
				
				if(!empty($data['job_skills'])){
					$skills=implode(",",$data['job_skills']);
				}

				if($data['job_fix_type']!='')
				{
					$where = "job_fix_type = '".$data['job_fix_type']."' and ".$where;
				}

				/*
				if($data['job_budget']!='')
				{
					//$where = "job_budget='".number_format((float)$data['job_budget'], 2, '.', '')."'"." and job_fix_type='".$data['job_fix_type']."' and ".$where;
					$where = "(job_budget='".number_format((float)$data['job_budget'], 2, '.', '')."'"." and job_fix_type='".$data['job_fix_type']."') and ".$where;

				}*/
				/*
				if($data['job_min_price']!='')
				{
					/*$where = "((job_min_price <= '".$data['job_min_price']."' and job_max_price>='".$data['job_min_price']."') or 
					(job_min_price <= '".$data['job_max_price']."' and job_max_price>='".$data['job_max_price']."'))"." and ".$where;
					$where = "(job_min_price >= '".$data['job_min_price']."' or job_budget >= '".$data['job_min_price']."')"." and ".$where;
				}
				
				if($data['job_max_price']!='')
				{
					/*$where = "((job_min_price <= '".$data['job_min_price']."' and job_max_price>='".$data['job_min_price']."') or 
					(job_min_price <= '".$data['job_max_price']."' and job_max_price>='".$data['job_max_price']."'))"." and ".$where;
					$where = "(job_max_price <= '".$data['job_max_price']."' or job_budget <= '".$data['job_max_price']."')"." and ".$where;
				}*/

				if($data['job_min_price']!='' && $data['job_max_price']!='')
				{
					$where = "((job_min_price >= '".$data['job_min_price']."' and job_max_price <= '".$data['job_max_price']."')".
						"or (job_budget >= '".number_format((float)$data['job_min_price'], 2, '.', '')."' and job_budget <= '".number_format((float)$data['job_max_price'], 2, '.', '')."'))"." and ".$where;
				}elseif($data['job_min_price']!='' && $data['job_max_price']=='')
				{
					$where = "(job_min_price >= '".$data['job_min_price'].
						"' and job_budget >= '".$data['job_min_price']."')"." and ".$where;
				}elseif($data['job_min_price']=='' && $data['job_max_price']!='')
				{
					$where = "(job_max_price <= '".$data['job_max_price'].
						"' and job_budget <= '".$data['job_max_price']."')"." and ".$where;
				}

				if($skills!='')
				{
					$where = "job_skills.skill_id IN ('".$skills."')"." and ".$where;
				}
				if($data['job_closing_date']!='')
				{
					$where = "job_closing_date <= '".date('Y-m-d',strtotime($data['job_closing_date']))."'"." and ".$where;
				}
				if($data['job_category']!='')
				{
					$where = "job_category = '".$data['job_category']."'"." and ".$where;
				}
				if($data['job_type_of_work']!='')
				{
					$where = "job_type_of_work = '".$data['job_type_of_work']."'"." and ".$where;
				}
				if($data['job_type']!='')
				{
					$where = "job_type = '".$data['job_type']."'"." and ".$where;
				}
				$extra['order'] = 'job.job_id desc';
				$where = chop($where," and ");
				$sql = "SELECT `job`.*, `categories`.`category_title`, `job_skills`.`skill_id`, GROUP_CONCAT(\" \",skill_title) AS `skills`, `users`.`user_id`, `users`.`user_first_name`, `users`.`user_last_name`, `users`.`user_image`, `subscriptions`.`subscription_id`, `subscriptions`.`status` FROM `job`
 LEFT JOIN `categories` ON job.job_category = categories.category_id
 LEFT JOIN `job_skills` ON job.job_id = job_skills.job_id
 LEFT JOIN `skills` ON skills.skill_id = job_skills.skill_id
 LEFT JOIN `users` ON users.user_id = job.job_user_id
 LEFT JOIN (SELECT * FROM job_subscriptions where job_subscriptions.subscription_id = 2) subscriptions ON job.job_id = subscriptions.job_id WHERE (".$where.") GROUP BY `job`.`job_id` ORDER BY `job`.`job_id` desc";

				$jobs = $this->modelStatic->Super_Get('job',$where,"fetchAll",$extra,$joinArr);
				//$jobs = $this->modelStatic->Super_Raw($sql);
				if(count($jobs) > 30)
				{
					$jobs = array_slice($jobs, 0, 30);
				}
				$sponsoredJobs = array();
				$otherJobs = array();
				foreach($jobs as $index => $job)
				{
					if(($job['subscription_id'] == 2) && ($job['status'] == 1))
						$sponsoredJobs[] = $job;
					else
						$otherJobs[] = $job;
				}
				$jobs = array_merge($sponsoredJobs, $otherJobs);
				$this->view->jobs = $jobs;
				//prd($jobs);
				$form->populate($posted_data);

				$this->view->posted_data = $posted_data;
			}
			
		}
		$this->view->form = $form;
	}
	
	public function getsimilarjobsAction()
	{
		$location = $this->getRequest()->getParam('location');
		$ids = $this->getRequest()->getParam('ids');
		$jobliststatus = $this->getRequest()->getParam('jobliststatus');
		if($jobliststatus==1){
			$joinArr = array(
			'0' => array('0'=>'job_applications','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_app_status','job_app_user_id','job_app_message')),
			);
			$extra = array('group'=>'job_id','fields'=>array('*','COUNT(job_app_job_id) as Applicants'));
		}else{
			$joinArr=array(
				'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
				'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('skill_id')),
				'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
				'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name","user_image")),
				'4' => array('0'=>'job_applications','1'=>'job_applications.job_app_job_id=job.job_id','2'=>'left','3'=>array("job_app_user_id","job_app_status")),
			);
			/*$joinArr = array(
				'0' => array('0'=>'users','1'=>'job_user_id = user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
			);*/
			$extra = array('group'=>'job.job_id');
		}
		//$jobs = $this->modelStatic->Super_Get('job',"job_location='".$location."' and job.job_id IN(".$ids.")","fetchAll",$extra,$joinArr);
		$sql = "SELECT `job`.*, `categories`.`category_title`, `job_skills`.`skill_id`, GROUP_CONCAT(\" \",skill_title) AS `skills`, `users`.`user_id`, `users`.`user_first_name`, `users`.`user_last_name`, `users`.`user_image`, `job_applications`.`job_app_user_id`, `job_applications`.`job_app_status`, `subscriptions`.`subscription_id`, `subscriptions`.`status` FROM `job` LEFT JOIN `categories` ON job.job_category = categories.category_id LEFT JOIN `job_skills` ON job.job_id = job_skills.job_id LEFT JOIN `skills` ON skills.skill_id = job_skills.skill_id LEFT JOIN `users` ON users.user_id = job.job_user_id LEFT JOIN `job_applications` ON job_applications.job_app_job_id=job.job_id
LEFT JOIN (SELECT * FROM job_subscriptions where job_subscriptions.subscription_id = 2) subscriptions ON job.job_id = subscriptions.job_id
WHERE (job_location='$location' and job.job_id IN($ids)) GROUP BY `job`.`job_id";
		//echo $location.$ids;die;
		$jobs = $this->modelStatic->Super_Raw($sql);
		print_r(json_encode($jobs));exit;
	}
	
	public function savejobAction()
	{
		$job_id = $this->getRequest()->getParam('jobId');
		$saveData = array(
			'jobs_saved_job_id'=>$job_id,
			'jobs_saved_user_id'=>$this->view->user->user_id
		);
		$this->modelStatic->Super_Insert('jobs_saved',$saveData);
		exit;
	}
	
	public function jobpreviewAction()
	{
		$this->_helper->layout->disableLayout();
		$job_id = $this->getRequest()->getParam('id');
		
		/* Check if user has already applied for the job - Starts */
		$CheckJobApplication = $this->modelStatic->Super_Get("job_applications","job_app_user_id='".$this->view->user->user_id."' and job_app_job_id='".$job_id."'","fetch");
		//prd($CheckJobApplication);
		if(!empty($CheckJobApplication)){
			echo "0";exit;			
		}
		/* Check if user has already applied for the job - Ends */
		
		$joinArr=array(
			'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
			'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array('')),
			'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
			'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name")),
		);
		$job = $this->modelStatic->Super_Get('job',"job.job_id='".$job_id."'","fetch",$extra=array('group'=>'job.job_id'),$joinArr);
		$this->view->job = $job;
		
		/* Getting user details-Starts */
			$joinArr=array(
				'0' => array('0'=>'user_details','1'=>'users.user_id = user_details.ud_user_id','2'=>'left',
				'3'=>array('ud_user_id','ud_contact_no','ud_contact_via','ud_location','ud_intrested_in','ud_experience','ud_worker_company','ud_worker_about','ud_looking_for',
				'ud_exp_required','ud_hirer_company','ud_hirer_about','ud_pitch')),
			);
			$UserDetails = $this->modelStatic->Super_Get('users',"user_id='".$job['user_id']."'","fetch",$extra=array(),$joinArr);
			$this->view->UserDetails = $UserDetails;
		/* Getting user details-Ends */
		
		/* Getting all the jobs user has posted(LOOKING For job) - Starts */
		$HireJobs = $this->modelStatic->Super_Get('job',"job_user_id='".$job['user_id']."'","fecthAll");
		$this->view->HireJobs = $HireJobs;
		/* Getting all the jobs user has posted(LOOKING For job) - Ends */
		
		/* Getting user rating and votes - Starts */
			$extra=array('fields'=>array('rate'=>'SUM(job_rate)','count'=>'COUNT(job_rating_id)'));
			$RateData = $this->modelStatic->Super_Get('job_rating',"job_rate_for='".$UserDetails['user_id']."'",'fetch',$extra);
			$this->view->RateData = $RateData;
		/* Getting user rating and votes - Ends */
		
		/* Check if job saved */
		$jobSaveData = $this->modelStatic->Super_Get("jobs_saved","jobs_saved_job_id='".$job_id."' and jobs_saved_user_id='".$this->view->user->user_id."'",'fetch');
		$jobSaved=0;
		if(!empty($jobSaveData)){
			$jobSaved=1;
		}
		echo "||".$job['job_latitude']."||".$job['job_longitude']."||".$jobSaved;
	}
	
	public function jobsAction()
	{
		global $objSession;
		$type = $this->getRequest()->getParam('type');
		$this->view->type = $type;
		$user_id = $this->getRequest()->getParam('user_id');
		/* LOOKING FOR WORK - Starts */
		if($type=='lookingforwork'){
			$this->view->title = 'LOOKING FOR WORK';
			$joinArr=array(
				'0' => array('0'=>'job','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_id','job_title')),
			);
			//$Jobs = $this->modelStatic->Super_Get('job_applications',"job_app_user_id='".$user_id."'","fecthAll",$extra=array('pagination'=>'true'),$joinArr);
			$Jobs = $this->modelStatic->Super_Get('job_applications',"job_app_user_id='".$user_id."' and job_app_status='0'","fecthAll",$extra=array('pagination'=>'true'),$joinArr);
		}
		/* LOOKING FOR WORK - Ends */
		/* PREVIOUS JOBS - Starts */
		else if($type=='previousjobs'){
			$this->view->title = 'PREVIOUS JOBS';
			$joinArr=array(
				'0' => array('0'=>'job','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_id','job_title')),
			);
			$Jobs = $this->modelStatic->Super_Get('job_applications',"job_app_user_id='".$user_id."' and job_app_status='1'","fecthAll",$extra=array('pagination'=>'true'),$joinArr);
		}
		/* PREVIOUS JOBS - Ends */
		/* LOOKING For job - Starts */
		else if($type=='lookingtohire'){
			$this->view->title = 'LOOKING TO HIRE';
			//add code here
			$Jobs = $this->modelStatic->getLookingToHireJobs($user_id);
		}
		/* LOOKING For job - Ends */
		/* PREVIOUS HIRINGS - Starts */
		else if($type=='previoushirings'){
			$this->view->title = 'PREVIOUS HIRINGS';
			$joinArr=array(
				'0' => array('0'=>'job_applications','1'=>'job.job_id = job_applications.job_app_job_id','2'=>'left','3'=>array('job_app_id','job_app_user_id', 'job_app_date')),
			);
			$Jobs = $this->modelStatic->Super_Get('job',"job_user_id='".$user_id."' and job_app_status='1'","fecthAll",$extra=array('pagination'=>'true'),$joinArr);
		}
		/* PREVIOUS HIRINGS - Ends */
		$this->view->Jobs = $Jobs;

		if(is_array($Jobs))
		{
			$adapter= new Zend_Paginator_Adapter_Array($Jobs);
		}
		else
		{
			$adapter= new Zend_Paginator_Adapter_DbSelect($Jobs);
		}

		$paginator = new Zend_Paginator($adapter);
		$page = $this->_getParam('page',1);
		$rec_counts = 10; // Item per page
		$paginator->setItemCountPerPage($rec_counts);
		$paginator->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator = $paginator;
	}
	
	public function applyjobAction()
	{
		$this->_helper->layout->disableLayout();
		$job_id = $this->getRequest()->getParam('id');
		$joinArr=array(
			'0' => array('0'=>'categories','1'=>'job.job_category = categories.category_id','2'=>'left','3'=>array('category_title')),
			'1' => array('0'=>'job_skills','1'=>'job.job_id = job_skills.job_id','2'=>'left','3'=>array(''),),
			'2' => array('0'=>'skills','1'=>'skills.skill_id = job_skills.skill_id','2'=>'left','3'=>array('GROUP_CONCAT(" ",skill_title) as skills')),
			'3' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array("user_id","user_first_name","user_last_name")),
		);
		$job = $this->modelStatic->Super_Get('job',"job.job_id='".$job_id."'","fetch",$extra=array('group'=>'job.job_id'),$joinArr);
		$this->view->job = $job;
		
		/* Check if user has already applied for this job - Starts */
			$getAppData = $this->modelStatic->Super_Get('job_applications',"job_app_job_id='".$job_id."' and job_app_user_id='".$this->view->user->user_id."'",'fetch');
			$this->view->getAppData = $getAppData;
		/* Check if user has already applied for this job - Ends */
		
		if(!empty($_REQUEST['ApplyMessage'])){
			
			$myOffer = str_replace('$', '', $_REQUEST['job_app_offer']);
			$myOffer = str_replace(' ', '', $myOffer);
			
			$JobApplyData = array(
				'job_app_job_id' => $job_id,
				'job_app_user_id' => $this->view->user->user_id,
				'job_app_message' => $_REQUEST['ApplyMessage'],
				//'job_app_offer' => $_REQUEST['job_app_offer'],
				'job_app_offer' => $myOffer,
				'job_app_date' => date('Y-m-d H:i:s')	,
			);
			if(!empty($getAppData)){
				$jobAppData=$this->modelStatic->Super_Insert('job_applications',$JobApplyData,"job_app_id='".$getAppData['job_app_id']."'");
			}else{
				$jobAppData=$this->modelStatic->Super_Insert('job_applications',$JobApplyData);
				$jobDetails = $this->modelStatic->Super_Get('job',"job_id='".$job_id."'",'fetch');
				$userDetails = $this->modelStatic->Super_Get('users',"user_id='".$jobDetails['job_user_id']."'",'fetch');
				$emailData = array(
					'name'=>$userDetails['user_first_name'].' '.$userDetails['user_last_name'],
					'email'=>$userDetails['user_email'],
					'job_title'=>$jobDetails['job_title'],
					'job_id'=>$jobDetails['job_id'],
				);
				
				$this->modelEmail->sendEmail('job_application_notification',$emailData);
				
				/* SEND NOTIFICATION TO USER FOR RATING START */
				$notifyData=array(
							'notify_type'=>1,
							'notify_for_user_id'=>$job['job_user_id'],
							'notify_refer_id'=>$jobAppData->inserted_id,
							'notify_date'=>date('Y-m-d H:i:s'),
						);
			   $this->modelStatic->Super_Insert("notifications",$notifyData);
			}
		}
	}
	
	public function serviceprovidersAction()
	{
		global $objSession;
		$type = $this->getRequest()->getParam('type');
		$this->view->type = $type;
		
		$joinArr=array(
			'0' => array('0'=>'user_details','1'=>'user_id = ud_user_id','2'=>'full','3'=>array('ud_location','ud_intrested_in','ud_experience','ud_worker_company',
			'ud_worker_about','ud_latitude','ud_longitude')),
		);
		$AllUsers = $this->modelStatic->Super_Get('users',"user_type='user' and user_status='1'",'fetchAll',$extra=array('pagination'=>true),$joinArr);
		$this->view->AllUsers = $AllUsers;
		//prd($AllUsers);
		$form = new Application_Form_Job();
		$form->searchserviceproviders();
		
		if ($this->getRequest()->isPost()){ 
 			$posted_data  = $this->getRequest()->getPost();
			$form->populate($posted_data);
			$db = Zend_Registry::get('db');
			$search_txt_val = $db->quote("%".trim($posted_data['keyword'])."%");
			$search_location_val = $db->quote("%".trim($posted_data['location'])."%");
			
			$where = "user_type='user' ";
			if(!empty($posted_data['keyword'])){
				$where = $where." and (user_first_name LIKE $search_txt_val or user_last_name LIKE $search_txt_val or CONCAT(user_first_name, ' ', user_last_name) LIKE $search_txt_val)";
			}
			
			if(!empty($posted_data['location'])){
				$where = $where." and (ud_location LIKE $search_location_val)";
			}
			
			$skillWhere='';
			if(!empty($posted_data['skills'])){
				$skillArray = $posted_data['skills'];
				foreach($skillArray as $skills)
				{
					$skillWhere = $skillWhere." FIND_IN_SET(".$skills.",ud_intrested_in) or";
				}
				$skillWhere = chop($skillWhere,'or');
			}
			
			if($skillWhere!=''){
				$where = $where.' and ('.$skillWhere.')';
			}
			$AllUsers = $this->modelStatic->Super_Get('users',$where,'fetchAll',$extra=array('pagination'=>true),$joinArr);
			$this->view->AllUsers = $AllUsers;
		}
		$this->view->form = $form;
		
		$adapter= new Zend_Paginator_Adapter_DbSelect($AllUsers);
		$paginator = new Zend_Paginator($adapter);
		$page = $this->_getParam('page',1);
		$rec_counts = 12; // Item per page
		$paginator->setItemCountPerPage($rec_counts);
		$paginator->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator = $paginator;
	}
	
	public function jobhireAction()
	{
		global $objSession;
		$user_id = $this->getRequest()->getParam('user_id');
		$this->view->user_id = $user_id;
		$type = $this->getRequest()->getParam('type');
		$this->view->type = $type;
		
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = job.job_user_id','2'=>'left','3'=>array('user_id','user_first_name','user_last_name')),
		);
		$jobs = $this->modelStatic->Super_Get('job',"job_user_id='".$this->view->user->user_id."'","fetchAll",$extra=array('pagination'=>true,'group'=>'job.job_id'),$joinArr);
		
		/* PAGINATOR- PREVIOUSLY POSTED CARES */
		$adapter= new Zend_Paginator_Adapter_DbSelect($jobs);
		$paginator = new Zend_Paginator($adapter);
		$page = $this->_getParam('page',1);
		$rec_counts = 10; // Item per page
		$paginator->setItemCountPerPage($rec_counts);
		$paginator->setCurrentPageNumber($page);
		$paginationControl = new Zend_View_Helper_PaginationControl($paginator, 'sliding', 'pagination-control.phtml');
		$this->view->paginationControl = $paginationControl;
		$this->view->paginator = $paginator;
	}
	
	public function completejobAction()
	{
		global $objSession;
		$jobapp_id = $this->getRequest()->getParam('jobapp_id');
		$job_id = $this->getRequest()->getParam('job_id');
		$this->modelStatic->Super_Insert('job_applications',array('job_app_complete_status'=>1),"job_app_id='".$jobapp_id."'");
		$jobAppData=$this->modelStatic->Super_Get('job_applications',"job_app_id='".$jobapp_id."'","fetch",array('fields'=>'job_app_user_id'));
		
		/* SEND NOTIFICATION TO USE5 FOR JOB COMPLETION START */
		$notifyData=array(
					'notify_type'=>3,
					'notify_for_user_id'=>$jobAppData['job_app_user_id'],
					'notify_refer_id'=>$jobapp_id,
					'notify_date'=>date('Y-m-d H:i:s'),
				);
	   $this->modelStatic->Super_Insert("notifications",$notifyData);
			   
			   
		$objSession->successMsg='Job Completed Successfully.';
		$this->_redirect('job-details/'.$job_id);
	}
	
	public function hireinviteAction()
	{
		global $objSession;
		$user_id = $this->getRequest()->getParam('user_id');
		$userDetails = $this->modelStatic->Super_Get('users',"user_id='".$user_id."'",'fetch');
		
		$job_id = $this->getRequest()->getParam('job_id');
		$JobDetails = $this->modelStatic->Super_Get('job',"job_id='".$job_id."'",'fetch');
		
		$emailData = array(
			'name'=>$userDetails['user_first_name'].' '.$userDetails['user_last_name'],
			'email'=>$userDetails['user_email'],
			'job_title'=>$JobDetails['job_title'],
			'job_id'=>$JobDetails['job_id'],
		);
		$this->modelEmail->sendEmail('invitation_for_job',$emailData);
		$objSession->successMsg='Job invitation sent successfully.';
		//$this->_redirect('service-providers');
		$this->_redirect('workers');
	}

	public function emailToAdmin($jobId,$filePath,$job_budget_type){


		$user_id = 	$logged_identity = Zend_Auth::getInstance()->getInstance();
		$userInfo = $user_id->getIdentity();
		$skillTitles=array();
		$hirerEmail = $userInfo->user_email;
		$where="ud_user_id=".$userInfo->user_id;
		$hirerPhone = $this->modelStatic->Super_Get('user_details',$where,"fetchAll",["fields"=>"user_details.ud_contact_no"],[]);
		$where = "category_id=".$this->getRequest()->getPost('job_category');
		$job_category = $this->modelStatic->Super_Get('categories',$where,"fetchAll",["fields"=>"categories.category_title"],[]);
		$job_category = $job_category[0]["category_title"];
		$parms = $this->getRequest()->getParams();
		$phone = $hirerPhone[0]["ud_contact_no"];
		$deadLine=$this->getRequest()->getPost('job_closing_date');
		$job_budget_type=$job_budget_type;
		$job_fix_type=$this->getRequest()->getPost('job_fix_type');

		$job_budget=$this->getRequest()->getPost('job_budget');
		$job_title=$this->getRequest()->getPost('job_title');
		$typeofworkAray = array('0'=>'Full Time','1'=>'Part Time');
		$typeofwork  = $typeofworkAray[$this->getRequest()->getPost("job_type_of_work")];
		$job_min_price=$this->getRequest()->getPost('job_min_price');
		$job_max_price=$this->getRequest()->getPost('job_max_price');
		$job_code=$this->getRequest()->getPost('job_location');
		$job_skills = implode(',',$this->getRequest()->getPost('job_skills'));
		$where = "skill_id IN ($job_skills)";
		$skill_titles = $this->modelStatic->Super_Get('skills',$where,"fetchAll",["fields"=>"skills.skill_title"],[]);
		//$db->query("SELECT skill_title FROM skills WHERE skill_id IN ($job_skills)")->fetchAll();


		foreach ($skill_titles as $skill):

			$skillTitles[] = $skill["skill_title"];

		endforeach;

		$data = [

			"skill_titles" => $skillTitles,
			"hirer_email"  => $hirerEmail,
			"hirer_phone"  => $phone,
			"dead_line"    => $deadLine,
			"job_budget"   => $job_budget,
			"job_min_price"=> $job_min_price,
			"job_max_price"=> $job_max_price,
			"post_code"    => $job_code,
			"type_of_work" => $typeofwork,
			"job_title"	   => $job_title,
			"job_category"=> $job_category,
			"job_budget_type"=> $job_budget_type,
			"job_fix_type"=> $job_fix_type,
			"job_link" => $jobId,







		];

		$this->modelUser->sendEmailToAdmin($data,$filePath);
	}
	/*public function getjobsAction()
	{
		$data = $_REQUEST;
		$joinArr=array(
			'0' => array(
				'0'=>'job_skills',
				'1'=>'job.job_id = job_skills.job_id',
				'2'=>'left',
				'3'=>array('job_skill_id'),
			),
		);
		$where = '';
		if($_REQUEST['job_budget']!=''){
			$where = "job_budget='".$_REQUEST['job_budget']."'"." or ".$where;
		}else if($_REQUEST['job_min_price']!=''){
			$where = "(job_min_price < = '".$_REQUEST['job_min_price']."' and job_max_price>='".$_REQUEST['job_min_price']."') or 
			(job_min_price < = '".$_REQUEST['job_max_price']."' and job_max_price>='".$_REQUEST['job_max_price']."')"." or ".$where;
		}
		if($_REQUEST['job_skills']!=''){
			$where = "job_skill_id IN '".$_REQUEST['job_skills']."'"." or ".$where;
		}
		if($_REQUEST['job_closing_date']!=''){
			$where = "job_closing_date='".date('Y-m-d',strtotime($_REQUEST['job_closing_date']))."'"." or ".$where;
		}
		chop($where," or ");
		echo $where;
		print_r($data);exit;
	}*/
}