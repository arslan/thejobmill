<?php
class UserController extends Zend_Controller_Action
{
  	private $modelUser ,$modelContent; 
	 
	public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
 	}

	public function indexAction(){
 		$this->_redirect('user/login');
 	}
	
 	public function loginAction(){
		global $objSession;

 		$this->view->pageHeading = "Sign in to The Job Mill";
		$auth = Zend_Auth::getInstance(); 
		if ($auth->hasIdentity()){
            $objSession->infoMsg ='It seems you are already logged into the system ';
            $this->_redirect('profile');
        }
 		$form = new Application_Form_User();
		$form->login_front();
		
		$blog_id = $this->getRequest()->getParam('blog_id');
		$forum_id = $this->getRequest()->getParam('forum_id');
		
		if($forum_id!=''){
			$forumTopicData = $this->modelUser->Super_Get('forum_topics',"forum_id='".$forum_id."'","fetch",array('fields'=>array('forum_topic')));
			if (empty($forumTopicData['forum_topic'])){
				$objSession->errorMsg ='Invalid Request';
				$this->_redirect('login');
        	}
		}
		$type = $this->getRequest()->getParam('type');
		
		
		/*If You login by the form*/
		if ($this->getRequest()->isPost()){ // Post Form Data
 			$posted_data  = $this->getRequest()->getPost();
			if($posted_data['remember_me']==1){
				setcookie("UserEmail",$posted_data['user_email'],time() + (10 * 365 * 24 * 60 * 60), '/');
			}
			if ($form->isValid($this->getRequest()->getPost()))
			{ // Form Valid
				$received_data  = $form->getValues();
  				/* Zend_Auth Setup Code */
				$authAdapter = new Zend_Auth_Adapter_DbTable($this->_getParam('db'), 'users', 'user_email', 'user_password'," ? AND (user_type='user') and  user_status = '1' and  user_verified_status = '1'");
  				// Set the input credential values
 				$authAdapter->setIdentity($received_data['user_email']);
				$authAdapter->setCredential(md5($received_data['user_password']));
				$result = $auth->authenticate($authAdapter);// Perform the authentication query, saving the result				
				if($result->isValid()){ // IF Auth Get the Record 
				
 					$data = $authAdapter->getResultRowObject(null); //Now get a result row without user_password set is here

					if(isset($_GET['url'])){
						$this->getRequest()->getServer('HTTP_REFERER');

						/*if($data->user_payment_status==0 && $data->user_type=='service_provider' && $this->view->site_configs['subscription_mode']==1)
						{
							Zend_Auth::getInstance()->clearIdentity();
							$this->_redirect('user/activate/status/1/email/'.urlencode($data->user_email));
						}
						else
						{*/
							$auth->getStorage()->write($data); //Now seession set is here
							if(!empty($type)){
								$this->_redirect('forums');
							}
							else if(!empty($blog_id)){
								$this->_redirect('blog/'.$blog_id);
							}
							else if(!empty($forum_id)){
								$objSession->successMsg = "Submit your Post/Comment on this Topic.";
								$this->_redirect('topic-detail/'.$forum_id);
							}
							else{
								$this->_redirect(urldecode($_GET['url']));
							}
						//}
					}else{
						/*if($data->user_payment_status==0 && $data->user_type=='service_provider' && $this->view->site_configs['subscription_mode']==1)
						{
							Zend_Auth::getInstance()->clearIdentity();
							$this->_redirect('user/activate/status/1/email/'.urlencode($data->user_email));
						}
						else
						{*/
							$auth->getStorage()->write($data); //Now seession set is here
							if(!empty($type)){
								$objSession->successMsg = "Post Your Topic Now";
								$this->_redirect('forums');
							}
							else if(!empty($forum_id)){
								$objSession->successMsg = "Submit your Post/Comment on this Topic";
								$this->_redirect('topic-detail/'.$forum_id);
							}
							else if(!empty($blog_id)){
								$this->_redirect('blog/'.$blog_id);
							}else{
								//$this->_redirect('profile');
								$this->_redirect('user-profile/'.$data->user_id);
							}
						//}
					}
				}else{ 
					// Auth Not Valid
					Zend_Auth::getInstance()->clearIdentity();
					$objSession->errorMsg = "Email or password is invalid";
  				}			
			}
 			$objSession->errorMsg = "Email or password is invalid";
 		} // End Post Form
		$this->view->form = $form;
	}

	public function registerAction(){
 		global $objSession;
		$this->view->pageHeading="Create a new The Job Mill Account";
		$form = new  Application_Form_User();
		$form->register();	 		
		if($this->getRequest()->isPost()){/* begin : isPost() */			
			$posted_data = $this->getRequest()->getPost();
 			if($form->isValid($posted_data)){ /* Begin : isValid()  */
				$this->modelUser->getAdapter()->beginTransaction();
				 $data = $form->getValues();
				 $data['password'] = $data['user_password'];
				 $data['user_password']=md5($data['user_password']);				 
				 $data['user_created']=date('Y-m-d H:i:s');
				 
				 $isInserted = $this->modelUser->add($data);
				if(is_object($isInserted)){
					 if($isInserted->success){

							$data=[
								"ud_contact_no" => $data['user_mobile_no'],
								"ud_user_id" => $isInserted->inserted_id,
								
							];
						 $this->modelStatic->Super_Insert('user_details',$data);
						 $this->modelUser->getAdapter()->commit();
						 $objSession->successMsg = " Registration Successful.";
						 
						 $this->_redirect('/login?registration=1');
						 
						 /*if($data['user_type']=='service_provider'){
							if($this->view->site_configs['subscription_mode']==0){
								$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
							}else{
								$this->_redirect('user/subscription/key/'.urlencode($data['user_email']));
							}
						 }*/
						 $this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
					 }
					 $this->modelUser->getAdapter()->rollBack();

					 if($isInserted->error){
						 if(isset($isInserted->exception)){/* Genrate Message related to the current Exception  */
						 }
						 $objSession->errorMsg = $isInserted->message;							 
					 }
				}else{
					$objSession->errorMsg = "Please Check Information again ";
				}
			}/* end : isValid()  */
			else{/* begin : else isValid() */
				$objSession->errorMsg = "Please Check Information Again";
 			}/* end : else isValid() */
 		}/* end : isPost() */

		$this->view->form = $form;
	}
	/*Social media sign up*/
	
	public function subscriptionAction()
	{
		global $objSession;
		$form = new Application_Form_User();
		$form->subscription();
		$this->view->form = $form;
		$email = $this->getRequest()->getParam('key');
		$this->view->email = $email;
		
		$SubscriptionData = $this->modelUser->Super_Get('subscription','1','fetchAll');
		$this->view->SubscriptionData = $SubscriptionData;
		//prd($SubscriptionData);
		if($this->getRequest()->getPost())
		{
			$posted_data  =  $this->getRequest()->getPost();
			
			$Subscription_idArray = explode("subscribe",key($posted_data));
			$Subscription_id = $Subscription_idArray[1];
			$SubscriptionInfo = $this->modelUser->Super_Get('subscription',"sub_id='".$Subscription_id."'",'fetch');
			
			$sitepaypal_id = $this->view->site_configs['site_paypal'];
			$success_url = SITE_HTTP_URL."/user/paypalstatus/status/1/key/".$email."/sub_type/".$posted_data[key($posted_data)]."/sub_id/".$Subscription_id;
			$cancel_url = SITE_HTTP_URL."/user/paypalstatus/status/0";
			$return_url = SITE_HTTP_URL."/user/paypalstatus/status/2/key/".$email."/sub_type/".$posted_data[key($posted_data)]."/sub_id/".$Subscription_id;
			$url = 'https://www.paypal.com/cgi-bin/webscr';
			
			if($posted_data[key($posted_data)]==1) /* Monthly Subscription(1 Months) */
			{
				$srt = "1";
			}
			else if($posted_data[key($posted_data)]==3) /* Quaterly Subscription(3 Months) */
			{
				$srt = "3";
			}
			else if($posted_data[key($posted_data)]==6) /* Half Yearly Subscription(6 Months) */
			{
				$srt = "6";
			}
			else if($posted_data[key($posted_data)]==12) /* Yearly Subscription(12 Months) */
			{
				$srt = "12";
			}
			$subscription_price=$SubscriptionInfo['sub_price'];

			echo "<h1 align='center'>You are redirecting Please wait.</h1>";	
			echo '<form name="_xclick" action="'.$url.'" name="payment_frm" id="payment_frm" method="post" align="center">';
			echo "<input type='hidden' name='cmd' value='_xclick-subscriptions'>";
			echo '<input type="hidden" name="business" value="'.$sitepaypal_id.'">';
			echo "<input type='hidden' name='currency_code' value='USD'>";
			echo "<input type='hidden' name='no_shipping' value='1'>";
			echo "<input type='image' src='https://www.paypal.com/en_GB/i/btn/x-click-but20.gif' border='0' name='submit' style='display:none' 
			alt='Make payments with PayPal - it's fast, free and secure!'>";
			echo '<input name="custom" value="10" type="hidden">';
			echo '<input type="hidden" name="a3" value="'.$subscription_price.'">';
			echo "<input type='hidden' name='p3' value=".$srt.">";
			echo '<input type="hidden" name="t3" value="M">';
			echo "<input type='hidden' name='src' value='1'>";
			echo "<input type='hidden' name='sra' value='1'>";
			//echo "<input type='hidden' name='srt' value=".$srt.">";
			echo '<input type="hidden" name="notify_url" id="notify_url" value="'.$success_url.'">';
			echo '<input type="hidden" name="return" id="return" value="'.$return_url.'">';
			echo '<input type="hidden" name="cancel_return" value="'.$cancel_url.'">';
			echo "</form>";
			echo '<script>window.document._xclick.submit();</script>';exit;	
		}
	}
	
	public function paypalstatusAction()
	{
		global $objSession;
		$status = $this->getRequest()->getParam('status');
		
		if($status==0) /* Cancel URL */
		{
			$objSession->errrorMsg='Your payment was cancelled';
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
		}
		else if($status==1) /* Success URL - NOTIFY URL */
		{
			
			$email = $this->getRequest()->getParam('key');
			$sub_type = $this->getRequest()->getParam('sub_type');
			$sub_id = $this->getRequest()->getParam('sub_id');
			
			$SubscriptionInfo = $this->modelUser->Super_Get('subscription',"sub_id='".$sub_id."'",'fetchAll');
			
			$DataUpdate = array(
				'user_payment_status' => '1',
				'user_subscription' => $sub_id,
				'user_subscription_type'=>$sub_type,
			);
			
			$UserData = $this->modelUser->Super_Get('users',"user_email='".$email."'",'fetch');
			
			if($UserData['user_payment_status']=='0')
			{
				$this->modelUser->Super_Insert('users',$DataUpdate,"user_email='".$email."'");
			}
			
			if($UserData['user_subscription_id']=='')
			{
				/* Updating user subscription_id */ 
				$NotifyUpdate = array('user_subscription_id'=>$_REQUEST['subscr_id']);
				$this->modelUser->Super_Insert('users',$NotifyUpdate,"user_email='".$email."'");
				
				$EmailData=array(
				'email'=>$email,
				'name'=>$UserData['user_first_name'],
				'subscription_amount'=>$_REQUEST['mc_gross'],
				'subscription_id'=>$_REQUEST['subscr_id']
				);
				/*$modelEmail = new Application_Model_Email();
				$modelEmail->sendEmail('paypal_subscription',$EmailData);*/
				mail("test@techdemolink.co.in","My subject",print_r( $EmailData, true ));
			}
		}
		else if($status==2) /* Return URL */
		{
			$email = $this->getRequest()->getParam('key');
			$sub_type = $this->getRequest()->getParam('sub_type');
			$sub_id = $this->getRequest()->getParam('sub_id');
			
			$SubscriptionInfo = $this->modelUser->Super_Get('subscription',"sub_id='".$sub_id."'",'fetchAll');
			
			$DataUpdate = array(
				'user_payment_status' => '1',
				'user_subscription' => $sub_id,
				'user_subscription_type'=>$sub_type,
			);
			
			$UserData = $this->modelUser->Super_Get('users',"user_email='".$email."'",'fetch');
			
			if($UserData['user_payment_status']=='0')
			{
				//prn($this->modelUser->Super_Insert('users',$DataUpdate,"user_email='".$email."'"));
				$this->modelUser->Super_Insert('users',$DataUpdate,"user_email='".$email."'");
			}
			
			//prd($DataUpdate);
			$objSession->successMsg = 'Payment completed successfully';
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
		}
	}
	
  	public function forgotpasswordAction(){
 		global $objSession;	
		$this->view->pageHeading="Forgot Password";
		$form = new  Application_Form_User();
 		$form->forgotPassword();
 		if($this->getRequest()->getPost()){
			$posted_data  =  $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
 				$received_data = $form->getValues();
				$UserData = $this->modelUser->Super_Get("users","user_email='".$received_data['user_email']."'","fetch");

				if($UserData['user_login_type']=='social'){
					$objSession->errorMsg = 'This email address is connected to a Facebook Account. Please login with the Facebook Login';
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
				}
 				$isSend = $this->modelUser->resetPassword($received_data['user_email']);
 				if($isSend){
					$objSession->successMsg = "Mail has been sent to your account.";
					$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
				}else{
					$objSession->errorMsg = "Please Check Information Again";
				}
			}else{
				$objSession->errorMsg = "Please Check Information Again";
  			}
		}
		$this->view->form = $form;
	}

	public function resetpasswordAction(){
		 global $objSession;
		 $this->view->pageHeading = "Reset Password";
		 $form = new Application_Form_User();
		 $form->resetPassword();
 		 $key = $this->_getParam('key');
		 /*if(empty($key)){
 			 $objSession->errorMsg = "Invalid Request for Reset Password ";
			 $this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
		 }*/
 		 $user_info = $this->modelUser->get(array("key"=>"$key"));
		
		 if($user_info['user_secret_question']!='')
		 {
			 $form->user_secret_question->setValue($user_info['user_secret_question']);
		 }
		 if(!$user_info){
			 $objSession->errorMsg = "Invalid Request for Password Reset , Please try again";
			 $this->_redirect("user/login");
		 }
 		 if($this->getRequest()->getPost()){
			 $posted_data  = $this->getRequest()->getPost();
			 
			 if($form->isValid($posted_data)){
				if(($posted_data['user_secret_answer']==$user_info['user_secret_answer']))
				{
					$data_to_update = $form->getValues() ;
					$data_to_update['pass_resetkey']="";
					$data_to_update['user_reset_status']="0";
					$data_to_update['user_password'] = md5($data_to_update['user_password']);
					unset($data_to_update['user_secret_question']);
					unset($data_to_update['user_secret_answer']);
					$ischeck = $this->modelUser->add($data_to_update,$user_info['user_id']);
					//prd($ischeck);
					if($ischeck){
						$objSession->successMsg = "Password change Successful.";
						$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
					}
				}else{
					$objSession->errorMsg = "Wrong secret answer";
					$this->_redirect('user/resetpassword/key/'.$key);
				}
			 }else{
					$objSession->errorMsg = "Please Check Information Again";
 			 }/* end : Else isValid() */
		 }/* end  : isPost()  */
		 $this->view->form = $form;
	 }

	 public function activateAction(){
		global $objSession;
		$this->view->pageHeading = "Activate Account";
 		$key = $this->_getParam('key');
		$user_info = $this->modelUser->get(array("key"=>"$key"));
		
		$status = $this->getRequest()->getParam('status');
		$this->view->status = $status;
		if($status==1)
		{
			$email = $this->getRequest()->getParam('email');
			$UserData = $this->modelUser->Super_Get('users',"user_email='".$email."'",'fetch');
			$this->view->UserData = $UserData;
		}
		
		if(!$user_info && $status=='')
		{
			$objSession->errorMsg = "Invalid Request for Account Activation";
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
		}
		
		if(!empty($key))
		{
			$this->modelUser->add(array('pass_resetkey'=>'',"user_reset_status"=>"0",'user_verified_status'=>"1"),$user_info['user_id']);
			$objSession->successMsg = "Your Account is Successfully Activated, Please Login.";
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
		}
	}
	
	public function changepasswordAction(){
		global $objSession;
 		if(!$this->view->user){
			$objSession->infoMsg = "Please Login First to make Changes";
			$this->_redirect("login");
		}
		$this->view->pageHeading = "Change Password";
		$form = new Application_Form_User();
		$form->changePassword();
		if($this->getRequest()->getPost()){
			$posted_data = $this->getRequest()->getPost();
			if($form->isValid($posted_data)){
				$checkOldPassword = $this->modelUser->get(array("where"=>" user_password='".md5($posted_data['user_old_password'])."' and user_id=".$this->view->user->user_id));
				if($checkOldPassword){
 	 				if($posted_data['user_password'] == $posted_data['user_rpassword']){
						//prd($posted_data);
  						$ischeck = $this->modelUser->add($form->getValues(), $checkOldPassword['user_id']);
						if($ischeck){
							$objSession->successMsg = "Password change Successful.";
							$this->_redirect('user/changepassword');
						}else{
							$objSession->errorMsg = "Please Check Information Again";
						}
					}else{
 						$form->user_password->setErrors(array('Password Mismatch'));
						$form->user_rpassword->setErrors(array('Password Mismatch'));
						$objSession->errorMsg = "Please type the same password";
						$this->render('changepassword');
					}
				}
				else{
				$form->user_old_password->setErrors(array('Old Password is not match'));
				$objSession->errorMsg = "This Old Password is not match";
				$this->render('changepassword');
				}
			}else{
				$objSession->errorMsg = "Please Check Information Again";
 				$this->render('changepassword');
			}
		}
		$this->view->form = $form;
	}

	public function sendverificationemailsAction()
	{
		$userModel = new Application_Model_User();
		$modelEmail = new Application_Model_Email();
		$users = $userModel->getAll(array('where'=>"user_verified_status = '0'"));
		foreach($users as $user)
		{
			echo "send email to ".$user['user_email']."<br>";
			$response = $modelEmail->sendEmail('registration_email',$user);
		}
		exit;
	}

	public function sendverificationAction(){

 		global $objSession;
		$modelEmail = new Application_Model_Email();
  		$data_form_values = (array) $this->view->user ;
   		if($this->view->user->user_email_verified!="1"){
  			$user_email_key = md5("ASDFUITYU"."!@#$%^$%&(*_+".time());
			$data_to_update = array("user_email_verified"=>"0","user_email_key"=>$user_email_key);
			$this->modelUser->update($data_to_update, 'user_id = '.$this->view->user->user_id);
			$data_form_values['user_email_key'] = $user_email_key ;
			$modelEmail->sendEmail('email_verification',$data_form_values);
 			$objSession->successMsg = "Email Successfully Send to your email address, please follow the verification link to verify the email address.";
 		}else{
			$objSession->infoMsg = "Your Email Address is already verified";
		}
  		$this->_redirect("profile");
	}

	public function verifyemailAction(){
		global $objSession;
		$key = $this->_getParam('key');
		if(empty($key)){
			$objSession->errorMsg = "Please Check Verifications link again";
			 $this->_redirect("login");	
		}
 		$user_info = $this->modelUser->get(array("where"=>"user_email_key='".$key."'"));
 		 if(!$user_info){
			 $objSession->errorMsg = "Invalid Request for Account Activation ";
			 $this->_redirect("profile");
		 }
		 $this->modelUser->update(array('user_email_verified'=>'1',"user_email_key"=>""),"user_id=".$user_info['user_id']);
		 $objSession->successMsg = "Your Email Address is successfully verified.";
		 $this->_redirect("profile");
 	}

	private function upload_user_image(){
 		$adapter = new Zend_File_Transfer_Adapter_Http();
		$video = $adapter->getFileInfo('user_image');
   		$video_extension = $video['user_image']['name'];
 		$extension = explode('.',$video['user_image']['name']); 
 		$extension = array_pop($extension);
  		$name_for_video = md5(rand(1,999)."@#$%@#&^#$@".time()).".".$extension;
  		rename(ROOT_PATH .'/images/profile/'.$video_extension ,  ROOT_PATH .'/images/profile/'.$name_for_video);
		return $name_for_video ;
  	}

	public function checkemailAction(){
 		$email_address = strtolower($this->_getParam('user_email'));
		$exclude = strtolower($this->_getParam('exclude'));
		$user_id = false ;
		if(!empty($exclude)){
			 $user = $this->view->user;
			 $user_id =$user->user_id;
		}
		$email = $this->modelUser->checkEmail($email_address,$user_id);
		$rev = $this->_getParam("rev");
		if(!empty($rev)){
 			if($email)
				echo json_encode("true");
 			else
				echo json_encode("`$email_address` is not registered , please enter valid email address ");
 			exit();
 		}
		if($email)
			echo json_encode("`$email_address` already exists , please enter any other email address ");
		else
			echo json_encode("true");
		exit();
	}

	public function checkpasswordAction(){
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$user_password = md5($this->_getParam('user_old_password'));
 			$user = $this->modelUser->get(array('where'=>"user_password='".$user_password."' and user_id=".$this->view->user->user_id));
			if(!$user){
				echo json_encode("Old Password Mismatch , Please Enter Correct old password");
			}else{
				echo json_encode("true");	
			}
		}else{
			echo json_encode("Please Login For Make Changes.");
		}
 		exit();
	}

	public function deleteaccountAction()
	{
		global $objSession;
		if($this->getRequest()->getPost()){
			$posted_data = $this->getRequest()->getPost();
		}
		
		$UpdateArray = array('user_status'=>'0','user_deactivate_status'=>'1','user_deactive_reason'=>$posted_data['reason']);
		$this->modelUser->Super_Insert("users",$UpdateArray,"user_id='".$this->view->user->user_id."'");
		$auth = Zend_Auth::getInstance();
		if($this->view->user){
			$user =  $this->view->user;
			if($user->user_login_type!="normal"){
				if($user->user_oauth_provider=="facebook"){
 					$facebook = new Facebook(array(
						'appId' => Zend_Registry::get("keys")->facebook->appId ,
						'secret' =>Zend_Registry::get("keys")->facebook->secret ,
						'cookie' => true
					));
					$auth->clearIdentity();
					session_destroy();
					$logout_url = $facebook->getLogoutUrl(array( 'next' => APPLICATION_URL."/login"));
					header("Location:".$logout_url);
					$objSession->successMsg = "Account deleted successfully.";
					exit();
 				}
 			}
			$auth->clearIdentity();
		}
		$objSession->successMsg = "Account deleted successfully.";
		$this->_redirect('');
	}

  	public function logoutAction(){ 
 	    global $objSession;	
		$auth = Zend_Auth::getInstance();
		if($this->view->user){
			$user =  $this->view->user;
			if($user->user_login_type!="normal"){
				if($user->user_oauth_provider=="facebook"){
 					$facebook = new Facebook(array(
						'appId' => Zend_Registry::get("keys")->facebook->appId ,
						'secret' =>Zend_Registry::get("keys")->facebook->secret ,
						'cookie' => true
					));
					$auth->clearIdentity();
					session_destroy();
					$logout_url = $facebook->getLogoutUrl(array( 'next' => APPLICATION_URL."/login"));
					header("Location:".$logout_url);
					$objSession->successMsg = "You are now logged out.";
					exit();
 				}
 			}
			$auth->clearIdentity();
			$objSession->successMsg = "You are now logged out.";
			$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
		}
		$objSession->successMsg = "You are now logged out.";
		$this->_helper->getHelper("Redirector")->gotoRoute(array(),"front_login");
	}

	public function smssendAction(){

		$mobile=$this->getRequest()->getParam("mobile");
		$str = '';
		$a = "abcdefghijklmnopqrstuvwxyz0123456789";
		$b = str_split($a);
		for ($i=1; $i <= 6 ; $i++) {
			$str .= $b[rand(0,strlen($a)-1)];
		}

		$this->modelStatic->Super_Delete("sms_codes","mobile_no=".$mobile);


		$data=[
			"mobile_no" => $mobile,
			"code" => $str,

		];
		$this->modelStatic->Super_Insert('sms_codes',$data);

		$url = "http://api.smsbroadcast.com.au/api-adv.php?username=thejobmill&password=Getterdone1&to=".$mobile."&from=TheJobMill&message=Your verification code : ".$str;
		$response = file_get_contents($url);
			var_dump($response);
		exit;


	}
	public function smsconfirmAction(){

		$code=$this->getRequest()->getParam("code");
		$code = $this->modelStatic->Super_Get('sms_codes', "code='" . $code . "'", 'fetch');

		if ($code){

			echo $this->_helper->json(["status"=>"true"]);

		}


		echo $this->_helper->json(["status"=>"false"]);
	}
	
	
	
	

}