<?php
class MessageController extends Zend_Controller_Action
{
  	private $modelUser ,$modelContent; 
	 
	public function init(){
 		$this->modelUser = new Application_Model_User();
		$this->modelStatic = new Application_Model_Static();
		$this->modelEmail = new Application_Model_Email();
 	}
	
	public function indexAction(){
		global $objSession;
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = messages.msg_from','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
		);
		$ReceivedMessages = $this->modelStatic->Super_Get('messages',"msg_to='".$this->view->user->user_id."'",'fetchAll',$extra=array(),$joinArr);
		$this->view->messages = $ReceivedMessages;
 	}
		
		
	public function retrievejobsAction(){
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	
	
		$param1 = $this->_request->getParam('param1');
		$param2 = $this->_request->getParam('param2');
	
		$model = new Application_Model_User();
		$JobData = $model->Super_Get('job',"job_user_id='".$param1."' OR job_user_id='".$param2."'","fetchAll",$extra=array('fields'=>array('job_id','job_title')));			
		
		$output = array();
		
		$JobArray = array(''=>'Select Job');
		foreach($JobData as $job)
		{
			
			$checkValidation = 0;
			
			$JobApplicationData = $model->Super_Get('job_applications',"job_app_job_id='".$job['job_id']."' AND job_app_status='1' AND (job_app_user_id='".$param1."' OR job_app_user_id='".$param2."')","fetchAll",$extra=array('fields'=>array('job_app_id','job_app_job_id')));
			
			foreach($JobApplicationData as $jobapplication){
				$checkValidation = 1;
			}
					
			if ($checkValidation == 1){
				$row = array();
				$row[0] = $job['job_id'];
				$row[1] = $job['job_title'];
				$output[] = $row;		
			}
			
		}
	
		
		echo json_encode($output);
		
	}	
	
	
	public function retrievejobsoldAction(){
		
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	
	
		$model = new Application_Model_User();
		$JobData = $model->Super_Get('job',"job_status='1'","fetchAll",$extra=array('fields'=>array('job_id','job_title')));			
		
		$JobArray = array(''=>'Select Job');
		foreach($JobData as $job)
		{
			$JobArray[$job['job_id']]=$job['job_title'];
		}
	
	
		$param1 = $this->_request->getParam('param1');
		$param2 = $this->_request->getParam('param2');
	
		$results = array(
			'return1' => 'value1',
			'return2' => 'value2'
		);
	
		//$this->_response->setBody(json_encode($results));
		//$this->_response->setBody("OKKK");
		
		echo json_encode($results);
		
	}	
		
	
	public function composeAction(){
		global $objSession;
		$draft_id = $this->getRequest()->getParam('draft_id');
		$form = new Application_Form_Message($draft_id);
		$form->compose();
		$this->view->form = $form;
		$type = $this->getRequest()->getParam('type'); // Reply or Forward or Compose => type=null(COMPOSE) type=1(REPLY) type=2(FORWARD)
		$message_id = $this->getRequest()->getParam('message_id');
		
		$user_id = $this->getRequest()->getParam('user_id');
		if($user_id!='\d+'){
			$form->msg_to->setValue($user_id);
		}
		
		if($type!='\d+')
		{
			$messageDetails = $this->modelStatic->Super_Get('messages',"msg_id='".$message_id."'",'fetch');
			if($type=='1') /* REPLY */
			{
				if($messageDetails['msg_from']==$this->view->user->user_id){
					$form->msg_to->setValue($messageDetails['msg_to']);
				}else{
					$form->msg_to->setValue($messageDetails['msg_from']);
				}
				$form->msg_job->setValue($messageDetails['msg_job']);
			}
			else if($type=='2') /* FORWARD */
			{
				$form->msg_subject->setValue($messageDetails['msg_subject']);
				$form->msg_text->setValue($messageDetails['msg_text']);
				$form->msg_job->setValue($messageDetails['msg_job']);
				$form->msg_file->setValue($messageDetails['msg_file']);
			}
		}else{
			$messageDetails = $this->modelStatic->Super_Get("drafts","msg_id='".$draft_id."'");
			if(!empty($messageDetails)){
				$form->populate($messageDetails);
			}
		}
		
		$this->view->messageDetails = $messageDetails;
		
		if($this->getRequest()->isPost())
		{ 
			$DraftId = $this->getRequest()->getParam('DraftId');
 			$posted_data  = $this->getRequest()->getPost();
			if ($form->isValid($this->getRequest()->getPost()))
			{ 
				$Formdata  = $form->getValues();
				$JobDetails = $this->modelStatic->Super_Get('job',"job_id='".$Formdata['msg_job']."'","fetch");
				if($JobDetails['job_user_id']==$this->view->user->user_id){
					$msg_type='1'; /* Message from hirer */
				}else{
					$msg_type='0'; /* Message from worker */
				}
				
				$MessageData = array(
					'msg_from' => $this->view->user->user_id,
					'msg_to' => $Formdata['msg_to'],
					'msg_job' => $Formdata['msg_job'],
					'msg_text' => $Formdata['msg_text'],
					'msg_subject' => $Formdata['msg_subject'],
					'msg_file' => $Formdata['msg_file'],
					'msg_type' => $msg_type,
					'msg_date' => date('Y-m-d H:i:s')
				);
				
			   $messageId=$this->modelStatic->Super_Insert('messages',$MessageData);
				
				/* SEND NOTIFICATION TO RECEIVER FOR NEW MESSAGE START */
			   $notifyData=array(
							'notify_type'=>0,
							'notify_for_user_id'=>$Formdata['msg_to'],
							'notify_refer_id'=>$messageId->inserted_id,
							'notify_date'=>date('Y-m-d H:i:s'),
						);
			   $this->modelStatic->Super_Insert("notifications",$notifyData);
				
				// Delete the draft 
				$this->modelStatic->Super_Delete("drafts","msg_id='".$DraftId."'");
				// Delete all empty drafts 
				$this->modelStatic->Super_Delete("drafts","msg_draft_status='0'");
				
				$objSession->successMsg='Message Sent Successfully.';
				$this->_redirect('sent');
			}
		}else{
			$DraftFirstData = array('msg_from'=>$this->view->user->user_id,'msg_date' => date('Y-m-d H:i:s'));
			if($type=='\d+' || ($type=='')){
				$result = $this->modelStatic->Super_Insert("drafts",$DraftFirstData);
				$DraftId = $result->inserted_id;
				$this->view->DraftId = $DraftId;
				
				// Delete all empty drafts except the one created above
				$this->modelStatic->Super_Delete("drafts","msg_draft_status='0' and msg_id!='".$DraftId."'");
			}
		}
 	}
	
	public function savedraftAction()
	{
		$DraftId = $this->getRequest()->getParam('DraftId');
		
		$tmp_name = $_FILES["msg_file"]["tmp_name"];
        $name = $_FILES["msg_file"]["name"];
        move_uploaded_file($tmp_name, MESSAGES_PATH."/".$name);
		
		$MessageData = array(
			'msg_to' => $_REQUEST['msg_to'],
			'msg_job' => $_REQUEST['msg_job'],
			'msg_text' => $_REQUEST['msg_text'],
			'msg_subject' => $_REQUEST['msg_subject'],
			'msg_file' => $name,
			'msg_date' => date('Y-m-d H:i:s'),
			'msg_draft_status'=>'1'
		);
		//prd($MessageData);
		$this->modelStatic->Super_Insert("drafts",$MessageData,"msg_id='".$DraftId."'");
		/*print_r($MessageData);*/
		exit;
	}
	
	public function draftsAction()
	{
		global $objSession;
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = drafts.msg_to','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
		);
		$DraftMessages = $this->modelStatic->Super_Get('drafts',"msg_from='".$this->view->user->user_id."'",'fetchAll',$extra=array(),$joinArr);
		$this->view->DraftMessages = $DraftMessages;
	}
	
	public function detailsAction(){
		global $objSession;
		$message_id = $this->getRequest()->getParam('message_id');
		
		/* Update Message Read Status Code - Starts */
		$messageupdate=array(
			'msg_status'=>'1',
		);
		$this->modelStatic->Super_Insert('messages',$messageupdate,"msg_id='".$message_id."'");
		/* Update Message Read Status Code - Ends */
		
		$joinArr=array(
			'0' => array('0'=>'users as from',
						 '1'=>'from.user_id = messages.msg_from',
						 '2'=>'left',
						 '3'=>array('user_id as from_user_id','user_first_name as from_first_name','user_last_name as from_last_name','user_image as from_image')),
			'1' => array('0'=>'users as to',
						 '1'=>'to.user_id = messages.msg_to',
						 '2'=>'left',
						 '3'=>array('user_id as to_user_id','user_first_name as to_first_name','user_last_name as to_last_name','user_image as to_image')),
		);
		$messageDetails = $this->modelStatic->Super_Get('messages',"msg_id='".$message_id."'","fetch",$extra=array(),$joinArr);
		$this->view->messageDetails = $messageDetails;
	}
	
	public function hirersAction(){
		global $objSession;
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = messages.msg_from','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
		);
		$HirersMessages = $this->modelStatic->Super_Get('messages',"msg_to='".$this->view->user->user_id."' and msg_type='1'",'fetchAll',$extra=array(),$joinArr);
		$this->view->messages = $HirersMessages;
 	}
	
	public function workersAction(){
		global $objSession;
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = messages.msg_from','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
		);
		$WorkersMessages = $this->modelStatic->Super_Get('messages',"msg_to='".$this->view->user->user_id."' and msg_type='0'",'fetchAll',$extra=array(),$joinArr);
		$this->view->messages = $WorkersMessages;
 	}
	
	public function sentAction(){
		global $objSession;
		$joinArr=array(
			'0' => array('0'=>'users','1'=>'users.user_id = messages.msg_to','2'=>'left','3'=>array('user_id','user_first_name','user_last_name','user_image')),
		);
		$SentMessages = $this->modelStatic->Super_Get('messages',"msg_from='".$this->view->user->user_id."'",'fetchAll',$extra=array(),$joinArr);
		$this->view->messages = $SentMessages;
 	}
	
	public function removemessagesAction(){
		global $objSession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if(isset($formData['messages']) and count($formData['messages'])){
				 foreach($formData['messages'] as $key=>$values){
					$removed = $this->modelStatic->Super_Delete("messages","msg_id IN ($values)");
				 }
 				$objSession->successMsg = " Messages(s) Deleted Successfully.";
				
 			}else{
				$objSession->errorMsg = " Select message to delete ";
			}
 			$this->_redirect('/sent/?removed=1');	 
		} 
 	}
	
	public function removedraftsAction(){
		global $objSession;
 		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
 		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if(isset($formData['messages']) and count($formData['messages'])){
				 foreach($formData['messages'] as $key=>$values){
					$removed = $this->modelStatic->Super_Delete("drafts","msg_id IN ($values)");
				 }
 				$objSession->successMsg = " Draft(s) Deleted Successfully.";
				
 			}else{
				$objSession->errorMsg = " Select message to delete ";
			}
 			$this->_redirect('/drafts/?removed=1');	 
		} 
 	}
	
	public function downloadAction()
	{
		$doc = $this->getRequest()->getParam('file');
		$file = MESSAGES_PATH.'/'.$doc; //path to the file on disk
		
		/*Code to download the file*/
		if (file_exists($file)) 
		{
			//set appropriate headers
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($file));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			
			//read the file from disk and output the content.
			readfile($file);
			exit;
		}
	}
}