module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            js: {
                src: [
                    'public/plugins/jquery-1.11.0.min.js',
                    'public/front_js/TweenMax.min.js',
                    'public/plugins/jquery.pulsate.min.js',
                    'public/front_js/bootstrap.min.js',
                    'public/plugins/jquery.lazyload.min.js',
                    'public/plugins/jquery-validation/js/jquery.validate.min.js',
                    'public/plugins/jquery-validation/js/additional-methods.min.js',
                    'public/front_js/holder.js',
                    'public/front_js/custom.js',
                    'public/plugins/jquery-migrate-1.2.1.min.js',
                    'public/front_js/nprogress.js',
                    'public/plugins/jquery.blockui.min.js',
                    'public/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                    'public/front_js/general.js',
                    'public/plugins/bootstrapValidator/bootstrapValidator.js',
                    'public/plugins/bootstrapValidator/bootstrap-formhelpers.js',

                ],
                dest: 'public/front_js/build/production.js',
            },
            css: {
                src: [
                    'public/front_css/bootstrap.css',
                    'public/front_css/hover.css',
                    'public/front_css/nprogress.css',
                    'public/plugins/font-awesome/css/font-awesome.min.css',
                    'public/admin_css/components.css',
                    'public/plugins/bootstrap-datepicker/css/datepicker.css',
                    'public/front_css/style_custom.css',
                    'public/front_css/bootstrap.min.css',
                    'public/front_css/bootstrap-formhelpers-min.css',

                ],
                dest: 'public/front_css/build/production.css',
            }
        },
        uglify: {
            jsBuild: {
                src: 'public/front_js/build/production.js',
                dest: 'public/front_js/build/production.min.js'
            }
        },
        cssmin: {
            cssBuild: {
                src: 'public/front_css/build/production.css',
                dest: 'public/front_css/build/production.min.css'
            }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');


    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat','uglify','cssmin']);

};/**
 * Created by Shayan solution on 4/14/2016.
 */
